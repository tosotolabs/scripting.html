AudioNode.prototype._connect = AudioNode.prototype.connect;
AudioNode.prototype.connect = function () {
    var args = Array.prototype.slice.call(arguments);
    if (args[0]._isCompositeAudioNode)
    args[0] = args[0]._input;

    return this._connect.apply(this, args);
}

class CompositeAudioNode {

    get _isCompositeAudioNode () {
        return true;
    }

    constructor (context, options) {
        options = options || {}
        this.context = context;
        this._input = this.context.createGain();
        this._output = this.context.createGain();
    }

    connect () {
        this._output.connect.apply(this._output, arguments);
    }

    disconnect () {
        this._output.disconnect.apply(this._output, arguments);
    }
}

class Filter extends CompositeAudioNode {
    get frequency () {
        return this.dampening;
    }
    get dampening () {
        return mergeParams(this._filters.map(f => f.frequency));
    }
    get Q () {
        return mergeParams(this._filters.map(f => f.Q));
    }
    set rolloff (rolloff) {
        rolloff = parseInt(rolloff, 10);
        let possibilities = [-12, -24, -48, -96];
		let cascadingCount = possibilities.indexOf(rolloff);
		//check the rolloff is valid
		if (cascadingCount === -1){
			throw new RangeError("Filter: rolloff can only be -12, -24, -48 or -96");
		}
		cascadingCount++;
		this._rolloff = rolloff;
        //first disconnect the filters and throw them away
		this._input.disconnect();
        for (let i = 0; i < this._filters.length; i++){
			this._filters[i].disconnect();
			this._filters[i] = null;
		}
        this._filters = new Array(cascadingCount);
        let current = this._input;
        for (let count = 0; count < cascadingCount; count++){
			let filter = this.context.createBiquadFilter();
			filter.type = this._type;
            filter.frequency.setValueAtTime(this._frequency, this.context.currentTime);
            filter.Q.setValueAtTime(this._Q, this.context.currentTime);
			this._filters[count] = filter;
            current = current.connect(filter);
		}
        current.connect(this._output);
    }

    constructor(audioCtx, options) {
        super(audioCtx, options);
        this._type = options.type;
        this._frequency = options.frequency;
        this._Q = options.Q || 1;
        this._filters = [];
        this.rolloff = options.rolloff || -12;
    }
}

class FeedbackCombFilter extends CompositeAudioNode {
    get resonance () {
        return this._feedback.gain;
    }
    get delayTime () {
        return this._delay.delayTime;
    }
    constructor(audioCtx, options) {
        super(audioCtx, options);
        this._delay = this._input = this._output = new DelayNode(audioCtx, options);
        this._feedback = audioCtx.createGain();
        this._feedback.gain.setValueAtTime(options.resonance, audioCtx.currentTime);

        this._delay.connect(this._feedback).connect(this._delay);
    }
}

class LowpassCombFilter extends CompositeAudioNode {
    get resonance () {
        return this._combFilter.resonance;
    }
    get dampening () {
        return this._lowPass.dampening;
    }
    get delayTime () {
        return this._combFilter.delayTime;
    }

    constructor (audioCtx, options) {
        super(audioCtx, options);
        let freq = options.dampening,
            rolloff = options.rolloff == undefined ? -12 : options.rolloff;

        this._combFilter = this._output = new FeedbackCombFilter(audioCtx, options);
        this._lowPass = this._input = new Filter(audioCtx, {frequency: freq, rolloff: rolloff, type: 'lowpass'});

        this._lowPass.connect(this._combFilter);
    }
}

class Freeverb extends CompositeAudioNode {
    get wet () {
        return this._wet.gain.value;
    }
    get dry () {
        return this._dry.gain.value;
    }
    set wet(v) {
        this._wet.gain.setValueAtTime(v, this.context.currentTime);
        this._dry.gain.setValueAtTime(1-v, this.context.currentTime);
    }
    set dry(v) {
        this._dry.gain.setValueAtTime(v, this.context.currentTime);
        this._wet.gain.setValueAtTime(1-v, this.context.currentTime);
    }
    get roomSize() {
        return mergeParams(this._combFilters.map(comb => comb.resonance))
    }
    get dampening() {
        return mergeParams(this._combFilters.map(comb => comb.dampening))
    }

    constructor (audioCtx, options) {
        super(audioCtx, options);
        let resonance = options.roomSize == undefined ? 0.1 : options.roomSize,
            dampening = options.dampening == undefined ? 3000 : options.dampening,
            wet = options.wet == undefined ? 0.5 : options.wet,
            dry = 1-wet,
            sampleRate = options.sampleRate || 48000;

        const COMB_FILTER_TUNINGS = [1557, 1617, 1491, 1422, 1277, 1356, 1188, 1116].map(delayPerSecond => delayPerSecond / sampleRate);
        const ALLPASS_FREQUENCES = [225, 556, 441, 341];

        this._input.channelCountMode = 'explicit';
        this._input.channelCount = 2;
        this._wet = audioCtx.createGain();
        this._wet.gain.setValueAtTime(wet, audioCtx.currentTime);
        this._dry = audioCtx.createGain();
        this._dry.gain.setValueAtTime(dry, audioCtx.currentTime);
        this._merger = audioCtx.createChannelMerger(2);
        this._splitter = audioCtx.createChannelSplitter(2);
        let highpass = audioCtx.createBiquadFilter(); // hmm
        highpass.type = 'highpass';
        highpass.frequency.value = 200;
        this._input.connect(this._wet);
        this._wet.connect(this._splitter);
        this._input.connect(this._dry);
        this._dry.connect(this._output);
        this._merger.connect(highpass);
        highpass.connect(this._output);

        this._allPassFiltersL = ALLPASS_FREQUENCES.map(frequency => new BiquadFilterNode(audioCtx, {type: 'allpass', frequency}));
        this._allPassFiltersL.forEach((pass,idx)=> {
            let prev_pass = this._allPassFiltersL[idx-1];
            if (prev_pass) prev_pass.connect(pass);
        });
        this._allPassFiltersR = ALLPASS_FREQUENCES.map(frequency => new BiquadFilterNode(audioCtx, {type: 'allpass', frequency}));
        this._allPassFiltersR.forEach((pass,idx)=> {
            let prev_pass = this._allPassFiltersR[idx-1];
            if (prev_pass) prev_pass.connect(pass);
        });
        this._allPassFiltersL[this._allPassFiltersL.length-1].connect(this._merger, 0, 0);
        this._allPassFiltersR[this._allPassFiltersR.length-1].connect(this._merger, 0, 1);

        this._combFilters = COMB_FILTER_TUNINGS.map(delayTime => new LowpassCombFilter(audioCtx, {dampening: dampening, resonance: resonance, delayTime: delayTime}));
        const combLeft = this._combFilters.slice(0,4);
        const combRight = this._combFilters.slice(4,8);

        combLeft.forEach(comb => {
            this._splitter.connect(comb._input, 0);
            comb.connect(this._allPassFiltersL[0]);
        });
        combRight.forEach(comb => {
            this._splitter.connect(comb._input, 1);
            comb.connect(this._allPassFiltersR[0]);
        });
    }
}

// Allows to set a parameter value for multiple nodes (of same kind) at the same time
function mergeParams(params){
    const singleParam = params[0]
    const parameter = {};
    const audioNodeMethods = Object.getOwnPropertyNames(AudioParam.prototype)
    .filter(prop => typeof singleParam[prop] === 'function')

    //allows things like parameter.setValueAtTime(x, ctx.currentTime)
    // audioNodeMethods.forEach(method => {
    //     parameter[method] = (...argums) => {
    //         const args = Array.prototype.slice.call(argums);
    //         params.forEach((param) => {
    //             singleParam[method].apply(param, args);
    //         });
    //     }
    // });

    //allows to do parameter.value = x
    Object.defineProperties(parameter, {
        value: {
            get: function () {
                return singleParam.value
            },
            set: function (value) {
                params.forEach(param => {
                    param.value = value
                });
            }
        }
    });

    parameter.setValueAtTime = (value, ct)=> {
        params.forEach(param => {
            param.setValueAtTime(value, ct);
        });
    }

    return parameter;
}
