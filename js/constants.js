// Constants.js

// Developer
const DEV_MODE = true;
const DEV_USE_THEME_QUANTUM = false;
const DEV_QUANTUM = ()=> { return 600 };

// Memnat
const ASPECT_TYPE_PURE = 1;
const ASPECT_TYPE_COMPOUND = 2;

const ARROW_TYPE_ACKNOWLEDGEMENT = 0;
// Similarity is realized upon acknowledgement.
const ARROW_TYPE_SIMILARITY = 1;
// Causation is conscious similarity.
const ARROW_TYPE_CAUSATION = 2;
// Dependence is causation with burden.
const ARROW_TYPE_DEPENDENCE = 3; const ARROW_TYPE_FOUNDATION = 3;
// Spectrum is an arbitrary selection of two with the in-between implied.
const ARROW_TYPE_SPECTRUM = 4; const ARROW_TYPE_SCALE = 4;

const SIGNAL_ARROWS = [ARROW_TYPE_ACKNOWLEDGEMENT, ARROW_TYPE_SIMILARITY];
const VERSUS_ARROWS = [ARROW_TYPE_ACKNOWLEDGEMENT, ARROW_TYPE_SIMILARITY];
const CONTEXT_ARROWS = [ARROW_TYPE_ACKNOWLEDGEMENT, ARROW_TYPE_SIMILARITY, ARROW_TYPE_CAUSATION, ARROW_TYPE_DEPENDENCE, ARROW_TYPE_SPECTRUM];

const NUM_DECIMAL_PLACES = 10;

const CLICKED_ON_THEME_TITLE = 1;
const CLICKED_ON_SHELL_TITLE = 2;
const CLICKED_ON_CANVAS = 3;

// 1: Ratonale is distinction. Parralel to one another.
const THEME_TYPE_SIGNAL = 1;
// 2: Rationale is community. Perpendicular.
const THEME_TYPE_VERSUS = 2;
// 3: Rationale is relationship. Peripheral.
const THEME_TYPE_CONTEXT = 3;
// Rationale is change/diversity. Progress.
const THEME_TYPE_MOD_QUAD = 4;
// Rationale is sovereignty. Process.
// Here, they know the aspects by name, onw with processes.
const THEME_TYPE_TEXT = 5;

const MEMNAT_WIDTH = 180,
    MEMNAT_HEIGHT = 180,
    n_ss = ['a','b','c','d','e','f'],
    vn_ss = ['la','lb','lc','ra','rb','rc'],
    mfbj_ss = ['m','f','b','j'],
    n_colors = ['Red','MediumSpringGreen','Orange','DodgerBlue','Yellow','Fuchsia'];

const NODE_LABELS = n_ss;
const SIGNAL_LABELS = n_ss;
const CONTEXT_LABELS = n_ss;
const VERSUS_LABELS = vn_ss;
const MOD_QUAD_LABELS = mfbj_ss;

// Scripting
const CONTROLS_PANEL_ASPECT_STATE = 7;
const CONTROLS_PANEL_THEME_STATE = 6;
const CONTROLS_PANEL_SHELL_STATE = 4;
const CONTROLS_PANEL_ENTITY_STATE = 3;
const CONTROLS_PANEL_LINE_STATE = 2;
const CONTROLS_PANEL_SCENE_STATE = 1;

const ITEM_TYPE_ASPECT = 'aspect';
const ITEM_TYPE_THEME = 'theme';
const ITEM_TYPE_SHELL = 'shell';
const ITEM_TYPE_ENTITY = 'entity';
const ITEM_TYPE_PROCESS = 'process';
const ITEM_TYPE_SCHOOL = 'school';
const ITEM_TYPE_SCENE = 'scene';
const ITEM_TYPE_SCRIPT = 'script';
const ITEM_TYPE_CUD = 'cud';

const ITEMS_TYPE_ASPECTS = 'aspects';
const ITEMS_TYPE_THEMES = 'themes';
const ITEMS_TYPE_SHELLS = 'shells';
const ITEMS_TYPE_ENTITIES = 'entities';
const ITEMS_TYPE_PROCESSES = 'processes';
const ITEMS_TYPE_SCHOOLS = 'schools';
const ITEMS_TYPE_LINES = 'lines';
const ITEMS_TYPE_SCENES = 'scenes';
const ITEMS_TYPE_SCRIPTS = 'scripts';

const SIMULATION_EVENT_PLAY = 'play';
const SIMULATION_EVENT_PAUSE = 'pause';
const SIMULATION_EVENT_STOP = 'stop';
const SIMULATION_EVENT_PROGRESS = 'progress';
const SIMULATION_EVENT_RECORD_START = 'recordstart';
const SIMULATION_EVENT_RECORD_END = 'recordend';

const SOUND_MIN_FREQUENCY = 0.01;
const SOUND_INTENSITY_MIN = 0;
const SOUND_INTENSITY_MAX = 31; // ~=30 Db - most intelligible max
const SOUND_INTENSITY_DEFAULT = 1;
const SOUND_INTENSITY_DEFAULT_0_1 = 0.3;
const SOUND_DURATION_DEFAULT = 6.0;

const SOT_MIN_CHARACTER_LENGTH = 30;
const SOT_CHARACTER_LENGTH_DEFAULT = 500;

const CHAR_MAX_LENGTH = 255;

const SCROLL_BEHAVIOR = 'auto';

const MF = 'mf';
const FM = 'fm';
const BJ = 'bj';
const JB = 'jb';
const MB = 'mb';
const BM = 'bm';
const FB = 'fb';
const BF = 'bf';
const MJ = 'mj';
const JM = 'jm';
const FJ = 'fj';
const JF = 'jf';

const MIN_FM = 0.01;
const MAX_FM = 1-MIN_FM;

const VOICE_TYPE_ASPECT = 'aspect';
const VOICE_TYPE_THEME = 'theme';

const VOICE_DOMAIN_AUDITORY = 'auditory';
const VOICE_DOMAIN_VISUAL = 'visual';

const BRANE_TYPE_IMAGE = 'image';
const BRANE_TYPE_AUDIO = 'audio';

const PERSONAS_PER_SIDE = 6;

const GRAND_CONDUCTOR = 'Grand Conductor';
const PLAYER = 'Player';
const DREAMER = 'Dreamer';
const HAND = 'Hand';
const SATELLITE = 'Satellite';
const ROGUE = 'Rogue';

const SOUND_FREQUENCY_MIN = 20;
const SOUND_FREQUENCY_MAX = 20000;

const BOUND_POSITIVE = (v)=>{return v < 0 ? 0 : v}
const BOUND_FULL = (v)=>{return Math.range(v,-1,1)}
const WRAP_POSITIVE = Math.abs;

const SOUND_MODULO = (v)=>{return v < SOUND_FREQUENCY_MIN ? SOUND_FREQUENCY_MIN : (v > SOUND_FREQUENCY_MAX ? SOUND_MODULO(v%SOUND_FREQUENCY_MAX) : v)}
const VISION_MODULO = (v,w)=> {return Math.floor(v%(w*w))}

const IDENTITY = (v)=>{return v};
const VOID = ()=>{}
const AsyncFunction = Object.getPrototypeOf(async function(){}).constructor;

// Workers

// const MINIMUM_CHANGE = ()=> { return { frequency:getRandomNumber(MIN_FREQUENCY,20000), intensity:getRandomNumber(0,1) } }
const MINIMUM_CHANGE = ()=> { return { frequency:0, intensity:0 } }

const JOB_TYPE_WORK = 'work';
const JOB_TYPE_CLEAR = 'clear';

// Process Worker
const JOB_TYPE_PROCESS = 'process';
const JOB_TYPE_VOICE = 'voice';
const JOB_TYPE_RELOAD = 'reload';
const JOB_TYPE_MOD_QUAD = 'modQuad';
const JOB_TYPE_LISTENER = 'listener';

// Automation Worker
const ANALOGO = 'analogo';
const GLISSANDO = 'glissando';
const PORTAMENTO = 'portamento';
const VIBRATO = 'vibrato';
const UNITY = 'unity';
const JOB_TYPE_AUTOMATION = 'automation';
const JOB_TYPE_BREAK_UNITY = 'break-unity';
const JOB_TYPE_DELETE_UNITY = 'delete-unity';
const JOB_TYPE_MODIFY = 'modify';
const JOB_TYPE_RESET = 'reset';

// Rogue Worker
const JOB_TYPE_DELETE = 'delete';
const JOB_TYPE_DECAY = 'decay';
const JOB_TYPE_ROGUE = 'rogue';

// Miscellaneous
const SQRT2 = Math.SQRT2;
const SQRT3 = Math.sqrt(3);
