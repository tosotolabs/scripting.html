'use strict'

importScripts('constants.js', 'dexie.js');

let packager = null;
let memnat = null;

onmessage = async function(e) {
    let data = e.data,
        type = data.type;

    packager = data.packager;

    if ((packager.name === 'web') && (memnat == null || !memnat.isOpen())) {
        memnat = new Dexie(packager.module.name);
        await memnat.open();
    }

    let count = 0;

    switch (type) {
        case ITEM_TYPE_SCRIPT:
            let sid = data.scriptId;

            switch (packager.name) {
                case 'electron':
                    const fs = require('fs');
                    const path = require('path');
                    let p = path.join(packager.scriptsDir,sid+'.json');
                    fs.readFile(p, {encoding: 'utf-8'}, (err,res)=> {
                        if (err) {
                            postMessage({ type:'error', error:err, workId:data.workId});
                            return;
                        }

                        let script = JSON.parse(res),
                            sceneIds = script.sceneIds;

                        delete script.sceneIds;

                        let scenes = {};
                        let sid = '';
                        for (let i = 0; i < sceneIds.length; ++i) {
                            sid = sceneIds[i];
                            p = path.join(packager.scenesDir,sid+'.json');
                            try {
                                res = fs.readFileSync(p, {encoding: 'utf-8'});
                                scenes[sid] = JSON.parse(res);
                            } catch (err) {
                                console.error(err);
                            }
                        }

                        script.scenes = scenes;

                        postMessage({ type:'done', item:script, workId:data.workId});
                    });
                    break;
                default:
                    let script = null,
                        scenes = {};
                    memnat.transaction('rw', memnat.table('scripts'), memnat.table('scenes'), async ()=> {
                        script = await memnat.table('scripts').get(sid);
                        for (let i = 0; i < script.sceneIds.length; ++i) {
                            let s = await memnat.table('scenes').get(script.sceneIds[i]);
                            scenes[s.id] = s;
                        }
                    }).then(()=> {
                        script.scenes = scenes;
                        postMessage({ type:'done', item:script, workId:data.workId});
                    }).catch((err)=> {
                        postMessage({type:'error', error:err, workId:data.workId});
                    });
            }
            break;
        case ITEMS_TYPE_SCRIPTS:
            let scripts_ = null, scripts = [];
            switch (packager.name) {
                case 'electron':
                    const fs = require('fs');
                    const path = require('path');
                    fs.readFile(packager.scriptsFile, {encoding: 'utf-8'}, (err,res)=> {
                        if (err) {
                            postMessage({ type:'error', error:err, workId:data.workId});
                            return;
                        }

                        scripts_ = JSON.parse(res);
                        for (let k in scripts_) scripts.push(scripts_[k]);

                        postScripts();
                    });
                    break;
                default:
                    memnat.transaction('rw', memnat.table('scripts_lite'), async ()=> {
                        scripts = await memnat.table('scripts_lite').toArray();
                    }).then(()=> {
                        postScripts();
                    }).catch((err)=> {
                        postMessage({ type:'error', error:err, workId:data.workId});
                    });
            }

            function postScripts() {
                scripts.sort((a,b)=> {
                    if (a.saveTime > b.saveTime) {
                        return -1;
                    } else {
                        return 1;
                    }
                });

                count = scripts.length;

                postMessage({ type:'done', items:scripts, count:count, workId:data.workId});
            }
            break;
        case ITEM_TYPE_ASPECT:
        case ITEM_TYPE_THEME:
        case ITEM_TYPE_SHELL:
        case ITEM_TYPE_ENTITY:
        case ITEM_TYPE_PROCESS:
        case ITEM_TYPE_SCHOOL:
            let id = data.id,
                header = headerForType(type);
            switch (packager.name) {
                case 'electron':
                    const fs = require('fs');
                    const path = require('path');
                    let p = path.join(packager[header+'Dir'],id+'.json');
                    fs.readFile(p, {encoding: 'utf-8'}, (err,res)=> {
                        if (err) {
                            postMessage({ type:'error', error:err, workId:data.workId});
                            return;
                        }

                        let item = JSON.parse(res);
                        postMessage({ type:'done', item:item, workId:data.workId});
                    });
                    break;
                default:
                    // TODO: remove Dexie.js. Use native indexedDB
                    // var req = indexedDB.open('memnat'),
                    //     db;
                    // req.onsuccess = (ev)=> {
                    //     db = ev.target.result;
                    //     var transaction = db.transaction(["aspects"], "readwrite");
                    //     console.log(workerId,memnat.tables,transaction.objectStore('aspects'));
                    // }
                    // console.log(workerId,memnat._allTables)
                    let item;

                    memnat.transaction('rw', header, async ()=> {
                        item = await memnat.table(header).get(id);
                        // if (!item) throw new Error('missing item with ID: '+id);
                    }).then(()=> {
                        postMessage({ type:'done', item:item, workId:data.workId});
                    }).catch((err)=> {
                        console.error(err);
                        postMessage({ type:'error', error:err, workId:data.workId});
                    });

            }
            break;
        case ITEMS_TYPE_ASPECTS:
        case ITEMS_TYPE_THEMES:
        case ITEMS_TYPE_SHELLS:
        case ITEMS_TYPE_ENTITIES:
        case ITEMS_TYPE_PROCESSES:
        case ITEMS_TYPE_SCHOOLS:
        case ITEMS_TYPE_LINES:
        case ITEMS_TYPE_SCENES:
            let put_back = null;
            if (data.persist) {
                await new Promise((resolve,reject)=> {
                    let worker = new Worker(packager.PERSIST_WORKER_PATH);
                    worker.onmessage = (e)=> {
                        if (e.data.type === 'error') {
                            console.error('Error occured while pre-persisting items.', e.data.error);
                            put_back = data.persist;
                        }
                        resolve();

                        worker.terminate();
                    }
                    worker.postMessage({
                        type: ITEM_TYPE_CUD,
                        packager: packager,
                        cud: data.persist,
                        workId: data.workId
                    });
                });
            }
            let ids = data.ids,
                query = data.query || {}, // {start, limit, comparators}
                items = [];
            query.start = query.start || 0;
            query.limit = ids ? ids.length : (query.limit || 30);
            query.resolutions = query.resolutions || [];
            query.comparators = query.comparators || [];

            let end = query.start+query.limit;

            switch (packager.name) {
                case 'electron':
                    const fs = require('fs');
                    const path = require('path');
                    let p = '',
                        id = '',
                        res = null;

                    if (ids) {
                        let got = {};
                        for (let i = 0; i < ids.length; ++i) {
                            id = ids[i];
                            if (got[id] && got[id] > -1) {
                                items.push(items[got[id]]);
                                continue;
                            }
                            p = path.join(packager[type+'Dir'],id+'.json');
                            try {
                                res = fs.readFileSync(p, {encoding: 'utf-8'});
                                res = JSON.parse(res);

                                // TODO: resolve with directory search

                                let pass = filter(res,query.comparators,query.all);
                                if (pass) {
                                    items.push(res);
                                    got[id] = i;
                                } else {
                                    got[id] = -1;
                                }
                            } catch (err) {
                                console.error(err);
                            }
                        }

                    } else {
                        try {
                            files = fs.readDirSync(packager[type+'Dir'],'utf-8');
                        } catch(err) {
                            files = [];
                            console.error(err);
                        }

                        for (let i = 0; i < files.length; ++i) {
                            let res = JSON.parse(files[i]);

                            // TODO: resolve with directory search

                            let pass = filter(res,query.comparators,query.all);
                            if (pass) items.push(res);
                        }
                    }

                    if (query.sort) {
                        let k, ak, bk, ord;
                        let igc = query.sort.ignoreCase;
                        items.sort((a,b)=> {
                            k = query.sort.key;
                            ord = query.sort.order;
                            ak = a[k];
                            bk = b[k];
                            if (igc) {
                                ak = ak.toLowerCase();
                                bk = bk.toLowerCase();
                            }
                            cond = ord === SORT_ASCENDING ? ak < bk : ak > bk
                            if (cond) return -1;
                            return 1;
                        });
                    }

                    count = items.length;

                    items = take(items,query.start,end);

                    postMessage({ type:'done', items:items, count:count, workId:data.workId});

                    break;
                default:
                    memnat.transaction('rw', memnat.tables, async ()=> {
                        if (ids) {
                            let got = {},
                                id = '';
                            for (let i = 0; i < ids.length; ++i) {
                                id = ids[i];
                                if (got[id] && got[id] > -1) {
                                    items.push(items[got[id]]);
                                    continue;
                                }
                                let res = await memnat.table(type).get(id);

                                if (query.resolutions.length) {
                                    for(let ri = 0; ri < query.resolutions.length; ++ri) {
                                        let reso = query.resolutions[ri],
                                            skey = reso.subjectKey,
                                            key = reso.key,
                                            newKey = reso.newKey || key,
                                            dkey = reso.donorKey || skey,
                                            dtype = reso.donorType,
                                            dnkeys = reso.donationKeys;
                                        // Resolve only if it is explicitly enforced or the item
                                        // at the `key` is null.
                                        if (!reso.enforce && (res[key] != undefined)) continue;
                                        let repl = res[key],
                                            donor = null;
                                        if (Array.isArray(repl)) {
                                            let repls = repl,
                                                donors = [];
                                            for (let di = 0; di < repls.length; ++di) {
                                                repl = repls[di];
                                                donor = (await memnat.table(dtype).where(dkey).equals(repl).toArray())[0];
                                                if (!donor) console.warn('Resolution: found 0 donors');
                                                donors.push(donor);
                                            }

                                            donor = donors;
                                        } else {
                                            donor = (await memnat.table(dtype).where(dkey).equals(res[skey]).toArray())[0];
                                        }

                                        resolveItem(res,donor,key,newKey,dnkeys);
                                    }
                                }

                                let pass = filter(res,query.comparators,query.all);
                                if (pass) {
                                    items.push(res);
                                    got[id] = i;
                                } else {
                                    got[id] = -1;
                                }
                            }
                        } else {
                            let recs = await memnat.table(type).toArray();

                            for (let i = 0; i < recs.length; ++i) {
                                let res = recs[i];

                                if (query.resolutions.length) {
                                    for(let ri = 0; ri < query.resolutions.length; ++ri) {
                                        let reso = query.resolutions[ri],
                                            skey = reso.subjectKey,
                                            key = reso.key,
                                            newKey = reso.newKey || key,
                                            dkey = reso.donorKey || skey,
                                            dtype = reso.donorType,
                                            dnkeys = reso.donationKeys;
                                        // Resolve only if it is explicitly enforced or the item
                                        // at the `key` is null.
                                        if (!reso.enforce && (res[key] != undefined)) continue;
                                        let repl = res[key],
                                            donor = null;
                                        if (Array.isArray(repl)) {
                                            let repls = repl,
                                                donors = [];
                                            for (let di = 0; di < repls.length; ++di) {
                                                repl = repls[di];
                                                donor = (await memnat.table(dtype).where(dkey).equals(repl).toArray())[0];
                                                if (!donor) console.warn('Resolution: found 0 donors');
                                                donors.push(donor);
                                            }

                                            donor = donors;
                                        } else {
                                            donor = (await memnat.table(dtype).where(dkey).equals(res[skey]).toArray())[0];
                                        }

                                        resolveItem(res,donor,key,newKey,dnkeys);
                                    }
                                }

                                let pass = filter(res,query.comparators,query.all);
                                if (pass) items.push(res);
                            }
                        }

                        if (query.sort) {
                            let k, ak, bk, ord, cond;
                            let igc = query.sort.ignoreCase;
                            items.sort((a,b)=> {
                                k = query.sort.key;
                                ord = query.sort.order;
                                ak = a[k];
                                bk = b[k];
                                if (igc) {
                                    ak = ak.toLowerCase();
                                    bk = bk.toLowerCase();
                                }
                                cond = ord === SORT_ASCENDING ? ak < bk : ak > bk
                                if (cond) return -1;
                                return 1;
                            });
                        }

                        count = items.length;

                        items = take(items,query.start,end);

                        // await resolveItems(items, query.resolutions);

                    }).then(()=> {
                        postMessage({ type:'done', items:items, count:count, putBack: put_back, workId:data.workId});
                    }).catch((err)=> {
                        postMessage({type:'error', error:err, workId:data.workId});
                    });
            }
            break;
        default:
    }
}

const resolveItems = async (items, resos)=> {
    if (resos.length === 0) return;

    for (let ii = 0; ii < items.length; ++ii) {

        let item = items[ii];

        for(let ri = 0; ri < resos.length; ++ri) {
            let reso = resos[ri],
                skey = reso.subjectKey,
                key = reso.key,
                newKey = reso.newKey || key,
                dkey = reso.donorKey || skey,
                dtype = reso.donorType,
                dnkeys = reso.donationKeys;
            // Resolve only if it is explicitly enforced or the item
            // at the `key` is null.
            if (!reso.enforce && (item[key] != undefined)) continue;
            let repl = item[key],
                donor = null;
            if (Array.isArray(repl)) {
                let repls = repl,
                    donors = [];
                for (let di = 0; di < repls.length; ++di) {
                    repl = repls[di];
                    donor = (await memnat.table(dtype).where(dkey).equals(repl).toArray())[0];
                    if (!donor) console.warn('Resolution: found 0 donors');
                    donors.push(donor);
                }

                donor = donors;
            } else {
                donor = (await memnat.table(dtype).where(dkey).equals(item[skey]).toArray())[0];
            }

            resolveItem(item,donor,key,newKey,dnkeys);
        }
    }
}

const resolveItem = (item,donor,key,newKey,dks)=> {
    let og = item[key],
        res = null;

    if (Array.isArray(og)) {
        // donor is an array
        let donors = donor;
        res = [];
        for (let di = 0; di < donors.length; ++di) {
            donor = donors[di] || {};
            let dn = {};
            for (let dki = 0; dki < dks.length; ++dki) {
                let dk = dks[dki];
                dn[dk] = donor[dk];
            }
            res.push(dn);
        }
    } else {
        let dn = {};
        for (let dki = 0; dki < dks.length; ++dki) {
            let dk = dks[dki];
            dn[dk] = donor[dk];
            // NOTE: if the value at `key` is a scalar value, then the
            // donor value is given to the item directly.
            item[dk] = dn[dk];
        }
        res = dn;
    }

    item[newKey] = res;
}

const take = (items,start,end)=> {
    if (end > items.length) end = items.length;

    let items_ = items;
    items = [];
    for (let i = start; i < end; ++i) {
        let item = items_[i];
        if (item) items.push(item);
    }

    return items;
}

const PREDICATE_CONTAINS = 'contains';
const PREDICATE_IS_LESS_THAN = 'islessthan';
const PREDICATE_EQUALS = 'equals';
const PREDICATE_IS_GREATER_THAN = 'isgreaterthan';
const PREDICATE_STARTS_WITH = 'startswith';
const PREDICATE_ENDS_WITH = 'endswith';
const PREDICATE_IN_RANGE = 'inrange';

const SORT_ASCENDING = 'asc';
const SORT_DESCENDING = 'desc';

// comps == [{key,pred,value,igc}]
const filter = (item,comps,all)=> {
    const res = [];

    function check(v,cv,pred) {
        pred = pred.toLowerCase();
        switch (pred) {
            case PREDICATE_CONTAINS:
            case PREDICATE_IN_RANGE:
                if (cv.includes) {
                    return cv.includes(v);
                } else if (!isNaN(v) && !isNaN(cv)) {
                    let l1 = (v > cv[0]) && (v < cv[1]),
                        l2 = (v < cv[0]) && (v > cv[1]);
                    return l1 || l2;
                }
                break;
            case PREDICATE_IS_LESS_THAN:
                return v < cv;
                break;
            case PREDICATE_EQUALS:
                return v === cv;
                break;
            case PREDICATE_IS_GREATER_THAN:
                return v > cv;
                break;
            case PREDICATE_STARTS_WITH:
                return cv.startsWith && cv.startsWith(v);
                break;
            case PREDICATE_ENDS_WITH:
                return cv.endsWith && cv.endsWith(v);
                break;
            default:

        }
    }

    for (let i = 0; i < comps.length; ++i) {
        let comp = comps[i],
            k = comp.key,
            pred = comp.predicate,
            cv = item[k],
            v = comp.value,
            igc = comp.ignoreCase
        if (igc && (typeof cv === 'string') && (typeof v === 'string')) {
            cv = cv.toLowerCase();
            v = v.toLowerCase();
        }

        let ch = check(v,cv,pred);
        if (!ch && all) {
            return false;
        } else if (!all) {
            return ch;
        }
    }

    return true;
}

const headerForType = (type)=> {
    switch (type) {
        case ITEM_TYPE_ASPECT:
            return ITEMS_TYPE_ASPECTS;
        case ITEM_TYPE_THEME:
            return ITEMS_TYPE_THEMES;
        case ITEM_TYPE_SHELL:
            return ITEMS_TYPE_SHELLS;
        case ITEM_TYPE_ENTITY:
            return ITEMS_TYPE_ENTITIES;
        case ITEM_TYPE_PROCESS:
            return ITEMS_TYPE_PROCESSES;
        case ITEM_TYPE_SCHOOL:
            return ITEMS_TYPE_SCHOOLS;
        default:

    }
}
