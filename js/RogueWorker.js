// Takes care of moving system 'light' balls.
// Should inform Sound of the rogues to delete.

// NOTE: the up vectors of rogues are left as is. How accurate are they if left alone?

importScripts('constants.js', 'three.js');

const Three = THREE;

const rogues = new Map();

const reuse_vec = new Three.Vector3();
let otime, dt;

onmessage = function (e) {
    let data = e.data,
        ct = data.currentTime;

    switch (data.type) {
        case JOB_TYPE_WORK:

            if (otime == undefined) otime = ct;
            dt = ct - otime;
            otime = ct;

            for (const [nid, rogue] of rogues) {

                reuse_vec.copy(rogue.forward).multiplyScalar(dt*rogue.speed);

                rogue.position.add(reuse_vec);

                postRogue(rogue);

                let fully_decayed = true;

                rogue.parts.forEach((p, idx) => {
                    if (!p.decayed) {
                        let birth = p.startTime,
                            age = ct-birth;

                        if (age >= p.duration) decayPart(p, idx, rogue);
                    }

                    fully_decayed = fully_decayed === false ? false : p.decayed;
                });

                if (fully_decayed) deleteRogue(rogue);
            }
            break;
        case JOB_TYPE_ROGUE:
            rogues.set(data.rogue.id, new Rogue(data.rogue));
            // console.log(data.rogue);
            break;
        case JOB_TYPE_CLEAR:
            rogues.clear();
            break;
        default:
    }
}

function postRogue(rogue) {
    postMessage({
        type: JOB_TYPE_ROGUE,
        id: rogue.id,
        voiceId: rogue.voiceId,
        position: rogue.position
    });
}

function decayPart(part, idx, rogue) {
    part.decayed = true;

    postMessage({
        type: JOB_TYPE_DECAY,
        id: rogue.id,
        part: idx
    });
}

function deleteRogue(rogue) {
    let rid = rogue.id;
    rogues.delete(rid);

    postMessage({
        type: JOB_TYPE_DELETE,
        id: rid,
        voiceId: rogue.voiceId
    });
}

function Rogue(rogue) {
    this.id = rogue.id;
    this.voiceId = rogue.voiceId;
    this.speed = rogue.speed;
    this.forward = new Three.Vector3().copy(rogue.forward);
    this.position = new Three.Vector3().copy(rogue.position);
    this.parts = rogue.parts;
}
