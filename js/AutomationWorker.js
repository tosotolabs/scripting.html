importScripts('constants.js', 'utils.js');

// Automation Circuitry 101: Always start with Analogo as it sets the others' initials.
// Automation Circuitry is the beginning of magic -- music.
const allJobTypes = [ANALOGO, GLISSANDO, PORTAMENTO, VIBRATO];

let mainAutomation;

onmessage = doAutomation;

function doAutomation(e) {
    let data = e.data || e,
        ct = data.currentTime;

    let automation;

    switch (data.type) {
        case JOB_TYPE_WORK:
            let t = false;
            traverseAutomations(mainAutomation, (automation)=> {
                if (!automation) return false; // because work() may run after being asked to clear.
                automation.work(ct);
                postAutomation(automation);

                return true;
            });
            break;
        case JOB_TYPE_AUTOMATION:
            mainAutomation = new Automation(data.automation, ct);
            console.log(mainAutomation);
            break;
        case UNITY:
            getAutomation(data.coordinates).createUnity(data.nodeId);
            break;
        case JOB_TYPE_BREAK_UNITY:
            getAutomation(data.coordinates).rogueAutomations[data.nodeId].break(data.part);
            break;
        case JOB_TYPE_DELETE_UNITY:
            delete getAutomation(data.coordinates).rogueAutomations[data.nodeId];
            break;
        case JOB_TYPE_MODIFY:
            automation = getAutomation(data.coordinates);
            automation.reset({
                frequency: data.frequency,
                intensity: data.intensity,
                period: data.period
            }, automation.currentTime, true);
            break;
        case JOB_TYPE_RESET:
            getAutomation(data.coordinates).reset({
                frequency: data.frequency,
                intensity: data.intensity,
                period: data.period,
                systemType: data.systemType
            }, ct);
            break;
        case JOB_TYPE_CLEAR:
            mainAutomation = null;
            break;
        default:

    }
}

function postAutomation(automation) {
    // Vibrato is set by Glissando, Portamento, and Analogo.
        //  It also outlasts any other Automation.
    // Glissando will only post a value when its time
        // has come.
    // Glissando sets Portamento.
    // Analogo sets Glissando, Portamento, and Vibrato,
        // but it is only one shot, similar to Glissando.
    // Therefore, set Analogo before all others at first,
        // then let the perpetual Vibrato serve as
        // the output of the Automation.
    // Input: Analogo | Output: Vibrato
    postMessage({
        nodeId: automation.nodeId,
        type: JOB_TYPE_AUTOMATION,
        value: automation.value()
    });

    if (automation.rogueAutomations) {
        for (let k in automation.rogueAutomations) postAutomation(automation.rogueAutomations[k]);
    }
}

function getAutomation(coords) {
    let autom, subs=[mainAutomation];
    coords.forEach(coord => {
        autom = subs[coord];
        subs = autom.subAutomations;
    });

    return autom;
}

function traverseAutomations(autom, cb, p) {
    let cont = cb(autom);
    if (cont && autom.subAutomations) {
        autom.subAutomations.forEach(sbautom => {
            traverseAutomations(sbautom, cb, autom);
        });
    }
}

// This assumes interaction (arrow values) will stay the same throughout.
// Use the Analogo to update the initials of all Automation jobs.
function Automation(opts, ct) {
    this.nodeId = opts.nodeId;
    this._automation = opts.automation;

    this.analogo = new Analogo();
    this.portamento = new Portamento();
    this.glissando = new Glissando();
    this.vibrato = new Vibrato();

    this.frequency = (v)=> {
        if (v == undefined) {
            return this.vibrato.value; // output
        } else {
            this._automation.frequency.analogo.value = v; // input for next reset
        }
    }
    this.intensity = (v)=> {
        if (v == undefined) {
            return this._automation.intensity.value;
        } else {
            this._automation.intensity.value = v; // input for next reset
        }
    }
    this.period = (v)=> {
        if (v == undefined) {
            return this._automation.period;
        } else {
            this._automation.period = v; // input for next reset
        }
    }
    this._value = {}
    this.value = ()=> {return this._value}

    this.rogueAutomations = {}

    if (opts.subAutomations) {
        this.subAutomations = opts.subAutomations.map(sb => {
            return new Automation(sb, ct);
        });
    }

    // NOTE: this will (and should) replace the rogue of this voice,
        // if it exists.
    this.createUnity = (nid)=> { // `this` must have subAutomations
        let ropts = {
            nodeId: nid,
            automation: this
        }

        this.rogueAutomations[nid] = new Unity(ropts, ct);
    }

    this.work = (ct)=> {
        let job, done;
        allJobTypes.forEach(type => {
            job = this[type];
            if (job.off) return;
            done = job.work(ct, this);
            if (done) job.rest();
        });

        for (let k in this.rogueAutomations) this.rogueAutomations[k].work(ct);

        this._value.frequency = this.frequency();
        this._value.intensity = this.intensity();
    }

    this.reset = (opts, ct, partial)=> {

        this.currentTime = ct;

        if (opts) {
            // Set my own initial
            this.frequency(opts.frequency);
            this.intensity(opts.intensity);
            this.period(opts.period);

            // Calculate new targets for my subAutomations
            if (this.subAutomations && (this.subAutomations.length > 1) && !partial) {
                switch (opts.systemType) {
                    // TODO: see `reduceMeanings` in `getJourney`
                    case THEME_TYPE_SIGNAL:
                        for (let i = 0; i < NODE_LABELS.length; ++i) {
                            let automation = this.subAutomations[i]._automation,
                                analogo = automation.frequency.analogo,
                                freq = analogo.value;

                            // pivoting
                            analogo.initial = freq;
                            analogo.value = freq; // Analogo unit will have its value set and scaled when explicitly reset.
                        }
                        break;
                    case THEME_TYPE_VERSUS:
                        for (let i = 0; i < NODE_LABELS.length; ++i) {
                            let ni = NODE_LABELS[i],
                                automation = this.subAutomations[i]._automation,
                                analogo = automation.frequency.analogo,
                                freq = analogo.value,
                                // intst = theme.orchestra[i+1].intensity, // Useful when more arrows come
                                mo_ = {};
                            VERSUS_ARROWS.forEach(arr => {
                                let moa = automation.arrows[arr];
                                mo_[arr] = {};
                                if (moa.count) {
                                    mo_[arr].avg = moa.sum/moa.count;
                                    // Calculate the average frequency of its orients.
                                    let fsum = 0;
                                    moa.fsum.forEach(oi => fsum+=this.subAutomations[oi]._automation.frequency.analogo.value);
                                    mo_[arr].favg = fsum/moa.count;
                                } else {
                                    mo_[arr].avg = 0;
                                    mo_[arr].favg = freq; // hmm...this is for all arrow types
                                }
                            });
                            let sim = mo_[ARROW_TYPE_SIMILARITY];
                            // TODO: more frequency and intensity related arrows

                            // pivoting
                            analogo.initial = freq;
                            analogo.value = rangedAverage(freq, sim.favg, sim.avg);
                        }
                        break;
                    case THEME_TYPE_CONTEXT:
                        for (let i = 0; i < NODE_LABELS.length; ++i) {
                            let ni = NODE_LABELS[i],
                                automation = this.subAutomations[i]._automation,
                                analogo = automation.frequency.analogo,
                                glissando = automation.frequency.glissando,
                                portamento = automation.frequency.portamento,
                                vibrato = automation.frequency.vibrato,
                                freq = analogo.value,
                                mo_ = {};
                            CONTEXT_ARROWS.forEach(arr => {
                                let moa = automation.arrows[arr];
                                mo_[arr] = {};
                                if (moa.count) {
                                    mo_[arr].avg = moa.sum/moa.count;
                                    // Calculate the average frequency of its orients.
                                    let fsum = 0;
                                    moa.fsum.forEach(oi => fsum+=this.subAutomations[oi]._automation.frequency.analogo.value);
                                    mo_[arr].favg = fsum/moa.count;
                                } else {
                                    mo_[arr].avg = 0;
                                    mo_[arr].favg = freq; // hmm...this is for all arrow types
                                }
                            });
                            let sim = mo_[ARROW_TYPE_SIMILARITY],
                                cau = mo_[ARROW_TYPE_CAUSATION],
                                dep = mo_[ARROW_TYPE_DEPENDENCE],
                                spe = mo_[ARROW_TYPE_SPECTRUM];

                            // pivoting
                            analogo.initial = freq;
                            analogo.value = rangedAverage(freq, sim.favg, sim.avg);
                            glissando.value = dep.favg;
                            portamento.value = cau.favg;
                            vibrato.value = spe.favg;
                        }
                        break;
                    default:

                }
            }
        }

        if (!partial) {
            this.analogo.reset(this._automation.frequency.analogo, ct, this._automation.period, this);
            this.glissando.reset(this._automation.frequency.glissando, ct, this._automation.period);
            this.portamento.reset(this._automation.frequency.portamento, ct, this._automation.period);
            this.vibrato.reset(this._automation.frequency.vibrato, ct, this._automation.period);
        }
    }

    this.reset(null, ct);
}

// Means 'Similar' in Italian for ARROW_TYPE_SIMILARITY.
    // Etymology of 'analog' states, 'up to ratio'
function Analogo() {

    this.rest = ()=> {
        this.off = true;
    }

    this.reset = (opts, ct, period, autom)=> {
        this.off = false;
        this.initial = opts.initial; // for theory completion
        this.target = opts.value*opts.ratio;
        this.period = period;
        this.startTime = ct;
        this.endTime = ct+opts.time*period;
    }

    this.work = (currentTime, autom)=> {
        // This has been laid out redundantly to showcase the similarity in circuitry across
            // units and how they may be setup in reality. this.value will always be this.initial
            // if startTime===endTime i.e. opts.time===0. Otherwise, it will be this.target.
        let freq_range = this.target-this.initial,
            time_scale = (currentTime-this.startTime)/(this.endTime-this.startTime); // will be either 0 or ~1
        time_scale = isNaN(time_scale) || (time_scale === Infinity) ? 0 : 1; // ideally/naturally, the above would work accurately
        this.value = this.initial+freq_range*time_scale;
        if (isNaN(this.value)) this.value = this.initial;
        autom.portamento.initial = autom.glissando.initial = autom.vibrato.initial = this.value;

        return currentTime >= this.endTime; // always true
    }
}

function Portamento() {

    this.rest = ()=> {
        this.off = true;
    }

    this.reset = (opts, ct, period)=> {
        this.off = false;
        this.period = period;
        this.target = opts.value;
        this.startTime = ct;
        this.endTime = ct+opts.time*period;
    }

    this.work = (currentTime, autom)=> {
        if (currentTime >= this.endTime) return true;
        // If opts.time was 0, then time_range === 0, this.value === Infinity, and this.value === this.initial
            // So a time of 0 renders the effect a one-shot identity function.
        let time_range = this.endTime-this.startTime,
            freq_range = this.target-this.initial;
        this.value = this.initial+Math.exp( (currentTime-this.startTime)/time_range )/Math.E*freq_range;
        if (isNaN(this.value) || (Math.abs(this.value) === Infinity)) this.value = this.initial;

        // The vibrato should change between this new value
        // and its target.
        autom.vibrato.initial = this.value;

        return false;
    }
}

// Think of a Glissando as a unit that sets the voice's frequency, via the Portamento->Vibrato,
    // to the same initial value right up until its endTime, when it sets it to the target.
function Glissando() {

    this.rest = () => {
        this.off = true;
    }

    this.reset = (opts, ct, period)=> {
        this.off = false;
        this.period = period;
        this.target = opts.value;
        this.startTime = ct;
        this.endTime = ct+opts.time*period;
    }

    this.work = (currentTime, autom)=> {
        if (currentTime >= this.time) { // WHY DOES THIS SAY this.time?
            // If opts.time was 0, then time_scale === 0, this.value === this.initial
                // So a time of 0 renders the effect a one-shot identity function.
            let freq_range = this.target-this.initial,
                time_scale = (currentTime-this.startTime)/(this.endTime-this.startTime); // will be either 0 or ~1
            time_scale = isNaN(time_scale) || (time_scale === Infinity) ? 0 : 1; // ideally/naturally, the above would work accurately
            this.value = this.initial+freq_range*time_scale;
            // The portamento should start from this new value
            // and glide to its target in a shorter time frame.
            autom.portamento.startTime = currentTime;
            autom.portamento.initial = this.value;

            // The vibrato should change between this new value
            // and its target.
            autom.vibrato.initial = this.value;

            return true;
        }

        return false;
    }
}

function Vibrato() {

    this.rest = () => {
        // this.off = true; // forever vibrating
    }

    this.reset = (opts, ct, period)=> {
        this.target = opts.value;
        this.period = period;
        this.depth = opts.depth;
        this.startTime = ct;
        this.endTime = ct+opts.time*period; // opts.time===Infinity; forever vibrating;
    }

    this.work = (currentTime, autom)=> {
        // If this.depth was 0, then this.value === this.initial
            // So a depth of 0 renders the effect a continuous identity function.
        let time = Math.sin(Math.PI*2*currentTime*this.period)+1; // bring to [0,2]
        time/=2; // bring to [0,1];
        let range = this.target-this.initial,
            time_scale = (currentTime-this.startTime)/(this.endTime-this.startTime); // will be either NaN or 0
        time_scale = isNaN(time_scale) || (time_scale === Infinity) ? 0 : 1; // ideally/naturally, the above would work accurately
        this.value = this.initial+time*range*this.depth*time_scale;
        if (isNaN(this.value) || (Math.abs(this.value) === Infinity)) this.value = this.initial; // for the outermost system
        return currentTime >= this.endTime;
    }
}

// AKA Debris, Rogue, Entanglement, Measurement
// Represents a shallow copy of a theme.
function Unity(opts, ct) {
    this.nodeId = opts.nodeId;
    this.automation = new Automation({nodeId: this.nodeId+'-unity', automation: opts.automation._automation}, ct); // captured before reset AKA Entanglement
    // TODO: move this to a clone function
    this.subAutomations = opts.automation.subAutomations.map(sb => { return new Automation({nodeId: this.nodeId+'-'+sb.nodeId+'-unity', automation: sb._automation}, ct); /*captured before reset*/ });

    this._value = {}
    this.value = ()=>{return this._value}

    this.work = (ct)=> {
        this._value.frequency = this._value.intensity = 0;

        let count = 0;

        this.subAutomations.forEach(sb => {
            if (!sb.off) {
                sb.work(ct);
                this._value.frequency+=sb.frequency();
                this._value.intensity+=sb.intensity();
                count++;
            }
        });

        if (count) {
            this._value.frequency/=count;
            this._value.intensity/=count;
        }

        if (!this.automation.off) {
            this.automation.work(ct);
            this._value.frequency = (this._value.frequency+this.automation.frequency())/2;
            this._value.intensity = (this._value.intensity+this.automation.intensity())/2;
        }
    }

    this.break = (part)=> {
        if (part === 0) {
            this.automation.off = true;
        } else {
            this.subAutomations[part-1].off = true;
        }
    }
}
