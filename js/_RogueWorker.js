// Takes care of moving systems, pure voices, choruses.
// Should be able to adjust chorus members' positions.
// Should inform Sound of the voices to delete.

// NOTE: the up vectors of rogues are left as is. How accurate are they if left alone?

importScripts('three.js');

const Three = THREE;

const JOB_TYPE_WORK = 'work';
const JOB_TYPE_DELETE = 'delete';
const JOB_TYPE_CLEAR = 'clear';
const JOB_TYPE_ROGUE = 'rogue';
const JOB_TYPE_CHORUS = 'chorus';

const rogues = new Map();

const scene = new Three.Scene(); // ?

const reuse_vec = new Three.Vector3();
let otime, dt;

onmessage = function (e) {
    let data = e.data,
        ct = data.currentTime;

    switch (data.type) {
        case JOB_TYPE_WORK:

            if (otime == undefined) otime = ct;
            dt = ct - otime;
            otime = ct;

            for (const [nid, rogue] of rogues) {
                let birth = rogue.startTime,
                    age = ct-birth;

                if (age >= rogue.duration) {
                    if (rogue.orchestra) {
                        rogue.orchestra.forEach(r => {
                            if (!r) return;
                            rogues.set(r.nodeId, r);
                        });
                    }
                    deleteRogue(rogue);
                } else {
                    // TODO: move along `rogue` forward trajectory while updating vectors
                        // Also maybe move chorus
                        // if conductor, move and check children
                        // Post orientation update message for Sound
                    // XXX: NOTE: conductors are skipped.
                        // May want to visualize this.

                    reuse_vec.copy(rogue.forward).multiplyScalar(dt*rogue.speed);

                    updateRogue(rogue, reuse_vec);

                    postRogue(rogue);

                    if (rogue.orchestra) {
                        rogue.orchestra = rogue.orchestra.map(voice => {
                            if (!voice) return voice;
                            return traverseSystem(voice, v => {
                                let birth = v.startTime,
                                    age = ct-birth;

                                if (age >= v.duration) {
                                    if (v.orchestra) {
                                        v.orchestra.forEach(r => {
                                            if (!r) return;
                                            rogues.set(r.nodeId, r);
                                        });
                                    }

                                    deleteRogue(v);
                                    // TODO: replace this child
                                    console.trace('WILL DELETE', v.nodeId, rogue.nodeId);

                                    return false;
                                } else {
                                    // TODO: move along `rogue` forward trajectory while updating vectors
                                        // Also maybe move chorus
                                        // Post orientation update message for Sound
                                    updateRogue(v, reuse_vec);

                                    postRogue(v);

                                    return true;
                                }
                            });
                        });
                    }
                }
            }
            break;
        case JOB_TYPE_ROGUE:
            rogues.set(data.nodeId, new Rogue(data.rogue));
            // console.log(data.rogue);
            break;
        case JOB_TYPE_CLEAR:
            rogues.clear();
            while(scene.children.length > 0) scene.remove(scene.children[0]);
            break;
        default:
    }
}

function updateRogue(rogue, inc_vec) {
    rogue.object.position.add(inc_vec);
    rogue.object.getWorldDirection(rogue.forward);

    // NOTE:TODO:FIX: Maybe. Test this eventually.
    // rogue.up.copy( rogue.object.up.add(inc_vec).normalize() );
    rogue.object.up.add(inc_vec).normalize();

    if (rogue.leftChorus) {
        rogue.leftChorus.forEach(c => {
            c.object.position.add(inc_vec);
            c.object.getWorldDirection(c.forward);

            // NOTE:TODO:FIX: Maybe. Test this eventually.
            // c.up.copy( c.object.up.add(inc_vec).normalize() );
            c.object.up.add(inc_vec).normalize();
        });
    }

    if (rogue.rightChorus) {
        rogue.rightChorus.forEach(c => {
            c.object.position.add(inc_vec);
            c.object.getWorldDirection(c.forward);

            // NOTE:TODO:FIX: Maybe. Test this eventually.
            // c.up.copy( c.object.up.add(inc_vec).normalize() );
            c.object.up.add(inc_vec).normalize();
        });
    }

    rogue.object.updateMatrixWorld();
}

function postRogue(rogue) {
    let nid = rogue.nodeId;
    postMessage({
        type: JOB_TYPE_ROGUE,
        nodeId: nid,
        up: rogue.object.up,
        forward: rogue.forward,
        position: rogue.object.position,
    });
}

function deleteRogue(rogue) {
    let nid = rogue.nodeId;
    rogues.delete(nid);
    if (rogue.leftChorus) rogue.leftChorus.forEach(c => scene.remove(c.object));
    if (rogue.rightChorus) rogue.rightChorus.forEach(c => scene.remove(c.object));
    scene.remove(rogue.object);

    postMessage({
        type: JOB_TYPE_DELETE,
        nodeId: nid
    });
}

function traverseSystem(voice, cb) {
    let cont = cb(voice);
    if (voice.orchestra && cont) {
        voice.orchestra = voice.orchestra.map(v => {
            if (!v) return v;
            return traverseSystem(v, cb);
        });
    }
    return cont ? voice : null; // TODO: null should actually be a call to replace this voice
}

function Rogue(rogue) {
    this.nodeId = rogue.nodeId;
    this.speed = rogue.speed;
    this.startTime = rogue.startTime;
    this.duration = rogue.duration;
    this.forward = new Three.Vector3().copy(rogue.forward);
    this.orchestra = rogue.orchestra ? rogue.orchestra.map(r => { return new Rogue(r) }) : null;
    this.leftChorus = rogue.leftChorus ? rogue.leftChorus.map(r => { return new Rogue(r) }) : null;
    this.rightChorus = rogue.rightChorus ? rogue.rightChorus.map(r => { return new Rogue(r) }) : null;

    let obj = new Three.Object3D();
    scene.add(obj);
    obj.position.copy(rogue.position);
    obj.up.copy(rogue.up);
    obj.matrixWorld.fromArray(rogue.matrixWorld);
    obj.matrix.fromArray(rogue.matrix);
    obj.quaternion.fromArray(rogue.quaternion);
    obj.matrixAutoUpdate = true;

    this.object = obj;
}
