'use strict'

const Memnat = function() {

    function themeTypeName(t) {
        switch (t) {
            case THEME_TYPE_SIGNAL: return { name: 'Signal', pr:'Parralel' };
            case THEME_TYPE_VERSUS: return { name: 'Versus', pr:'Perpendicular' };
            case THEME_TYPE_CONTEXT: return { name: 'Context', pr:'Peripheral' };
            case THEME_TYPE_MOD_QUAD: return { name: 'Modifying Quadrilateral', pr:'Progress' };
            case THEME_TYPE_TEXT: return { name: 'Text', pr:'Process' };
            default:
        }
    }

    this.ASPECT_TYPE_PURE = ASPECT_TYPE_PURE;
    this.ASPECT_TYPE_COMPOUND = ASPECT_TYPE_COMPOUND;

    this.ARROW_TYPE_ACKNOWLEDGEMENT = ARROW_TYPE_ACKNOWLEDGEMENT;
    this.ARROW_TYPE_SIMILARITY = ARROW_TYPE_SIMILARITY;
    this.ARROW_TYPE_CAUSATION = ARROW_TYPE_CAUSATION;
    this.ARROW_TYPE_DEPENDENCE = ARROW_TYPE_DEPENDENCE;
    this.ARROW_TYPE_FOUNDATION = ARROW_TYPE_FOUNDATION;
    this.ARROW_TYPE_SPECTRUM = ARROW_TYPE_SPECTRUM;
    this.ARROW_TYPE_SCALE = ARROW_TYPE_SCALE;

    this.SIGNAL_ARROWS = SIGNAL_ARROWS;
    this.VERSUS_ARROWS = VERSUS_ARROWS;
    this.CONTEXT_ARROWS = CONTEXT_ARROWS;

    this.NUM_DECIMAL_PLACES = NUM_DECIMAL_PLACES;

    this.CLICKED_ON_THEME_TITLE = CLICKED_ON_THEME_TITLE;
    this.CLICKED_ON_SHELL_TITLE = CLICKED_ON_SHELL_TITLE;
    this.CLICKED_ON_CANVAS = CLICKED_ON_CANVAS;

    this.THEME_TYPE_CONTEXT = THEME_TYPE_CONTEXT;
    this.THEME_TYPE_SIGNAL = THEME_TYPE_SIGNAL;
    this.THEME_TYPE_VERSUS = THEME_TYPE_VERSUS;
    this.THEME_TYPE_TEXT = THEME_TYPE_TEXT;
    this.THEME_TYPE_MOD_QUAD = THEME_TYPE_MOD_QUAD;

    this.themeTypeName = themeTypeName;

    this.NODE_LABELS = NODE_LABELS;
    this.SIGNAL_LABELS = SIGNAL_LABELS;
    this.CONTEXT_LABELS = CONTEXT_LABELS;
    this.VERSUS_LABELS = VERSUS_LABELS;
    this.MOD_QUAD_LABELS = MOD_QUAD_LABELS;

    // Aspects can be shared across multiple themes.
    this.Aspect = function(props) {
        // id,n,abbr,type,ons
        this.id = props.id;
        this.name = props.name;
        const abbr_fs = new Map();
        this.abbr_ = props.abbr;
        this.abbr = (str,f)=> {
            if(str != undefined) {
                if (f) {
                    abbr_fs.set(str,f);
                } else {
                    if (str === this.abbr_) return;
                    this.abbr_ = str;
                    for (const [k,f] of abbr_fs) {
                        f(str);
                    }
                }
            } else {
                return this.abbr_;
            }
        }
        this.frequencies_ = props.frequencies || [0,0,0];
        this.frequencies = (idx,freq)=> {
            if (freq != undefined) {
                this.frequencies_[idx] = freq;
            } else if (idx != undefined) {
                return this.frequencies_[idx];
            }else {
                return this.frequencies_;
            }
        }
        this.cover_ = props.cover || '';
        this.cover = (cover)=> {
            if (cover == undefined) {
                return this.cover_;
            } else {
                this.cover_ = cover;
            }
        }
        this.type = props.type;
        this.onHighlight = props.onHighlight;
    }

    // A theme exists at only one place at a time.
    this.Theme = function(props) {
        // id,n,sid,es,ons,onea
        this.id = props.id;
        this.name_ = props.name;
        this.name = (str)=> {
            if(str != undefined) {
                this.name_ = str;
            } else {
                return this.name_;
            }
        }
        this.shellId = props.shellId;
        this.diversity = props.diversity == undefined ? 0 : props.diversity;
        this.meaning = props.meaning;
        this.quantum = props.quantum || 1;
        this.squat = props.squat || 1; // TODO: this is an odd feature
        this.duration_ = props.duration == undefined ? 3 : props.duration;
        this.duration = (duration)=> {
            if (duration != undefined) {
                this.duration_ = duration;
            } else {
                return this.duration_;
            }
        }
        this.processId_ = props.processId || '';
        this.processId = (processId)=> {
            if (processId != undefined) {
                this.processId_ = processId;
            } else {
                return this.processId_;
            }
        }
        this.intensities_ = props.intensities || [1,1,1,1,1,1];
        this.intensities = (idx,intst)=> {
            if (intst != undefined) {
                this.intensities_[idx] = intst;
            } else if (idx != undefined) {
                return this.intensities_[idx];
            } else {
                return this.intensities_;
            }
        }
        this.durations_ = props.durations || [0,0,0,0,0,0];
        this.durations = (idx,d)=> {
            if (d != undefined) {
                this.durations_[idx] = d;
            } else if (idx != undefined) {
                return this.durations_[idx];
            } else {
                return this.durations_;
            }
        }
        this.processIds_ = props.processIds || ['','','','','',''];
        this.processIds = (idx,procid)=> {
            if (procid != undefined) {
                this.processIds_[idx] = procid;
            } else if (idx != undefined) {
                return this.processIds_[idx];
            } else {
                return this.processIds_;
            }
        }
        this.onSelect = props.onSelect;
        this.onMeaningChange = props.onMeaningChange;
        this.onMeaningCommit = props.onMeaningCommit;
        this.width = props.width || 1;
        this.depth = props.depth || 1;
        this.type = props.type;

        switch (this.type) {
            case THEME_TYPE_SIGNAL:
                this.meaningOf = (pos)=> {
                    return this.meaning[n_ss[pos]];
                }
                break;
            case THEME_TYPE_VERSUS:
                this.meaningOf = (spos,epos)=> {
                    return this.meaning[vn_ss[spos]+vn_ss[epos]];
                }
                break;
            case THEME_TYPE_CONTEXT:
                this.meaningOf = (spos,epos)=> {
                    return this.meaning[n_ss[spos]+n_ss[epos]];
                }
                break;
            case THEME_TYPE_MOD_QUAD:
                this.meaningOf = (spos,epos)=> {
                    return this.meaning[mfbj_ss[spos]+mfbj_ss[epos]];
                }
                break;
            case THEME_TYPE_TEXT:
                this.meaningOf = ()=> {
                    return this.meaning;
                }
                break;
            default:

        }
    }

    let toggleNode = (props)=> {
            let n = props.node,
                t_col = props.onColor,
                f_col = props.offColor,
                ensure = props.ensure,
                isp = props.isPrimary;
            n.isSelected = ensure ? true : !n.isSelected;
            let fill = n.isSelected ? t_col : f_col;
            n.elem.fill(fill);
            if (n.isPrimary != undefined) {
                n.isPrimary = isp ? true : false;
            }
        },
        drawAspectLabel = (props)=> {
            let draw = props.draw,
                str = props.value,
                x = props.x,
                y = props.y,
                type = props.type,
                label = props.label,
                font = props.font || {};

            let font_ = {
                family: 'Georgia',
                size: 8,
                anchor: 'middle'
            };

            for (let k in font) font_[k] = font[k];
            font = font_;

            let abbr = str.substring(0,6);
            if (str.length > 6) abbr +='...';
            abbr += (type === ASPECT_TYPE_PURE ? '*' : '');

            if (label) {
                label.text(abbr);
            } else {
                label = draw.text(abbr);
                label.font(font);
                label.x(x);
                label.y(y);
                label.addClass('tosoto-standard-link2');
                label.style('cursor','pointer');
            }

            return label;
        },
        drawTitle = (draw,_t,n,d,c,w,f)=> {
            let t_ = n.substring(0,20);
            if (n.length > 20) t_+='...';
            if (d) t_+='-'+d;
            let t = null;

            if (_t) {
                _t.text(t_);
            } else {
                t = draw.text(t_);
                t.font({
                    family: 'Georgia',
                    size: 8,
                    anchor: 'end'
                });
                if (c) t.addClass(c);
                t.x(w-10);
                t.y(5);
                t.style('cursor','pointer');
            }

            if (f) f(t);

            return t;
        },
        redrawPath = (p,s,p2,pc)=> {
            let t = lerp(s,p2,[pc,pc]);
            p.value = pc;
            p.plot('M'+s[0]+' '+s[1]+' L'+t[0]+' '+t[1]);
            p.dmove(5,5);
        },
        clearSelection = (nodes,infoSel,sel1,sel2,def_col,f_col)=> {
            if (infoSel) nodes[infoSel].elem.stroke({color: def_col});
            if (sel1) {
                nodes[sel1].isSelected = true; // to ensure it is turned off
                toggleNode({
                    node: nodes[sel1],
                    offColor: f_col
                });
                nodes[sel1].elem.stroke({color: def_col});
            }
            if (sel2) {
                nodes[sel2].isSelected = true; // to ensure it is turned off
                toggleNode({
                    node: nodes[sel2],
                    offColor: f_col
                });
                nodes[sel2].elem.stroke({color: def_col});
            }
        },
        // markers
        drawArrows = (props)=> {
            let draw = props.draw,
                w = props.width,
                h = props.height,
                group = props.group,
                res = {};
            let sim_arr = draw.marker(0,0, (marker)=> {
                    marker.viewbox({x:0, y:0, width:0, height:0});
                }),
                caus_arr = draw.marker(w+10,h+10, (marker)=> {
                    marker.polygon('0,303.4 200,500 0,696.6').fill('#404040');
                    // marker.attr('viewBox','0 0 1000 1000');
                    marker.viewbox({x:0, y:0, width:1000, height:1000});
                    marker.ref(0,500);
                    marker.attr('markerUnits','strokeWidth');
                }),
                dep_arr = draw.marker(w,h, (marker)=> {
                    marker.rect(100,600).y(200).fill('#404040');
                    marker.viewbox({x:0, y:0, width:1000, height:1000});
                    marker.ref(0,500);
                    marker.attr('markerUnits','strokeWidth');
                }),
                spec_arr = draw.marker(w,h, (marker)=> {
                    marker.rect(75,587).x(127.5).y(205).fill('#404040');
                    marker.rect(330,100).x(0).y(158).fill('#404040');
                    marker.rect(330,100).x(0).y(742).fill('#404040');
                    // marker.rect(587,75).x(50).y(462.5).fill('#404040');
                    // marker.rect(100,330).x(584.2).y(335).fill('#404040');
                    // marker.rect(100,330).x(0).y(335).fill('#404040');
                    marker.viewbox({x:0, y:0, width:1000, height:1000});
                    // marker.ref(0,500);
                    marker.ref(165,500);
                    marker.attr('markerUnits','strokeWidth');
                });

                res[ARROW_TYPE_SIMILARITY] = sim_arr;
                res[ARROW_TYPE_CAUSATION] = caus_arr;
                res[ARROW_TYPE_DEPENDENCE] = dep_arr;
                res[ARROW_TYPE_SPECTRUM] = spec_arr;

            return res;
        }

    this.Context = function(props) {

        let theme = props.theme,
            asps = props.aspects,
            container = props.container,
            shell_name = props.shellName,
            theme_position = props.themePosition;

        this.id = container;

        let draw = SVG(container).size('100%','100%'),
            backButton = draw.rect(MEMNAT_WIDTH,MEMNAT_HEIGHT).addClass('memnat-back-button').fill('rgba(0,0,0,0)'),
            graph = draw.group(),
            nodes_ = draw.group(),
            b_edges_ = draw.group(),
            markers_ = draw.group(),
            d_edges_1_ = draw.group(),
            d_edges_2_ = draw.group(),
            minis_ = draw.group(),

            aspectLabels = draw.group(),
            title_label = null,
            shell_label = null,
            position_label = null,
            far_left_label = null,
            left_label = null,
            delete_label = null,
            right_label = null,
            far_right_label = null,
            far_left_f = null, left_f = null, delete_f = null, right_f = null, far_right_f = null,
            e_pos0_col = 'rgba(255,0,0,0.8)',
            e_pos1_col = 'rgba(0,0,255,0.45)',
            initX = 85,
            initY = 40,
            nodes = {
                a: {
                    coords: [initX,initY],
                    elem: null,
                    label: null,
                    color: n_colors[0],
                    tag: n_ss[0],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false,
                    miniElems: [],
                    aspect: asps[0]
                },
                b: {
                    coords: [initX+40,initY+25],
                    elem: null,
                    label: null,
                    color: n_colors[1],
                    tag: n_ss[1],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false,
                    miniElems: [],
                    aspect: asps[1]
                },
                c: {
                    coords: [initX+40,initY+70],
                    elem: null,
                    label: null,
                    color: n_colors[2],
                    tag: n_ss[2],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false,
                    miniElems: [],
                    aspect: asps[2]
                },
                d: {
                    coords: [initX,initY+95],
                    elem: null,
                    label: null,
                    color: n_colors[3],
                    tag: n_ss[3],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false,
                    miniElems: [],
                    aspect: asps[3]
                },
                e: {
                    coords: [initX-40,initY+70],
                    elem: null,
                    label: null,
                    color: n_colors[4],
                    tag: n_ss[4],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false,
                    miniElems: [],
                    aspect: asps[4]
                },
                f: {
                    coords: [initX-40,initY+25],
                    elem: null,
                    label: null,
                    color: n_colors[5],
                    tag: n_ss[5],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false,
                    miniElems: [],
                    aspect: asps[5]
                }
            },
            b_edges = {},
            d_edges_1 = {},
            d_edges_2 = {},
            minis = {},
            sel1 = null,
            sel2 = null,
            infoSel = null,
            blackPrimary = "#404040",
            whitePrimary = '#f5f5f5',
            primaryColor = blackPrimary,
            secondaryColor = '#797979',
            screenX = screenY = 0,
            commit = null;

        draw.viewbox({x:0, y:0, width:MEMNAT_WIDTH, height:MEMNAT_HEIGHT});
        draw.attr('overflow','hidden');
        draw.attr('preserveAspectRatio', 'xMinYMin meet');
        draw.attr('width', null);
        draw.attr('height', null);

        graph.add(nodes_);
        graph.add(b_edges_);
        graph.add(d_edges_1_);
        graph.add(d_edges_2_);

        let fixActive = ()=> {
                let n1 = nodes[sel1],
                    n2 = nodes[sel2];
                if (n1 && n1.isActive) {
                    n1.isActive = false;
                }
                if (n2 && n2.isActive) {
                    n2.isActive = false;
                }
            },
            getPathPoints = (p)=> {
                let k = p.tag,
                    e = b_edges[k],
                    s = [e.source.x(),e.source.y()],
                    t = [e.target.x(),e.target.y()],
                    tag1 = k.substring(0,1),
                    tag2 = k.substring(1,2),
                    points = {};
                points[tag1] = s;
                points[tag2] = t;

                return points;
            },
            nodeOutgoings = (tag)=> {
                let aoes = [];
                for (let i = 0; i < n_ss.length; i++) {
                    let n_s = n_ss[i];
                    if (n_s == tag) continue;
                    let e = theme.meaning[tag+n_s];
                    aoes.push({epos:n_ss.indexOf(nodes[n_s].tag),value:e.value,arrowType:e.arrowType});
                }
                return aoes
            },
            getPathIncrease = (from, to, angle, mcs)=> {
                let mag_ft = calcMag([from.x,from.y],[to.x,to.y]),
                    mag_tm = calcMag([mcs.x,mcs.y],[to.x,to.y]),
                    pc_dist = 0;

                let angle_condition = false,
                    pos_condition = false;

                pc_dist = (mag_ft-mag_tm)/mag_ft;
                pc_dist = pc_dist > 1 ? 1 : (pc_dist < 0 ? 0 : pc_dist);

                switch (from.tag) {
                    case 'a':
                        switch (to.tag) {
                            case 'b':
                                angle_condition = angle < 90;
                                pos_condition = mcs.x >= to.x;
                                break;
                            case 'c':
                                angle_condition = angle < 90;
                                pos_condition = mcs.y >= to.y;
                                break;
                            case 'd':
                                angle_condition = angle < 90;
                                pos_condition = mcs.y >= to.y;
                                break;
                            case 'e':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 300);
                                pos_condition = mcs.y >= to.y;
                                break;
                            case 'f':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 330);
                                pos_condition = mcs.x <= to.x;
                                break;
                        }
                        break;
                    case 'b':
                        switch (to.tag) {
                            case 'a':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 330);
                                pos_condition = mcs.x <= to.x;
                                break;
                            case 'c':
                                angle_condition = angle < 90;
                                pos_condition = mcs.y >= to.y;
                                break;
                            case 'd':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 300);
                                pos_condition = mcs.y >= to.y;
                                break;
                            case 'e':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 330);
                                pos_condition = mcs.x <= to.x;
                                break;
                            case 'f':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 360);
                                pos_condition = mcs.x <= to.x;
                                break;
                        }
                        break;
                    case 'c':
                        switch (to.tag) {
                            case 'a':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 300);
                                pos_condition = mcs.y <= to.y;
                                break;
                            case 'b':
                                angle_condition = angle < 90;
                                pos_condition = mcs.y <= to.y;
                                break;
                            case 'd':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 330);
                                pos_condition = mcs.x <= to.x;
                                break;
                            case 'e':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 360);
                                pos_condition = mcs.x <= to.x;
                                break;
                            case 'f':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 300 && angle <= 330);
                                pos_condition = mcs.x <= to.x;
                                break;
                        }
                        break;
                    case 'd':
                        switch (to.tag) {
                            case 'a':
                                angle_condition = angle < 90;
                                pos_condition = mcs.y <= to.y;
                                break;
                            case 'b':
                                angle_condition = angle < 90;
                                pos_condition = mcs.y <= to.y;
                                break;
                            case 'c':
                                angle_condition = angle < 90;
                                pos_condition = mcs.x >= to.x;
                                break;
                            case 'e':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 330);
                                pos_condition = mcs.x <= to.x;
                                break;
                            case 'f':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 300 && angle <= 330);
                                pos_condition = mcs.y <= to.y;
                                break;
                        }
                        break;
                    case 'e':
                        switch (to.tag) {
                            case 'a':
                                angle_condition = angle < 90;
                                pos_condition = mcs.y <= to.y;
                                break;
                            case 'b':
                                angle_condition = angle < 90;
                                pos_condition = mcs.x >= to.x;
                                break;
                            case 'c':
                                angle_condition = angle < 90;
                                pos_condition = mcs.x >= to.x;
                                break;
                            case 'd':
                                angle_condition = angle < 90;
                                pos_condition = mcs.x >= to.x;
                                break;
                            case 'f':
                                angle_condition = angle < 90;
                                pos_condition = mcs.y <= to.y;
                                break;
                        }
                        break;
                    case 'f':
                        switch (to.tag) {
                            case 'a':
                                angle_condition = angle < 90;
                                pos_condition = mcs.x >= to.x;
                                break;
                            case 'b':
                                angle_condition = angle < 90;
                                pos_condition = mcs.x >= to.x;
                                break;
                            case 'c':
                                angle_condition = angle < 90;
                                pos_condition = mcs.x >= to.x;
                                break;
                            case 'd':
                                angle_condition = angle < 90;
                                pos_condition = mcs.y >= to.y;
                                break;
                            case 'e':
                                angle_condition = angle < 90;
                                pos_condition = mcs.y >= to.y;
                                break;
                        }
                        break;
                }

                if (angle_condition && pos_condition) pc_dist = 1;

                return pc_dist;
            },
            updateMeaning = (scs_diff,mcs_,pc,ping,ctrl)=> {
                //- check if two nodes are selected && that one of them is active
                //- get the edge between them
                //- redraw the edge considering the angle between
                // the mouse position and the active node
                // use screenX & screenY globals

                let n1 = nodes[sel1],
                    n2 = nodes[sel2],
                    active = null;

                if (n1 && n1.isActive) active = sel1;
                if (n2 && n2.isActive) active = sel2;

                if (active === null || sel1 === null || sel2 === null || ((pc == undefined) && !ctrl)) return;

                let k = n_ss.indexOf(sel1) < n_ss.indexOf(sel2) ? sel1+sel2 : sel2+sel1,
                    _k = active === sel1 ? sel1+sel2 : sel2+sel1,
                    path1 = d_edges_1[k],
                    path2 = d_edges_2[k];

                // change redrawPath so that I can decide beforehand which side it should start from
                // if path1.from && path2.from are null, set it and redraw path1
                // if path1.from is equal to the active, redraw it
                // if path2.from is equal to the active, redraw it

                // calculate how much of edge to draw
                // transform screen coords to svg coords
                let p1 = null,
                    p2 = null,
                    p2_sel = active === sel1 ? sel2 : sel1,
                    points = null,
                    scs = [0,0],
                    mcs = mcs_ ? draw.point(mcs_[0],mcs_[1]) : null,
                    angle = 0,
                    r = Math.min(1, Math.max(0, pc)) || 0;
                if ((!path1.from && !path2.from) || (path1.from === active)) {
                    path1.from = active;
                    theme.meaning[_k].position = 0;
                    points = getPathPoints(path1);
                    // redraw path1
                    p1 = points[active];
                    p2 = points[p2_sel];
                    if (scs_diff) {
                        scs = [p1[0]+scs_diff[0], p1[1]+scs_diff[1]];
                        angle = rad2deg(calcAngle(p2,scs,p1));
                        r = getPathIncrease({
                            x: p1[0],
                            y: p1[1],
                            tag: active
                        },{
                            x: p2[0],
                            y: p2[1],
                            tag: p2_sel
                        }, angle, mcs);
                    }
                    r = r.toFixedNumber(NUM_DECIMAL_PLACES);
                    theme.meaning[_k].value = r;
                    path1.value = r;
                    if (r === 0) path1.from = null; // allow position to be reclaimed
                    redrawPath(path1,p1,p2,r);
                } else if (!path2.from || (path2.from === active)) {
                    path2.from = active;
                    theme.meaning[_k].position = 1;
                    points = getPathPoints(path2);
                    // redraw path2
                    p1 = points[active];
                    p2 = points[p2_sel];
                    if (scs_diff) {
                        scs = [p1[0]+scs_diff[0], p1[1]+scs_diff[1]];
                        angle = rad2deg(calcAngle(p2,scs,p1));
                        r = getPathIncrease({
                            x: p1[0],
                            y: p1[1],
                            tag: active
                        },{
                            x: p2[0],
                            y: p2[1],
                            tag: p2_sel
                        }, angle, mcs);
                    }
                    r = r.toFixedNumber(NUM_DECIMAL_PLACES);
                    theme.meaning[_k].value = r;
                    path2.value = r;
                    if (r === 0) path2.from = null; // allow position to be reclaimed
                    redrawPath(path2,p1,p2,r);
                }

                commit = {
                    spos: n_ss.indexOf(_k.substring(0,1)),
                    epos: n_ss.indexOf(_k.substring(1,2)),
                    key: _k,
                    value: theme.meaning[_k].value
                }

                if ((scs_diff || ping) && theme.onMeaningChange) theme.onMeaningChange(commit);
            },
            diversityCallback = null;

        draw.mousemove((e)=> {
            updateMeaning([e.screenX-screenX,e.screenY-screenY], [e.clientX,e.clientY], null, null, e.ctrlKey);
        });

        backButton.style({cursor:'pointer'});
        backButton.click(()=> { if (theme.onSelect) theme.onSelect(CLICKED_ON_CANVAS, container); });

        document.addEventListener('mouseup', fixActive);
        document.addEventListener('mouseleave', fixActive);
        const doCommit = ()=> {
            if (commit && theme.onMeaningCommit) {
                theme.onMeaningCommit(commit);
                commit = null;
            }
        }
        ['mouseup','mouseleave'].forEach(e => {
            document.addEventListener(e, doCommit);
        });

        title_label = drawTitle(draw,null,theme.name(),null,'tosoto-standard-link2',MEMNAT_WIDTH);
        if (shell_name) shell_label = drawTitle(draw,null,shell_name,null,'tosoto-standard-link2',MEMNAT_WIDTH,(l)=> { l.y(MEMNAT_HEIGHT-12) });
        if (theme_position) position_label = drawTitle(draw,null,theme_position+'',null,null,MEMNAT_WIDTH,(l)=> {l.x(10).y(MEMNAT_HEIGHT-12);});
        far_left_label = drawTitle(draw,null,'«',null,null,MEMNAT_WIDTH);
        left_label = drawTitle(draw,null,'‹',null,null,MEMNAT_WIDTH);
        delete_label = drawTitle(draw,null,'-',null,null,MEMNAT_WIDTH);
        right_label = drawTitle(draw,null,'›',null,null,MEMNAT_WIDTH);
        far_right_label = drawTitle(draw,null,'»',null,null,MEMNAT_WIDTH);
        [
            [far_left_label,10],
            [left_label,20],
            [delete_label,30],
            [right_label,40],
            [far_right_label,50]
        ].map(l_ => {
            l_[0].font({anchor:'start', size:12});
            l_[0].x(l_[1]).dy(-3);
            l_[0].addClass('direction-label');
        });

        title_label.click(()=> { if (theme.onSelect) theme.onSelect(CLICKED_ON_THEME_TITLE, container); });
        if (shell_label) shell_label.click(()=> { if (theme.onSelect) theme.onSelect(CLICKED_ON_SHELL_TITLE, container); });

        theme.name = (str)=> {
            if(str) {
                theme.name_ = str;
                title_label.text(str);
                return undefined;
            } else {
                return theme.name_;
            }
        }

        // Draw nodes
        for (let i = 0; i < n_ss.length; ++i) {
            let n_s = n_ss[i],
                n = nodes[n_s],
                c_ = nodes_.group().translate(n.coords[0], n.coords[1]),
                c = c_.circle(10);
            c_.fill('#f5f5f5');
            c_.stroke({color:blackPrimary, width:1.25});
            c_.style('cursor','pointer');
            c_.addClass('node-'+n_ss[i]);
            n.elem = c_;

            let lprops = {
                    draw: draw,
                    value: n.aspect.abbr(),
                    x: c_.cx(),
                    y: c_.cy(),
                    type: n.aspect.type,
                    font: {anchor:'end'}
                },
                dx = 0,
                dy = 0,
                anchor = 'middle';

            switch (n.tag) {
                case 'a':
                    dy = -27;
                    break;
                case 'b':
                    anchor = 'start';
                    dx = 5;
                    dy = 5;
                    break;
                case 'c':
                    anchor = 'start';
                    dx = -10;
                    dy = 12;
                    break;
                case 'd':
                    dy = 17;
                    break;
                case 'e':
                    anchor = 'end';
                    dx = 10;
                    dy = 12;
                    break;
                case 'f':
                    anchor = 'end';
                    dx = -5;
                    dy = 5;
                    break;
            }
            lprops.x+=dx;
            lprops.y+=dy;
            lprops.font.anchor = anchor;
            let label = drawAspectLabel(lprops);
            n.label = label;
            label.click(()=> {
                c_.fire('mousedown', {isActive: false});
                // if (n.aspect.onHighlight) n.aspect.onHighlight(theme.id, n_ss.indexOf(n.tag), nodeOutgoings(n.tag));
            });
            label.dblclick(()=> c_.fire('dblclick'));
            aspectLabels.add(label);

            n.aspect.abbr(theme.id, (str)=> {
                if(str) {
                    drawAspectLabel({draw:draw,value:str,type:n.aspect.type,label:n.label});
                    return undefined;
                } else {
                    let t = label.text()
                    return n.aspect.type === ASPECT_TYPE_PURE ? t.substring(0,t.length-1) : t;
                }
            });

            // only two nodes can be selected at once
            c_.on('dblclick', (e)=> {
                let color = primaryColor,
                    ocolor = secondaryColor;
                if (sel1) {
                    color = secondaryColor;
                    ocolor = primaryColor;
                }
                toggleNode({
                    node: n,
                    onColor: color,
                    offColor: whitePrimary,
                    isPrimary: color === primaryColor
                });
                if (n.isSelected) {
                    if (sel1) {
                        if (sel2) {
                            toggleNode({
                                node: nodes[sel2],
                                offColor: whitePrimary
                            });
                        }
                        sel2 = n.tag;
                    } else {
                        sel1 = n.tag
                    }
                } else {
                    if (sel1 === n.tag) {
                        sel1 = null;
                        if (sel2) {
                            sel1 = sel2;
                            toggleNode({
                                node: nodes[sel1],
                                ensure: true,
                                isPrimary: true,
                                onColor: primaryColor
                            });
                        }
                    }
                    sel2 = null;
                }
            });

            c_.on('mousedown', (e)=> {
                n.isActive = e.detail ? ((e.detail.isActive === undefined) ? true : e.detail.isActive) : true;
                screenX = e.screenX;
                screenY = e.screenY;

                let shouldHighlight = e.detail ? ((e.detail.shouldHighlight === undefined) ? true : e.detail.shouldHighlight) : true;

                if (shouldHighlight && infoSel !== n.tag) {
                    if (infoSel) nodes[infoSel].elem.stroke({color:blackPrimary});
                    infoSel = n.tag;
                }
                if (shouldHighlight && c_.attr('stroke') === blackPrimary) c_.stroke({color:n.color});

                let shouldPing = e.detail ? ((e.detail.shouldPing === undefined) ? true : e.detail.shouldPing) : true;
                if (n.aspect.onHighlight && shouldPing) n.aspect.onHighlight(theme.id, n_ss.indexOf(n.tag), nodeOutgoings(n.tag));
            });

            c_.on('mouseup', (e)=> {
                n.isActive = false;
            });

            // c_.click((e)=> {
            //     if (infoSel !== n.tag) {
            //         if (infoSel) nodes[infoSel].elem.stroke({color:blackPrimary});
            //         infoSel = n.tag;
            //     }
            //     if (c_.attr('stroke') === blackPrimary) c_.stroke({color:n.color});
            //
            //     if (n.aspect.onHighlight) n.aspect.onHighlight(theme.id, n.aspect.id, nodeOutgoings(n.tag));
            // });
        }

        // Draw sketch lines
        for (let i = n_ss.length-1; i > 0; --i) {
            let b_s = n_ss[i],
                b = nodes[b_s].elem;
            for (let j = 0; j < i; ++j) {
                let a_s = n_ss[j],
                    a = nodes[a_s].elem,
                    ab = a.connectable({
                        container: b_edges_,
                        markers: markers_
                    }, b);
                ab.setLineColor(blackPrimary);
                ab.line.stroke({width: 0.5});
                ab.line.style({'stroke-dasharray': 3});
                ab.line.back();

                b_edges[a_s+b_s] = ab;
            }
        }

        b_edges_.backward();

        let arrows = drawArrows({
            width: 15,
            height: 15,
            draw: draw
        });

        // Draw minis
        for (let k in nodes) {
            let n = nodes[k],
                kpos = n_ss.indexOf(k),
                c_ = n.elem,
                s = [c_.x(),c_.y()],
                minis_lines = draw.group();

            for (let j in nodes) {
                if (k === j) continue;
                let nt = nodes[j],
                    // len = 9,
                    jpos = n_ss.indexOf(j),
                    len = Math.abs(jpos - kpos) % 2 === 0 ? 9 : 7,
                    pc = len/calcMag(s,[nt.elem.x(),nt.elem.y()]),
                    t = lerp(s,[nt.elem.x(),nt.elem.y()],[pc,pc]),
                    p = draw.path('M'+s[0]+' '+s[1]+' L'+t[0]+' '+t[1]);
                p.fill('none').dmove(5,5);
                p.stroke({
                    color:'rgba(40,40,40,1)',
                    width: 0.5,
                    linecap: 'round',
                    linejoin: 'round'
                });
                n.miniElems.push(p);
                minis_lines.add(p);
            }
            switch (n.tag) {
                case 'a':
                    minis_lines.dy(-16);
                    break;
                case 'b':
                    minis_lines.dx(16);
                    minis_lines.dy(-5);
                    break;
                case 'c':
                    minis_lines.dx(16);
                    minis_lines.dy(5);
                    break;
                case 'd':
                    minis_lines.dy(16);
                    break;
                case 'e':
                    minis_lines.dx(-16);
                    minis_lines.dy(5);
                    break;
                case 'f':
                    minis_lines.dx(-16);
                    minis_lines.dy(-5);
                    break;
                default:

            }
            minis_.add(minis_lines);
        }

        // Draw value edges
        for (let k in b_edges) {
            // bottom edge
            let e = b_edges[k],
                s = [e.source.x(),e.source.y()],
                t = [e.target.x(),e.target.y()],
                p = draw.path('M'+s[0]+' '+s[1]+' L'+s[0]+' '+s[1]+' z');
            p.fill('none').dmove(5,5);
            p.stroke({
                color: e_pos0_col,
                width: 1.25,
                linecap: 'round',
                linejoin: 'round'
            });

            p.tag = k;
            p.value = 0.0;

            d_edges_1[k] = p;
            d_edges_1_.add(p);

            // top edge
            t = [e.source.x(),e.source.y()],
            s = [e.target.x(),e.target.y()],
            p = draw.path('M'+s[0]+' '+s[1]+' L'+s[0]+' '+s[1]+' z');
            p.fill('none').dmove(5,5);
            p.stroke({
                color: e_pos1_col,
                width: 2,
                linecap: 'round',
                linejoin: 'round'
            });

            p.tag = k;
            p.value = 0.0;

            d_edges_2[k] = p;
            d_edges_2_.add(p);
        }

        if (theme.meaning) {
            for (let k_ in theme.meaning) {
                let k = k_,
                    e_ = theme.meaning[k],
                    e = b_edges[k],
                    s = null,
                    t = null;
                if (e) {
                    s = [e.source.x(),e.source.y()];
                    t = [e.target.x(),e.target.y()];
                } else {
                    k = n_reverse(k);
                    e = b_edges[k];
                    t = [e.source.x(),e.source.y()];
                    s = [e.target.x(),e.target.y()];
                }

                let p = e_.position === 0 ? d_edges_1[k] : d_edges_2[k];

                if (e_.value === 0) {
                     // e_.arrowType = ARROW_TYPE_SIMILARITY; // i think this was a fix; why else would I desire this?
                     continue;
                }

                p.value = e_.value;
                let tag = k_.substring(0,1);
                p.from = tag;
                let arr = arrows[e_.arrowType],
                    n = nodes[tag],
                    opos = n_ss.indexOf(k_.substring(1,2));
                opos = opos > n_ss.indexOf(tag) ? opos-1 : opos;
                n.miniElems[opos].marker('end',arr);
                redrawPath(p,s,t,p.value);
            }
        } else {
            let es = {};

            for (let k in b_edges) {

                es[k] = {
                    value: 0.0,
                    position: 0,
                    arrowType: ARROW_TYPE_SIMILARITY
                }

                es[n_reverse(k)] = {
                    value: 0.0,
                    position: 0,
                    arrowType: ARROW_TYPE_SIMILARITY
                }
            }

            theme.meaning = es;
        }

        d_edges_2_.before(nodes_);
        d_edges_1_.before(d_edges_2_);

        this.highlightAspect = (pos)=> {
            nodes[n_ss[pos]].elem.fire('mousedown', {isActive:false});
        }

        this.selectAspect = (pos,lead)=> {
            let tag = n_ss[pos];

            if (sel1) toggleNode({node:nodes[sel1], offColor:whitePrimary});
            if (sel2) toggleNode({node:nodes[sel2], offColor:whitePrimary});

            if (lead) {
                let osel1 = sel1 === tag ? sel2 : sel1;
                sel1 = tag;
                toggleNode({
                    node:nodes[sel1],
                    onColor:primaryColor,
                    isPrimary:true
                });
                sel2 = osel1;
                if (sel2) {
                    toggleNode({
                        node:nodes[sel2],
                        onColor:secondaryColor
                    });
                }
            } else if (sel1 && sel2) {
                sel1 = sel1 === tag ? sel2 : sel1;
                toggleNode({node:nodes[sel1],onColor:primaryColor,isPrimary:true});
                sel2 = tag;
                toggleNode({
                    node:nodes[sel2],
                    onColor:secondaryColor
                });
            } else {
                sel1 = sel1 || tag;
                sel2 = sel1 === tag ? sel2 : tag;
                if (sel1) {
                    toggleNode({
                        node:nodes[sel1],
                        onColor:sel1 === tag ? secondaryColor : primaryColor,
                        isPrimary:sel1 !== tag
                    });
                }
                if (sel2) {
                    toggleNode({
                        node:nodes[sel2],onColor:sel2 === tag ? secondaryColor : primaryColor,
                        isPrimary:sel2 !== tag
                    });
                }
            }
        }

        this.setMeaningValue = (spos,epos,v,silent)=> {
            if (spos === epos) return;
            if ((!sel1 || !sel2) && (spos == undefined || epos == undefined)) return;
            if (sel1) toggleNode({node:nodes[sel1],offColor:whitePrimary});
            if (sel2) toggleNode({node:nodes[sel2],offColor:whitePrimary});
            sel1 = typeof spos === 'string' ? spos : n_ss[spos] || sel1;
            sel2 = typeof epos === 'string' ? epos : n_ss[epos] || sel2;
            let ping = false;
            nodes[sel1].elem.fire('mousedown', {isActive:false, shouldPing:false, shouldHighlight:!silent});
            if (spos == undefined || epos == undefined) {
                v = Math.min(1, Math.max(0, theme.meaning[sel1+sel2].value+v));
                ping = true;
            }
            v = v.toFixedNumber(NUM_DECIMAL_PLACES);
            toggleNode({node:nodes[sel1], onColor:primaryColor, isPrimary:true});
            toggleNode({
                node:nodes[sel2],
                onColor:secondaryColor
            });
            nodes[sel1].isActive = true;
            updateMeaning(null,null,v,ping);
            nodes[sel1].isActive = false;
            if (commit && theme.onMeaningCommit) {
                theme.onMeaningCommit(commit);
                commit = null;
            }
        }

        this.updateMeaning = (meaning)=> {
            let v, a, b, ab, ba;
            for (let adx = 0; adx < 6; ++ adx) {
                a = n_ss[adx];
                for (let bdx = adx+1; bdx < 6; ++bdx) {
                    b = n_ss[bdx];
                    ab = a+b;
                    ba = b+a;
                    v = meaning[ab].value.toFixedNumber(NUM_DECIMAL_PLACES);
                    this.setMeaningValue(adx,bdx,v,true);
                    v = meaning[ba].value.toFixedNumber(NUM_DECIMAL_PLACES);
                    this.setMeaningValue(bdx,adx,v,true);
                }
            }

            // TODO: maybe set back the previously active nodes
            toggleNode({node:nodes[sel1], offColor:whitePrimary});
            toggleNode({node:nodes[sel2], offColor:whitePrimary});
        }

        this.shellName = (str)=> {
            if (shell_label) {
                if(str) {
                    drawTitle(draw,shell_label,str,null,null,MEMNAT_WIDTH);
                    return undefined;
                } else {
                    return shell_label.text();
                }
            }

            return undefined;
        }

        this.setPosition = (n)=> { if (position_label) position_label.text(n); }

        this.setArrow = (spos,epos,arr_t,ping)=> {
            let arr = arrows[arr_t],
                tag = n_ss[spos],
                n = nodes[tag];
            theme.meaning[tag+n_ss[epos]].arrowType = arr_t;
            epos = epos > spos ? epos-1 : epos;
            n.miniElems[epos].marker('end',arr || null);
            if (ping && this.onArrowSet) this.onArrowSet(spos,epos,arr_t);
        }

        this.onArrowSet = props.onArrowSet;

        this.setFarLeftCallback = f => {
            far_left_label.off('click');
            far_left_f = f;
            far_left_label.click(() => far_left_f(theme.id));
        }
        this.setLeftCallback = f => {
            left_label.off('click');
            left_f = f;
            left_label.click(() => left_f(theme.id));
        }
        this.setRightCallback = f => {
            right_label.off('click');
            right_f = f;
            right_label.click(() => right_f(theme.id));
        }
        this.setFarRightCallback = f => {
            far_right_label.off('click');
            far_right_f = f;
            far_right_label.click(() => far_right_f(theme.id));
        }
        this.setDeleteCallback = f => {
            delete_label.off('dblclick');
            delete_f = f;
            delete_label.dblclick(() => delete_f(theme.id,container));
        }
        this.setDiversityChangeCallback = f => { diversityCallback = f; }

        this.valueForMeaning = (spos,epos)=> {
            return theme.meaningOf(spos,epos).value;
        }

        this.clearSelection = ()=> {
            clearSelection(nodes,infoSel,sel1,sel2,blackPrimary,whitePrimary);
            infoSel = sel1 = sel2 = null;
        }
    }

    this.Signal = function(props) {
        let theme = props.theme,
            asps = props.aspects,
            container = props.container,
            shell_name = props.shellName,
            theme_position = props.themePosition;

        this.id = container;

        let draw = SVG(container).size('100%','100%'),
            backButton = draw.rect(MEMNAT_WIDTH,MEMNAT_HEIGHT).addClass('memnat-back-button').fill('rgba(0,0,0,0)'),
            graph = draw.group(),
            nodes_ = draw.group(),
            b_edges_ = draw.group(),
            d_edges_ = draw.group(),
            markers_ = draw.group(),
            aspectLabels = draw.group(),
            title_label = null,
            shell_label = null,
            position_label = null,
            far_left_label = null,
            left_label = null,
            delete_label = null,
            right_label = null,
            far_right_label = null,
            far_left_f = null, left_f = null, delete_f = null, right_f = null, far_right_f = null,
            initX = 20,
            initY = 100,
            nodes = {
                a: {
                    coords: [initX,initY],
                    elem: null,
                    label: null,
                    color: n_colors[0],
                    tag: n_ss[0],
                    isActive: false,
                    isSelected: false,
                    aspect: asps[0]
                },
                b: {
                    coords: [initX+25,initY],
                    elem: null,
                    label: null,
                    color: n_colors[1],
                    tag: n_ss[1],
                    isActive: false,
                    isSelected: false,
                    aspect: asps[1]
                },
                c: {
                    coords: [initX+50,initY],
                    elem: null,
                    label: null,
                    color: n_colors[2],
                    tag: n_ss[2],
                    isActive: false,
                    isSelected: false,
                    aspect: asps[2]
                },
                d: {
                    coords: [initX+75,initY],
                    elem: null,
                    label: null,
                    color: n_colors[3],
                    tag: n_ss[3],
                    isActive: false,
                    isSelected: false,
                    aspect: asps[3]
                },
                e: {
                    coords: [initX+100,initY],
                    elem: null,
                    label: null,
                    color: n_colors[4],
                    tag: n_ss[4],
                    isActive: false,
                    isSelected: false,
                    aspect: asps[4]
                },
                f: {
                    coords: [initX+125,initY],
                    elem: null,
                    label: null,
                    color: n_colors[5],
                    tag: n_ss[5],
                    isActive: false,
                    isSelected: false,
                    aspect: asps[5]
                }
            },
            d_edges = {},
            sel = null,
            infoSel = null,
            blackPrimary = "#404040",
            whitePrimary = '#f5f5f5',
            screenX = screenY = 0,
            max_dy = 40,
            min_y = initY-max_dy,
            commit = null;

        draw.viewbox({x:0, y:0, width:MEMNAT_WIDTH, height:MEMNAT_HEIGHT});
        draw.attr('overflow','hidden');
        draw.attr('preserveAspectRatio', 'xMinYMin meet');
        draw.attr('width', null);
        draw.attr('height', null);

        graph.add(b_edges_);
        graph.add(d_edges_);
        graph.add(nodes_);

        let fixActive = ()=> {
                let n = nodes[sel];
                if (n) n.isActive = false;
            },
            getPathPoints = (p)=> {
                let k = p.tag,
                    e = b_edges[k],
                    s = [e.source.x(),e.source.y()],
                    t = [e.target.x(),e.target.y()],
                    tag1 = k.substring(0,1),
                    tag2 = k.substring(1,2),
                    points = {};
                points[tag1] = s;
                points[tag2] = t;

                return points;
            },
            updateMeaning = (e,pc,ping)=> {
                //- check if two nodes are selected && that one of them is active
                //- get the edge between them
                //- redraw the edge considering the angle between
                // the mouse position and the active node
                // use screenX & screenY globals

                let n = nodes[sel],
                    active = null;

                if (n && n.isActive) active = n.tag;
                if (active === null || sel === null || ((pc == undefined) && (!e || !e.ctrlKey))) return;

                let idx = n_ss.indexOf(sel),
                    left_tag = n_ss[idx-1],
                    right_tag = n_ss[idx+1],
                    left_path = left_tag ? d_edges[left_tag+sel] : null,
                    right_path = right_tag ? d_edges[sel+right_tag] : null;

                let sp = null;
                if (e) {
                    sp = draw.point(e.clientX, e.clientY);
                } else {
                    sp = {};
                    sp.y = initY-pc*max_dy;
                }
                let y = Math.min(initY, Math.max(min_y, sp.y));

                n.elem.y(y);
                if (left_path) left_path.update();
                if (right_path) right_path.update();

                let v = Math.abs(initY-y)/max_dy;
                v = v.toFixedNumber(NUM_DECIMAL_PLACES);
                theme.meaning[n.tag].value = v;

                commit = {pos: n_ss.indexOf(n.tag), key:n.tag, value:v}

                if ((ping == undefined || ping) && theme.onMeaningChange) theme.onMeaningChange(commit);
            },
            diversityCallback = null;

            draw.mousemove((e)=> {
                updateMeaning(e);
            });

            backButton.style({cursor:'pointer'});
            backButton.click(()=> { if (theme.onSelect) theme.onSelect(CLICKED_ON_CANVAS, container); });

            document.addEventListener('mouseup', fixActive);
            document.addEventListener('mouseleave', fixActive);
            const doCommit = ()=> {
                if (commit && theme.onMeaningCommit) {
                    theme.onMeaningCommit(commit);
                    commit = null;
                }
            }
            ['mouseup','mouseleave'].forEach(e => {
                document.addEventListener(e, doCommit);
            });

            title_label = drawTitle(draw,null,theme.name(),null,'tosoto-standard-link2',MEMNAT_WIDTH);
            if (shell_name) shell_label = drawTitle(draw,null,shell_name,null,'tosoto-standard-link2',MEMNAT_WIDTH,(l)=> { l.y(MEMNAT_HEIGHT-12) });
            if (theme_position) position_label = drawTitle(draw,null,theme_position+'',null,null,MEMNAT_WIDTH,(l)=> {l.x(10).y(MEMNAT_HEIGHT-12);});
            far_left_label = drawTitle(draw,null,'«',null,null,MEMNAT_WIDTH);
            left_label = drawTitle(draw,null,'‹',null,null,MEMNAT_WIDTH);
            delete_label = drawTitle(draw,null,'-',null,null,MEMNAT_WIDTH);
            right_label = drawTitle(draw,null,'›',null,null,MEMNAT_WIDTH);
            far_right_label = drawTitle(draw,null,'»',null,null,MEMNAT_WIDTH);
            [
                [far_left_label,10],
                [left_label,20],
                [delete_label,30],
                [right_label,40],
                [far_right_label,50]
            ].map(l_ => {
                l_[0].font({anchor:'start', size:12});
                l_[0].x(l_[1]).dy(-3);
                l_[0].addClass('direction-label');
            });

            title_label.click(()=> { if (theme.onSelect) theme.onSelect(CLICKED_ON_THEME_TITLE, container); });
            if (shell_label) shell_label.click(()=> { if (theme.onSelect) theme.onSelect(CLICKED_ON_SHELL_TITLE, container); });

            theme.name = (str)=> {
                if(str) {
                    theme.name_ = str;
                    title_label.text(str);
                    return undefined;
                } else {
                    return theme.name_;
                }
            }

            // Draw nodes
            for (let i = 0; i< n_ss.length; ++i) {
                let n_s = n_ss[i],
                    n = nodes[n_s],
                    c_ = nodes_.group().translate(n.coords[0], n.coords[1]),
                    c = c_.circle(10);
                c_.fill('#f5f5f5');
                c_.stroke({color:blackPrimary, width:1.25});
                c_.style('cursor','pointer');
                c_.addClass('node-'+n_ss[i]);
                n.elem = c_;

                let label = drawAspectLabel({
                    draw: draw,
                    value: n.aspect.abbr(),
                    x: c_.cx(),
                    y: c_.cy()+20,
                    type: n.aspect.type,
                    // font: {anchor:'end'}
                });

                label.rotate(-75);
                label.click(()=> {
                    c_.fire('mousedown', {isActive: false});
                    // if (n.aspect.onHighlight) n.aspect.onHighlight(theme.id, n_ss.indexOf(n.tag), theme.meaning[n.tag].value);
                });
                label.dblclick(()=> c_.fire('dblclick'));
                aspectLabels.add(label);
                n.label = label;

                n.aspect.abbr(theme.id, (str)=> {
                    if(str) {
                        drawAspectLabel({draw:draw,value:str,type:n.aspect.type,label:n.label});
                        return undefined;
                    } else {
                        let t = label.text()
                        return n.aspect.type === ASPECT_TYPE_PURE ? t.substring(0,t.length-1) : t;
                    }
                });

                c_.on('dblclick', (e)=> {
                    if (sel && (sel !== n_s)) toggleNode({node:nodes[sel],onColor:blackPrimary,offColor:whitePrimary});
                    toggleNode({node:nodes[n_s],onColor:blackPrimary,offColor:whitePrimary});
                    sel = null;
                    if (n.isSelected) sel = n_s;
                });

                c_.on('mousedown', (e)=> {
                    n.isActive = e.detail ? ((e.detail.isActive === undefined) ? true : e.detail.isActive) : true;
                    screenX = e.screenX;
                    screenY = e.screenY;

                    let shouldHighlight = e.detail ? ((e.detail.shouldHighlight === undefined) ? true : e.detail.shouldHighlight) : true;

                    if (shouldHighlight && infoSel !== n.tag) {
                        if (infoSel) nodes[infoSel].elem.stroke({color:blackPrimary});
                        infoSel = n.tag;
                    }
                    if (shouldHighlight && c_.attr('stroke') === blackPrimary) c_.stroke({color:n.color});

                    let shouldPing = e.detail ? ((e.detail.shouldPing === undefined) ? true : e.detail.shouldPing) : true;
                    if (n.aspect.onHighlight && shouldPing) n.aspect.onHighlight(theme.id, n_ss.indexOf(n.tag), {value: theme.meaning[n.tag].value} );
                });

                c_.on('mouseup', (e)=> {
                    n.isActive = false;
                });
            }

            // Draw graph lines

            // Draw top line
            let top_p = draw.path('M'+initX+' '+initY+' L'+nodes.f.coords[0]+' '+initY+' z');
            top_p.fill('none').dmove(5,5);
            top_p.stroke({
                color:'rgba(40,40,40,1)',
                width: 0.5,
                linecap: 'round',
                linejoin: 'round'
            });
            b_edges_.add(top_p);

            // Draw left line
            let left_p = draw.path('M'+initX+' '+initY+' L'+initX+' '+(initY-max_dy)+' z');
            left_p.fill('none').dmove(5,5);
            left_p.stroke({
                color:'rgba(40,40,40,1)',
                width: 0.5,
                linecap: 'round',
                linejoin: 'round'
            });
            b_edges_.add(left_p);

            // Draw middle line
            let middle_p = draw.path('M'+initX+' '+(initY-max_dy/2)+' L'+nodes.f.coords[0]+' '+(initY-max_dy/2)+' z');
            middle_p.fill('none').dmove(5,5);
            middle_p.stroke({
                color:'rgba(40,40,40,1)',
                width: 0.5,
                linecap: 'round',
                linejoin: 'round',
                'stroke-dasharray': 3
            });
            middle_p.style({'stroke-dasharray': 3})
            b_edges_.add(middle_p);

            // Draw bottom line
            let bottom_p = draw.path('M'+initX+' '+initY+' L'+nodes.f.coords[0]+' '+initY+' z');
            bottom_p.fill('none').dmove(5,5);
            bottom_p.stroke({
                color:'rgba(40,40,40,1)',
                width: 0.5,
                linecap: 'round',
                linejoin: 'round'
            });
            b_edges_.add(bottom_p);

            for (let i = 1; i < n_ss.length; ++i) {
                let a_s = n_ss[i-1],
                    b_s = n_ss[i],
                    a = nodes[a_s].elem,
                    b = nodes[b_s].elem,
                    ab = a.connectable({
                        container: d_edges_,
                        markers: markers_
                    }, b);
                ab.setLineColor(blackPrimary);
                ab.line.stroke({width: 0.5});
                // ab.line.style({'stroke-dasharray': 3});
                ab.line.back();

                d_edges[a_s+b_s] = ab;
            }

            if (!theme.meaning) {
                theme.meaning = {};

                for (let idx = 0; idx < n_ss.length; ++idx) {
                    let n_s = n_ss[idx],
                        n = nodes[n_s],
                        left_tag = n_ss[idx-1],
                        right_tag = n_ss[idx+1],
                        left_path = left_tag ? d_edges[left_tag+sel] : null,
                        right_path = right_tag ? d_edges[sel+right_tag] : null;

                    theme.meaning[n_s] = { arrowType: ARROW_TYPE_ACKNOWLEDGEMENT, value: 0.5 }; // halfway to allow detune in either direction
                    n.elem.y(initY-max_dy/2);
                    if (left_path) left_path.update();
                    if (right_path) right_path.update();
                }
            }
            for (let i = 0; i < n_ss.length; ++i) {
                sel =  n_ss[i];
                let n = nodes[sel];
                n.isActive = true;
                updateMeaning(null, theme.meaning[sel].value);
                n.isActive = false;
                sel = null;
            }

            this.shellName = (str)=> {
                if (shell_label) {
                    if(str) {
                        drawTitle(draw,shell_label,str,null,null,MEMNAT_WIDTH);
                        return undefined;
                    } else {
                        return shell_label.text();
                    }
                }

                return undefined;
            }

            this.setPosition = (n)=> { if (position_label) position_label.text(n); }

            this.highlightAspect = (pos)=> {
                nodes[n_ss[pos]].elem.fire('mousedown', {isActive:false});
            }

            this.selectAspect = (pos) => {
                if (sel) toggleNode({node:nodes[sel],offColor:whitePrimary});
                sel = n_ss[pos];
                toggleNode({node:nodes[sel],onColor:blackPrimary});
            }

            this.setMeaningValue = (pos,v,silent)=> {
                if (!sel && pos == undefined) return;
                if (sel) toggleNode({node:nodes[sel],onColor:blackPrimary,offColor:whitePrimary});
                sel = typeof pos === 'string' ? pos : n_ss[pos] || sel;
                let ping = false;
                if (pos == undefined) {
                    v = Math.min(1, Math.max(0, theme.meaning[sel].value+v));
                    ping = true;
                }
                v = v.toFixedNumber(NUM_DECIMAL_PLACES);
                let n = nodes[sel];
                n.elem.fire('mousedown', {isActive:false, shouldPing:false, shouldHighlight:!silent});
                toggleNode({node:n,onColor:blackPrimary,offColor:whitePrimary});
                n.isActive = true;
                updateMeaning(null,v,ping);
                n.isActive = false;
                if (commit && theme.onMeaningCommit) {
                    theme.onMeaningCommit(commit);
                    commit = null;
                }
            }

            this.updateMeaning = (meaning)=> {
                let v, a;
                for (let adx = 0; adx < 6; ++adx) {
                    a = n_ss[adx];
                    v = meaning[a].value.toFixedNumber(NUM_DECIMAL_PLACES);
                    this.setMeaningValue(adx,v,true);
                }

                toggleNode({node:nodes[sel], offColor:whitePrimary});
            }

            this.setFarLeftCallback = f => {
                far_left_label.off('click');
                far_left_f = f;
                far_left_label.click(() => far_left_f(theme.id));
            }
            this.setLeftCallback = f => {
                left_label.off('click');
                left_f = f;
                left_label.click(() => left_f(theme.id));
            }
            this.setRightCallback = f => {
                right_label.off('click');
                right_f = f;
                right_label.click(() => right_f(theme.id));
            }
            this.setFarRightCallback = f => {
                far_right_label.off('click');
                far_right_f = f;
                far_right_label.click(() => far_right_f(theme.id));
            }
            this.setDeleteCallback = f => {
                delete_label.off('dblclick');
                delete_f = f;
                delete_label.dblclick(() => delete_f(theme.id,container));
            }
            this.setDiversityChangeCallback = f => { diversityCallback = f; }

            this.valueForMeaning = (pos)=> {
                return theme.meaningOf(pos).value;
            }

            this.clearSelection = ()=> {
                clearSelection(nodes,infoSel,sel,null,blackPrimary,whitePrimary);
                infoSel = sel = null;
            }
    }

    this.Versus = function(props) {
        let theme = props.theme,
            asps = props.aspects,
            container = props.container,
            shell_name = props.shellName,
            theme_position = props.themePosition;

        this.id = container;

        let draw = SVG(container).size('100%','100%'),
            backButton = draw.rect(MEMNAT_WIDTH,MEMNAT_HEIGHT).addClass('memnat-back-button').fill('rgba(0,0,0,0)'),
            graph = draw.group(),
            nodes_ = draw.group(),
            b_edges_ = draw.group(),
            d_edges_1_ = draw.group(),
            d_edges_2_ = draw.group(),
            minis_ = draw.group(),
            markers_ = draw.group(),
            aspectLabels = draw.group(),
            title_label = null,
            shell_label = null,
            position_label = null,
            far_left_label = null,
            left_label = null,
            delete_label = null,
            right_label = null,
            far_right_label = null,
            far_left_f = null, left_f = null, delete_f = null, right_f = null, far_right_f = null,
            e_pos0_col = 'rgba(255,0,0,0.8)',
            e_pos1_col = 'rgba(0,0,255,0.45)',
            initX = 50,
            initY = 40,
            nodes = {
                // Left side nodes
                la: {
                    coords: [initX,initY],
                    elem: null,
                    label: null,
                    color: n_colors[0],
                    tag: vn_ss[0],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false,
                    miniElems: [],
                    aspect: asps[0]
                },
                lb: {
                    coords: [initX,initY+40],
                    elem: null,
                    label: null,
                    color: n_colors[1],
                    tag: vn_ss[1],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false,
                    miniElems: [],
                    aspect: asps[1]
                },
                lc: {
                    coords: [initX,initY+80],
                    elem: null,
                    label: null,
                    color: n_colors[2],
                    tag: vn_ss[2],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false,
                    miniElems: [],
                    aspect: asps[2]
                },

                // Right side nodes
                ra: {
                    coords: [initX+75,initY],
                    elem: null,
                    label: null,
                    color: n_colors[3],
                    tag: vn_ss[3],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false,
                    miniElems: [],
                    aspect: asps[3]
                },
                rb: {
                    coords: [initX+75,initY+40],
                    elem: null,
                    label: null,
                    color: n_colors[4],
                    tag: vn_ss[4],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false,
                    miniElems: [],
                    aspect: asps[4]
                },
                rc: {
                    coords: [initX+75,initY+80],
                    elem: null,
                    label: null,
                    color: n_colors[5],
                    tag: vn_ss[5],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false,
                    miniElems: [],
                    aspect: asps[5]
                }
            },
            b_edges = {},
            d_edges_1 = {},
            d_edges_2 = {},
            left_sel = null,
            right_sel = null,
            infoSel = null,
            blackPrimary = "#404040",
            whitePrimary = '#f5f5f5',
            primaryColor = blackPrimary,
            secondaryColor = '#797979',
            screenX = screenY = 0,
            commit = null;

        draw.viewbox({x:0, y:0, width:MEMNAT_WIDTH, height:MEMNAT_HEIGHT});
        draw.attr('preserveAspectRatio', 'xMinYMin meet');
        draw.attr('width', null);
        draw.attr('height', null);

        graph.add(nodes_);
        graph.add(b_edges_);
        graph.add(d_edges_1_);
        graph.add(d_edges_2_);

        let fixActive = ()=> {
                let n1 = nodes[left_sel],
                    n2 = nodes[right_sel];
                if (n1) n1.isActive = false;
                if (n2) n2.isActive = false;
            },
            getPathPoints = (p)=> {
                let k = p.tag,
                    e = b_edges[k],
                    s = [e.source.x(),e.source.y()],
                    t = [e.target.x(),e.target.y()],
                    tag1 = k.substring(0,2),
                    tag2 = k.substring(2,4),
                    points = {};
                points[tag1] = s;
                points[tag2] = t;

                return points;
            },
            nodeOutgoings = (tag)=> {
                let aoes = [],
                    oside = tag.substring(0,1) === 'l' ? 'r' : 'l',
                    abc = ['a','b','c'];
                for (let i = 0; i < abc.length; i++) {
                    let n_s = oside+abc[i],
                        e = theme.meaning[tag+n_s];
                    aoes.push({epos:vn_ss.indexOf(nodes[n_s].tag), epos_:i,value:e.value,arrowType:e.arrowType});
                }
                return aoes
            },
            getPathIncrease = (from, to, angle, mcs)=> {
                let mag_ft = calcMag([from.x,from.y],[to.x,to.y]),
                    mag_tm = calcMag([mcs.x,mcs.y],[to.x,to.y]),
                    pc_dist = 0;

                let angle_condition = false,
                    pos_condition = false;

                pc_dist = (mag_ft-mag_tm)/mag_ft;
                pc_dist = pc_dist > 1 ? 1 : (pc_dist < 0 ? 0 : pc_dist);

                let from_side = from.tag.substring(0,1),
                    from_id = from.tag.substring(1,2),
                    to_side = to.tag.substring(0,1),
                    to_id = to.tag.substring(1,2);

                angle_condition = from_side === 'l' ? angle < 90 : angle > 270 || angle < 90;

                switch (from_side) {
                    case 'l':
                        pos_condition = mcs.x >= to.x;
                        if (from_id === 'a' && to_id === 'c') {
                            pos_condition = mcs.y >= to.y;
                            break;
                        }
                        if (from_id === 'c' && to_id === 'a') {
                            pos_condition = mcs.y <= to.y;
                            break;
                        }
                        break;
                    case 'r':
                        pos_condition = mcs.x <= to.x;
                        if (from_id === 'a' && to_id === 'c') {
                            pos_condition = mcs.y >= to.y;
                            break;
                        }
                        if (from_id === 'c' && to_id === 'a') {
                            pos_condition = mcs.y <= to.y;
                            break;
                        }
                        break;
                }

                if (angle_condition && pos_condition) pc_dist = 1;

                return pc_dist;
            },
            updateMeaning = (scs_diff,mcs_,pc,ping,ctrl)=> {

                let ln = nodes[left_sel],
                    rn = nodes[right_sel],
                    active = null;

                if (ln && ln.isActive) active = left_sel;
                if (rn && rn.isActive) active = right_sel;

                if (active === null || left_sel === null || right_sel === null || ((pc == undefined) && !ctrl)) return;

                let k = left_sel+right_sel,
                    _k = active === left_sel ? left_sel+right_sel : right_sel+left_sel,
                    path1 = d_edges_1[k],
                    path2 = d_edges_2[k];

                let p1 = null,
                    p2 = null,
                    p2_sel = active === left_sel ? right_sel : left_sel,
                    points = null,
                    scs = [0,0],
                    mcs = mcs_ ? draw.point(mcs_[0],mcs_[1]) : null,
                    angle = 0,
                    r = Math.min(1, Math.max(0, pc)) || 0;
                if ((!path1.from && !path2.from) || (path1.from === active)) {
                    path1.from = active;
                    theme.meaning[_k].position = 0;
                    points = getPathPoints(path1);
                    // redraw path1
                    p1 = points[active];
                    p2 = points[p2_sel];
                    if (scs_diff) {
                        scs = [p1[0]+scs_diff[0], p1[1]+scs_diff[1]];
                        angle = rad2deg(calcAngle(p2,scs,p1));
                        r = getPathIncrease({
                            x: p1[0],
                            y: p1[1],
                            tag: active
                        },{
                            x: p2[0],
                            y: p2[1],
                            tag: p2_sel
                        }, angle, mcs);
                    }
                    r = r.toFixedNumber(NUM_DECIMAL_PLACES);
                    theme.meaning[_k].value = r;
                    path1.value = r;
                    if (r === 0) path1.from = null; // allow position to be reclaimed
                    redrawPath(path1,p1,p2,r);
                } else if (!path2.from || (path2.from === active)) {
                    path2.from = active;
                    theme.meaning[_k].position = 1;
                    points = getPathPoints(path2);
                    // redraw path2
                    p1 = points[active];
                    p2 = active === left_sel ? points[right_sel] : points[left_sel];
                    if (scs_diff) {
                        scs = [p1[0]+scs_diff[0], p1[1]+scs_diff[1]];
                        angle = rad2deg(calcAngle(p2,scs,p1));
                        r = getPathIncrease({
                            x: p1[0],
                            y: p1[1],
                            tag: active
                        },{
                            x: p2[0],
                            y: p2[1],
                            tag: p2_sel
                        }, angle, mcs);
                    }
                    r = r.toFixedNumber(NUM_DECIMAL_PLACES);
                    theme.meaning[_k].value = r;
                    path2.value = r;
                    if (r === 0) path2.from = null; // allow position to be reclaimed
                    redrawPath(path2,p1,p2,r);
                }

                commit = {
                    spos: vn_ss.indexOf(_k.substring(0,2)),
                    epos: vn_ss.indexOf(_k.substring(2,4))%3,
                    epos_: vn_ss.indexOf(_k.substring(2,4))%3,
                    key: _k,
                    value: theme.meaning[_k].value
                }

                if ((scs_diff || ping) && theme.onMeaningChange) theme.onMeaningChange(commit);
            },
            diversityCallback = null;

        draw.mousemove((e)=> {
            updateMeaning([e.screenX-screenX,e.screenY-screenY], [e.clientX,e.clientY], null, null, e.ctrlKey);
        });

        backButton.style({cursor:'pointer'});
        backButton.click(()=> { if (theme.onSelect) theme.onSelect(CLICKED_ON_CANVAS, container); });

        document.addEventListener('mouseup', fixActive);
        document.addEventListener('mouseleave', fixActive);
        const doCommit = ()=> {
            if (commit && theme.onMeaningCommit) {
                theme.onMeaningCommit(commit);
                commit = null;
            }
        }
        ['mouseup','mouseleave'].forEach(e => {
            document.addEventListener(e, doCommit);
        });

        title_label = drawTitle(draw,null,theme.name(),null,'tosoto-standard-link2',MEMNAT_WIDTH);
        if (shell_name) shell_label = drawTitle(draw,null,shell_name,null,'tosoto-standard-link2',MEMNAT_WIDTH,(l)=> { l.y(MEMNAT_HEIGHT-12) });
        if (theme_position) position_label = drawTitle(draw,null,theme_position+'',null,null,MEMNAT_WIDTH,(l)=> {l.x(10).y(MEMNAT_HEIGHT-12);});
        far_left_label = drawTitle(draw,null,'«',null,null,MEMNAT_WIDTH);
        left_label = drawTitle(draw,null,'‹',null,null,MEMNAT_WIDTH);
        delete_label = drawTitle(draw,null,'-',null,null,MEMNAT_WIDTH);
        right_label = drawTitle(draw,null,'›',null,null,MEMNAT_WIDTH);
        far_right_label = drawTitle(draw,null,'»',null,null,MEMNAT_WIDTH);
        [
            [far_left_label,10],
            [left_label,20],
            [delete_label,30],
            [right_label,40],
            [far_right_label,50]
        ].map(l_ => {
            l_[0].font({anchor:'start', size:12});
            l_[0].x(l_[1]).dy(-3);
            l_[0].addClass('direction-label');
        });

        title_label.click(()=> { if (theme.onSelect) theme.onSelect(CLICKED_ON_THEME_TITLE, container); });
        if (shell_label) shell_label.click(()=> { if (theme.onSelect) theme.onSelect(CLICKED_ON_SHELL_TITLE, container); });

        theme.name = (str)=> {
            if(str) {
                theme.name_ = str;
                title_label.text(str);
                return undefined;
            } else {
                return theme.name_;
            }
        }

        // Draw nodes
        for (let i = 0; i < vn_ss.length; ++i) {
            let n_s = vn_ss[i],
                n = nodes[n_s],
                c_ = nodes_.group().translate(n.coords[0], n.coords[1]),
                c = c_.circle(10);
            c_.fill('#f5f5f5');
            c_.stroke({color:blackPrimary, width:1.25});
            c_.style('cursor','pointer');
            c_.addClass('node-'+vn_ss[i]);
            n.elem = c_;

            let lprops = {
                    draw: draw,
                    value: n.aspect.abbr(),
                    x: c_.cx(),
                    y: c_.cy(),
                    type: n.aspect.type,
                    font: {anchor:'end'}
                },
                dx = 0,
                dy = 0,
                anchor = 'middle';

            switch (n.tag) {
                case 'la':
                    dx = -10;
                    anchor = 'end';
                    break;
                case 'lb':
                    dx = -10;
                    anchor = 'end';
                    break;
                case 'lc':
                    dx = -10;
                    anchor = 'end';
                    break;
                case 'ra':
                    dx = 10;
                    anchor = 'start';
                    break;
                case 'rb':
                    dx = 10;
                    anchor = 'start';
                    break;
                case 'rc':
                    dx = 10;
                    anchor = 'start';
                    break;
                default:
            }
            lprops.x+=dx;
            lprops.y+=dy;
            lprops.font.anchor = anchor;
            let label = drawAspectLabel(lprops);
            n.label = label;
            label.click(()=> {
                c_.fire('mousedown', {isActive: false});
                // if (n.aspect.onHighlight) n.aspect.onHighlight(theme.id, vn_ss.indexOf(n.tag), nodeOutgoings(n.tag));
            });
            label.dblclick(()=> c_.fire('dblclick'));
            aspectLabels.add(label);

            n.aspect.abbr(theme.id, (str)=> {
                if(str) {
                    drawAspectLabel({draw:draw,value:str,type:n.aspect.type,label:n.label});
                    return undefined;
                } else {
                    let t = label.text()
                    return n.aspect.type === ASPECT_TYPE_PURE ? t.substring(0,t.length-1) : t;
                }
            });

            c_.on('dblclick', (e)=> {
                // if side node and side node was lead transfer lead to other side
                    // otherwise keep secondary
                let side = n.tag.substring(0,1),
                    side_sel = side === 'l' ? left_sel : right_sel,
                    oside_sel = side === 'l' ? right_sel : left_sel;
                let color = primaryColor,
                    ocolor = secondaryColor;
                if ((side_sel && nodes[side_sel].elem.fill() === primaryColor) || oside_sel) {
                    color = secondaryColor;
                    ocolor = primaryColor;
                }
                toggleNode({
                    node: n,
                    onColor: color,
                    offColor: whitePrimary,
                    isPrimary: color === primaryColor
                });
                if (n.isSelected) {
                    if (side === 'l') {
                        if (left_sel) toggleNode({node:nodes[left_sel],offColor:whitePrimary});
                        left_sel = n.tag;
                        if (right_sel) toggleNode({node:nodes[right_sel],onColor:ocolor,ensure:true,isPrimary:ocolor === primaryColor});
                    } else {
                        if (right_sel) toggleNode({node:nodes[right_sel],offColor:whitePrimary});
                        right_sel = n.tag;
                        if (left_sel) toggleNode({node:nodes[left_sel],onColor:ocolor,ensure:true,isPrimary:ocolor === primaryColor});
                    }
                } else {
                    if (side === 'l') {
                        left_sel = null;
                        if (right_sel) toggleNode({node:nodes[right_sel],onColor:primaryColor,ensure:true,isPrimary:true});
                    } else {
                        right_sel = null;
                        if (left_sel) toggleNode({node:nodes[left_sel],onColor:primaryColor,ensure:true,isPrimary:true});
                    }
                }
            });

            c_.on('mousedown', (e)=> {
                n.isActive = e.detail ? ((e.detail.isActive === undefined) ? true : e.detail.isActive) : true;
                screenX = e.screenX;
                screenY = e.screenY;

                let shouldHighlight = e.detail ? ((e.detail.shouldHighlight === undefined) ? true : e.detail.shouldHighlight) : true;

                if (shouldHighlight && infoSel !== n.tag) {
                    if (infoSel) nodes[infoSel].elem.stroke({color:blackPrimary});
                    infoSel = n.tag;
                }
                if (shouldHighlight && c_.attr('stroke') === blackPrimary) c_.stroke({color:n.color});

                let shouldPing = e.detail ? ((e.detail.shouldPing === undefined) ? true : e.detail.shouldPing) : true;
                if (n.aspect.onHighlight && shouldPing) n.aspect.onHighlight(theme.id, vn_ss.indexOf(n.tag), nodeOutgoings(n.tag));
            });

            c_.on('mouseup', (e)=> {
                n.isActive = false;
            });
        }

        // Draw sketh lines
        for (let i = 0; i < 3; ++i) {
            let a_s = vn_ss[i];
            for (let j = 3; j < 6; ++j) {
                let b_s = vn_ss[j],
                    a = nodes[a_s].elem,
                    b = nodes[b_s].elem,
                    ab = a.connectable({
                            container: b_edges_,
                            markers: markers_
                        }, b);
                ab.setLineColor(blackPrimary);
                ab.line.stroke({width: 0.5});
                ab.line.style({'stroke-dasharray': 3});
                ab.line.back();

                b_edges[a_s+b_s] = ab;
            }
        }

        b_edges_.backward();

        let arrows = drawArrows({
            width: 15,
            height: 15,
            draw: draw
        });

        // Draw minis
        for (let k in nodes) {
            let n = nodes[k],
                kpos = vn_ss.indexOf(k),
                c_ = n.elem,
                s = [c_.x(),c_.y()],
                minis_lines = draw.group(),
                side = n.tag.substring(0,1);

            for (let j in nodes) {
                if (j.substring(0,1) === side) continue;
                let nt = nodes[j],
                    // len = 9,
                    jpos = vn_ss.indexOf(j),
                    len = Math.abs(jpos-kpos)%2 === 0 ? 9 : 7,
                    pc = len/calcMag(s,[nt.elem.x(),nt.elem.y()]),
                    coords = [nt.elem.x(),nt.elem.y()];
                if (k.substring(1,2) === 'a' && j.substring(1,2) === 'c') {
                    coords[1]+=15;
                } else if (k.substring(1,2) === 'c' && j.substring(1,2) === 'a') {
                    coords[1]-=15;
                }
                let t = lerp(s,coords,[pc,pc]),
                    p = draw.path('M'+s[0]+' '+s[1]+' L'+t[0]+' '+t[1]+' z');
                p.fill('none').dmove(5,5);
                p.stroke({
                    color:'rgba(40,40,40,1)',
                    width: 0.5,
                    linecap: 'round',
                    linejoin: 'round'
                });
                n.miniElems.push(p);
                minis_lines.add(p);
            }
            switch (k) {
                case 'la':
                    minis_lines.dx(-20).dy(-15);
                    break;
                case 'lb':
                    minis_lines.dx(-20).dy(-10);
                    break;
                case 'lc':
                    minis_lines.dx(-20).dy(20);
                    break;
                case 'ra':
                    minis_lines.dx(20).dy(-15);
                    break;
                case 'rb':
                    minis_lines.dx(20).dy(-10);
                    break;
                case 'rc':
                    minis_lines.dx(20).dy(20);
                    break;
                default:

            }
            minis_.add(minis_lines);
        }

        // Draw value edges
        for (let k in b_edges) {
            // bottom edge
            let e = b_edges[k],
                s = [e.source.x(),e.source.y()],
                t = [e.target.x(),e.target.y()],
                p = draw.path('M'+s[0]+' '+s[1]+' L'+s[0]+' '+s[1]+' z');
            p.fill('none').dmove(5,5);
            p.stroke({
                color: e_pos0_col,
                width: 1.25,
                linecap: 'round',
                linejoin: 'round'
            });

            p.tag = k;
            p.value = 0.0;

            d_edges_1[k] = p;
            d_edges_1_.add(p);

            // top edge
            t = [e.source.x(),e.source.y()],
            s = [e.target.x(),e.target.y()],
            p = draw.path('M'+s[0]+' '+s[1]+' L'+s[0]+' '+s[1]+' z');
            p.fill('none').dmove(5,5);
            p.stroke({
                color: e_pos1_col,
                width: 2,
                linecap: 'round',
                linejoin: 'round'
            });

            p.tag = k;
            p.value = 0.0;

            d_edges_2[k] = p;
            d_edges_2_.add(p);
        }

        if (theme.meaning) {
            for (let k_ in theme.meaning) {
                let k = k_,
                    e_ = theme.meaning[k],
                    e = b_edges[k],
                    s = null,
                    t = null;
                if (e) {
                    s = [e.source.x(),e.source.y()];
                    t = [e.target.x(),e.target.y()];
                } else {
                    k = k.substring(2,4)+k.substring(0,2);
                    e = b_edges[k];
                    t = [e.source.x(),e.source.y()];
                    s = [e.target.x(),e.target.y()];
                }

                if (k.substring(0,1) === k.substring(2,3)) {
                    console.warn('Theme loading: Ignoring edge between nodes on the same side.');
                    continue;
                }

                if (e_.value === 0) continue;

                let p = e_.position === 0 ? d_edges_1[k] : d_edges_2[k];

                p.value = e_.value;
                p.from = k_.substring(0,2);
                // TODO: draw arrows in mini displays using e_.arrowType
                redrawPath(p,s,t,p.value);
            }
        } else {
            let es = {};

            for (let k in b_edges) {

                es[k] = {
                    value: 0.0,
                    position: 0,
                    arrowType: ARROW_TYPE_SIMILARITY
                }

                es[k.substring(2,4)+k.substring(0,2)] = {
                    value: 0.0,
                    position: 0,
                    arrowType: ARROW_TYPE_SIMILARITY
                }
            }

            theme.meaning = es;
        }

        d_edges_2_.before(nodes_);
        d_edges_1_.before(d_edges_2_);

        this.highlightAspect = (pos)=> {
            nodes[vn_ss[pos]].elem.fire('mousedown', {isActive:false});
        }

        this.selectAspect = (pos,lead)=> {
            let tag = vn_ss[pos],
                sel2 = tag.substring(0,1) === 'l' ? right_sel : left_sel;

            if (left_sel) toggleNode({node:nodes[left_sel],offColor:whitePrimary});
            if (right_sel) toggleNode({node:nodes[right_sel],offColor:whitePrimary});

            if (lead) {
                toggleNode({node:nodes[tag],onColor:primaryColor,isPrimary:true});
                if (sel2) toggleNode({node:nodes[sel2],onColor:secondaryColor});
            } else {
                if (sel2) toggleNode({node:nodes[sel2],onColor:primaryColor,isPrimary:true});
                toggleNode({node:nodes[tag],onColor:secondaryColor});
            }
            left_sel = tag.substring(0,1) === 'l' ? tag : sel2;
            right_sel = tag.substring(0,1) === 'r' ? tag : sel2;
        }

        this.setMeaningValue = (spos,epos,v,silent)=> {
            if ((spos < 3 && epos < 3) || (spos > 3 && epos > 3) || spos === epos) return;
            if ((!left_sel || !right_sel) && (spos == undefined || epos == undefined)) return;
            let sel1 = typeof spos === 'string' ? spos : vn_ss[spos],
                sel2 = typeof epos === 'string' ? epos : vn_ss[epos],
                ping = false;
            if (sel1 != undefined && sel2 != undefined) {
                if (sel1.substring(0,1) === sel2.substring(0,1)) return;
                if (left_sel) toggleNode({node:nodes[left_sel],offColor:whitePrimary});
                if (right_sel) toggleNode({node:nodes[right_sel],offColor:whitePrimary});
                left_sel = sel1.substring(0,1) === 'l' ? sel1 : sel2;
                right_sel = left_sel === sel1 ? sel2 : sel1;
            } else if (!left_sel || !right_sel) {
                return;
            } else {
                sel1 = nodes[left_sel].isPrimary ? left_sel : right_sel;
                sel2 = sel1 === right_sel ? left_sel : right_sel;
                toggleNode({node:nodes[sel1],offColor:whitePrimary});
                toggleNode({node:nodes[sel2],offColor:whitePrimary});
                v = Math.min(1, Math.max(0, theme.meaning[sel1+sel2].value+v));
                ping = true;
            }
            v = v.toFixedNumber(NUM_DECIMAL_PLACES);
            nodes[sel1].elem.fire('mousedown', {isActive:false, shouldPing:false, shouldHighlight:!silent});
            // This is buggy, because events happen out of sync.
            // nodes[sel1].elem.fire('dblclick');
            // nodes[sel2].elem.fire('dblclick');
            toggleNode({node:nodes[sel1],onColor:primaryColor,isPrimary:true});
            toggleNode({node:nodes[sel2],onColor:secondaryColor});
            nodes[sel1].isActive = true;
            updateMeaning(null,null,v,ping);
            nodes[sel1].isActive = false;
            if (commit && theme.onMeaningCommit) {
                theme.onMeaningCommit(commit);
                commit = null;
            }
        }

        this.updateMeaning = (meaning)=> {
            let v, a, b, ab, ba;
            for (let adx = 0; adx < 3; ++ adx) {
                a = vn_ss[adx];
                for (let bdx = 3; bdx < 6; ++bdx) {
                    b = vn_ss[bdx];
                    ab = a+b;
                    ba = b+a;
                    v = meaning[ab].value.toFixedNumber(NUM_DECIMAL_PLACES);
                    this.setMeaningValue(adx,bdx,v,true);
                    v = meaning[ba].value.toFixedNumber(NUM_DECIMAL_PLACES);
                    this.setMeaningValue(bdx,adx,v,true);
                }
            }

            toggleNode({node:nodes[sel1], offColor:whitePrimary});
            toggleNode({node:nodes[sel2], offColor:whitePrimary});
        }

        this.shellName = (str)=> {
            if (shell_label) {
                if(str) {
                    drawTitle(draw,shell_label,str,null,null,MEMNAT_WIDTH);
                    return undefined;
                } else {
                    return shell_label.text();
                }
            }

            return undefined;
        }

        this.setPosition = (n)=> { if (position_label) position_label.text(n); }

        this.onArrowSet = props.onArrowSet;

        this.setFarLeftCallback = f => {
            far_left_label.off('click');
            far_left_f = f;
            far_left_label.click(() => far_left_f(theme.id));
        }
        this.setLeftCallback = f => {
            left_label.off('click');
            left_f = f;
            left_label.click(() => left_f(theme.id));
        }
        this.setRightCallback = f => {
            right_label.off('click');
            right_f = f;
            right_label.click(() => right_f(theme.id));
        }
        this.setFarRightCallback = f => {
            far_right_label.off('click');
            far_right_f = f;
            far_right_label.click(() => far_right_f(theme.id));
        }
        this.setDeleteCallback = f => {
            delete_label.off('dblclick');
            delete_f = f;
            delete_label.dblclick(() => delete_f(theme.id,container));
        }
        this.setDiversityChangeCallback = f => { diversityCallback = f; }

        this.valueForMeaning = (spos,epos)=> {
            return theme.meaningOf(spos,epos).value;
        }

        this.clearSelection = ()=> {
            clearSelection(nodes,infoSel,left_sel,right_sel,blackPrimary,whitePrimary);
            infoSel = left_sel = right_sel = null;
        }
    }

    this.ModQuad = function(props) {
        let theme = props.theme,
            container = props.container;

        this.id = container;

        // const MFBJ_asps = [
        //     new Memnat.Aspect({
        //         id: 'mother',
        //         name: 'Mother',
        //         abbr: 'M',
        //         type: ASPECT_TYPE_PURE
        //     }),
        //     new Memnat.Aspect({
        //         id: 'father',
        //         name: 'Father',
        //         abbr: 'F',
        //         type: ASPECT_TYPE_PURE
        //     }),
        //     new Memnat.Aspect({
        //         id: 'belief',
        //         name: 'Belief',
        //         abbr: 'B',
        //         type: ASPECT_TYPE_PURE
        //     }),
        //     new Memnat.Aspect({
        //         id: 'justice',
        //         name: 'Justice',
        //         abbr: 'J',
        //         type: ASPECT_TYPE_PURE
        //     })
        // ];

        let draw = SVG(container).size('100%','100%'),
            backButton = draw.rect(MEMNAT_WIDTH,MEMNAT_HEIGHT).addClass('memnat-back-button').fill('rgba(0,0,0,0)'),
            graph = draw.group(),
            nodes_ = draw.group(),
            b_edges_ = draw.group(),
            markers_ = draw.group(),
            d_edges_1_ = draw.group(),
            d_edges_2_ = draw.group(),
            aspectLabels = draw.group(),
            e_pos0_col = 'rgba(255,0,0,0.8)',
            e_pos1_col = 'rgba(0,0,255,0.45)',
            initX = 85,
            initY = 36,
            nodes = {
                m: {
                    coords: [initX,initY],
                    elem: null,
                    label: null,
                    color: n_colors[0],
                    tag: mfbj_ss[0],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false
                },
                f: {
                    coords: [initX,initY+99],
                    elem: null,
                    label: null,
                    color: n_colors[1],
                    tag: mfbj_ss[1],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false
                },
                b: {
                    coords: [initX-50,initY+48],
                    elem: null,
                    label: null,
                    color: n_colors[2],
                    tag: mfbj_ss[2],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false
                },
                j: {
                    coords: [initX+50,initY+48],
                    elem: null,
                    label: null,
                    color: n_colors[3],
                    tag: mfbj_ss[3],
                    isActive: false,
                    isSelected: false,
                    isPrimary: false
                }
            },
            b_edges = {},
            d_edges_1 = {},
            d_edges_2 = {},
            sel1 = null,
            sel2 = null,
            infoSel = null,
            blackPrimary = "#404040",
            whitePrimary = '#f5f5f5',
            primaryColor = blackPrimary,
            secondaryColor = '#797979',
            screenX = screenY = 0,
            commit = null;

        draw.viewbox({x:0, y:0, width:MEMNAT_WIDTH, height:MEMNAT_HEIGHT});
        draw.attr('overflow','hidden');
        draw.attr('preserveAspectRatio', 'xMinYMin meet');
        draw.attr('width', null);
        draw.attr('height', null);

        graph.add(nodes_);
        graph.add(b_edges_);
        graph.add(d_edges_1_);
        graph.add(d_edges_2_);

        let fixActive = ()=> {
                let n1 = nodes[sel1],
                    n2 = nodes[sel2];
                if (n1) n1.isActive = false;
                if (n2) n2.isActive = false;
            },
            getPathPoints = (p)=> {
                let k = p.tag,
                    e = b_edges[k],
                    s = [e.source.x(),e.source.y()],
                    t = [e.target.x(),e.target.y()],
                    tag1 = k.substring(0,1),
                    tag2 = k.substring(1,2),
                    points = {};
                points[tag1] = s;
                points[tag2] = t;

                return points;
            },
            nodeOutgoings = (tag)=> {
                let aoes = [];
                for (let i = 0; i < mfbj_ss.length; i++) {
                    let n_s = mfbj_ss[i];
                    if (n_s == tag) continue;
                    let e = theme.meaning[tag+n_s];
                    aoes.push({epos:mfbj_ss.indexOf(nodes[n_s].tag),value:e.value,arrowType:e.arrowType});
                }
                return aoes
            },
            getPathIncrease = (from, to, angle, mcs)=> {
                let mag_ft = calcMag([from.x,from.y],[to.x,to.y]),
                    mag_tm = calcMag([mcs.x,mcs.y],[to.x,to.y]),
                    pc_dist = 0;

                let angle_condition = false,
                    pos_condition = false;

                pc_dist = (mag_ft-mag_tm)/mag_ft;
                pc_dist = pc_dist > 1 ? 1 : (pc_dist < 0 ? 0 : pc_dist);

                switch (from.tag) {
                    case 'm':
                        switch (to.tag) {
                            case 'f':
                                angle_condition = angle < 90;
                                pos_condition = mcs.y >= to.y;
                                break;
                            case 'b':
                                angle_condition = angle > 270 || angle < 90;
                                pos_condition = mcs.x <= to.x && mcs.y >= to.y;
                                break;
                            case 'j':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 330);
                                pos_condition = mcs.x >= to.x && mcs.y >= to.y;
                                break;
                        }
                        break;
                    case 'f':
                        switch (to.tag) {
                            case 'm':
                                angle_condition = angle < 90;
                                pos_condition = mcs.y <= to.y;
                                break;
                            case 'b':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 330);
                                pos_condition = mcs.x <= to.x && mcs.y >= to.y;
                                break;
                            case 'j':
                                angle_condition = angle < 90;
                                pos_condition = mcs.x >= to.x && mcs.y >= to.y;
                                break;
                        }
                        break;
                    case 'b':
                        switch (to.tag) {
                            case 'm':
                                angle_condition = angle < 90;
                                pos_condition = mcs.x >= to.x && mcs.y <= to.y;
                                break;
                            case 'f':
                                angle_condition = angle < 90;
                                pos_condition = mcs.x >= to.x && mcs.y >= to.y;
                                break;
                            case 'j':
                                angle_condition = angle < 90;
                                pos_condition = mcs.x >= to.x;
                                break;
                        }
                        break;
                    case 'j':
                        switch (to.tag) {
                            case 'm':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 330);
                                pos_condition = mcs.x <= to.x && mcs.y <= to.y;
                                break;
                            case 'f':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 330);
                                pos_condition = mcs.x <= to.x && mcs.y >= to.y;
                                break;
                            case 'b':
                                angle_condition = (angle >= 0 && angle < 90) || (angle > 270 && angle <= 360);
                                pos_condition = mcs.x <= to.x;
                                break;
                        }
                        break;
                }

                if (angle_condition && pos_condition) pc_dist = 1;

                return pc_dist;
            },
            updateMeaning = (scs_diff,mcs_,pc,ping,ctrl)=> {
                //- check if two nodes are selected && that one of them is active
                //- get the edge between them
                //- redraw the edge considering the angle between
                // the mouse position and the active node
                // use screenX & screenY globals

                let n1 = nodes[sel1],
                    n2 = nodes[sel2],
                    active = null;

                if (n1 && n1.isActive) active = sel1;
                if (n2 && n2.isActive) active = sel2;

                if (active === null || sel1 === null || sel2 === null || ((pc == undefined) && !ctrl)) return;

                let k = mfbj_ss.indexOf(sel1) < mfbj_ss.indexOf(sel2) ? sel1+sel2 : sel2+sel1,
                    _k = active === sel1 ? sel1+sel2 : sel2+sel1,
                    path1 = d_edges_1[k],
                    path2 = d_edges_2[k];

                // change redrawPath so that I can decide beforehand which side it should start from
                // if path1.from && path2.from are null, set it and redraw path1
                // if path1.from is equal to the active, redraw it
                // if path2.from is equal to the active, redraw it

                // calculate how much of edge to draw
                // transform screen coords to svg coords
                let p1 = null,
                    p2 = null,
                    p2_sel = active === sel1 ? sel2 : sel1,
                    points = null,
                    scs = [0,0],
                    mcs = mcs_ ? draw.point(mcs_[0],mcs_[1]) : null,
                    angle = 0,
                    r = Math.min(1, Math.max(0, pc)) || 0;
                if ((!path1.from && !path2.from) || (path1.from === active)) {
                    path1.from = active;
                    theme.meaning[_k].position = 0;
                    points = getPathPoints(path1);
                    // redraw path1
                    p1 = points[active];
                    p2 = points[p2_sel];
                    if (scs_diff) {
                        scs = [p1[0]+scs_diff[0], p1[1]+scs_diff[1]];
                        angle = rad2deg(calcAngle(p2,scs,p1));
                        r = getPathIncrease({
                            x: p1[0],
                            y: p1[1],
                            tag: active
                        },{
                            x: p2[0],
                            y: p2[1],
                            tag: p2_sel
                        }, angle, mcs);
                    }
                    // r = Math.range(0, 1, (r+theme.meaning[_k].value).toFixedNumber(NUM_DECIMAL_PLACES));
                    r = r.toFixedNumber(NUM_DECIMAL_PLACES);
                    theme.meaning[_k].value = r;
                    path1.value = r;
                    if (r === 0) path1.from = null; // allow position to be reclaimed
                    redrawPath(path1,p1,p2,r);
                } else if (!path2.from || (path2.from === active)) {
                    path2.from = active;
                    theme.meaning[_k].position = 1;
                    points = getPathPoints(path2);
                    // redraw path2
                    p1 = points[active];
                    p2 = points[p2_sel];
                    if (scs_diff) {
                        scs = [p1[0]+scs_diff[0], p1[1]+scs_diff[1]];
                        angle = rad2deg(calcAngle(p2,scs,p1));
                        r = getPathIncrease({
                            x: p1[0],
                            y: p1[1],
                            tag: active
                        },{
                            x: p2[0],
                            y: p2[1],
                            tag: p2_sel
                        }, angle, mcs);
                    }
                    // r = Math.range(0, 1, (r+theme.meaning[_k].value).toFixedNumber(NUM_DECIMAL_PLACES));
                    r = r.toFixedNumber(NUM_DECIMAL_PLACES);
                    theme.meaning[_k].value = r;
                    path2.value = r;
                    if (r === 0) path2.from = null; // allow position to be reclaimed
                    redrawPath(path2,p1,p2,r);
                }
                commit = {
                    spos: mfbj_ss.indexOf(_k.substring(0,1)),
                    epos: mfbj_ss.indexOf(_k.substring(1,2)),
                    key: _k,
                    value: theme.meaning[_k].value
                }

                if ((scs_diff || ping) && theme.onMeaningChange) theme.onMeaningChange(commit);
            },
            diversityCallback = null;

        draw.mousemove((e)=> {
            updateMeaning([e.screenX-screenX,e.screenY-screenY], [e.clientX,e.clientY], null, null, e.ctrlKey);
        });

        backButton.style({cursor:'pointer'});
        backButton.click(()=> { if (theme.onSelect) theme.onSelect(CLICKED_ON_CANVAS, container); });

        document.addEventListener('mouseup', fixActive);
        document.addEventListener('mouseleave', fixActive);
        const doCommit = ()=> {
            if (commit && theme.onMeaningCommit) {
                theme.onMeaningCommit(commit);
                commit = null;
            }
        }
        ['mouseup','mouseleave'].forEach(e => {
            document.addEventListener(e, doCommit);
        });

        // Draw nodes
        for (let k in nodes) {
            let n = nodes[k],
                c_ = nodes_.group().translate(n.coords[0], n.coords[1]),
                c = c_.circle(10);
            c_.fill('#f5f5f5');
            c_.stroke({color:blackPrimary, width:1.25});
            c_.style('cursor','pointer');
            c_.addClass('node-'+k);
            n.elem = c_;

            let lprops = {
                    draw: draw,
                    value: n.tag.toUpperCase(),
                    x: c_.cx(),
                    y: c_.cy(),
                    type: ASPECT_TYPE_COMPOUND,
                    font: {}
                },
                dx = 0,
                dy = 0,
                anchor = 'middle';

            switch (n.tag) {
                case 'm':
                    dy = -17;
                    break;
                case 'f':
                    dy = 12;
                    break;
                case 'b':
                    anchor = 'end';
                    dx = -10;
                    dy = -3;
                    break;
                case 'j':
                    anchor = 'start';
                    dx = 10;
                    dy = -3;
                    break;
                default:
            }
            lprops.x+=dx;
            lprops.y+=dy;
            lprops.font.anchor = anchor;
            let label = drawAspectLabel(lprops);
            n.label = label;
            label.click(()=> {
                c_.fire('mousedown', {isActive: false});
                // if (this.onAspectHighlight) this.onAspectHighlight(mfbj_ss.indexOf(n.tag), nodeOutgoings(n.tag));
            });
            label.dblclick(()=> c_.fire('dblclick'));
            aspectLabels.add(label);

            // only two nodes can be selected at once
            c_.on('dblclick', (e)=> {
                let color = primaryColor,
                    ocolor = secondaryColor;
                if (sel1) {
                    color = secondaryColor;
                    ocolor = primaryColor;
                }
                toggleNode({
                    node: n,
                    onColor: color,
                    offColor: whitePrimary,
                    isPrimary: color === primaryColor
                });
                if (n.isSelected) {
                    if (sel1) {
                        if (sel2) toggleNode({node:nodes[sel2],offColor:whitePrimary});
                        sel2 = n.tag;
                    } else {
                        sel1 = n.tag
                    }
                } else {
                    if (sel1 === n.tag) {
                        sel1 = null;
                        if (sel2) {
                            sel1 = sel2;
                            toggleNode({node:nodes[sel1],onColor:primaryColor,ensure:true,isPrimary:true});
                        }
                    }
                    sel2 = null;
                }
            });

            c_.on('mousedown', (e)=> {
                n.isActive = e.detail ? ((e.detail.isActive === undefined) ? true : e.detail.isActive) : true;
                screenX = e.screenX;
                screenY = e.screenY;

                let shouldHighlight = e.detail ? ((e.detail.shouldHighlight === undefined) ? true : e.detail.shouldHighlight) : true;

                if (shouldHighlight && infoSel !== n.tag) {
                    if (infoSel) nodes[infoSel].elem.stroke({color:blackPrimary});
                    infoSel = n.tag;
                }
                if (shouldHighlight && c_.attr('stroke') === blackPrimary) c_.stroke({color:n.color});

                let shouldPing = e.detail ? ((e.detail.shouldPing === undefined) ? true : e.detail.shouldPing) : true;
                if (this.onAspectHighlight && shouldPing) this.onAspectHighlight(mfbj_ss.indexOf(n.tag), nodeOutgoings(n.tag));
            });

            c_.on('mouseup', (e)=> {
                n.isActive = false;
            });

            // c_.click((e)=> {
            //     if (infoSel !== n.tag) {
            //         if (infoSel) nodes[infoSel].elem.stroke({color:blackPrimary});
            //         infoSel = n.tag;
            //     }
            //     if (c_.attr('stroke') === blackPrimary) c_.stroke({color:n.color});
            //
            //     if (n.aspect.onHighlight) n.aspect.onHighlight(theme.id, n.aspect.id, nodeOutgoings(n.tag));
            // });
        }

        // Draw sketch lines
        for (let i = mfbj_ss.length-1; i > 0; --i) {
            let b_s = mfbj_ss[i],
                b = nodes[b_s].elem;
            for (let j = 0; j < i; ++j) {
                let a_s = mfbj_ss[j],
                    a = nodes[a_s].elem,
                    ab = a.connectable({
                        container: b_edges_,
                        markers: markers_
                    }, b);
                ab.setLineColor(blackPrimary);
                ab.line.stroke({width: 0.5});
                ab.line.style({'stroke-dasharray': 3});
                ab.line.back();

                b_edges[a_s+b_s] = ab;
            }
        }

        b_edges_.backward();

        // Drawn value edges
        for (let k in b_edges) {
            // bottom edge
            let e = b_edges[k],
                s = [e.source.x(),e.source.y()],
                t = [e.target.x(),e.target.y()],
                p = draw.path('M'+s[0]+' '+s[1]+' L'+s[0]+' '+s[1]+' z');
            p.fill('none').dmove(5,5);
            p.stroke({
                color: e_pos0_col,
                width: 1.25,
                linecap: 'round',
                linejoin: 'round'
            });

            p.tag = k;
            p.value = 0.0;

            d_edges_1[k] = p;
            d_edges_1_.add(p);

            // top edge
            t = [e.source.x(),e.source.y()],
            s = [e.target.x(),e.target.y()],
            p = draw.path('M'+s[0]+' '+s[1]+' L'+s[0]+' '+s[1]+' z');
            p.fill('none').dmove(5,5);
            p.stroke({
                color: e_pos1_col,
                width: 2,
                linecap: 'round',
                linejoin: 'round'
            });

            p.tag = k;
            p.value = 0.0;

            d_edges_2[k] = p;
            d_edges_2_.add(p);
        }

        if (theme.meaning) {
            for (let k_ in theme.meaning) {
                let k = k_,
                    e_ = theme.meaning[k],
                    e = b_edges[k],
                    s = null,
                    t = null;
                if (e) {
                    s = [e.source.x(),e.source.y()];
                    t = [e.target.x(),e.target.y()];
                } else {
                    k = n_reverse(k);
                    e = b_edges[k];
                    t = [e.source.x(),e.source.y()];
                    s = [e.target.x(),e.target.y()];
                }

                if (e_.value === 0) continue;

                let p = e_.position === 0 ? d_edges_1[k] : d_edges_2[k];

                p.value = e_.value;
                p.from = k_.substring(0,1);
                redrawPath(p,s,t,p.value);
            }
        } else {
            let es = {};

            for (let k in b_edges) {

                es[k] = {
                    value: 0.0,
                    position: 0,
                    arrowType: ARROW_TYPE_SIMILARITY
                }

                es[n_reverse(k)] = {
                    value: 0.0,
                    position: 0,
                    arrowType: ARROW_TYPE_SIMILARITY
                }
            }

            theme.meaning = es;
        }

        d_edges_2_.before(nodes_);
        d_edges_1_.before(d_edges_2_);

        this.highlightAspect = (pos)=> {
            nodes[mfbj_ss[pos]].elem.fire('mousedown', {isActive:false});
        }

        this.selectAspect = (pos,lead)=> {
            let tag = mfbj_ss[pos];

            if (sel1) toggleNode({node:nodes[sel1],offColor:whitePrimary});
            if (sel2) toggleNode({node:nodes[sel2],offColor:whitePrimary});

            if (lead) {
                let osel1 = sel1 === tag ? sel2 : sel1;
                sel1 = tag;
                toggleNode({node:nodes[sel1],onColor:primaryColor,isPrimary:true});
                sel2 = osel1;
                if (sel2) toggleNode({node:nodes[sel2],onColor:secondaryColor});
            } else if (sel1 && sel2) {
                sel1 = sel1 === tag ? sel2 : sel1;
                toggleNode({node:nodes[sel1],onColor:primaryColor,isPrimary:true});
                sel2 = tag;
                toggleNode({node:nodes[sel2],onColor:secondaryColor});
            } else {
                sel1 = sel1 || tag;
                sel2 = sel1 === tag ? sel2 : tag;
                if (sel1) toggleNode({node:nodes[sel1],onColor:sel1 === tag ? secondaryColor : primaryColor,isPrimary:sel1 !== tag});
                if (sel2) toggleNode({node:nodes[sel2],onColor:sel2 === tag ? secondaryColor : primaryColor,isPrimary:sel2 !== tag});
            }
        }

        this.setMeaningValue = (spos,epos,v,silent)=> {
            if (spos === epos) return;
            if ((!sel1 || !sel2) && (spos == undefined || epos == undefined)) return;
            if (sel1) toggleNode({node:nodes[sel1],offColor:whitePrimary});
            if (sel2) toggleNode({node:nodes[sel2],offColor:whitePrimary});
            sel1 = typeof spos === 'string' ? spos : mfbj_ss[spos] || sel1;
            sel2 = typeof epos === 'string' ? epos : mfbj_ss[epos] || sel2;
            let ping = false;
            nodes[sel1].elem.fire('mousedown', {isActive:false, shouldPing:false, shouldHighlight:!silent});
            if (spos == undefined || epos == undefined) {
                v = Math.min(1, Math.max(0, theme.meaning[sel1+sel2].value+v));
                ping = true;
            }
            v = v.toFixedNumber(NUM_DECIMAL_PLACES);
            toggleNode({node:nodes[sel1], onColor:primaryColor, isPrimary:true});
            toggleNode({node:nodes[sel2], onColor:secondaryColor});
            nodes[sel1].isActive = true;
            updateMeaning(null,null,v,ping);
            nodes[sel1].isActive = false;
            if (commit && theme.onMeaningCommit) {
                theme.onMeaningCommit(commit);
                commit = null;
            }
        }

        this.updateMeaning = (meaning)=> {
            let v, a, b, ab, ba;
            for (let adx = 0; adx < 4; ++ adx) {
                a = mfbj_ss[adx];
                for (let bdx = adx+1; bdx < 4; ++bdx) {
                    b = mfbj_ss[bdx];
                    ab = a+b;
                    ba = b+a;
                    v = meaning[ab].value.toFixedNumber(NUM_DECIMAL_PLACES);
                    this.setMeaningValue(adx,bdx,v,true);
                    v = meaning[ba].value.toFixedNumber(NUM_DECIMAL_PLACES);
                    this.setMeaningValue(bdx,adx,v,true);
                }
            }

            toggleNode({node:nodes[sel1], offColor:whitePrimary});
            toggleNode({node:nodes[sel2], offColor:whitePrimary});
        }

        this.valueForMeaning = (spos,epos)=> {
            return theme.meaningOf(spos,epos).value;
        }

        this.onAspectHighlight = null;

        this.clearSelection = ()=> {
            clearSelection(nodes,infoSel,sel1,sel2,blackPrimary,whitePrimary);
            infoSel = sel1 = sel2 = null;
        }
    }

    this.Text = function(props) {
        // Remove 'preserveAspectRatio' OR write it in HTML
        let theme = props.theme,
            c = props.container,
            focus = props.focus;

        this.id = c;

        let container = document.getElementById(c),
            txa = null;

        this.activate = ()=> {
            if (txa) txa.focus();
        }

        this.saveData = (blur)=> {
            if (blur) {
                txa.blur();
            } else {
                theme.meaning = txa.value;
            }
        }

        loadTemplate('templates/textual-theme.html', t => {
            txa = t.querySelector('textarea');
            container.appendChild(t);
            let txa_jq = $(txa);
            txa.value = theme.meaning || '';
            txa.setAttribute('placeholder', theme.name_ || 'Enter text here');
            theme.name = (str)=> {
                if (str) {
                    txa.setAttribute('placeholder', str || 'Enter text here');
                    theme.name_ = str;
                } else {
                    return theme.name_;
                }
            }
            autosize(txa_jq);
            txa_jq.focus(()=> {
                if(theme.onSelect) theme.onSelect(CLICKED_ON_CANVAS,c);
            });
            txa_jq.blur(()=> {
                this.saveData();
                if (theme.onMeaningChange) theme.onMeaningChange();
                if (theme.onMeaningCommit) theme.onMeaningCommit();
            });
            if (focus) txa.focus();
        });
    }
}
