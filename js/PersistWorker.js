'use strict'

importScripts('constants.js', 'dexie.js');

// Supports saving script and performing CUD ops. Saving a
// script will trigger CUD ops on items so they should be
// included in CUD format.

let stats = {
    create: { count: 0, size: 0 },
    update: { count: 0, size: 0 },
    delete: { count: 0, size: 0 },
}
let packager = null;
let memnat = null;
let cud = null;
let final_script = null;
let items_ready = {
    create: false,
    update: false,
    delete: false
}
let script_ready = false;
let items = [
    ITEMS_TYPE_ASPECTS,
    ITEMS_TYPE_THEMES,
    ITEMS_TYPE_SHELLS,
    ITEMS_TYPE_ENTITIES,
    ITEMS_TYPE_PROCESSES,
    ITEMS_TYPE_SCHOOLS,
    ITEMS_TYPE_SCENES,
    ITEMS_TYPE_SCRIPTS];

onmessage = async function(e) {
    let data = e.data,
        type = data.type;

    packager = data.packager;

    if ((packager.name === 'web') && (memnat == null || !memnat.isOpen())) {
        memnat = new Dexie(packager.module.name);
        await memnat.open();
    }

    cud = typeof data.cud === 'string' ? JSON.parse(data.cud) : data.cud;

    stats.create.size = items.reduce((acc,it)=> {return acc+(cud.sizes.create[it] || 0)}, 0);
    stats.update.size = items.reduce((acc,it)=> {return acc+(cud.sizes.update[it] || 0)}, 0);
    stats.delete.size = items.reduce((acc,it)=> {return acc+(cud.sizes.delete[it] || 0)}, 0);

    const deleteItems = ()=> {
        if(stats.delete.size === 0) {

            let msg = { type:'done', workId: data.workId }
            if (script_ready) {
                if (final_script) msg.script = final_script;
                postMessage(msg);
            } else {
                items_ready.delete = true;
            }
            return;
        }
        for (const dthing in cud.delete) {
            for (const ditem in cud.delete[dthing]) {
                deleteItem(ditem, dthing, ()=> {
                    // on delete ready
                    let msg = { type:'done', workId: data.workId }
                    if (final_script) msg.script = final_script;
                });
            }
        }
    }

    const updateItems = ()=> {
        if (stats.update.size === 0) {
            items_ready.update = true;
            deleteItems();
            return;
        }

        for (const uthing in cud.update) {
            for (const uitem in cud.update[uthing]) {
                updateItem(uitem, cud.update[uthing][uitem], uthing, ()=> {
                    // on update ready
                    deleteItems();
                });
            }
        }
    }

    const cudItems = ()=> {
        if (stats.create.size === 0) {
            items_ready.create = true;
            updateItems();
        } else {
            for (const cthing in cud.create) {
                for (const citem in cud.create[cthing]) {
                    createItem(citem, cud.create[cthing][citem], cthing, ()=> {
                        // on create ready
                        updateItems();
                    });
                }
            }
        }
    }

    switch (type) {
        case ITEM_TYPE_SCRIPT:

            cudItems();

            let lines = JSON.parse(data.lines),
                scenes = JSON.parse(data.scenes),
                script = data.script,
                currentScene = data.currentScene;

            const compileScript = ()=> {

                script.saveTime = Date.now();

                const lineIds = [];
                const lines_arr = [];
                for (const num in lines) {
                    let l = lines[num];
                    let e = l.entity;
                    let l_ = {
                        creationTime: l.creationTime,
                        saveTime: script.saveTime,
                        id: l.id,
                        themeIds: l.themes.map(t => t._theme.id),
                        modQuad: l.modQuad ? {
                                theme: {
                                    id: l.modQuad.theme._theme.id,
                                    name: l.modQuad.theme._theme.name_,
                                    type: l.modQuad.theme._theme.type,
                                    meaning: l.modQuad.theme._theme.meaning,
                                    position: l.modQuad.theme.position
                                }
                            } : null,
                        number: l.number,
                        entityId: e ? e.id : null
                    }

                    lineIds.push(l.id);
                    lines_arr.push(l_)
                }
                script.lines = lines_arr;

                const sceneIds = [];
                const scenes_arr = [];

                for (let pos in scenes) {
                    let s = scenes[pos],
                        s_ = {
                            id: s.id,
                            creationTime: s.creationTime,
                            position: s.position, // may have changed in scenes menu,
                            name: s.name, // TODO: may have changed in scenes menu
                            lineIds: s.lineIds,
                            // TODO: save scene authors
                        }

                    if (s.id === currentScene) {
                        s_.lineIds = lineIds;
                        s_.saveTime = script.saveTime;
                        currentScene = s_;
                    }

                    sceneIds[pos-1] = s.id;
                    scenes_arr.push(s_);
                }
                script.sceneIds = sceneIds;
                script.scenes = scenes_arr;

                return script;
            }

            // Save aspects, themes, shells, entities in own object
            // Refer to their IDs in higher objects
            // All objects of a kind are loaded into their maps except
            // for lines. The new save file will need to pre-include the
            // lines in the old save and update the lines of the current
            // scene.

            final_script = compileScript(lines,scenes,script);

            let scripts = null,
                lite = null;
            switch (packager.name) {
                case 'electron':
                    const fs = require('fs');
                    const path = require('path');
                    let p = '';

                    for (let i = 0; i < final_script.lines.length; ++i) {
                        let line = final_script.lines[i],
                            lid = line.id;
                        p = path.join(packager.linesDir,lid+'.json');
                        try {
                            fs.writeFileSync(p, JSON.stringify(line,null,4), {encoding:'utf-8'});
                        } catch (err) {
                            console.error(err);
                        }
                    }

                    delete final_script.lines;

                    // let sceneIds = new Array(final_script.scenes.length);
                    for (let i = 0; i < final_script.scenes.length; ++i) {
                        let scene = final_script.scenes[i],
                            sid = scene.id;
                        // sceneIds[scene.position-1] = sid;

                        if (scene === currentScene) continue;

                        p = path.join(packager.scenesDir,sid+'.json');
                        let res = {};

                        try {
                            res = JSON.parse(fs.readFileSync(p, 'utf-8'));
                            // res = scene;
                            res.position = scene.position;
                        } catch(err) {
                            res = scene;
                            console.error(err);
                        }

                        try {
                            fs.writeFileSync(p, JSON.stringify(res,null,4), {encoding:'utf-8'});
                        } catch (err) {
                            console.error(err);
                        }
                    }
                    // final_script.sceneIds = sceneIds;

                    p = path.join(packager.scenesDir,currentScene.id+'.json');
                    try {
                        fs.writeFileSync(p, JSON.stringify(currentScene,null,4), {encoding:'utf-8'});
                    } catch (err) {
                        console.error(err);
                    }

                    delete final_script.scenes;

                    fs.readFile(packager.scriptsFile, {encoding: 'utf-8'}, (err, res)=> {
                        if (err) {
                            postMessage({type:'error', error:err, workId: data.workId });
                            return;
                        }

                        scripts = JSON.parse(res);
                        lite = scripts[final_script.id] || { id: final_script.id };
                        lite.saveTime = final_script.saveTime;
                        lite.creationTime = final_script.creationTime;
                        lite.name = final_script.name;
                        scripts[final_script.id] = lite;

                        fs.writeFile(packager.scriptsFile, JSON.stringify(scripts, null, 4), {encoding:'utf-8'}, (err)=> {
                            if (err) {
                                postMessage({type:'error', error:err, workId: data.workId });
                                return;
                            }

                            let thisData = JSON.stringify(final_script, null, 4);
                            let savePath = path.join(packager.scriptsDir, final_script.id+'.json');

                            fs.writeFile(savePath, thisData, {encoding:'utf-8'}, (err)=> {
                                if (err) {
                                    postMessage({type:'error', error:err, workId: data.workId });
                                    return;
                                }

                                if (items_ready.delete) {
                                    postMessage({ type:'done', script: final_script, workId: data.workId });
                                } else {
                                    script_ready = true;
                                }
                            });
                        });
                    });
                    break;
                default:
                    lite = { id: final_script.id };
                    lite.saveTime = final_script.saveTime;
                    lite.creationTime = final_script.creationTime;
                    lite.name = final_script.name;
                    let lines_ = final_script.lines,
                        scenes_ = final_script.scenes;
                    delete final_script.lines;
                    delete final_script.scenes;

                    memnat.transaction('rw', memnat.table('scripts_lite'), memnat.table('scripts'), memnat.table('lines'), memnat.table('scenes'), ()=> {
                        memnat.table('lines').bulkPut(lines_);
                        memnat.table('scenes').bulkPut(scenes_);
                        memnat.table('scripts').put(final_script);
                        memnat.table('scripts_lite').put(lite);

                    }).then(()=> {
                        (function checkStats() {
                            if (items_ready.delete) {
                                postMessage({ type:'done', script: final_script, workId: data.workId });
                            } else {
                                script_ready = true;
                            }
                        })()
                    }).catch((err)=> {
                        console.error(err);
                        postMessage({type:'error', error:err, workId: data.workId });
                    });
            }

            break;
        case ITEM_TYPE_CUD:
            script_ready = true;
            cudItems();
            break;
        default:
    }
}

function createItem(itemId, item, header, onready) {

    item = stripItem(item, header);

    function checkStats() {
        stats.create.count++;
        items_ready.create = stats.create.count === stats.create.size;

        if (items_ready.create && onready) onready();
    }

    switch (packager.name) {
        case 'electron':
            const path = require('path');
            let p = path.join(packager[header+'Dir'],itemId+'.json');
            const fs = require('fs');
            fs.writeFile(p,JSON.stringify(item,null,4), (err)=> {
                if (err) console.error(err);
                checkStats();
            });
            break;
        default:
            memnat.transaction('rw', memnat.table(header), async ()=> {
                memnat.table(header).put(item);
            }).catch((err)=> {
                console.error(err);
            }).then(()=> { checkStats() });
    }
}

function updateItem(itemId, item, header, onready) {

    function checkStats() {
        stats.update.count++;
        items_ready.update = stats.update.count === stats.update.size;

        if (items_ready.update && onready) onready();
    }

    item.saveTime = Date.now();

    switch (packager.name) {
        case 'electron':
            const path = require('path');
            const fs = require('fs');
            let p = path.join(packager[header+'Dir'],itemId+'.json');
            fs.readFile(p, 'utf-8', (err, res)=> {
                if (err) {
                    console.error(err);
                    return;
                }

                res = JSON.parse(res);
                for (let k in item) res[k] = item[k];

                fs.writeFile(p,JSON.stringify(res,null,4), (err)=> {
                    if (err) console.error(err);
                    checkStats();
                });
            });
            break;
        default:
            memnat.transaction('rw', memnat.table(header), async ()=> {
                memnat.table(header).update(itemId,item);
            }).catch((err)=> {
                console.error(err);
            }).then(()=> { checkStats() });
    }
}

function deleteItem(itemId, header, onready) {

    function checkStats() {
        stats.delete.count++;
        items_ready.delete = stats.delete.count === stats.delete.size;

        if (items_ready.delete && onready) onready();
    }

    switch (packager.name) {
        case 'electron':
            const path = require('path');
            const fs = require('fs');
            if (header === 'scripts') {
                // NOTE: scripts_lite is accounted for by scripts stats
                try {
                    let scripts = fs.readFileSync(packager.scriptsFile, {encoding: 'utf-8'});
                    scripts = JSON.parse(scripts);
                    delete scripts[itemId];

                    fs.writeFile(packager.scriptsFile, JSON.stringify(scripts, null, 4), {encoding:'utf-8'});
                } catch (err) {
                    console.error(err);
                }

                let script_path = path.join(packager.scriptsDir,itemId+'.json');

                try {

                    let script = fs.readFileSync(script_path, {encoding: 'utf-8'});
                    script = JSON.parse(script);

                    let sceneIds = script.sceneIds, sid = null, scene = null, scene_path = null,
                        lineIds = null, lid = null, line_path = null;

                    for (let i = 0; i < sceneIds.length; ++i) {
                        sid = sceneIds[i];
                        scene_path = path.join(packager.scenesDir,sid+'.json');
                        try {
                            scene = fs.readFileSync(scene_path, {encoding: 'utf-8'});
                            scene = JSON.parse(scene);
                        } catch (err) {
                            console.error(err);
                            continue;
                        }

                        lineIds = scene.lineIds;

                        for (let lc = 0; lc < lineIds.length; ++lc) {
                            lid = lineIds[lc];
                            line_path = path.join(packager.linesDir,lid+'.json');
                            try {
                                fs.unlinkSync(line_path);
                            } catch (err) {
                                console.error(err);
                                continue;
                            }
                        }

                        try {
                            fs.unlinkSync(scene_path);
                        } catch (err) {
                            console.error(err);
                        }
                    }

                    fs.unlink(script_path, (err)=> {
                        if (err) console.error(err);

                        checkStats();
                    });
                } catch (err) {
                    console.error(err);
                    checkStats();
                    return;
                }
            } else {
                let p = path.join(packager[header+'Dir'],itemId+'.json');
                fs.unlink(p, (err)=> {
                    if (err) console.error(err);

                    checkStats();
                });
            }
            break;
        default:
            if (header === 'scripts') {
                memnat.transaction('rw', memnat.table('scripts'), memnat.table('scripts_lite'), memnat.table('scenes'), memnat.table('lines'), async ()=> {
                    let script = await memnat.table('scripts').get(itemId),
                        sceneIds = script.sceneIds, sid = null, scene = null,
                        lineIds = null, lid = null;
                    for (let sc = 0; sc < sceneIds.length; ++sc) {
                        sid = sceneIds[sc];
                        scene = await memnat.table('scenes').get(sid);
                        lineIds = scene.lineIds;
                        for (let lc = 0; lc < lineIds.length; ++lc) {
                            lid = lineIds[lc];
                            memnat.table('lines').delete(lid);
                        }
                        memnat.table('scenes').delete(sid);
                    }
                    memnat.table('scripts').delete(itemId);
                    memnat.table('scripts_lite').delete(itemId);
                }).catch((err)=> {
                    console.error(err);
                }).then(()=> { checkStats() });
            } else {
                memnat.transaction('rw', memnat.table(header), ()=> {
                    memnat.table(header).delete(itemId);
                }).catch((err)=> {
                    console.error(err);
                }).then(()=> { checkStats() });
            }
    }
}

const stripItem = (item, header)=> {
    let res = {};

    switch (header) {
        case ITEMS_TYPE_ASPECTS:
            // NOTE: set aspect
            res = {
                id: item._aspect.id,
                abbr: item._aspect.abbr_,
                type: item._aspect.type,
                frequencies: item._aspect.frequencies_,
                cover: item._aspect.cover_
            }
            if (res.type === ASPECT_TYPE_PURE) res.name = item._aspect.name;
            break;
        case ITEMS_TYPE_THEMES:
            // NOTE: set theme
            res = {
                id: item._theme.id,
                name: item._theme.name_,
                position: item.position,
                shellId: item._theme.shellId,
                diversity: item._theme.diversity,
                meaning: item._theme.meaning,
                quantum: item._theme.quantum,
                squat: item._theme.squat,
                duration: item._theme.duration_,
                processId: item._theme.processId_,
                intensities: item._theme.intensities_,
                durations: item._theme.durations_,
                processIds: item._theme.processIds_,
                type: item._theme.type,
                width: item._theme.width || 1,
                depth: item._theme.depth || 1
            }
            break;
        case ITEMS_TYPE_SHELLS:
            res = {
                id: item.id,
                name: item.name_,
                aspectIds: item.aspectIds
            }
            break;
        case ITEMS_TYPE_ENTITIES:
            res = {
                id: item.id,
                name: item.name,
            }
            break;
        case ITEMS_TYPE_PROCESSES:
            res = {
                id: item.id,
                name: item.name,
                path: item.path
            }
            break;
        case ITEMS_TYPE_SCHOOLS:
            res = {
                id: item.id,
                name: item.name,
                path: item.path
            }
            break;
        // TODO lines, scenes
    }

    res.creationTime = item.creationTime || Date.now();
    res.saveTime = Date.now();

    return res;
}
