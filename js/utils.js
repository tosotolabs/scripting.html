'use strict'

/***************** DOM MANIPULATION *************/
let loadTemplate = (file, fb, fe)=> {
    let xhttp = new XMLHttpRequest(),
        elem = null;

    xhttp.onreadystatechange = ()=> {
        if (xhttp.readyState == 4) {
            if (xhttp.status == 200) {
                elem = $.parseHTML(xhttp.responseText)[0];
                fb(elem);
            }
        }
    }

    xhttp.open("GET", file, true);
    xhttp.send();

    return;
}

/***************** DOM STYLING *****************/
const display = (e,d)=> {
    if (d && $(e).css('display') == 'none') {
        $(e).css('display','block');
    } else if (!d && $(e).css('display') == 'block') {
        $(e).css('display','none');
    }
}

const toggleDisplay = (e,f) => {
    if ($(e).css('display') == 'none') {
        display(e,true);
        return true;
    } else {
        display(e,false);
        return false;
    }
}

/***************** STRINGS *****************/
const n_reverse = (str)=> { return str.split('').reverse().join('') }

const uuid = (hy)=> {
    var lut = [];

    for ( var i = 0; i < 256; i ++ ) {

        lut[ i ] = ( i < 16 ? '0' : '' ) + ( i ).toString( 16 );

    }

    var d0 = Math.random() * 0xffffffff | 0;
    var d1 = Math.random() * 0xffffffff | 0;
    var d2 = Math.random() * 0xffffffff | 0;
    var d3 = Math.random() * 0xffffffff | 0;
    hy = hy ? '-' : '';
    var uuid = lut[ d0 & 0xff ] + lut[ d0 >> 8 & 0xff ] + lut[ d0 >> 16 & 0xff ] + lut[ d0 >> 24 & 0xff ] + hy +
    lut[ d1 & 0xff ] + lut[ d1 >> 8 & 0xff ] + hy + lut[ d1 >> 16 & 0x0f | 0x40 ] + lut[ d1 >> 24 & 0xff ] + hy +
    lut[ d2 & 0x3f | 0x80 ] + lut[ d2 >> 8 & 0xff ] + hy + lut[ d2 >> 16 & 0xff ] + lut[ d2 >> 24 & 0xff ] +
    lut[ d3 & 0xff ] + lut[ d3 >> 8 & 0xff ] + lut[ d3 >> 16 & 0xff ] + lut[ d3 >> 24 & 0xff ];

    // .toUpperCase() here flattens concatenated strings to save heap memory space.
    return uuid.toUpperCase();
}

const guid = uuid;

const guid_ = (hy)=> {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1).toUpperCase();
    }

    hy = hy ? '-' : '';

    return s4() + s4() + hy + s4() + hy + s4() + hy + s4() + hy + s4() + s4() + s4();

}

const shorten = (str_,sz)=> {
    let cut = str_.length - sz;
    if (cut <= 0) return str_;
    let mid = (str_.length-cut)/2,
        lstr = str_.substring(0,mid),
        rstr = str_.substring(mid+cut,str_.length);
    return lstr+'...'+rstr;
}

/***************** URL *****************/
const isValidURL = (str)=> {
    try {
        let url = new URL(str);
    } catch (e) {
        return false;
    }

    return true;
}

const urlJoin = (root,str)=> {
    if (!root) return str;
    if (!str) return root;
    return root += root.charAt(root.length-1) === '/' || str.charAt(0) === '/' ? str : '/'+str;
}

/***************** NUMBERS *****************/
Number.prototype.toFixedNumber = function(x, base) {
    let pow = Math.pow(base||10,x);
    return Math.round(this*pow) / pow;
}

Number.prototype.countDecimals = function () {
    if(Math.floor(this.valueOf()) === this.valueOf()) return 0;
    return this.toString().split(".")[1].length || 0;
}

const getRandomInt = (min,max)=> {
    return Math.floor(getRandomNumber(min,max));
}

const getRandomNumber = (min,max)=> {
    return (Math.random() * (max-min)) + min;
}

const randomColor = ()=> {
    return Math.random() * 0xffffff;
}

const boundFull = (v)=> {return Math.range(v,-1,1)}
const boundPos = (v)=> {return Math.range(v,0,1)}

/***************** ARRAYS *****************/
Array.prototype.isEqualTo = function(b) {
    let a = this;
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length != b.length) return false;

    // If you don't care about the order of the elements inside
    // the array, you should sort both arrays here.
    // Please note that calling sort on an array will modify that array.
    // you might want to clone your array first.

    for (let i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
    }
    return true;
}

Array.prototype.random = function() {
    return this[getRandomInt(0,this.length)];
}

const normalize = (arr)=> {
    let len = Math.sqrt( arr.reduce( (acc,cv)=> {return acc + cv*cv} , 0) );
    return arr.map(e => e/len);
}

const normalizeWithMax = (arr)=> {
    let max = arr[0];
    arr.map(it => { max = Math.max(it,max) });
    return arr.map(it => it/max);
}

const range = (arr)=> {
    let min = arr[0],
        max = arr[0];
    arr.map(it => {
        let it_ = isNaN(it.value) ? it : it.value,
            max_ = isNaN(max.value) ? max : max.value,
            min_ = isNaN(min.value) ? min : min.value;
        if (it_ > max_) {
            max = it;
        }

        if (it_ < min_) {
            min = it;
        }
    });
    min = isNaN(min.value) ? min : min.value;
    max = isNaN(max.value) ? max : max.value;

    return [min, max, max-min];
}

const minDiff = (v, arr, k)=> {
    let res = {diff:Infinity, index:0, e:arr[0]}
    arr.forEach((c, idx) => {
        let diff = Math.abs(c[k]-v);
        if (diff < res.diff) {res.diff=diff; res.e=c; res.index=idx};
    });
    return res;
}

const shuffle = (array)=> {

	var currentIndex = array.length;
	var temporaryValue, randomIndex;

	// While there remain elements to shuffle...
	while (0 !== currentIndex) {
		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}

	return array;

};

/***************** MATHEMATICS *****************/
Math.range = function(v,min,max) {
    return Math.min(Math.max(v,min),max);
}
const rad2deg = (rad)=> { return rad * 180 / Math.PI; };
const calcMag = (p1,p2)=> { return Math.sqrt( Math.pow(p1[0]-p2[0],2) + Math.pow(p1[1]-p2[1],2) ) };
// const calcSlope = (p1,p2)=> { return (p1[1]-p2[1])/(p1[0]/p2[0]); }
const lerp = (p1,p2,p3)=> { return [p1[0] + p3[0] * (p2[0] - p1[0]), p1[1] + p3[1] * (p2[1] - p1[1])]; };
const calcAngle = (a,b,c)=> {
    if (c) { return Math.abs(calcAngle(a, c) - calcAngle(b, c)); }

    let angle = Math.atan2(b[1] - a[1], b[0] - a[0]);

    while (angle < 0){ angle += 2 * Math.PI; }

    return angle;

    // let sc = calcMag(a,b),
    //     sb = calcMag(a,c),
    //     sa = calcMag(b,c),
    //     cosc = (Math.pow(sa,2)+Math.pow(sb,2)-Math.pow(sc,2))/(2*sa*sc);
    //
    // return Math.acos(cosc);
}

//`p` is the position in the range, from the lesser number
const rangedAverage = (a,b,p)=> {
    let w = Math.abs(a-b);
    p = w*p;
    if (a < b) {
        return a+p;
    } else {
        return b+p;
    }
}

// `b` is the bias towards `x`
const biasedAverage = (x,y,b)=> {
    if ((x === 0) && (y === 0)) return x;
    if (x === 0) return y/(2+b);
    return (x+y)/(2+(((y/x)-1)*b));
}

// `(a,x)` are the left value and its bias respectively.
// `(b,y)` are the right value and its bias respectively.
// The result is somewhere in the range of `a` and `b`
    // after considering their effort to be the result.
const tugofwar = ([a,x], [b,y])=> {
    let [[min,max], [rmin,rmax]] = a < b ? [[a,b],[x,y]] : [[b,a],[y,x]],
        p = Math.abs(b-a)/2,
        posx = min + (p - p * rmin),
        posy = max - (p - p * rmax);

    return (posx+posy)/2;

}
