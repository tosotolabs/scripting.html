'use strict'

const Scripting = function(scripting_opts) {

    scripting_opts = scripting_opts || new URLSearchParams();
    let packager = typeof scripting_opts === 'string' ? {name: scripting_opts} : {name: scripting_opts.get('packager') || 'web'},
        litePackager = { name: packager.name };
    this.packager = packager;
    this.litePackager = litePackager;

    const Three = packager.name === 'worker' ? null : THREE; // Three.js
    const Vector = Three ? Three.Vector3 : null;
    const ORIGIN = Vector ? new Vector() : null;

    this.Three = Three;

    this.CONTROLS_PANEL_ASPECT_STATE = CONTROLS_PANEL_ASPECT_STATE;
    this.CONTROLS_PANEL_THEME_STATE = CONTROLS_PANEL_THEME_STATE;
    this.CONTROLS_PANEL_SHELL_STATE = CONTROLS_PANEL_SHELL_STATE;
    this.CONTROLS_PANEL_ENTITY_STATE = CONTROLS_PANEL_ENTITY_STATE;
    this.CONTROLS_PANEL_LINE_STATE = CONTROLS_PANEL_LINE_STATE;
    this.CONTROLS_PANEL_SCENE_STATE = CONTROLS_PANEL_SCENE_STATE;

    this.ITEM_TYPE_ASPECT = ITEM_TYPE_ASPECT;
    this.ITEM_TYPE_THEME = ITEM_TYPE_THEME;
    this.ITEM_TYPE_SHELL = ITEM_TYPE_SHELL;
    this.ITEM_TYPE_ENTITY = ITEM_TYPE_ENTITY;
    this.ITEM_TYPE_PROCESS = ITEM_TYPE_PROCESS;
    this.ITEM_TYPE_SCHOOL = ITEM_TYPE_SCHOOL;
    this.ITEM_TYPE_SCENE = ITEM_TYPE_SCENE;
    this.ITEM_TYPE_SCRIPT = ITEM_TYPE_SCRIPT;
    this.ITEM_TYPE_CUD = ITEM_TYPE_CUD;

    this.ITEMS_TYPE_ASPECTS = ITEMS_TYPE_ASPECTS;
    this.ITEMS_TYPE_THEMES = ITEMS_TYPE_THEMES;
    this.ITEMS_TYPE_SHELLS = ITEMS_TYPE_SHELLS;
    this.ITEMS_TYPE_ENTITIES = ITEMS_TYPE_ENTITIES;
    this.ITEMS_TYPE_PROCESSES = ITEMS_TYPE_PROCESSES;
    this.ITEMS_TYPE_SCHOOLS = ITEMS_TYPE_SCHOOLS;
    this.ITEMS_TYPE_LINES = ITEMS_TYPE_LINES;
    this.ITEMS_TYPE_SCENES = ITEMS_TYPE_SCENES;
    this.ITEMS_TYPE_SCRIPTS = ITEMS_TYPE_SCRIPTS;

    const M = packager.name === 'worker' ? null : new Memnat();

    let aspects_map_ = {},
        themes_map_ = {},
        shells_map_ = {},
        entities_map_ = {},
        processes_map_ = {},
        lines_map_ = {},
        scenes_map_ = {};

    const aspects_map = { size: 0 },
        themes_map = { size: 0 },
        shells_map = { size: 0 },
        entities_map = { size: 0 },
        processes_map = { size: 0 },
        schools_map = {},
        lines_map = { size: 0 },
        scenes_map = { size: 0 };

    const getCircularReplacer = () => {
        const seen = new WeakSet();
        return (key, value) => {
            if (typeof value === "object" && value !== null) {
                if (seen.has(value)) { // must be theme.line causing this issue
                    let obj = {};
                    for (const k in value) obj[k] = value[k];
                    delete obj.line;
                    delete obj.elem;
                    if (obj.themes){
                        obj.themes = obj.themes.map(t_ => {
                            let t = {};
                            for (const k in t_) t[k] = t_[k];
                            delete t.line;
                            return t
                        });
                    }
                    return obj;
                }
                seen.add(value);
            }
            return value;
        };
    }

    const CUD = {
        createMethods: ['set', 'delete','size'],
        updateMethods: ['set', 'delete','size'],
        deleteMethods: ['add', 'delete','size'],
        create: {
            aspects: {},
            themes: {},
            shells: {},
            entities: {},
            processes: {},
            schools: {},
            scenes: {}, // TODO: yet to be supported
            scripts: {}, // TODO: yet to be supported
        },
        update: {
            aspects: {},
            themes: {},
            shells: {},
            entities: {},
            processes: {},
            schools: {},
            scenes: {}, // TODO: yet to be supported
            scripts: {}, // TODO: yet to be supported
        },
        delete: {
            aspects: {},
            themes: {},
            shells: {},
            entities: {},
            processes: {},
            schools: {},
            scenes: {},
            scripts: {},
        },

        drain: (cud)=> {
            cud = cud || CUD;
            let sizes = { create: {}, update: {}, delete: {} };

            for (let k in cud.create) {
                if (CUD.createMethods.includes(k)) continue;
                sizes.create[k] = cud.create[k].size;
                delete cud.create[k].size;
            }
            for (let k in cud.update) {
                if (CUD.updateMethods.includes(k)) continue;
                sizes.update[k] = cud.update[k].size;
                delete cud.update[k].size;
            }

            for (let k in cud.delete) {
                if (CUD.deleteMethods.includes(k)) continue;
                sizes.delete[k] = cud.delete[k].size;
                delete cud.delete[k].size;
            }

            let res = JSON.stringify({ create: cud.create, update: cud.update, delete: cud.delete, sizes: sizes }, getCircularReplacer());

            cud.reset();

            return res;
        },

        partialDrain: (cud, things)=> {
            cud = cud || CUD;
            let sizes = { create: {}, update: {}, delete: {} },
                res = { create: {}, update: {}, delete: {} };
            for (let ti = 0; ti < things.length; ++ti) {
                let t = things[ti];
                if (cud.create[t]) {
                    res.create[t] = cud.create[t];
                    sizes.create[t] = cud.create[t].size;
                    delete cud.create[t].size;
                    let def = {};
                    for (let defi = 0; defi < CUD.createMethods.length; ++defi) def[CUD.createMethods[defi]] = cud.create[t][CUD.createMethods[defi]];
                    cud.create[t] = def;
                    cud.create[t].size = 0;
                }
                if (cud.update[t]) {
                    res.update[t] = cud.update[t];
                    sizes.update[t] = cud.update[t].size;
                    delete res.update[t].size;
                    let def = {};
                    for (let defi = 0; defi < CUD.updateMethods.length; ++defi) def[CUD.updateMethods[defi]] = cud.update[t][CUD.updateMethods[defi]];
                    cud.update[t] = def;
                    cud.update[t].size = 0;
                }
                if (cud.delete[t]) {
                    res.delete[t] = cud.delete[t];
                    sizes.delete[t] = cud.delete[t].size;
                    delete res.delete[t].size;
                    let def = {};
                    for (let defi = 0; defi < CUD.deleteMethods.length; ++defi) def[CUD.deleteMethods[defi]] = cud.delete[t][CUD.deleteMethods[defi]];
                    cud.delete[t] = def;
                    cud.delete[t].size = 0;
                }
            }

            res = JSON.stringify({ create: res.create, update: res.update, delete: res.delete, sizes: sizes }, getCircularReplacer());

            return res;
        },

        new: ()=> {
            let cud = {
                create: {
                    aspects: {},
                    themes: {},
                    shells: {},
                    entities: {},
                    processes: {},
                    schools: {},
                    scenes: {}, // TODO: yet to be supported
                    scripts: {}, // TODO: yet to be supported
                },
                update: {
                    aspects: {},
                    themes: {},
                    shells: {},
                    entities: {},
                    processes: {},
                    schools: {},
                    scenes: {}, // TODO: yet to be supported
                    scripts: {}, // TODO: yet to be supported
                },
                delete: {
                    aspects: {},
                    themes: {},
                    shells: {},
                    entities: {},
                    processes: {},
                    schools: {},
                    scenes: {},
                    scripts: {},
                },

                drain: ()=> { return CUD.drain(cud) },
                reset: ()=> { return CUD.reset(cud) }
            }

            cud.reset();
            return cud;
        },

        reset: (cud)=> {
            cud = cud || CUD;
            for (let k in cud.create) {
                if (CUD.createMethods.includes(k)) {
                    cud.create[k] = {};
                    continue;
                }

                cud.create[k] = {
                    size: 0,
                    set: (id,item)=> {
                        if (!cud.create[k][id]) cud.create[k].size++;
                        cud.create[k][id] = item;
                    },
                    delete: (id)=> {
                        if (!cud.create[k][id]) return;
                        cud.create[k].size--;
                        delete cud.create[k][id];
                        cud.update[k].delete(id); // only update if created
                        cud.delete[k].delete(id); // only delete if created
                    }
                };
            }
            for (let k in cud.update) {
                if (CUD.updateMethods.includes(k)) {
                    cud.update[k] = {};
                    continue;
                }

                cud.update[k] = {
                    size: 0,
                    set: (id,prop,val)=> {
                        if (!cud.update[k][id]) {
                            cud.update[k].size++;
                            cud.update[k][id] = { id: id };
                        }
                        cud.update[k][id][prop] = val;
                    },
                    delete: (id)=> {
                        if (!cud.update[k][id]) return;
                        cud.update[k].size--;
                        delete cud.update[k][id];
                    }
                };
            }

            for (let k in cud.delete) {
                if (CUD.deleteMethods.includes(k)) {
                    cud.delete[k] = {};
                    continue;
                }

                cud.delete[k] = {
                    size: 0,
                    add: (id)=> {
                        if (!cud.delete[k][id]) cud.delete[k].size++;
                        cud.delete[k][id] = true;
                    },
                    delete: (id)=> {
                        if (!cud.delete[k][id]) return;
                        cud.delete[k].size--;
                        delete cud.delete[k][id];
                    }
                }
            }
        }
    }

    if (packager.name !== 'worker') CUD.reset();

    this.CUD = CUD;

    this.aspects = aspects_map;
    this.themes = themes_map;
    this.shells = shells_map;
    this.entities = entities_map;
    this.processes = processes_map;
    this.lines = lines_map;
    this.scenes = scenes_map;

    // A theme can be an aspect (ASPECT_TYPE_COMPOUND).
    const Aspect = function(aspect) {
        this.id = ()=> { return aspect.id };
        this._aspect = aspect;
        this.coords = new Set(); // for the current scene
        this.scriptCoords = new Set();
        this.creationTime = Date.now();
        this.isAspect = true;
    }

    const Theme = function(theme,line) {
        this.id = ()=> { return theme.id }
        this._theme = theme;
        this.line = line;
        this.position = 0;
        this.creationTime = Date.now();
        this.isTheme = true;
    }

    const Shell = function(id,n,aspIds) {
        this.id = id;
        this.name_ = n;
        this.name = (str)=> {
            if(str) {
                this.name_ = str;
                for (let coord of this.coords) {
                    let theme = themes_map.get_(coord);
                    if (theme) theme.memnat.shellName(str);
                }
            } else {
                return this.name_;
            }
        }
        this.aspectIds = aspIds;
        this.coords = new Set();
        this.scriptCoords = new Set();
        this.creationTime = Date.now();
        this.isShell = true;
    }

    const Entity = function(id,n) {
        this.id = id;
        this.name = n;
        this.coords = new Set();
        this.scriptCoords = new Set();
        this.creationTime = Date.now();
        this.isEntity = true;
    }

    const Process = function(id,n,p) {
        this.id = id;
        this.name = n;
        this.path = p;
        this.raw = null;
        this.tuning = null;
        this.coords = new Set();
        this.scriptCoords = new Set();
        this.creationTime = Date.now();
        this.isProcess = true;
    }

    // All SOTs must support Sound and Vision.
    const SchoolOfThought = function(id,n,p) {
        this.id = id;
        this.name = n;
        this.path = p;
        this.raw = null;
        // TODO: There should be one of each of these per Sensation Class.
        this.getJourney = VOID;
        this.getFrequency = SOUND_MODULO;
        this.getPosition = VISION_MODULO;
        this.waveform = ()=>{return 'sine'}
        this.characterLength = SOT_CHARACTER_LENGTH_DEFAULT;
        this.creationTime = Date.now();
        this.isSOT = true;
    }

    const Line = function(id,num,elem) {
        this.id = id;
        this.number = num;
        this.elem = elem;
        this.entity = null;
        this.modQuad = null;
        this.themes = [];
        this.creationTime = Date.now();
        this.isLine = true;

        this.domEntity = ()=> {
            return this.elem.querySelector('.line-entity');
        }
        this.domNumber = ()=> {
            return this.elem.querySelector('.line-number');
        }
        this.domPrepend = () => {
            return this.elem.querySelector('.prepend-line');
        }
        this.domAppend = () => {
            return this.elem.querySelector('.append-line')
        }
        this.domAddTheme = () => {
            return this.elem.querySelector('.add-theme')
        }
        this.domRemove = () => {
            return this.elem.querySelector('.remove-line')
        }
    }

    const Scene = function(id,name,pos,lineIds) {
        this.id = id;
        this.name = name || id;
        this.position = pos || 0;
        // Only the current scene will have its lines loaded.
        // However, when the script is loaded, all aspects,
        // themes, shells, and entities will be loaded.
        // When the current scene is to be switched, all
        // other information of the new scene will be loaded
        // around the current states of the storages.
        this.lineIds = lineIds;
        this.isScene = true;
    }

    const Script = function(id,name,data) {
        this.id = id;
        this.name = name || id;
        this.data = data;
        this.player = {}
        this.processRootPath = '';
        this.creationTime = Date.now();
        this.isScript = true;
    }

    const PUBLIC_SCHOOL = new SchoolOfThought('open-standard_v0.0.1', 'Open Standard', null);
    PUBLIC_SCHOOL.getFrequency = SOUND_MODULO;
    PUBLIC_SCHOOL.getPosition = VISION_MODULO;

    const loadProcess = (aproc,load,te)=> {
        return new Promise((resolve,reject)=> {
            // try to load process mod
            let url = aproc.path,
                valid = isValidURL(url);
            if (!valid) {
                url = urlJoin(this.script.processRootPath || (packager.name === 'electron' ? 'file://' : ''), url);
                if (!isValidURL(url)) {
                    let errmsg = 'Ignoring process, '+aproc.name+':'+aproc.id+', with strange url, '+url;
                    if (te) throw new Error(errmsg);
                    reject(errmsg);
                    return;
                }
            }
            switch (packager.name) {
                case 'electron':
                    packager.request.get({url: url, encoding:'utf-8'}, (err,res,body)=> {
                        if (!err && res.statusCode === 200) {
                            aproc.raw = body;
                            if (load) {
                                try {
                                    aproc.tuning = new Function('data',body);
                                    resolve();
                                } catch (e) {
                                    aproc.tuning = null;
                                    console.error(e);
                                    let errmsg = 'An error occured while loading process, '+aproc.name+':'+aproc.id;
                                    if (te) throw new Error(errmsg);
                                    reject(errmsg);
                                }
                            } else {
                                resolve();
                            }
                        } else {
                            if (err) console.error(err);
                            let errmsg = 'An error occured while loading process, '+aproc.name+':'+aproc.id;
                            if (te) throw new Error(errmsg);
                            reject(errmsg);
                        }
                    });
                    break;
                default:
                    const xhr = new XMLHttpRequest();
                    xhr.timeout = 2000;
                    xhr.onreadystatechange = function(e) {
                        if (xhr.readyState === 4) {
                            if (xhr.status === 200) {
                                let body = xhr.response;
                                aproc.raw = body;
                                if (load) {
                                    try {
                                        aproc.tuning = new Function('data',body);
                                        resolve();
                                    } catch (e) {
                                        aproc.tuning = null;
                                        console.error(e);
                                        let errmsg = 'An error occured while loading process, '+aproc.name+':'+aproc.id;
                                        if (te) throw new Error(errmsg);
                                        reject(errmsg);
                                    }
                                } else {
                                    resolve();
                                }
                            } else {
                                reject('Got a status of '+xhr.status);
                            }
                        }
                    }
                    xhr.ontimeout = function () {
                        let errmsg = 'Timeout occured while loading process, '+aproc.name+':'+aproc.id;
                        if (te) throw new Error(errmsg);
                        reject(errmsg);
                    }
                    xhr.onerror = function(e) {
                        console.error(e);
                        let errmsg = 'An error occured while loading process, '+aproc.name+':'+aproc.id;
                        if (te) throw new Error(errmsg);
                        reject(errmsg);
                    }

                    xhr.open('get', url, true);
                    xhr.send();
            }
        });
    }
    this.loadProcess = loadProcess;

    const loadAudioBuffer = (url)=> {

        return new Promise((resolve,reject)=> {
            let valid = isValidURL(url);
            if (!valid) {
                url = urlJoin(this.script.processRootPath || (packager.name === 'electron' ? 'file://' : ''), url);
                if (!isValidURL(url)) {
                    reject('URL is weird.');
                    return;
                }
            }
            let ctx = new AudioContext();
            switch (packager.name) {
                case 'electron':
                    packager.request.get({url: url, encoding:null}, (err,res,body)=> {
                        if (!err && res.statusCode === 200) {
                            ctx.decodeAudioData(body).then((buffer)=> {
                                resolve({buffer, type: BRANE_TYPE_AUDIO})
                            }).catch((e)=> {
                                reject(e);
                            });
                        } else {
                            reject(err);
                        }
                    });
                    break;
                default:
                    const xhr = new XMLHttpRequest();
                    xhr.responseType = 'arraybuffer';
                    xhr.timeout = 2000;
                    xhr.onreadystatechange = function(e) {
                        if (xhr.readyState === 4) {
                            if (xhr.status === 200) {
                                let body = xhr.response;
                                ctx.decodeAudioData(body).then((buffer)=> {
                                    resolve({buffer, type: BRANE_TYPE_AUDIO})
                                }).catch((e)=> {
                                    reject(e);
                                });
                            } else {
                                reject( 'Got a status of '+xhr.status);
                            }
                        }
                    }
                    xhr.ontimeout = function () {
                        reject('Timeout occured');
                    }
                    xhr.onerror = function(e) {
                        reject(e);
                    }

                    xhr.open('get', url, true);
                    xhr.send();
            }
        });
    }

    const loadImage = (url)=> {
        return new Promise((resolve,reject)=> {
            let valid = isValidURL(url);
            if (!valid) {
                url = urlJoin(this.script.processRootPath || (packager.name === 'electron' ? 'file://' : ''), url);
                if (!isValidURL(url)) {
                    reject('URL is weird.');
                    return;
                }
            }
            const image = new Image();
            image.onload = () => resolve({buffer:image, type:BRANE_TYPE_IMAGE});
            image.onerror = () => reject('Failed to load image at '+url);
            image.src = url;
        });
    }

    const loadBrane = (url)=> {
        let ext = url.split('.').pop();
        switch (ext) {
            case 'png':
                return loadImage(url);
                break;
            default:
                return loadAudioBuffer(url);
        }
    }
    this.loadBrane = loadBrane;

    function CoreProcess(props) {
        this.value = props.value == undefined ? 0 : props.value;
        this.times = props.times == undefined ? 1 : props.times;
        this.total = props.total == undefined ? 0 : props.total;
        this.base = props.base = undefined ? 0 : props.base;
        this.scale = props.scale == undefined ? 1 : props.scale;
        this.stamp = props.stamp == undefined ? performance.now() : props.stamp;
        this.boundValue = props.boundValue;
        this.update = props.update;
    }
    this.CoreProcess = CoreProcess;

    const runupdcore = (core, entry, stamp)=> {
        let v = core.value,
            ototal = core.total,
            otimes = core.times,
            times = otimes+1,
            stampdiff = (stamp-core.stamp) || 1,
            sm = ototal+entry/stampdiff,
            nfq = sm/times,
            ofq = ototal/otimes,
            denom = Math.max(nfq, ofq),
            nov = denom === 0 ? 0 : (nfq-ofq)/denom;
        nov = boundFull(nov);
        // Consider the base using the normalized novelty.
            // Leap from the average with the novelty.
            // This takes care of the issue of the value
                // being 0 or stubborn to change.
        v = rangedAverage(v,core.base,(nov+1)/2)*(1+nov);
        core.value = core.boundValue(v);
        core.total = sm;
        core.times = times;
        core.stamp = stamp == undefined ? core.stamp : stamp;

        return {
            novelty: denom === 0 ? 0 : nov, //(nov+1)/2 ),
            value: core.value*core.scale
        }
    }

    this.testCore = (core, core2, entry, inc)=> {
        if (!core) {
            core = new CoreProcess({base: 10000, value:2000});
            core2 = new CoreProcess({base: 10000, value:2000});
        }
        let res = runupdcore(core, entry, null, (v)=>{return Math.range(v, SOUND_MIN_FREQUENCY, Infinity)});
        let res2 = runupdcore(core2, entry, core2.stamp+inc, (v)=>{return Math.range(v, SOUND_MIN_FREQUENCY, Infinity)});
        console.log(res.novelty, res.value);
        console.log(res2.novelty, res2.value);
        console.log('-');

        return [core, core2];
    }

    const ADAP = function(len) {
        let combo_ = [];
        this.combo = combo_;
        this.length = len || 6;
        let diversity_ = 0,
            average_ = 0;
        this.diversity = () => { return diversity_; }
        this.average = ()=> { return average_; }
        this.flexibleLength = false;
        let oldd = 0;
        let oldds = [];
        const electWithTarget = (elems, target, adjustDiversity)=> {

            while (combo_.length > this.length) combo_.shift();

            let chosen = { diff: Infinity },
                div = 0,
                diff = 0,
                nc = null,
                avg = 0,
                avgs = null,
                e = null;

            for (let idx = 0; idx < elems.length; ++idx) {

                e = elems[idx].value;

                combo_.push(e);

                nc = combo_ //normalize(combo_);
                avg = 0;
                avgs = nc.map(it => {
                    avg+=it;
                    return 0;
                });

                avg/=nc.length;

                for (let i = 0; i < nc.length-1; ++i) {
                    for (let j = i+1; j < nc.length; ++j) {
                        let a = nc[i], b = nc[j];
                        let vd = Math.abs(a-b),
                            pd = Math.abs(i-j);
                        let v = vd/pd;
                        avgs[i] = avgs[i]+v;
                        avgs[j] = avgs[j]+v;
                    }
                }

                div = avg === 0 ? 0 : avgs.reduce((acc,cv)=> {
                    return acc + cv/(nc.length-1) || 0;
                }, 0) / nc.length / avg;

                // A little indirection.
                // TODO: Justify this.
                let d = Math.abs(div-oldd);
                oldds.push(d);
                while (oldds.length > this.length) oldds.shift();
                let avg_ = oldds.reduce((acc,cv)=> {
                    return acc+cv;
                },0)/oldds.length;
                // oldd = div;
                let div_;
                let num, denom;
                // Entry among the others
                    // find its place
                if (avg_ > d) { // compares to average
                    num = d;
                    denom = avg_;
                }  else { // above average; others compared to entry; serves as model
                    num = avg_;
                    denom = d;
                }
                if (!denom) {
                    div_ = 0;
                } else {
                    div_ = num/denom;
                }
                div_ = adjustDiversity(div_, elems[idx]);
                diff = Math.abs(target-div_);
                if (diff < chosen.diff) {
                    chosen = {idx: idx, diff: diff, diff_: d, diversity: div, diversity_: div_, average: avg}
                }

                combo_.pop();
                oldds.pop();
            }

            diversity_ = chosen.diversity_;
            average_ = chosen.average;
            oldd = chosen.diversity;
            oldds.push(chosen.diff_);
            // console.log(elems, chosen);
            combo_.push(elems[chosen.idx].value);

            // // A little indirection.
            // // TODO: Justify this.
            // let d = Math.abs(diversity_-oldd);
            // oldds.push(d);
            // while (oldds.length > this.length) oldds.shift();
            // avg = oldds.reduce((acc,cv)=> {
            //     return acc+cv;
            // },0)/oldds.length;
            // oldd = diversity_;
            // let num, denom;
            // // Entry among the others
            //     // find its place
            // if (avg > d) { // compares to average
            //     num = d;
            //     denom = avg;
            // }  else { // above average; others compared to entry; serves as model
            //     num = avg;
            //     denom = d;
            // }
            // if (!denom) {
            //     diversity_ = 0;
            // } else {
            //     diversity_ = num/denom;
            // }

            return elems[chosen.idx];
        }

        const electChosen = (elems, adjustDiversity)=> {

            combo_ = combo_.concat(elems.map(e => { return e.value == undefined ? e : e.value }));
            while (combo_.length > this.length) combo_.shift();

            let nc = combo_,//normalize(combo_),
                // nc = normalize(combo_),
                avg = 0,
                avgs = nc.map(it => {
                    avg+=it;
                    return 0;
                });
            avg/=nc.length;

            for (let i = 0; i < nc.length-1; ++i) {
                for (let j = i+1; j < nc.length; ++j) {
                    let a = nc[i], b = nc[j];
                    let vd = Math.abs(a-b),
                        pd = Math.abs(i-j);
                    let v = vd/pd;
                    avgs[i] = avgs[i]+v;
                    avgs[j] = avgs[j]+v;
                }
            }

            diversity_ = avg === 0 ? 0 : avgs.reduce((acc,cv)=> {
                return acc + cv/(nc.length-1) || 0;
            }, 0) / nc.length / avg;
            average_ = avg;

            // A little indirection.
            // TODO: Justify this.
            let d = Math.abs(diversity_-oldd);
            oldds.push(d);
            while (oldds.length > this.length) oldds.shift();
            avg = oldds.reduce((acc,cv)=> {
                return acc+cv;
            }, 0) / oldds.length;
            oldd = diversity_;
            let num, denom;
            // Entry among the others
                // find its place
            if (avg > d) { // compares to average
                num = d;
                denom = avg;
            }  else { // above average; others compared to entry; serves as model
                num = avg;
                denom = d;
            }
            if (!denom) {
                diversity_ = 0;
            } else {
                diversity_ = num/denom;
            }
        }

        this.elect = (props={})=> {
            let elems = props.elements,
                target = props.target,
                modelWith = props.modelWith || [],
                adjustDiversity = props.adjustDiversity || IDENTITY;

            if (elems && (this.modelHigh || this.modelLow)) {
                let [min,max,rdiff] = range(elems.concat(modelWith));

                if (this.modelHigh) {
                    let mh = max-this.modelHigh*rdiff;
                    elems.forEach(e => {
                        let morph = Math.abs(e.value-mh)/rdiff
                        e.value += (e.value-average_)*(1-morph)
                    });
                }
                if (this.modelLow) {
                    let ml = min+this.modelLow*rdiff;
                    elems.forEach(e => {
                        let morph = Math.abs(e.value-ml)/rdiff
                        e.value += (e.value-average_)*(1-morph)
                    });
                }
            }

            if (!elems) return electChosen([], adjustDiversity);
            if (this.flexibleLength) this.length = Math.max(this.length, elems.length);
            if (target == undefined) return electChosen(elems, adjustDiversity);
            return electWithTarget(elems, target, adjustDiversity);
        }
    }
    this.ADAP = ADAP;

    function testSource(freq) {

        return new Promise((resolve, reject)=> {
            let ctx = new AudioContext();
            let ctx2 = new AudioContext();
            let source = ctx.createBufferSource();
            let channel = ctx.createGain();

            source.loop = true;
            source.connect(channel);

            let request = new XMLHttpRequest();

            request.open('GET', 'http://localhost:8000/resources/arcadian apples_L1_T3_{Theme}.wav', true);

            request.responseType = 'arraybuffer';

            request.onload = function() {
                var audioData = request.response;

                ctx2.decodeAudioData(audioData, function(buffer) {
                    let myBuffer = buffer;
                    source.buffer = myBuffer;
                    channel.connect(ctx.destination);
                    source.start();
                    resolve({source,channel});
                });
            }
            request.onerror = (e)=> {
                console.error(e);
                reject(e);
            }
            request.send();
        });
    }
    this.testSource = testSource;

    function testTone(freq) {
        let mf = 0.5,
            bj = 0.2,
            jf = 1;

        this.mf = mf;
        this.jf = jf;

        const tone = window.Tone || {context: new (window.AudioContext || window.webkitAudioContext)()}
        const context = tone.context.rawContext || tone.context;

        function Voice(freq, pos, reverb) {
            // this.osc = new Tone.Oscillator(freq || 440,'sine').sync().start(0);
            // this.volume = new Tone.Gain();
            this.osc = context.createOscillator();
            this.node.type = 'sine';
            this.node.frequency.setValueAtTime(freq || 440, context.currentTime);
            this.volume = context.createGain();
            this.volume.gain.setValueAtTime(1, context.currentTime);
            // this.flux = new Tone.Gain();
            this.flux = context.createGain();
            this.flux.gain.setValueAtTime(1, context.currentTime);
            // let ifo = new Tone.Oscillator(10,'sine').sync().start(0);
            // this.ifo = ifo;
            let ifo = context.createOscillator();
            ifo.type = 'sine';
            ifo.frequency.setValueAtTime(10, context.currentTime);
            this.ifo = ifo;
            // let depth = new Tone.Gain();
            // this.depth = depth;
            let depth = context.createGain();
            depth.gain.setValueAtTime(1, context.currentTime);
            this.depth = depth;
            // this.postgain = new Tone.Gain();
            // this.reverb = new Tone.Freeverb(0.99); // maybe = quantum = exp(1-fm)/exp(1)
            // pos = pos || [getRandomNumber(-10000,10000), getRandomNumber(-10000,10000), getRandomNumber(-10000,10000)];
            pos = pos || new Vector(getRandomNumber(-50,50), getRandomNumber(-50,50), getRandomNumber(-50,50));
            // this.panner = new Tone.Panner3D(pos.x,pos.y,pos.z);
            // this.panner.panningModel = 'HRTF';
            this.panner = context.createPanner();
            if (this.panner.positionX) {
                this.panner.positionX.setValueAtTime(pos.x, context.currentTime);
                this.panner.positionY.setValueAtTime(pos.y, context.currentTime);
                this.panner.positionZ.setValueAtTime(pos.z, context.currentTime);
            } else {
                this.panner.setPosition(pos.x,pos.y,pos.z);
            }
            this.panner.panningModel = 'HRTF';
            this.adap = new ADAP();

            this.node.connect(this.volume);
            this.volume.connect(this.flux);
            this.ifo.connect(this.depth);
            this.depth.connect(this.flux.gain);
            this.flux.connect(this.panner);
            this.panner.connect(reverb);
            // this.panner.connect(context.destination);

            this.range = (s,e)=> {
                let d = (e-s)/2;
                this.depth.gain.setValueAtTime( Math.abs(d), context.currentTime );
                this.flux.gain.setValueAtTime( (d+s+1)/2, context.currentTime );
            }

            this.start = ()=> {
                this.ifo.start(0);
                this.node.start(0);
            };
            this.stop = ()=> {
                // this.reverb.roomSize.value = 0;
                // this.reverb.wet.value = 0;
                this.node.stop();
                this.ifo.stop();
                this.flux.gain.setValueAtTime(1, context.currentTime);
                this.depth.gain.setValueAtTime(1, context.currentTime);
                // this.postgain.gain.setValueAtTime(1, context.currentTime);
            };
        }

        let voices = [];

        // NOTE: stop the reverb before stopping the oscillator.
        // NOTE: create own abstraction of Oscillator, Loop
        // TODO: create an issue on github with a version of this that uses random numbers
        // let //osc = new Tone.Oscillator(freq || 440,'sine').sync().start(0),
            // gain = new Tone.Gain(),
            // postgain = new Tone.Gain(),
            // panner = new Tone.Panner3D(-3,-3,-7),
            // freeverb = new Tone.Freeverb(1).toMaster(); // maybe = quantum = exp(1-fm)/exp(1)

        // Tone.Listener.forwardX = 1;
        // Tone.Listener.forwardY = 1;
        // Tone.Listener.forwardZ = 1;
        // Tone.Listener.upX = 0.5773502691896258;
        // Tone.Listener.upY = 0.5773502691896258;
        // Tone.Listener.upZ = 0.5773502691896258;
        // let reverbNode = freeverb, inputNumber;
        // while (Tone.isDefined(reverbNode.input)){
    	// 	if (Tone.isArray(reverbNode.input)){
    	// 		inputNumber = Tone.defaultArg(inputNumber, 0);
    	// 		reverbNode = reverbNode.input[inputNumber];
    	// 		inputNumber = 0;
    	// 	} else if (reverbNode.input){
    	// 		reverbNode = reverbNode.input;
    	// 	}
    	// }
        // this.reverb = reverbNode;
        // this.reverbT = freeverb;

        this.reverb = new Freeverb(context, {
            roomSize: 0.9,
            dampening: 1000,
            wet: 1,
            dry: 0
        });
        this.reverb.connect(context.destination);
        function orientAspect(idx,quantum) {

            let res = {};

            switch (idx) {
                case 0:
                    res.position = new Vector(0,0,0);
                    res.position2D = new Vector(0,0);
                    res.color = 0x000000; // black
                    break;
                case 1:
                    res.position = new Three.Vector3(0,1,0);
                    res.position2D = new Vector(0,1);
                    res.color = 0xff0000; // red
                    break;
                case 2:
                    res.position = new Three.Vector3(0,1,1);
                    res.position2D = new Vector(SQRT3/2, 0.5);
                    res.color = 0xdde84f; // gold
                    break;
                case 3:
                    res.position = new Three.Vector3(0,0,1);
                    res.position2D = new Vector(SQRT3/2, -0.5);
                    res.color = 0xca4fe8; // purple
                    break;
                case 4:
                    res.position = new Three.Vector3(1,0,1);
                    res.position2D = new Vector(0, -1);
                    res.color = 0x0000ff; // blue
                    break;
                case 5:
                    res.position = new Three.Vector3(1,0,0);
                    res.position2D = new Vector(-SQRT3/2, -0.5);
                    res.color = 0xe88e4f; // orange
                    break;
                case 6:
                    res.position = new Three.Vector3(1,1,0);
                    res.position2D = new Vector(-SQRT3/2, 0.5);
                    res.color = 0x00ff00; // green
                    break;
                case 7:
                    res.position = new Vector(1,1,1);
                    res.position2D = new Vector(0,0);
                    res.color = 0xb6b6b6; // grey
                    break;
                default:

            }

            res.position.multiplyScalar(quantum);
            res.position2D.multiplyScalar(quantum);
            return res;
        }
        let freqs = [440, 10219.523972168125, 12721.800045791117, 13212.295193309192, 11629.114916047774, 6880.013852074276];
        for (let vi = 0; vi < 6; ++vi) {
            let orientation = orientAspect(vi+1,50);
            voices.push(new Voice(getRandomNumber(100,400), orientation.position, context.destination));
        }
        let listener = context.listener;
        listener.positionX.setValueAtTime(0, context.currentTime);
        listener.positionY.setValueAtTime(0, context.currentTime);
        listener.positionZ.setValueAtTime(0, context.currentTime);
        listener.forwardX.setValueAtTime(0.5773502691896257, context.currentTime);
        listener.forwardY.setValueAtTime(0.5773502691896257, context.currentTime);
        listener.forwardZ.setValueAtTime(0.5773502691896257, context.currentTime);
        listener.upX.setValueAtTime(-0.4082482904638629, context.currentTime);
        listener.upY.setValueAtTime(0.8164965809277261, context.currentTime);
        listener.upZ.setValueAtTime(-0.4082482904638629, context.currentTime);

        let stats = new Stats(),
            stamps = voices.map(()=> 0),
            enddts = voices.map(()=> 0),
            loop = ()=> {
                // TODO: need to track the cycles to perform any needed
                // updates to all in a cycle. Each aspect and theme has
                // its own cycle. Aspects that finish a cycle before that
                // of their parents will be respawned again in that
                // cycle. (The Earth will be rebuilt should it be destroyed
                // before the Sun dies.) Should an aspects of a parent cycle
                // outlast that cycle, it will be flung into trajectory to
                // make space for a newer version of itself in the new
                // universal cycle.
                // Going through the sound tree, for each theme, if its
                // duration is used up, free all immediate children. Begin a
                // new cycle (increment `cycle` and generate a new `cycleId`).
                // Add those hurled systems and aspects to a collection of
                // `rogues` to have their position updated according to their
                // forward tracjectories. Be sure to remove themes from the
                // collection when all children are hurled. Note that a system
                // will move in the trajectory of its conductor, then when its
                // duration is used up, any lingering aspects will move along
                // their own trajectories set by their orientation in their
                // now-diminished parent.
                // NOTE: the choruses will need to move with aspects.
                // Manage cycle updates with a Tone.Timeline.
                // TODO: Oscillate every M->F second
                stats.begin();

                // let now = transport.seconds;
                let now = Date.now()/1000;

                // console.log(voices[0].flux.gain.value);

                for (let vi = 0; vi < voices.length; ++vi) {
                    let osc = voices[vi],
                        dt = now - stamps[vi],
                        enddt = enddts[vi],
                        flux = osc.flux,
                        // postgain = osc.postgain,
                        adap = osc.adap;

                    if (dt >= enddt) {
                        flux.gain.setValueAtTime(1, context.currentTime);
                        // postgain.gain.setValueAtTime(1, context.currentTime);
                    }

                    if (dt >= this.mf) {
                        stamps[vi] = now;

                        // adap.elect({
                        //     elements: [getRandomNumber(2000000, 6000000), getRandomNumber(1000, 3000), getRandomNumber(5, 10)]
                        // });
                        adap.elect({
                            elements: [Math.random(),getRandomNumber(0, 3),Math.random()]
                        });
                        let div = Math.range(adap.diversity(), 0, 1),
                            dip = 1-div*this.jf;
                        enddts[vi] = this.mf*div/2;
                        // console.log('osc'+vi,div,enddt);
                        flux.gain.setValueAtTime(1, context.currentTime);
                        // postgain.gain.setValueAtTime(dip*8, context.currentTime);
                    }
                }

                stats.end();
            }
        stats.showPanel(1);
        let sps = document.getElementById('statsPanels');
        stats.dom.classList.add('stats', 'float-left', 'mr-2');
        sps.appendChild(stats.dom);
        $(stats.dom).css('position','static');

        // freeverb.wet.value = 1;
        // freeverb.dampening.value = 500; // determined by F->J relation and size of the world
        // osc.chain(gain, panner, freeverb, Tone.Master);

        let updateInterval = 0.03;
        let blob = new Blob([
			//the initial timeout time
			"let timeoutTime = "+(updateInterval * 1000).toFixed(1)+";" +
			//onmessage callback
			"self.onmessage = function(msg){" +
			"	timeoutTime = parseInt(msg.data);" +
			"};" +
			//the tick function which posts a message
			//and schedules a new tick
			"function tick(){" +
			"	setTimeout(tick, timeoutTime);" +
			"	self.postMessage('tick');" +
			"}" +
			//call tick initially
			"tick();"
		]);
		let blobUrl = URL.createObjectURL(blob);
		let worker = new Worker(blobUrl);

		worker.onmessage = (e)=> {
            loop();
        }

        let start = ()=> {
            var worker = new Worker(blobUrl);

    		worker.onmessage = (e)=> {
                loop();
            }
            // stamp = time;
            voices.map(v => v.start());
            console.log(voices);
            // loop();
        }
        start();

        let stop = ()=> {
            this.reverb.roomSize.value = 0;
            this.reverb.wet = 0;
            voices.map(v => v.stop());
            worker.terminate();
        }

        this.voices = voices;
    }
    this.testTone = testTone;

    function Release(pos,dest,fb,mf) {
        this.destination = dest;
        this.position = pos;
        this.mf = mf || 1;
        this.fb = fb || 0;

        this.getPosition = (gtime)=> {

            // gtime*=this.mf;

            let time,x,y,z;
            let depart_endt = this.mf*this.fb/2,
                // sustain_endt = depart_endt+this.mf-depart_endt*2;
                // assuming mf is always 1,
                sustain_endt = this.fb/2+1-this.fb;

            if (gtime < depart_endt) {
                x = (this.destination.x-this.position.x)*depart_endt+this.position.x;
                y = (this.destination.y-this.position.y)*depart_endt+this.position.y;
                z = (this.destination.z-this.position.z)*depart_endt+this.position.z;
                let depart_end = new Three.Vector3(x,y,z);
                time = Math.exp(gtime)/Math.exp(1)*gtime;
                x = (depart_end.x-this.position.x)*time+this.position.x;
                y = (depart_end.y-this.position.y)*time+this.position.y;
                z = (depart_end.z-this.position.z)*time+this.position.z;
            } else if (gtime < sustain_endt) {
                x = (this.destination.x-this.position.x)*sustain_endt+this.position.x;
                y = (this.destination.y-this.position.y)*sustain_endt+this.position.y;
                z = (this.destination.z-this.position.z)*sustain_endt+this.position.z;
                let sustain_end = new Three.Vector3(x,y,z);
                x = (sustain_end.x-this.position.x)*gtime+this.position.x;
                y = (sustain_end.y-this.position.y)*gtime+this.position.y;
                z = (sustain_end.z-this.position.z)*gtime+this.position.z;
            } else {
                time = Math.exp(gtime)/Math.exp(1)*gtime;
                x = (this.destination.x-this.position.x)*time+this.position.x;
                y = (this.destination.y-this.position.y)*time+this.position.y;
                z = (this.destination.z-this.position.z)*time+this.position.z;
            }

            return new Three.Vector3(x,y,z);
        }

    }
    this.Release = Release;

    // This will set the position, up, and forward vectors of the
        // listener to match the destination while embarking.
    function ReleaseT(begin,dest,fb) {
        this.begin = begin;
        this.destination = dest;
        this.fb = fb || 0;

        this.lerp = (gtime)=> {

            let time;
            let px,py,pz;
            let ux,uy,uz;
            let fx,fy,fz;
            let depart_endt = this.fb/2,
                sustain_endt = this.fb/2+1-this.fb;

            if (gtime < depart_endt) {
                time = Math.exp(gtime)/Math.exp(1)*gtime;
                px = (this.destination.position.x-this.begin.position.x)*depart_endt*time+this.begin.position.x;
                py = (this.destination.position.y-this.begin.position.y)*depart_endt*time+this.begin.position.y;
                pz = (this.destination.position.z-this.begin.position.z)*depart_endt*time+this.begin.position.z;

                ux = (this.destination.up.x-this.begin.up.x)*depart_endt*time+this.begin.up.x;
                uy = (this.destination.up.y-this.begin.up.y)*depart_endt*time+this.begin.up.y;
                uz = (this.destination.up.z-this.begin.up.z)*depart_endt*time+this.begin.up.z;

                fx = (this.destination.forward.x-this.begin.forward.x)*depart_endt*time+this.begin.forward.x;
                fy = (this.destination.forward.y-this.begin.forward.y)*depart_endt*time+this.begin.forward.y;
                fz = (this.destination.forward.z-this.begin.forward.z)*depart_endt*time+this.begin.forward.z;
            } else if (gtime < sustain_endt) {
                px = (this.destination.position.x-this.begin.position.x)*sustain_endt*gtime+this.begin.position.x;
                py = (this.destination.position.y-this.begin.position.y)*sustain_endt*gtime+this.begin.position.y;
                pz = (this.destination.position.z-this.begin.position.z)*sustain_endt*gtime+this.begin.position.z;

                ux = (this.destination.up.x-this.begin.up.x)*sustain_endt*gtime+this.begin.up.x;
                uy = (this.destination.up.y-this.begin.up.y)*sustain_endt*gtime+this.begin.up.y;
                uz = (this.destination.up.z-this.begin.up.z)*sustain_endt*gtime+this.begin.up.z;

                fx = (this.destination.forward.x-this.begin.forward.x)*sustain_endt*gtime+this.begin.forward.x;
                fy = (this.destination.forward.y-this.begin.forward.y)*sustain_endt*gtime+this.begin.forward.y;
                fz = (this.destination.forward.z-this.begin.forward.z)*sustain_endt*gtime+this.begin.forward.z;
            } else {
                time = Math.exp(gtime)/Math.exp(1)*gtime;
                px = (this.destination.position.x-this.begin.position.x)*time+this.begin.position.x;
                py = (this.destination.position.y-this.begin.position.y)*time+this.begin.position.y;
                pz = (this.destination.position.z-this.begin.position.z)*time+this.begin.position.z;

                ux = (this.destination.up.x-this.begin.up.x)*time+this.begin.up.x;
                uy = (this.destination.up.y-this.begin.up.y)*time+this.begin.up.y;
                uz = (this.destination.up.z-this.begin.up.z)*time+this.begin.up.z;

                fx = (this.destination.forward.x-this.begin.forward.x)*time+this.begin.forward.x;
                fy = (this.destination.forward.y-this.begin.forward.y)*time+this.begin.forward.y;
                fz = (this.destination.forward.z-this.begin.forward.z)*time+this.begin.forward.z;
            }

            return {
                position: {x:px,y:py,z:pz},
                up: {x:ux,y:uy,z:uz},
                forward: {x:fx,y:fy,z:fz}
            };
        }

    }
    this.ReleaseT = ReleaseT;

    function downloadTone(secs) {
        // May need to pad samples at the extremes to curb extra clicking.
        let t = new testTone();
        let rec = new Recorder(t.reverb);
        rec.record();
        setTimeout(()=> {
            rec.stop();
            rec.exportWAV((file)=> {
                download(file,'lobby_music.wav','audio/wav');
            });
        }, secs*1000);
    }
    this.downloadTone = downloadTone;

    // `getFrequency` is a function which takes an itinerary ID to provide the current
        // frequency of an aspect/voice.
    const processItinerary = (plan, raw_itinerary, itinerary, adap, getFrequency=VOID)=> {
        itinerary.length = [];

        if (plan) {
            raw_itinerary = raw_itinerary.planned || raw_itinerary;

            // SOT
            // SOT TODO: allow traveling between all sub systems regardless of level

            // Apply filters to themes. Themes are made to look more like the average of the diversity
            // combo in fundamental frequency according to how far away from the highest fundamental
            // they are (M->B) or how far away from the lowest fundamental they are (F->J).
            // This is to say that ADAP has a higher tendency to choose those who differ from
            // the average the most.

            // SOT: Traveling methods used below are per level.

            // This assumes all are implicitly connected, allowing the traveler to move between
                // any two systems on the same level in the grand theme.
            // SOT: this method treats all systems as worlds that can be visited regardless of the
                // orientation of their frequencies.

            for (let i = 0, candidates = raw_itinerary[i]; candidates != null; ++i, candidates = raw_itinerary[i]) {
                let pures = [],
                    comps = [];
                candidates.forEach(cid => {
                    let c_ = raw_itinerary[cid],
                        c = Object.assign({}, c_),
                        val = getFrequency(cid);
                    c.value = val == undefined ? c.value : val;
                    if (c.aspectType === M.ASPECT_TYPE_PURE) {
                        pures.push(c.value); // In theory, this is allowed to be negative.
                    } else {
                        comps.push(c);
                    }
                });

                if (comps.length) {
                    // adap.length = Math.max(adap.length, comps.length); // grow as you go
                    // TODO: FIX: Negative frequency themes should 'push' the traveler.
                    let next = adap.elect({
                        elements: comps,
                        target: plan,
                        modelWith: pures
                    });
                    // delete next.aspectType;
                    // delete next.nextIds;
                    // delete next.value;
                    itinerary.push(next);
                }
            }
            // NOTE: it seems things tend towards stagnation unless diversity requirements change explicitly
                // TRY: two or more ADAPs which talk to one another
            // console.log(adap.diversity());

            // TODO: use models as in above method
            // This allows the possibility of the player getting stuck at a system.
                // Travel is possible if and only if there is a compound aspect in the current theme.
            // let next = raw_itinerary[props_.itid],
            //     nextIds = next.nextIds;
            // let itres = [];
            // while (next) {
            //     // adap.length = Math.max(adap.length, nextIds.length); // grow as you go
            //     next = adap.elect({
            //         elements: nextIds.map( nid => raw_itinerary[nid] ),
            //         target: plan
            //     });
            //     if (next) {
            //         nextIds = next.nextIds;
            //         // delete next.aspectType;
            //         // delete next.nextIds;
            //         // delete next.value;
            //         itinerary.push(next);
            //     }
            // }
        } else {
            raw_itinerary = raw_itinerary.floater || raw_itinerary;
            // NOTE: The current SOT has it so that a level could have zero average (?)
                // In any case, if the average count is 0,
                // the traveler stays at the current junction.
            for (let i = 0, itn_ = raw_itinerary[i]; itn_ != null; ++i, itn_ = raw_itinerary[i]) {
                let itn = {
                    position: new Three.Vector3(0,0,0),
                    up: new Three.Vector3(0,0,0),
                    forward: new Three.Vector3(0,0,0)
                }

                if (itn_.count) { // ?
                    itn.position.copy(itn_.posAvg).divideScalar(itn_.count);
                    itn.forward.copy(itn_.fAvg).divideScalar(itn_.count).normalize();
                    itn.up.copy(itn_.uAvg).divideScalar(itn_.count).normalize();

                    itinerary.push(itn);
                } else if (itinerary[i-1]) {
                    itinerary.push(itinerary[i-1]);
                }
            }
        }

        return itinerary;
    }

    const getJourney = async (_props_)=> {
        let itineraries_ = {
                planned: {},
                floater: {}
            },
            pos_ch = _props_.alterPositions, // SOT
            mq = {
                mf: _props_.modQuad ? _props_.modQuad.mf.value : 0,
                fm: _props_.modQuad ? _props_.modQuad.fm.value : 0,
                bj: _props_.modQuad ? _props_.modQuad.bj.value : 0,
                jb: _props_.modQuad ? _props_.modQuad.jb.value : 0,
                mb: _props_.modQuad ? _props_.modQuad.mb.value : 0,
                bm: _props_.modQuad ? _props_.modQuad.bm.value : 0,
                fb: _props_.modQuad ? _props_.modQuad.fb.value : 0,
                bf: _props_.modQuad ? _props_.modQuad.bf.value : 0,
                mj: _props_.modQuad ? _props_.modQuad.mj.value : 0,
                jm: _props_.modQuad ? _props_.modQuad.jm.value : 0,
                fj: _props_.modQuad ? _props_.modQuad.fj.value : 0,
                jf: _props_.modQuad ? _props_.modQuad.jf.value : 0,
            }

        let props_ = {
            voice: _props_.voice,
            // Could be used to assign specific tuning to theme if they are a part of another
            type: _props_.type,
            itid: GRAND_CONDUCTOR, // itinerary ID

            itineraries: itineraries_,

            scene: new Three.Scene()
        }
        Object.assign(props_, mq);

        console.log((await this.measureSystem(_props_.voice)));

        // TODO: quantum should be relative to the world unit, which should be a theme control
            // also return the systems without compression applied
        // props_.quantum = Math.exp(1-props_.fm)/Math.exp(1); // minimum size of quantum is 1/e, max is -/1-- the unit
        // props_.quantum = 15;

        const reuse_vec = new Vector();
        const meanings = {};
        const PLAN = props_.bm;

        let res = await getSoundTree_.bind(this)(props_);
        res = Object.assign(new Journey(), res);
        // res.leftChorus = [];
        // res.rightChorus = [];
        res.itinerary = [];
        res.modQuad = _props_.modQuad;
        res.scene = props_.scene;
        res.id = _props_.id;
        res.intensity = SOUND_INTENSITY_MAX; // for compatibility with laying out systems for play

        if ( (res.type === VOICE_TYPE_ASPECT) && (res.aspectType === M.ASPECT_TYPE_PURE) ) {
            res.itinerary.push({
                position: res.position,
                up: res.up,
                forward: res.forward
            });

            return res;
        }

        let mpid = this.script.player.masterId;

        // Grand conductor takes from Player to setup
        res.frequency = this.script.player.frequency;
        res.intensity = this.script.player.intensity*SOUND_INTENSITY_MAX;
        // res.duration = this.script.player.duration;

        res.superposition = new Vector(0,0,0);
        tintinnabula(res, res.mesh, res.itid, res.quantum, res.scene);
        let _info_ = {radius:0}
        deduceFrequencies(res, 0, new Vector(0,0,0), _info_);
        res.radius = _info_.radius;

        let player = {
            itid: PLAYER,
            simCoords: [0],
            master: mpid ? await processes_map.get(mpid) : null,
            frequency: this.script.player.frequency,
            intensity: this.script.player.intensity*SOUND_INTENSITY_MAX,
            duration: this.script.player.duration,
            period: res.period,
            width: res.width,
            depth: res.depth,
            IF: res.IF,
            superposition: res.superposition.clone()
        }

        res.player = player;

        let coords = player.simCoords.slice(0);
        coords.push(0);
        let meaning = await reduceMeanings(res, coords, new Automation({
            frequency: res.frequency,
            intensity: res.intensity,
            period: res.period
        }));

        res.simulation = {
            process: {
                id: player.itid,
                name: PLAYER,
                raw: player.master ? player.master.raw : null,
                subProcesses: [meaning.process]
            },
            automation: {
                nodeId: player.itid,
                automation: new Automation({
                    frequency: player.frequency,
                    intensity: player.intensity,
                    period: player.period
                }),
                subAutomations: [meaning.automation]
            }
        }

        // Gather itinerary

        let adap = new ADAP();
        res.bmADAP = adap;
        adap.flexibleLength = true;
        adap.modelHigh = props_.mb;
        adap.modelLow = props_.fj;

        function Network(nodes) {
            this.nodes_ = nodes;
            this.nodes = [];
            this.models = [];
            nodes.forEach((n,idx) => {
                if (n.aspectType === M.ASPECT_TYPE_PURE) {
                    n._idx = idx;
                    this.models.push(n);
                } else {
                    n._idx = idx;
                    n.vfreq = new CoreProcess({base:1, boundValue:BOUND_POSITIVE});
                    this.nodes.push(n);
                }
            });
            this.adap = new ADAP(this.nodes.length-1);
            this.current = 0;

            // plan: {diversity, modelHigh, modelLow}
            this.travel = (plan, ct)=> {
                if (ct == undefined) {
                    console.warn('The Network refuses to progress until it is given a time stamp.');
                    return this.nodes[this.current];
                }
                if (this.nodes.length === 1) return this.nodes[this.current];
                let s = this.nodes[this.current];
                this.current = (this.current+1)%this.nodes.length;
                // console.log(this.nodes_);
                let [min,max,rdiff] = range(this.nodes_),
                    mh = max - plan.modelHigh * rdiff,
                    ml = min + plan.modelLow * rdiff;
                let e = this.adap.elect({
                    elements: this.nodes,
                    target: plan.diversity,
                    adjustDiversity: (v, e)=> {
                        // if (e.itid === s.itid) return Infinity;
                        let vf = e.vfreq.value,
                            h = Math.abs(e.value-mh)/rdiff,
                            l = Math.abs(e.value-ml)/rdiff,
                            morph = rdiff === 0 ? 0 : (h+l)/2;
                            // morph = (h+l)/2;
                        v+=(v-plan.diversity)*(1-morph*(1-vf));
                        // console.log(v, plan.diversity, e.value, vf, rdiff, morph);
                        return v;
                    }
                });

                // update current's VFreq
                let res = runupdcore(e.vfreq, 1, ct);

                return e;
            }
        }
        res.network = new Network(itineraries_.systems);
        res.rawItinerary = itineraries_;
        res.processItinerary = processItinerary;

        processItinerary(PLAN, itineraries_, res.itinerary, res.bmADAP);

        return res;

        function Journey() {}

        // Aggregate fundamentals of themes and use to assign frequencies
            // to choir members.
        // Calculate the percentage each compound's frequency is out of parent,
            // Apply M->B & F->J filters to each level by comparing percentages
            // with pure aspects' frequency values.
        // SOT: The waves are summed from the outside in.
            // The below has it that the deeper an aspect is situated,
            // the more of the silent turbulence it experiences.This is
            // to say, those on the outer edges of the universe act as
            // pins.
        function deduceFrequencies(theme,fsum,upsum,info) {
            let bm = theme.width,
                bn = theme.depth,
                mm = Math.abs(bm);
            let zeros = Bessel.getZeros(mm,bn),
                radius = theme.quantum/2;
            let cf = /* theme.fundamental+fsum */ Math.abs(theme.fundamental)+fsum;

            // Depending on school, aspects may be filtered relative
            // to their immediate parent theme, their level, the entire
            // world of aspects, or some other scope.
            theme.chorusFundamental = cf;
            theme.frequency = theme.frequency || theme.fundamental/fsum*100;
            theme.IF = Math.pow(theme.frequency,theme.fundamental);
            tintinnabula(theme, cf);
            // XXX: This is the percentage of any medium's frequency range to represent
                // a system with a frequency that is outside that medium's range.
            // I noticed that the perpetuity in [0,20] is smaller than that of [20000,...].
                // Negative frequencies would only fallback to their positive counterparts.
                // I assume the laws of taming such differences in perpetuities is to 'bend time'
                // i.e. use an exponential curve to create matching perpetuities.
            // This is a systematic means of dealing with systems that vibrate outside the
                // decided limits across all sensation ranges.
                // Simply modulating (e.g. %20000+20) would seem random against the desire to
                    // keep the similarity (e.g. octave) during the change.
            // console.log(bm,bn,theme.fundamental,theme.frequency,Math.log(theme.frequency)/Math.E);

            // NOTE: the outermost theme will have a frequency of Infinity and
                // a period of 0 so as to say what goes on outside of this theme
                // is beyond the scope of this experience. Its aspects are the
                // only reliable constants.
                // I think this is about the way it should be. The edge of the universe
                // being least comprehensible. This also encourages the writer to create
                // themes with the story in mind foremost, let the theme be reused as an
                // aspect in another theme, and only from a parent shell can that theme be
                // studied. This will train us to let the sound be as the story implies.
            theme.period = 1000/theme.frequency; // how much of its time in our second

            itineraries_.planned[theme.itid].value = theme.frequency;
			
			theme.asPure = theme.orchestra[0].asPure;

            for (let ci = 1; ci < 7; ++ci) {
                let c = theme.orchestra[ci],
                    pos = orientAspect(ci, theme.quantum).position2D,
                    r = Math.sqrt(pos.x*pos.x+pos.y*pos.y),
                    phase = bm*Math.atan2(pos.y,pos.x),
                    angular = /* bm < 0 ?  Math.sin(phase) : */ Math.cos(phase), // bm is always positive
					// what was commented out forces points outside a circular dimension, such as the edges of a rectangle, to have a (0,0,0) superposition
                    y = /*r > radius ? 0 : */ Bessel.Jn(mm, r*zeros[bn-1]/radius)*angular;

                // c.duration = c.duration || theme.duration;
                c.superposition = theme.up.clone().multiplyScalar(y*theme.quantum).add(upsum);

                pos = c.position;
                let dist = Math.sqrt((pos.x*pos.x)+(pos.y*pos.y)+(pos.z*pos.z))
                if (dist > info.radius) info.radius = dist;

                if (c.aspectType === M.ASPECT_TYPE_COMPOUND) {
                    // let upsum_ = theme.up.clone().multiplyScalar(y*theme.quantum).add(upsum);
                    deduceFrequencies(c, cf, c.superposition, info);
                } else {
                    // SOT: Identity Frequency can be freq*fund, freq^fund, etc
                        // Useful for J->F gain oscillation i.e. an LFO
                    c.IF = Math.pow(c.frequency,theme.fundamental);
                    // From calculating period of a system.
                    // c.period = 1000/c.frequency;
                    c.period = theme.period; // adopted/bound/sewn/it 'moves' with the system
                    c.chorusFundamental = theme.chorusFundamental;
                    tintinnabula(c, cf);
                }
            }

            return info;
        }

        async function reduceMeanings(theme, coords, automation) {

            // For each system, deduce the final effect of arrows on each aspect's being
            let m = [],
                m_ = meanings[theme.voiceId];

            theme.meaning = m;

            // NOTE: need to do this for each system because frequencies can differ
                // even for two systems which are identical but in different places
                // in the journey.
            switch (theme.themeType) {
                case M.THEME_TYPE_SIGNAL: // calculate each aspect's frequency after detuning
                    for (let i = 0; i < n_ss.length; ++i) {
                        let ni = n_ss[i],
                            freq = theme.orchestra[i+1].frequency,
                            intst = theme.orchestra[i+1].intensity,
                            autom = new Automation({
                                frequency: freq,
                                intensity: intst,
                                period: theme.period
                            }, {
                            // Detuning is done according to the fundamental of the system
                                // so as to say things change along the workings of their system.
                            // SOT: Implications of detuning will be left as one level deep. However,
                                // if it is to go to the depths, the detune amount will need to
                                // passed on as a parameter in `reduceMeanings` and child themes
                                // will need to be handled after the parent. The provided detune
                                // amount can be averaged with the current detune amount to get
                                // the actual amount.
                                // As it is, the change in frequency for themes is useful for satellites,
                                    // for example.
                            analogo: { value: freq, ratio: Math.pow(theme.fundamental, (m_[ni][M.ARROW_TYPE_ACKNOWLEDGEMENT]-0.5)/0.5) } // TODO: FIX so ratio is within [0,1]
                        });
                        m.push(autom);
                        // console.log('CHECK', theme.fundamental, m_[ni], (m_[ni][M.ARROW_TYPE_ACKNOWLEDGEMENT]-0.5)/0.5);
                        theme.orchestra[i+1].automation = autom;
                    }
                    break;

                // For Versus, for each aspect,
                    // calculate the frequency after applying the Similarity average
                        // with the average of the other frequencies
                    // ...
                case M.THEME_TYPE_VERSUS:
                    for (let i = 0; i < n_ss.length; ++i) {
                        let ni = n_ss[i],
                            freq = theme.orchestra[i+1].frequency,
                            intst = theme.orchestra[i+1].intensity,
                            mo = m_[ni],
                            mo_ = {};
                        M.VERSUS_ARROWS.forEach(arr => {
                            let moa = mo[arr];
                            mo_[arr] = {};
                            if (moa.count) {
                                mo_[arr].avg = moa.sum/moa.count;
                                // Calculate the average frequency of its orients.
                                let fsum = 0;
                                moa.fsum.forEach(oi => fsum+=theme.orchestra[oi+1].frequency);
                                mo_[arr].favg = fsum/moa.count;
                            } else {
                                mo_[arr].avg = 0;
                                mo_[arr].favg = freq; // hmm...this is for all arrow types
                            }
                        });
                        let sim = mo_[M.ARROW_TYPE_SIMILARITY];
                        // TODO: more frequency and intensity related arrows

                        let autom = new Automation({
                            frequency: freq,
                            intensity: intst,
                            period: theme.period,
                            arrows: mo
                        }, {
                            analogo: { value: rangedAverage(freq, sim.favg, sim.avg) },
                        });
                        m.push(autom);
                        theme.orchestra[i+1].automation = autom;
                    }
                    break;
                // For Context, for each aspect,
                    // calculate the frequency after applying the Similarity average
                        // with the average of the other frequencies
                    // ...
                case M.THEME_TYPE_CONTEXT:
                    for (let i = 0; i < n_ss.length; ++i) {
                        let ni = n_ss[i],
                            freq = theme.orchestra[i+1].frequency,
                            intst = theme.orchestra[i+1].intensity,
                            mo = m_[ni],
                            mo_ = {};
                        M.CONTEXT_ARROWS.forEach(arr => {
                            let moa = mo[arr];
                            mo_[arr] = {};
                            if (moa.count) {
                                mo_[arr].avg = moa.sum/moa.count;
                                // Calculate the average frequency of its orients.
                                let fsum = 0;
                                moa.fsum.forEach(oi => fsum+=theme.orchestra[oi+1].frequency);
                                mo_[arr].favg = fsum/moa.count;
                            } else {
                                mo_[arr].avg = 0;
                                mo_[arr].favg = freq; // hmm...this is for all arrow types
                            }
                        });
                        let sim = mo_[M.ARROW_TYPE_SIMILARITY],
                            cau = mo_[M.ARROW_TYPE_CAUSATION],
                            dep = mo_[M.ARROW_TYPE_DEPENDENCE],
                            spe = mo_[M.ARROW_TYPE_SPECTRUM];

                        let autom = new Automation({
                            frequency: freq,
                            intensity: intst,
                            period: theme.period,
                            arrows: mo
                        }, {
                            analogo: { value: rangedAverage(freq, sim.favg, sim.avg) },
                            // NOTE: if the average is 0 such that there were 0 type arrows
                                // from this aspect, the effect would occur at the very
                                // beginning of the voice's lifetime as expected, and the
                                // result will be identical to the frequency alone.
                            portamento: {
                                value: cau.favg,
                                time: cau.avg
                            },
                            glissando: {
                                value: dep.favg,
                                time: dep.avg
                            },
                            vibrato: {
                                value: spe.favg,
                                depth: spe.avg
                            }
                        });
                        m.push(autom);
                        theme.orchestra[i+1].automation = autom;
                    }
                    break;
                default:

            }


            let subProcesses = [],
                subAutomations = [];

            let sum_freq = 0;
            let c_meaning, c_process, c_automation;
            let cidx;
            for (let ci = 1; ci < 7; ++ci) {
                let c = theme.orchestra[ci];
                cidx = ci-1;
                c.simCoords = coords.slice(0);
                c.simCoords.push(cidx);

                for (let chi = 1; chi < 7; ++chi) {
                    let chor = c.leftChorus[chi];

                    chor.simCoords = coords.slice(0);
                    chor.simCoords.push(cidx+chi); // NOTE: the extra 1 added in chi

                    chor = c.rightChorus[chi];
                    chor.simCoords = coords.slice(0);
                    chor.simCoords.push(cidx+chi+6);
                }

                if (c.aspectType === M.ASPECT_TYPE_COMPOUND) {
                    c_meaning = await reduceMeanings(c, c.simCoords, theme.meaning[cidx]);
                    subProcesses.push(c_meaning.process);
                    subAutomations.push(c_meaning.automation);

                    sum_freq += c.avgPureFreq;
                } else {
                    c_process = await processes_map.get(c.processId);
                    c_process = {
                        id: c.itid,
                        name: c_process ? c_process.name : 'Void',
                        raw: c_process ? c_process.raw : null
                    }
                    delete c.processId;
                    subProcesses.push(c_process);

                    c_automation = {
                        nodeId: c.itid,
                        automation: theme.meaning[cidx],
                    }
                    subAutomations.push(c_automation);

                    sum_freq += c.frequency;
                }
            }

            theme.simCoords = coords;

            theme.avgPureFreq = sum_freq/6;

            let proc = await processes_map.get(theme.processId);
            delete theme.processId;

            return {
                process: {
                    id: theme.itid,
                    name: proc ? proc.name : 'Void',
                    raw: proc ? proc.raw : null,
                    subProcesses: subProcesses
                },
                automation: {
                    nodeId: theme.itid,
                    automation: automation,
                    subAutomations: subAutomations
                }
            }
        }

        function Automation(props, fprops = {}, iprops = {}) {
            let freq = props.frequency,
                intst = props.intensity,
                pd = props.period,
                arrows = props.arrows;

            this.frequency = {
                analogo: {
                    value: freq,
                },
                portamento: {
                    value: freq,
                    time: 0
                },
                glissando: {
                    value: freq,
                    time: 0
                },
                vibrato: {
                    value: freq,
                    depth: 0
                }
            }
            this.intensity = {
                value: intst,
            }
            this.period = pd;
            this.arrows = arrows;
            Object.assign(this.frequency, fprops);
            Object.assign(this.intensity, iprops);
            this.frequency.analogo.initial = freq;
            this.frequency.analogo.time = 0; // input
            this.frequency.vibrato.time = Infinity; // output
            if (this.frequency.analogo.ratio == undefined) this.frequency.analogo.ratio = 1;
        }

        // NOTE:TODO: Junction type which can hold multiple voices in a path
            // so as to say move along these connections to get to the destination
            // all within one F->M second.
        function Junction(path) {
            this.path = path;
        }

        function orientAspect_(mesh,center) {
            mesh.lookAt(center);
            mesh.rotateX(-Math.PI/2);
            mesh.getWorldDirection(mesh.up);
            mesh.rotateX(Math.PI/2);
            // mesh.up.copy(reuse_vec);
        }

        function orientAspect(idx,quantum) {

            let res = {};

            switch (idx) {
                case 0:
                    res.position = new Vector(0,0,0);
                    res.position2D = new Vector(0,0);
                    res.color = 0x000000; // black
                    res.orient = orientAspect_;
                    break;
                case 1:
                    res.position = new Three.Vector3(0,1,0);
                    res.position2D = new Vector(0,1);
                    res.color = 0xff0000; // red
                    res.orient = orientAspect_;
                    break;
                case 2:
                    res.position = new Three.Vector3(0,1,1);
                    res.position2D = new Vector(SQRT3/2, 0.5);
                    res.color = 0xdde84f; // gold
                    res.orient = orientAspect_;
                    break;
                case 3:
                    res.position = new Three.Vector3(0,0,1);
                    res.position2D = new Vector(SQRT3/2, -0.5);
                    res.color = 0xca4fe8; // purple
                    res.orient = orientAspect_;
                    break;
                case 4:
                    res.position = new Three.Vector3(1,0,1);
                    res.position2D = new Vector(0, -1);
                    res.color = 0x0000ff; // blue
                    res.orient = orientAspect_;
                    break;
                case 5:
                    res.position = new Three.Vector3(1,0,0);
                    res.position2D = new Vector(-SQRT3/2, -0.5);
                    res.color = 0xe88e4f; // orange
                    res.orient = orientAspect_;
                    break;
                case 6:
                    res.position = new Three.Vector3(1,1,0);
                    res.position2D = new Vector(-SQRT3/2, 0.5);
                    res.color = 0x00ff00; // green
                    res.orient = orientAspect_;
                    break;
                case 7:
                    res.position = new Vector(1,1,1);
                    res.position2D = new Vector(0,0);
                    res.color = 0xb6b6b6; // grey
                    res.orient = orientAspect_;
                    break;
                default:

            }

            res.position.multiplyScalar(quantum);
            res.position2D.multiplyScalar(quantum);
            return res;
        }

        function drawHelpers(props) {
            let aspects = props.meshes,
                scene = props.group,
                line_color = props.lineColor,
                skip_conductor = props.skipConductor;
            let lineMat = ()=> {
                return new THREE.LineDashedMaterial({
                    color: line_color == undefined ? randomColor() : line_color,
                    linewidth: 10,
                    scale: 1,
                    dashSize: 5,
                    gapSize: 3,
                });
            }

            for (let idx = 0; idx < aspects.length; ++idx) {
                let iaspect = aspects[idx];
                for (let jdx = idx; jdx < aspects.length; ++jdx) {
                    // if (jdx-idx === 3) continue;
                    let jaspect = aspects[jdx];

                    let lineGeometry = new THREE.Geometry();
                    lineGeometry.vertices.push(
                        iaspect.position,
                        jaspect.position
                    );
                    let line = new THREE.Line(lineGeometry, lineMat());
                    line.computeLineDistances();
                    scene.add(line);
                }
            }

            let hexf = randomColor(),
                hexu = randomColor();

            function drawArrows(m) {
                let origin = m.position,
                    dir = m.getWorldDirection(reuse_vec),
                    length = 20,
                    // hex = 0x000000,
                    hlen = 5,
                    hw = 5;
                let arrowHelper = new THREE.ArrowHelper( dir, origin, length, hexf, hlen, hw );
                scene.add( arrowHelper );

                dir = m.up;
                arrowHelper = new THREE.ArrowHelper( dir, origin, length, hexu, hlen, hw );
                scene.add( arrowHelper );
            }

            if (!skip_conductor && aspects.length > 0) drawArrows(aspects[0]);
            for (let idx = 1; idx < aspects.length; ++idx) drawArrows(aspects[idx]);
        }

        function tintinnabula(res, conductor, itid, quantum, scene) {

            // Have to be consistent. Some places expect one or the other.
                // Others, like here, expect both.
                // Should expect both. Need wings.
            if (res.leftChorus) {
                let cf = conductor;
                // Chorus frequencies
                    // SOT
                for (let chi = 1; chi < 7; ++chi) {
                    let chor = res.leftChorus[chi];
                    let idx = chi-1;
                    chor.frequency = res.frequency*Math.pow(cf, -idx/6);
                    chor.duration = res.duration;
                    chor.intensity = res.intensity;

                    chor = res.rightChorus[chi];
                    chor.frequency = res.frequency*Math.pow(cf, idx/6);
                    chor.duration = res.duration;
                    chor.intensity = res.intensity;
                }

                return;
            }

            // SOT: Arrange chorus like either a cube or hexagon.
            // SOT: Max distance of choir from conductor is either quantum or 1/6 of quantum.

            let quantum6 = quantum/6;

            let roty = Math.PI/2*.75,
                rotz = -Math.PI/4;

            // left chorus
            conductor.rotateZ(rotz);
            conductor.rotateY(roty);

            conductor.rotateX(-Math.PI/2);
            let up = conductor.getWorldDirection(reuse_vec).clone();
            conductor.rotateX(Math.PI/2);

            let lcube = [{
                    position: conductor.position.clone(),
                    up: conductor.up.clone(),
                    forward: conductor.getWorldDirection(reuse_vec).clone(),
                    mesh: conductor
                }],
                lmeshes = [conductor],
                left_group = new THREE.Group();

            let mid_sphere = new THREE.Mesh( new THREE.SphereBufferGeometry( 10, 20, 20 ), new THREE.MeshNormalMaterial() );
            left_group.add( mid_sphere );

            let lorient = conductor.clone(false);
            lorient.material = new THREE.MeshBasicMaterial();
            lorient.material.color.setHex(0xb6b6b6); //grey
            lorient.up.copy(up);
            left_group.add(lorient);
            lorient.translateX(quantum6/2);
            lorient.translateY(quantum6/2);
            lorient.translateZ(quantum6/2);
            mid_sphere.position.copy(lorient.position);
            lorient.translateX(quantum6/2);
            lorient.translateY(quantum6/2);
            lorient.translateZ(quantum6/2);
            let orientation = orientAspect(7,quantum6);
            orientation.orient(lorient,mid_sphere.position);

            for (let idx = 1; idx < 7; ++idx) {
                let voice = conductor.clone(false);
                voice.material = new THREE.MeshBasicMaterial();
                voice.material.color.setHex(orientation.color);
                voice.up.copy(up);
                left_group.add(voice);
                orientation = orientAspect(idx,quantum6);
                let pos = orientation.position;
                voice.translateX(pos.x);
                voice.translateY(pos.y);
                voice.translateZ(pos.z);
                orientation.orient(voice,mid_sphere.position);

                lcube.push({
                    itid: itid+'-left-'+(idx-1),
                    position: voice.position.clone(),
                    up: voice.up.clone(),
                    forward: voice.getWorldDirection(reuse_vec).clone(),
                    mesh: voice
                });

                lmeshes.push(voice);
            }

            lcube.push({
                position: lorient.position.clone(),
                up: lorient.up.clone(),
                forward: lorient.getWorldDirection(reuse_vec).clone(),
                mesh: lorient
            });

            lmeshes.push(lorient);

            drawHelpers({
                meshes: lmeshes,
                group: left_group,
                lineColor: 0x000000,
                skipConductor: true
            });

            // right chorus
            conductor.rotateY(-roty);
            conductor.rotateZ(-2*rotz);
            conductor.rotateY(-2*roty);

            conductor.rotateX(-Math.PI/2);
            up.copy( conductor.getWorldDirection(reuse_vec) );
            conductor.rotateX(Math.PI/2);

            let rcube = [{
                    position: conductor.position.clone(),
                    up: conductor.up.clone(),
                    forward: conductor.getWorldDirection(reuse_vec).clone(),
                    mesh: conductor
                }],
                rmeshes = [conductor],
                right_group = new THREE.Group();

            mid_sphere = new THREE.Mesh( new THREE.SphereBufferGeometry( 10, 20, 20 ), new THREE.MeshNormalMaterial() );
            right_group.add( mid_sphere );

            let rorient = conductor.clone(false);
            rorient.material = new THREE.MeshBasicMaterial();
            rorient.material.color.setHex(0xb6b6b6); //grey
            rorient.up.copy(up);
            right_group.add(rorient);
            rorient.translateX(quantum6/2);
            rorient.translateY(quantum6/2);
            rorient.translateZ(quantum6/2);
            mid_sphere.position.copy(rorient.position);
            rorient.translateX(quantum6/2);
            rorient.translateY(quantum6/2);
            rorient.translateZ(quantum6/2);
            orientation = orientAspect(7,quantum6);
            orientation.orient(rorient,mid_sphere.position);

            for (let idx = 1; idx < 7; ++idx) {
                let voice = conductor.clone(false);
                voice.material = new THREE.MeshBasicMaterial();
                voice.material.color.setHex(orientation.color);
                voice.up.copy(up);
                right_group.add(voice);
                orientation = orientAspect(idx,quantum6);
                let pos = orientation.position;
                voice.translateX(pos.x);
                voice.translateY(pos.y);
                voice.translateZ(pos.z);
                orientation.orient(voice,mid_sphere.position);

                rcube.push({
                    itid: itid+'-right-'+(idx-1),
                    position: voice.position.clone(),
                    up: voice.up.clone(),
                    forward: voice.getWorldDirection(reuse_vec).clone(),
                    mesh: voice
                });

                rmeshes.push(voice);
            }

            rcube.push({
                position: rorient.position.clone(),
                up: rorient.up.clone(),
                forward: rorient.getWorldDirection(reuse_vec).clone(),
                mesh: rorient
            });

            rmeshes.push(rorient);

            drawHelpers({
                meshes: rmeshes,
                group: right_group,
                lineColor: 0x000000,
                skipConductor: true
            });

            conductor.rotateY(2*roty);
            conductor.rotateZ(rotz);

            lcube.group = left_group;
            rcube.group = right_group;

            lcube.quantum = rcube.quantum = quantum6;

            scene.add(left_group);
            scene.add(right_group);

            left_group.visible = false;
            right_group.visible = false;

            res.leftChorus = lcube;
            res.rightChorus = rcube;
        }

        async function getSoundTree_(props) {

            props.level = props.level || 0;

            let voice = props.voice,
                type = props.type,
                level = props.level,
                itid = props.itid,
                // mf = props.mf,
                // fm = props.fm,
                // bj = props.bj,
                // jb = props.jb,
                // mb = props.mb,
                // bm = props.bm,
                // fb = props.fb,
                // bf = props.bf,
                // mj = props.mj,
                // jm = props.jm,
                // fj = props.fj,
                // jf = props.jf,
                quantum = props.quantum,
                itineraries = props.itineraries,
                floater = itineraries.floater,
                planned = itineraries.planned;

                itineraries.systems = itineraries.systems || [];

            let scene = props.scene;

            let conductor = props.conductor;

            // listener settings
            if (!conductor) {
                let geometry = new THREE.CylinderBufferGeometry(0, 10, 10, 12),
                    material = new THREE.MeshBasicMaterial();
				geometry.rotateX(Math.PI/2);
                conductor = props.conductor = new Three.Mesh(geometry, material);
                scene.add(conductor);
            }

            let res = {};

            res.itid = itid;
            res.level = level;

            switch (type) {
                case VOICE_TYPE_ASPECT:
                    quantum = DEV_MODE ? (DEV_USE_THEME_QUANTUM ? (quantum || 1) : DEV_QUANTUM()) : (quantum || 1);

                    if (voice._aspect.type === M.ASPECT_TYPE_COMPOUND) {
                        let theme = await themes_map.get(voice._aspect.id);
                        props.voice = theme;
                        props.type = VOICE_TYPE_THEME;
                        res = await getSoundTree_.bind(this)(props);
                        res.aspectType = M.ASPECT_TYPE_COMPOUND;
                    } else {
                        res.position = conductor.position.clone();
                        res.up = conductor.up.clone();
                        res.forward =  conductor.getWorldDirection(new Vector());
                        res.mesh = conductor;

                        res.frequency = voice._aspect.frequencies(0);
                        res.aspectType = M.ASPECT_TYPE_PURE;
                        res.voiceId = voice._aspect.id;
                        res.name = voice._aspect.name;

                        if (voice._aspect.cover()) {
                            try {
                                res.brane = (await loadBrane(voice._aspect.cover()));
                            } catch(e) {
                                // delete res.brane;
                                console.error('Error occured while setting up cover: '+e);
                            }
                        }

                        planned[itid] = {
                            itid: itid,
                            value: res.frequency,
                            aspectType: M.ASPECT_TYPE_PURE
                        }

                        itineraries.systems.push(planned[itid]);
                    }
                    tintinnabula(res, conductor, itid, quantum, scene);
                    res.type = VOICE_TYPE_ASPECT;
                    break;
                case VOICE_TYPE_THEME:
                    res.type = VOICE_TYPE_THEME;

                    let _theme = voice._theme;

                    // NOTE: A theme's quantum may be set to 0 to signify that it should use the quantum of whatever parent
                    // birthed it.
                    quantum = DEV_MODE ? (DEV_USE_THEME_QUANTUM ? (_theme.quantum || quantum || 1) : DEV_QUANTUM()) : (_theme.quantum || quantum || 1);

                    // Create center point
                    let mid_sphere = new THREE.Mesh( new THREE.SphereBufferGeometry( 10, 20, 20 ), new THREE.MeshNormalMaterial() );
    				scene.add( mid_sphere );
					
					let scai = {
						position: conductor.position.clone(),
                        forward:  conductor.getWorldDirection(new Vector()),
                        up: conductor.up.clone(),
					}

                    // Set the up direction for voices in the conductor's world.
                    conductor.rotateX(-Math.PI/2);
                    conductor.getWorldDirection(conductor.up);
                    conductor.rotateX(Math.PI/2);

                    let orient = conductor.clone(false);
                    orient.material = new THREE.MeshBasicMaterial();
                    orient.material.color.setHex(0xb6b6b6); //grey
                    scene.add(orient);
                    orient.translateX(quantum/2);
                    orient.translateY(quantum/2);
                    orient.translateZ(quantum/2);
                    mid_sphere.position.copy(orient.position);
                    orient.translateX(quantum/2);
                    orient.translateY(quantum/2);
                    orient.translateZ(quantum/2);
                    let orientation = orientAspect(7,quantum);
                    orientation.orient(orient,mid_sphere.position);

                    let orchestra = [],
                        aspectIds = (await shells_map.get(_theme.shellId)).aspectIds,
                        asp = null,
                        vecs = null,
                        atps = [],
                        cdepth = 0,
                        next_opts = [],
                        asps_orient = [
                            orientAspect(1,quantum),
                            orientAspect(2,quantum),
                            orientAspect(3,quantum),
                            orientAspect(4,quantum),
                            orientAspect(5,quantum),
                            orientAspect(6,quantum),
                        ];

                    res.duration = _theme.duration();
                    res.depth = 1;
                    res.width = 1; // sum of every child theme's width
                    res.voiceId = _theme.id;

                    // For each aspect, gather the values of arrows of the same kind.
                    if (!meanings[_theme.id]) {

                        let m = {};
                        meanings[_theme.id] = m;

                        // Arrange into: { aspect: {arrowType: avg} }
                        switch (_theme.type) {
                            case M.THEME_TYPE_SIGNAL:
                                for (let i = 0; i < n_ss.length; ++i) {
                                    let n = n_ss[i];
                                    m[n] = {};
                                    m[n][M.ARROW_TYPE_ACKNOWLEDGEMENT] = _theme.meaning[n].value || _theme.meaning[n] || 0; // TODO: THIS IS TEMPORARY FIX FOR THEMES IN CHROME INDEXDB
                                }
                                break;
                            case M.THEME_TYPE_VERSUS:
                                for (let i = 0; i < 3; ++i) {
                                    let ni = n_ss[i];
                                    if (!m[ni]) {
                                        m[ni] = {};
                                        M.VERSUS_ARROWS.forEach(arr => m[ni][arr] = {sum:0, count:0, fsum:[]});
                                    }
                                    for (let j = 3; j < 6; ++j) {
                                        let nj = n_ss[j];
                                        if (!m[nj]) {
                                            m[nj] = {};
                                            M.VERSUS_ARROWS.forEach(arr => m[nj][arr] = {sum:0, count:0, fsum:[]});
                                        }
                                        let m_ = _theme.meaningOf(i,j);
                                        if (m_.value) {
                                            m[ni][m_.arrowType].sum+=m_.value;
                                            m[ni][m_.arrowType].fsum.push(j);
                                            m[ni][m_.arrowType].count++;
                                        }

                                        m_ = _theme.meaningOf(j,i);
                                        if (m_.value) {
                                            m[nj][m_.arrowType].sum+=m_.value;
                                            m[nj][m_.arrowType].fsum.push(i);
                                            m[nj][m_.arrowType].count++;
                                        }
                                    }
                                }
                                break;
                            case M.THEME_TYPE_CONTEXT:
                                for (let i = 0; i < n_ss.length; ++i) {
                                    let ni = n_ss[i];
                                    if (!m[ni]) {
                                        m[ni] = {};
                                        M.CONTEXT_ARROWS.forEach(arr => m[ni][arr] = {sum:0, count:0, fsum:[]});
                                    }
                                    for (let j = i+1; j < n_ss.length; ++j) {
                                        let nj = n_ss[j];
                                        if (!m[nj]) {
                                            m[nj] = {};
                                            M.CONTEXT_ARROWS.forEach(arr => m[nj][arr] = {sum:0, count:0, fsum:[]});
                                        }
                                        let m_ = _theme.meaningOf(i,j);
                                        if (m_.value) {
                                            m[ni][m_.arrowType].sum+=m_.value;
                                            m[ni][m_.arrowType].fsum.push(j);
                                            m[ni][m_.arrowType].count++;
                                        }

                                        m_ = _theme.meaningOf(j,i);
                                        if (m_.value) {
                                            m[nj][m_.arrowType].sum+=m_.value;
                                            m[nj][m_.arrowType].fsum.push(i);
                                            m[nj][m_.arrowType].count++;
                                        }
                                    }
                                }
                                break;
                            default:

                        }
                    }

                    if (pos_ch && _theme.type === M.THEME_TYPE_CONTEXT) {
                        // Calculate the positional differences of the aspects
                        // from their relationships.
                        let apos = null,
                            bpos = null,
                            diff_pos = null,
                            ab = 0,
                            ba = 0,
                            atag = '',
                            btag = '',
                            pos_avgs = {
                                0: new Three.Vector3(0,0,0),
                                1: new Three.Vector3(0,0,0),
                                2: new Three.Vector3(0,0,0),
                                3: new Three.Vector3(0,0,0),
                                4: new Three.Vector3(0,0,0),
                                5: new Three.Vector3(0,0,0)
                            }

                        for (let i = 0; i < aspectIds.length-1; ++i) {
                            apos = orientAspect(i,quantum).position;
                            for (let j = i+1; j < aspectIds.length; ++j) {
                                atag = n_ss[i];
                                btag = n_ss[j];
                                ab = _theme.meaning[atag+btag].value;
                                ba = _theme.meaning[btag+atag].value;
                                // If sum of ab+ba is greater than 1 then one is treading
                                // on the other's territory. Find middle ground in their
                                // intersection.
                                // sm = (1-(larger-smaller))/2
                                // lg = 1-sm
                                if (ab+ba > 1) {
                                    if (ab > ba) {
                                        ba = (1-(ab-ba))/2;
                                        ab = 1-ba;
                                    } else {
                                        ab = (1-(ba-ab))/2;
                                        ba = 1-ab;
                                    }
                                }

                                bpos = orientAspect(j,quantum).position;
                                diff_pos = apos.clone().sub(bpos);
                                pos_avgs[i].add(apos.sub(diff_pos.clone().multiplyScalar(ab)));
                                pos_avgs[j].add(bpos.sub(diff_pos.negate().multiplyScalar(ba)));
                            }
                        }

                        asps_orient[0].position.copy(pos_avgs[0].divideScalar(5));
                        asps_orient[1].position.copy(pos_avgs[1].divideScalar(5));
                        asps_orient[2].position.copy(pos_avgs[2].divideScalar(5));
                        asps_orient[3].position.copy(pos_avgs[3].divideScalar(5));
                        asps_orient[4].position.copy(pos_avgs[4].divideScalar(5));
                        asps_orient[5].position.copy(pos_avgs[5].divideScalar(5));
                    }

                    // If there is an outer theme, this will be replaced by an
                        // aspect Process, if there is one.
                    // res.process = await processes_map.get(_theme.processId());
                    res.processId = _theme.processId();

                    // Aspects
                    for (let ai = 0; ai < aspectIds.length; ++ai) {

                        asp = await aspects_map.get(aspectIds[ai]);
                        orientation = asps_orient[ai];

                        let ctheme = asp._aspect.type === M.ASPECT_TYPE_COMPOUND ? await themes_map.get(asp._aspect.id) : null,
                            cshell = ctheme ? (await shells_map.get(ctheme._theme.shellId)) : null,
                            citid = guid(),
                            cq = quantum;

                        // Create a conductor instance at this aspect's position via translation

                        let ccon = conductor.clone(false);
                        ccon.material = new THREE.MeshBasicMaterial();
                        ccon.material.color.setHex(orientation.color);
                        scene.add(ccon);
                        let pos = orientation.position;
                        ccon.translateX(pos.x);
                        ccon.translateY(pos.y);
                        ccon.translateZ(pos.z);
                        orientation.orient(ccon,mid_sphere.position);

                        if (ctheme) {
                            cq = DEV_MODE ? (DEV_USE_THEME_QUANTUM ? (ctheme._theme.quantum || quantum) : DEV_QUANTUM()) : (ctheme._theme.quantum || quantum);

                            // Rotate into its zone, so it is mid way between its extremes
                            ccon.rotateX(-Math.PI/4*.75);
                            ccon.rotateY(Math.PI*.75);

                            next_opts.push(citid);
                        }

                        let cprops = {
                                voice: asp,
                                type: VOICE_TYPE_ASPECT,
                                level: level+1,
                                itid: citid,
                                conductor: ccon,
                                // mf: mf,
                                // fm: fm,
                                // bj: bj,
                                // jb: jb,
                                // mb: mb,
                                // bm: bm,
                                // fb: fb,
                                // bf: bf,
                                // mj: mj,
                                // jm: jm,
                                // fj: fj,
                                // jf: jf,
                                scene: scene,
                                itineraries: itineraries,
                                quantum: cq
                            }

                        let cs = await getSoundTree_.bind(this)(cprops);
                        cs.intensity = _theme.intensities(ai);
                        cs.duration = _theme.durations(ai);
                        // let process = await processes_map.get(_theme.processIds(ai));
                        // cs.process = process;
                        cs.processId = _theme.processIds(ai);
                        orchestra.push(cs);

                        res.width+=(cs.width || 0);
                        cdepth = Math.max(cdepth, cs.depth || 0);
                    }

                    orientation = orientAspect(0,quantum);
                    if (level === 0) conductor.material.color.setHex(orientation.color);
                    orientation.orient(conductor,mid_sphere.position);

                    orchestra.push({
                        position: orient.position.clone(),
                        forward:  orient.getWorldDirection(new Vector()),
                        up: orient.up.clone(),
                        aspectType: 'orient',
                        frequency: 0,
                        mesh: orient
                    });

                    orchestra.unshift({
                        position: conductor.position.clone(),
                        forward:  conductor.getWorldDirection(new Vector()),
                        up: conductor.up.clone(),
                        aspectType: 'conductor',
                        frequency: 0,
                        mesh: conductor,
						asPure: scai
                    });

                    res.position = orchestra[0].position;
                    res.forward = orchestra[0].forward;
                    res.up = orchestra[0].up;
                    res.quantum = quantum;
                    res.mesh = orchestra[0].mesh;

                    res.name = _theme.name();
                    res.themeType = _theme.type;

                    // Visualization helpers
                    drawHelpers({
                        meshes: orchestra.map(member => member.mesh),
                        group: scene
                    });

                    res.orchestra = orchestra;
                    res.depth += cdepth;

                    // NOTE: this can be negative
                    res.fundamental = Bessel.Jn(res.width, res.depth);

                    if (!planned[level]) planned[level] = [];
                    planned[level].push(itid);
                    planned[itid] = {
                        itid: itid,
                        // value: res.fundamental, // calculated before itineraries are gathered
                        position: res.position,
                        up: res.up,
                        forward: res.forward,
                        nextIds: next_opts,
                        aspectType: M.ASPECT_TYPE_COMPOUND
                    }
                    itineraries.systems.push(planned[itid]);

                    let sign = res.fundamental >= 0 ? 1 : -1; // SOT: white hole effect
                    if (floater[level]) {
                        floater[level].posAvg.add( reuse_vec.copy(res.position).multiplyScalar(sign) );
                        floater[level].fAvg.add( reuse_vec.copy(res.forward).multiplyScalar(sign) );
                        floater[level].uAvg.add( reuse_vec.copy(res.up).multiplyScalar(sign) );
                        floater[level].count++;
                    } else {
                        floater[level] = {
                            posAvg: new Three.Vector3().copy( reuse_vec.copy(res.position).multiplyScalar(sign) ),
                            fAvg: new Three.Vector3().copy( reuse_vec.copy(res.forward).multiplyScalar(sign) ),
                            uAvg: new Three.Vector3().copy( reuse_vec.copy(res.up).multiplyScalar(sign) ),
                            count: 1
                        }
                    }
                    break;
                default:
            }

            return res;
        }
    }
    this.getJourney = PUBLIC_SCHOOL.getJourney = getJourney;

    // NOTE:
        // `playing` means traveling i.e. the journey is progressing.
        // To `pause` is to stop the halt the listener at the current position. The
            // voices will continue to be heard.
        // To `resume` is to continue with the itinerary from the position halted at.
        // To 'stop' is to resign completely from the journey. Voices will cease singing.
        // 'Time' is the distance covered between the first junction and the last junction.
        // 'Total' is the distance traveled all together.
    // This represents the stimuli of a designed body part.
    const Simulation = function(opts) {
        let SOUND_CONTEXT, VISION_CONTEXT;
        this.sound = {}
        this.vision = {}
        this.type = ''; // 'aspect' (pure and compound) or 'theme'
        this.voice_ = null;
        this.voice = ()=> { return this.voice_ };
        this.voiceInfo = null;
        this.curriculum = opts.temple;
        this.listener = {
            startPosition: (opts.sound && opts.sound.listener && opts.sound.listener.position) ? opts.sound.listener.position : new Three.Vector3(0,0,0),
            position_: new Three.Vector3(0,0,0),
            position: (x,y,z)=> {
                if (x == undefined) {
                    return this.listener.position_;
                } else {
                    this.listener.position_.set(x,y,z);
                    if (SOUND_CONTEXT.listener.positionX) {
                        SOUND_CONTEXT.listener.positionX.setValueAtTime(x, SOUND_CONTEXT.currentTime);
                        SOUND_CONTEXT.listener.positionY.setValueAtTime(y, SOUND_CONTEXT.currentTime);
                        SOUND_CONTEXT.listener.positionZ.setValueAtTime(z, SOUND_CONTEXT.currentTime);
                    } else {
                        SOUND_CONTEXT.listener.setPosition(x, y, z);
                    }
                }
            },
            startForward: (opts.sound && opts.sound.listener && opts.sound.listener.forward) ? opts.sound.listener.forward : new Three.Vector3(0,0,1),
            forward_: new Three.Vector3(0,0,1),
            forward: (x,y,z)=> {
                if (x == undefined) {
                    return this.listener.forward_;
                } else {
                    this.listener.forward_.set(x,y,z);
                    if (SOUND_CONTEXT.listener.forwardX) {
                        SOUND_CONTEXT.listener.forwardX.setValueAtTime(x, SOUND_CONTEXT.currentTime);
                        SOUND_CONTEXT.listener.forwardY.setValueAtTime(y, SOUND_CONTEXT.currentTime);
                        SOUND_CONTEXT.listener.forwardZ.setValueAtTime(z, SOUND_CONTEXT.currentTime);
                    } else {
                        let up = this.listener.up_;
                        SOUND_CONTEXT.listener.setOrientation(x, y, z, up.x, up.y, up.z);
                    }
                }
            },
            startUp: (opts.sound && opts.sound.listener && opts.sound.listener.up) ? opts.sound.listener.up : new Three.Vector3(0,1,0),
            up_: new Three.Vector3(0,1,0),
            up: (x,y,z)=> {
                if (x == undefined) {
                    return this.listener.up_;
                } else {
                    this.listener.up_.set(x,y,z);
                    if (SOUND_CONTEXT.listener.upX) {
                        SOUND_CONTEXT.listener.upX.setValueAtTime(x, SOUND_CONTEXT.currentTime);
                        SOUND_CONTEXT.listener.upY.setValueAtTime(y, SOUND_CONTEXT.currentTime);
                        SOUND_CONTEXT.listener.upZ.setValueAtTime(z, SOUND_CONTEXT.currentTime);
                    } else {
                        let fw = this.listener.forward_;
                        SOUND_CONTEXT.listener.setOrientation(fw.x, fw.y, fw.z, x, y, z);
                    }
                }
            }
        }
        const resetListener = ()=> {
            let vec = this.listener.startPosition;
            this.listener.position(vec.x, vec.y, vec.z);
            vec = this.listener.startForward;
            this.listener.forward(vec.x, vec.y, vec.z);
            vec = this.listener.startUp;
            this.listener.up(vec.x, vec.y, vec.z);
        }

        this.voice = (voice,type,info)=> {
            if (voice) {
                this.voice_ = voice;
                this.type = type;
                this.voiceInfo = info;
            } else {
                return this.voice_;
            }
        }
        this.journey = null;

        this.total = {time:0, travel:0, cycle:0};
        let time_ = null; // use null to (re)load journey
        this.time = ()=> { return time_; }

        this.playing = false;

        const TRANSFER_REVERB_ROOM_SIZE = 0.6;
        const TRANSFER_REVERB_DAMPENING = 6000; // MAYBE: set strategically; double reverb dampening i.e. diameter? or half?
        const TRANSFER_REVERB_WET = 1;
        const REVERB_ROOM_SIZE = 0;
        const REVERB_DAMPENING = 3000;
        const REVERB_WET = 1;

        // The play loop will set this to a function that will
        // update components of the scene e.g IFO/bloom flickering.
        let visualizationHook = null;

        const voices = new Map();
        this.voices = voices;

        // let message = {
        //     automation: {
        //         frequency: 0,
        //         intensity: 0
        //     },
        //     process: {
        //         frequency: 0,
        //         intensity: 0
        //     }
        // }

        const automationWorker = new Worker('../js/AutomationWorker.js');
        automationWorker.onmessage = (e)=> {
            let data = e.data;

            let v = voices.get(data.nodeId);

            if (!v) return;

            switch (data.type) {
                case JOB_TYPE_AUTOMATION:
                    // Object.assign(message.automation, data.value);
                    // if (data.value.frequency > 1000000) console.log('from automation', v.simulationId);

                    // Use these ones
                    // v.setFrequency(data.value.frequency, SOUND_CONTEXT.currentTime);
                    // v.setIntensity(data.value.intensity, SOUND_CONTEXT.currentTime);
                    // Use ^

                    // v.setFrequency((message.automation.frequency+message.process.frequency)/2, SOUND_CONTEXT.currentTime);
                    // TODO: Automations will eventually also send [0,~] intensity.
                    // v.setIntensity((message.automation.intensity+message.process.intensity*SOUND_INTENSITY_MAX)/2, SOUND_CONTEXT.currentTime);
                    break;
                default:
            }
        }

        const rogueWorker = new Worker('../js/RogueWorker.js');
        rogueWorker.onmessage = (e)=> {
            let data = e.data;

            let v = voices.get(data.voiceId);
            if (!v) return;

            switch (data.type) {
                case JOB_TYPE_DELETE:
                    v.stopRogue(data.id);
                    automationWorker.postMessage({
                        type: JOB_TYPE_DELETE_UNITY,
                        nodeId: v.simulationId,
                        coordinates: v.simCoords
                    });
                    voices.delete(data.id);
                    break;
                case JOB_TYPE_DECAY:
                    automationWorker.postMessage({
                        type: JOB_TYPE_BREAK_UNITY,
                        nodeId: v.simulationId,
                        coordinates: v.simCoords,
                        index: data.part
                    });
                    break;
                case JOB_TYPE_ROGUE:
                    let r = v.getRogue(data.id);
                    r.position.copy(data.position);
                    r.needsUpdate = true;
                    break;
                default:
            }
        }

        const processWorker = new Worker('../js/ProcessWorker.js');
        processWorker.onmessage = (e)=> {
            let data = e.data;
            let v = voices.get(data.id);
            if (!v) return;

            switch (data.type) {
                case JOB_TYPE_VOICE:
                    let res = null;
                    if (v) {
                        res = {
                            id: v.simulationId,
                            simCoords: v.simCoords,
                            frequency: v.frequency,
                            intensity: v.intensity,
                            initial: {
                                frequency: v.initial.frequency,
                                intensity: v.initial.intensity
                            },
                            startTime: v.startTime,
                            duration: v.duration,
                            period: v.period,
                            cycle: v.cycle,
                            position: v.position,
                            forward: v.forward,
                            up: v.up
                        }
                    }
                    processWorker.postMessage({
                        type: JOB_TYPE_VOICE,
                        jobId: data.jobId,
                        voice: res
                    });
                    break;
                case JOB_TYPE_PROCESS:
                    // console.log(v.simulationId, data, v.initial.frequency);
                    automationWorker.postMessage({
                        type: JOB_TYPE_MODIFY,
                        coordinates: v.simCoords,
                        frequency: v.initial.frequency*(1+data.value.frequency), // automation initial
                        intensity: v.initial.intensity*(1+data.value.intensity)
                    });
                    // Object.assign(message.process, data.value);
                    // v.setFrequency((message.automation.frequency+message.process.frequency)/2, SOUND_CONTEXT.currentTime);
                    // TODO: Automations will eventually also send [0,~] intensity.
                    // v.setIntensity((message.automation.intensity+message.process.intensity*SOUND_INTENSITY_MAX)/2, SOUND_CONTEXT.currentTime);
                    break;
                default:
            }
        }

        let mf, fm, mb, bm, fb, bf, mj, jm, fj, jf;
        let scale, diameter;

        this.modify = (k, commited)=> {
            if (!this.voiceInfo || !this.player) return;

            let val = this.voiceInfo.modQuad[k].value;
            let val_;
            switch (k) {
                case MF:
                    break;
                case FM:
                    val = Math.range(val, MIN_FM, MAX_FM);
                    // this.voiceInfo.modQuad[k].value = val; // !!
                    // fm = val === 1 ? 0.99999999999 : val;
                    // scale = 1-Math.exp(1-fm)/Math.E;
                    scale = 1-val;
                    this.total.time = this.total.travel*2*scale*this.player.duration;
                    val_ = this.journey.radius//*scale; // TODO: move out of this and set once if scale is useless (SOT?)
                    let w = this.journey.width,
                        h = this.journey.depth,
                        r = val_*(val_/(w*h));
                    // this.transferReverb.dampening.setValueAtTime(val_*2, SOUND_CONTEXT.currentTime);//6000//9000//val_*(val_/(w*h))*2;
                    this.transferReverb.dampening.setValueAtTime(6000, SOUND_CONTEXT.currentTime);
                    this.transferReverb.wet = 1;
                    this.reverb.dampening.setValueAtTime(val_, SOUND_CONTEXT.currentTime);//3000//4500//val_*(val_/(w*h));
                    // console.log(val_,r,r*2);
                    break;
                case BJ: // Reverberation
                    this.reverb.wet = val;
                    // this.transferReverb.wet = val;//Math.range(val*2,0,1);
                    // this.transferReverb.roomSize.setValueAtTime(val, SOUND_CONTEXT.currentTime);
                    this.reverb.roomSize.setValueAtTime(val, SOUND_CONTEXT.currentTime);
                    break;
                case JB:
                    break;
                case MB:
                    this.journey.bmADAP.modelHigh = val;
                    break;
                case BM:
                    break;
                case FB:
                    break;
                case BF:
                    diameter = this.journey.radius*scale*2*(1-val);
                    break;
                case MJ:
                    break;
                case JM:
                    break;
                case FJ:
                    this.journey.bmADAP.modelLow = val;
                    break;
                case JF:
                    break;
                default:
            }

            let mq_ = {}
            mq_[k] = val;
            processWorker.postMessage({
                type: JOB_TYPE_MOD_QUAD,
                modQuad: mq_
            });
        }

        // NOTE: Oscillators are initially silent and vibrating
            // at the minimum frequency.
        function Oscillator(opts) {
            let context = opts.context,
                freq = opts.frequency || SOUND_MIN_FREQUENCY,
                intst = opts.intensity || 0,
                pos = opts.position,
                fw = opts.forward,
                up = opts.up,
                flux = opts.flux,
                IF = opts.IF,
                ifo = opts.ifo,
                depth = opts.depth,
                source = opts.source,
                chain = opts.chain || [],
                bare = opts.bare,
                empty = opts.empty;
            // IF = Math.abs(IF) === Infinity ? 0 : IF;
            this.context = context;

            if (empty) {
                this.setFrequency = this.setVolume = VOID;
                return this;
            }

            if (source && !bare) {
                this.node = this.context.createBufferSource();
                this.node.loop = true;
                this.node.buffer = source.buffer;
                this.node.playbackRate.setValueAtTime(source.buffer.duration/source.duration, this.context.currentTime);
                freq = Math.range(freq,10,24000); // based on web audio docs
                this.filter = new Filter(context, {type:'bandpass',frequency:freq, Q:1});
                this.node.connect(this.filter);
            } else {
                this.node = this.context.createOscillator();
                this.node.type = 'sine';
                this.node.frequency.setValueAtTime(freq > 20000 ? SOUND_MIN_FREQUENCY : freq, this.context.currentTime);
            }
            this.volume = this.context.createGain();
            this.volume.gain.setValueAtTime(intst, this.context.currentTime);
            this.position = pos;
            this.forward = fw;
            this.up = up; // only here for completion
            this.panner = this.context.createPanner();
            this.alignPanner = ()=> {
                if (this.panner.positionX) {
                    this.panner.positionX.setValueAtTime(this.position.x, this.context.currentTime);
                    this.panner.positionY.setValueAtTime(this.position.y, this.context.currentTime);
                    this.panner.positionZ.setValueAtTime(this.position.z, this.context.currentTime);
                } else {
                    this.panner.setPosition(this.position.x,this.position.y,this.position.z);
                }
                if (this.panner.orientationX) {
                    this.panner.orientationX.setValueAtTime(this.forward.x, this.context.currentTime);
                    this.panner.orientationY.setValueAtTime(this.forward.y, this.context.currentTime);
                    this.panner.orientationZ.setValueAtTime(this.forward.z, this.context.currentTime);
                } else {
                    this.panner.setOrientation(this.forward.x,this.forward.y,this.forward.z);
                }
            }
            this.alignPanner();
            this.panner.panningModel = 'equalpower';
            this.panner.distanceModel = 'exponential'; // TODO: explore 'inverse'
            this.panner.rolloffFactor = 1;
            // Angle of sound projection
            this.panner.coneInnerAngle = opts.coneInnerAngle == undefined ? 90 : opts.coneInnerAngle;
            // Angle of residual sound projection
            this.panner.coneOuterAngle = opts.coneOuterAngle == undefined ? 125 : opts.coneOuterAngle;
            // How much to drop off as we from within
            // the angle of projection, innerAngle
            // Should be related to the reverb and elasticity(?)
            this.panner.coneOuterGain = opts.coneOuterGain == undefined ? 0 : opts.coneOuterGain;
            this.panner.refDistance = 1;

            this.chain = chain;

            this.setIF = (IF)=> {
                if (!this.ifo) return;
                this.IF = IF;
                // IF = Math.abs(IF) === Infinity ? 0 : IF;
                this.ifo.frequency.setValueAtTime(IF, this.context.currentTime);
            }

            if (bare) {
                this.node.connect(this.volume);
                this.volume.connect(this.panner);
            } else {
                // The ifo is an oscillator changing in [-1,1].
                    // Scale its out put by the depth i.e ifo.connect(depth).
                    // The depth is (end-start)/2, the midpoint of the span.
                    // [-1,1]*midpoint === [-dmid,dmid]. Now, the ifo->depth output
                    // will be in the deisred span. To bring the start of that
                    // span to the desired start, add the ifo->depth output to
                    // the flux, (end+start)/2, which is the midpoint of the
                    // desired range. [-dmid,dmid]+fmid === [start,end].
                // To render the ifo useless, set it to 0hz and the flux gain
                    // to 1. The depth will be 0 and it will add to the 1 flux
                    // to give a final gain of 1.

                // TODO: test ifo and depth by checking the `gain` value
                // in a loop; may also need a `range` gain; use an AnalyzerNode

                if (!flux) {
                    flux = this.context.createGain();
                    flux.gain.setValueAtTime(1, this.context.currentTime);
                    this.flux = flux;

                    flux.connect(this.panner);
                }

                if (!depth) {
                    depth = this.context.createGain();
                    depth.gain.setValueAtTime(0, this.context.currentTime);
                    this.depth = depth;
                    depth.connect(flux.gain);
                }

                if (!ifo) {
                    ifo = this.context.createOscillator();
                    ifo.type = 'sine'; // TODO: try square
                    this.ifo = ifo;
                    this.setIF(IF);

                    ifo.connect(depth);
                } else {
                    this.IF = ifo.frequency.value;
                }
                if (this.filter) {
                    this.filter.connect(this.volume);
                } else {
                    this.node.connect(this.volume);
                }
                this.volume.connect(flux);
            }
            chain.reduce((node,conn)=> node.connect(conn), this.panner);

            this.setFrequency = (freq,ct)=> {
                if ((freq < 20) || (freq > 20000)) freq = SOUND_MIN_FREQUENCY;
                if (this.filter) {
                    this.filter.frequency.setValueAtTime(freq, ct);
                } else {
                    this.node.frequency.setValueAtTime(freq,ct);
                }
            }

            this.setVolume = (v,ct)=> {
                this.volume.gain.setValueAtTime(v,ct);
            }

            this.setFilterRadius = (v,ct)=> {
                if (!this.filter) return;
                this.filter.Q.setValueAtTime((v*1000) || 1, ct);
            }

            this.setFlux = (depth,flux,ct)=> {
                // if ((this.IF === 0) || (Math.abs(this.IF) === Infinity)) depth=0; flux=1; // fluctuations are too fast to make sense of
                if (this.flux) this.flux.gain.setValueAtTime(flux,ct);
                if (this.depth) this.depth.gain.setValueAtTime(depth,ct);
            }

            this.start = (ct)=> {
                this.node.start(ct);
                if (this.ifo) this.ifo.start(ct);
            }

            this.stop = ()=> {
                this.panner.disconnect(); // for garbage collection ?

                if (this.ifo) this.ifo.stop();
                if (this.flux) this.flux.gain.setValueAtTime(1, this.context.currentTime);
                if (this.depth) this.depth.gain.setValueAtTime(0, this.context.currentTime);
            }
        }

        function VoiceNode(opts) {
            this.position = opts.position;
            this.orientation = opts.orientation;
            this.coneInnerAngle = opts.coneInnerAngle;
            this.coneOuterAngle = opts.coneOuterAngle;
            this.coneOuterGain = opts.coneOuterGain;
            this.rolloffFactor = opts.rolloffFactor || 1;
            this.refDistance = opts.refDistance || 1;

            const computeDistanceGain = (lpos, reuse_vec)=> {
                let distance_vec = reuse_vec.copy(this.position).sub(lpos),
                    distance = Math.sqrt(distance_vec.dot(distance_vec));

                // TODO: need to set a refDistance
                // from PannerNodeEngine::ExponentialGainFunction
                let res = Math.pow(Math.max(distance, this.refDistance)/this.refDistance, -this.rolloffFactor);
                return Math.max(0,res);
            }

            const computeConeGain = (lpos, reuse_vec)=> {
                // Omnidirectional source
                if (this.orientation.equals(ORIGIN) || ((this.coneInnerAngle === 360) && (this.coneOuterAngle === 360))) return 1;

                // Normalized source-listener vector
                let source_to_listener = reuse_vec.copy(lpos).sub(this.position);
                source_to_listener.normalize();

                // Angle between the source orientation and source-listener
                let dot_p = source_to_listener.dot(this.orientation),
                    angle = 180*Math.acos(dot_p)/Math.PI,
                    abs_angle = Math.abs(angle);

                // Divide by 2 here since API is entire angle instead of half-angle
                let abs_inner = Math.abs(this.coneInnerAngle)/2,
                    abs_outer = Math.abs(this.coneOuterAngle)/2,
                    gain = 1;

                if (abs_angle <= abs_inner) {
                    // Maximum scale
                    gain = 1;
                } else if (abs_angle >= abs_outer) {
                    // Minimum scale
                    gain = 0; // ? or MIN_SCALE(0.01)
                } else {
                    // Between inner and outer; inner -> outer; x goes from 0 -> 1
                    let x = (abs_angle-abs_inner)/(abs_outer-abs_inner);
                    gain = (1-x)+this.coneOuterGain*x;
                }

                return gain;
            }

            const computeAzimuthAndElevation = (lpos, lfront, lup, reuse_vec, reuse_vec2, reuse_vec3)=> {
                let source_listener = reuse_vec.copy(this.position).sub(lpos);
                if (source_listener.equals(ORIGIN)) return {azimuth:0, elevation:0}
                source_listener.normalize();

                // Project the source vector on the x-z plane
                let lright = reuse_vec2.crossVectors(lfront,lup),
                    up_proj = source_listener.dot(lup),
                    elevation = 90-180*Math.acos(up_proj)/Math.PI;
                if (elevation > 90) {
                    elevation = 180-elevation;
                } else if (elevation < -90) {
                    elevation = -180-elevation;
                }

                let proj_source = source_listener.sub(reuse_vec3.copy(lup).multiplyScalar(up_proj));
                if (proj_source.equals(ORIGIN)) return {azimuth:0, elevation} // source-listener direction is up or down

                proj_source.normalize();

                // Actually compute the angleand convert to degrees
                let proj = proj_source.dot(lright),
                    azimuth = 180*Math.acos(proj)/Math.PI;

                // Compute whether source is in front or behind the listener
                let front_back = proj_source.dot(lfront);
                if (front_back < 0) azimuth = 360-azimuth;

                // Rotate the azimuth from the right so it is relative to the listener front
                if ((azimuth >= 0) && (azimuth <= 270)) {
                    azimuth = 90-azimuth;
                } else {
                    azimuth = 450-azimuth;
                }

                return {azimuth, elevation}
            }

            // EqualPowerPanningFunction
            this.pan = (lpos,lup,lfront, reuse_vec,reuse_vec2,reuse_vec3)=> {
                if (this.orientation.equals(ORIGIN)) this.orientation.normalize();

                // Strictly stereo
                if (lpos.equals(this.position) && (this.coneInnerAngle === 360) && (this.coneOuterAngle === 360)) return null // ?;

                let {azimuth, elevation} = computeAzimuthAndElevation(lpos,lfront,lup, reuse_vec,reuse_vec2,reuse_vec3),
                    cone_gain = computeConeGain(lpos, reuse_vec);

                azimuth = Math.min(180, Math.max(-180,azimuth));
                if (azimuth < -90) {
                    azimuth = -180-azimuth;
                } else if (azimuth > 90) {
                    azimuth = 180-azimuth;
                }

                if (azimuth <= 0) {
                    azimuth = (azimuth+90)/90;
                } else {
                    azimuth/=90;
                }

                let distance_gain = computeDistanceGain(lpos, reuse_vec),
                    phase = 0.5*Math.PI*azimuth,
                    vertical = Math.cos(phase),
                    horizontal = Math.sin(phase);

                return {
                    distance: distance_gain,
                    cone: cone_gain,
                    scale: distance_gain*cone_gain, // MAYBE: only cone_gain
                    vertical,
                    horizontal
                }
            }
        }

        function Oculomotor(opts) {
            let context = opts.context,
                intst = opts.intensity || 0,
                pos = opts.position,
                fw = opts.forward,
                up = opts.up,
                flux = opts.flux,
                IF = opts.IF,
                ifo = opts.ifo,
                width = opts.width,
                height = opts.height,
                depth = opts.depth,
                source = opts.source,
                chain = opts.chain || [],
                bare = opts.bare;
            // this.context = context; // the final canvas context
            this.index = 0; // AKA frequency->pixel index
            if (source && !bare) {
                // with image; use new canvas to hold/edit image
                this.node = document.createElement('canvas');
                this.buffer = source.buffer;
                width = width || source.width;
                height = height || source.height;
                this.node.width = width;
                this.node.height = height;
                this.context = this.node.getContext('2d');
                this.context.drawImage(this.buffer,0,0,width,height);
                this.filter = new VisualFilter(Math.floor(width*height/2), 0.01);
            } else {
                // pure pixel

            }

            this.volume = intst/SOUND_INTENSITY_MAX; // alpha
            this.position = pos;
            this.forward = fw;
            this.up = up;
            this.panner = new VoiceNode({
                position: pos,
                orientation: fw,
                coneInnerAngle: opts.coneInnerAngle == undefined ? 90 : opts.coneInnerAngle,
                coneOuterAngle: opts.coneOuterAngle == undefined ? 125 : opts.coneOuterAngle,
                coneOuterGain: opts.coneOuterGain == undefined ? 0 : opts.coneOuterGain
            });
            this.alignPanner = VOID;
            // TODO: how to handle chain

            this.setFrequency = (x)=> {
                this.index = x;
                if (this.filter) this.filter.index = x;
            }

            this.setVolume = (alpha)=> {
                this.volume = alpha/SOUND_INTENSITY_MAX; // all other senses are relative to one-dimensional measure i.e. Sound
            }

            this.setFilterRadius = (v)=> {
                if (!this.filter) return;
                this.filter.radius = v;
            }

            this.setFlux = VOID;

            this.setIF = VOID;

            this.start = this.stop = VOID;

            this.applyFilter = (center_vec, point_vec)=> {
                this.filter.apply(this.context, this.volume, this.node.width, this.node.height, center_vec, point_vec);
            }

            this.resetContext = ()=> {
                this.context.clearRect(0,0,this.node.width,this.node.height);
                this.context.drawImage(this.buffer,0,0,this.node.width,this.node.height);
            }

            function VisualFilter(x,radius) {
                this.index = x || 0;
                this.radius = radius || 1;

                this.apply = (context, alpha, w, h, center_vec,point_vec)=> {
                    let img_data = context.getImageData(0,0,w,h);
                    let idx = this.index,
                        x_ = Math.floor(idx%w),
                        y_ = Math.floor(idx/w),
                        hw = Math.floor(w/2),
                        hh = Math.floor(h/2),
                        max_dist = Math.sqrt(hw*hw+hh*hh)*(this.radius || 1), // adding (|| 1) would mean nothing means all
                        x = x_-hw,
                        y = -(y_-hh);
                    let a = [];
                    center_vec.set(x,y,0);
                    for (let j = 0, i = 0; j < img_data.data.length; j+=4, i++) {
                        if (img_data.data[j+3] > 0) {
                            let px_ = Math.floor(i%w),
                                py_ = Math.floor(i/w),
                                px = px_-hw,
                                py = -(py_-hh);
                            point_vec.set(px,py,0);
                            let v = 0;
                            if (center_vec.distanceTo(point_vec) <= max_dist) v = Math.round(img_data.data[j+3]*alpha);
                            a.push({v,alpha,max_dist,hw,hh,x,y,point_vec,dist:center_vec.distanceTo(point_vec)});
                            img_data.data[j+3] = v;
                        }
                    }
                    // console.log(a);
                    context.putImageData(img_data,0,0);
                }
            }
        }

        function Voice(system, context, player, grand_conductor, grand_compound, conductor) {

            let ct = context.sound.currentTime;

            this.simulationId = system.itid || system.simulationId;
            this.simCoords = system.simCoords.slice(0);
            this.context = context;
            this.frequency = system.frequency;
            this.IF = system.IF;
            this.period = system.period;
            this.position = system.position;
            this.forward = system.forward;
            this.up = system.up;
            this.duration = system.duration;
            this.intensity = system.intensity || 0;
            this.cycle = 0;
            this.mesh = system.mesh;
            this.conductor = conductor;
            this.superposition = system.superposition;
            this.chorusFundamental = system.chorusFundamental;
            this.illusory = system.illusory;
            this.asPure = system.asPure;
            this.automation = system.automation;

            if (!player) {
                this.channel = system.channel;
                player = this;
            } else if (!grand_conductor) {
                // As Tosoto, the grand conductor will only project itself
                    // as much as the Player is present. The Player's eyes
                    // become its own eyes. Here, I capture the philosophy
                    // that only what is actively observed is rendered in
                    // reality. The specifics are as follows.
                grand_conductor = this;
                this.channel = player.channel;
            } else if (!grand_compound) {
                grand_compound = this;
                this.channel = context.sound.createGain();
                this.channel.gain.setValueAtTime(this.intensity, ct);
                this.channel.connect(grand_conductor.channel);
            } else {
                this.channel = conductor.channel;
            }

            this.initial = {
                frequency: this.frequency,
                intensity: this.intensity,
                position: this.position.clone(),
                forward: this.forward.clone(),
                up: this.up.clone()
            }

            this.scale = {
                frequency: 1,
                intensity: 1
            }

            // A Head to ponder with and guide the hand. Intuition.
            this.head = {
                question: new ADAP(),
                answer: new ADAP()
            }

            this.domain = VOICE_DOMAIN_AUDITORY; // Sound is the default

            if (system.orchestra) {
				this.quantum = system.quantum;
                this.signal = system.avgPureFreq;
                this.orbit = Math.PI*Math.pow(system.width/2, 2); // NOTE: width, here, is actually the number of compounds within
                this.width = system.width;
                this.depth = system.depth;
                this.systemType = system.themeType;
                this.virtuality = new ADAP();

                this.orchestra = [];

                // Consider Uni(t)sex tree formation
                if (this === player) {
                    this.orchestra.push( new Voice(system.orchestra[0], context, player, grand_conductor, grand_compound, this) );
                } else { // 6
                    if (this.duration === 0) this.duration = conductor.duration; // 0 === align with the conductor

                    // A Hand with six fingers.
                    // Might leave this as is. Gives the Player a way
                        // to manipulate voices alongside the grand
                        // theme.
                    // With one hand, the time for one chorus member is shared with
                        // the corresponding member on the other side.
                    // TODO: the frequency played by the Hand should
                        // be input into the core of the Voice being
                        // manipulated in change format.
                    this.hand = new Voice({
                        simulationId: this.simulationId+'-'+HAND,
                        simCoords: this.simCoords.slice(0),
                        aspectType: HAND,
                        frequency: SOUND_MIN_FREQUENCY,
                        intensity: 0,
                        position: this.position.clone(),
                        forward: this.forward.clone(),
                        up: this.up.clone(),
                        IF: this.IF,
                        // mesh: TODO
                        superposition: this.superposition,
                        oscillator: {
                            // coneInnerAngle: 360, // hmm...
                            // coneOuterAngle: 360, // hmm...
                            bare: true // otherwise flux, ifo, etc setup causes a lingering sound
                        }
                    }, context, player, grand_conductor, this, this);

                    this.responses = [{},{},{},{},{},{}];

                    // A Satellite to project its will. A clone of sorts.
                    // Represents a deep clone of the system.
                    this.satellite = new Voice({
                        simulationId: this.simulationId+'-'+SATELLITE,
                        simCoords: this.simCoords.slice(0),
                        aspectType: SATELLITE,
                        frequency: SOUND_MIN_FREQUENCY,
                        intensity: 0,
                        position: this.position.clone(),
                        forward: this.forward.clone(),
                        up: this.up.clone(),
                        IF: this.IF,
                        // mesh: TODO
                        superposition: this.superposition,
                        oscillator: {
                            // coneInnerAngle: 360, // hmm...
                            // coneOuterAngle: 360, // hmm...
                            // MAYBE satellites should gain the flux, ifo, and depth of the system
                            bare: true // but flux, ifo, etc setup causes a lingering sound
                        }
                    }, context, player, grand_conductor, this, this);

                    this.satellite.orbitalRadius = 0;

                    for (let ci = 1; ci < 7; ++ci) {
                        this.orchestra.push( new Voice(system.orchestra[ci], context, player, grand_conductor, grand_compound, this) );
                    }
                }
            } else {
                this.systemType = system.aspectType;

                let osc_opts = {
                    context: context.sound,
                    position: this.position,
                    forward: this.forward,
                    up: this.up,
                    IF: this.IF,
                    chain: [this.channel],
                    empty: this.illusory
                }

                if (system.brane) {
                    switch (system.brane.type) {
                        case BRANE_TYPE_AUDIO:
                            // TODO: SOT can dictate the max buffer length; characterLength -> bufferLength
                            if (this.duration === 0) this.duration = system.brane.buffer.duration; // 0 == align with the brane; convenience
                            osc_opts.source = {buffer:system.brane.buffer, duration:this.duration}

                            if (system.oscillator) Object.assign(osc_opts, system.oscillator); // redundant
                            this.oscillator = new Oscillator(osc_opts);
                            break;
                        case BRANE_TYPE_IMAGE:
                            if (this.duration === 0) {
                                this.duration = system.brane.buffer.width = system.brane.buffer.height = Math.range(system.brane.buffer.width,1,this.context.vision.characterLength()); // 0 == align with the brane; convenience
                                this.duration = Math.sqrt(this.duration);
                            } else {
                                // NOTE: TODO: SOT can dictate the max duration (size and buffer length)
                                let sq = this.duration*this.duration;
                                this.duration = Math.sqrt( Math.range(sq,system.brane.buffer.width,this.context.vision.characterLength()) );
                                system.brane.buffer.width = system.brane.buffer.height = sq;
                            }
                            osc_opts.source = {buffer:system.brane.buffer, width:system.brane.buffer.width, height:system.brane.buffer.height}; // square (only?)

                            // TODO: how to handle chain
                            this.oscillator = new Oculomotor(osc_opts);
                            this.oscillator.context.getFrequency = VISION_CONTEXT.getPosition;
                            this.domain = VOICE_DOMAIN_VISUAL;
                            this.drawInfo = {}
                            break;
                        default:

                    }
                } else {
                    if (this.duration === 0) this.duration = conductor.duration; // 0 == align with the conductor

                    if (system.oscillator) Object.assign(osc_opts, system.oscillator);
                    this.oscillator = new Oscillator(osc_opts);
                    this.oscillator.setFrequency(this.initial.frequency, ct);
                    this.oscillator.setVolume(this.initial.intensity, ct);
                }
            }

            if (system.leftChorus) {
                this.leftChorus = [];
                let c;
                for (let ci = 1; ci < 7; ++ci) {
                    c = system.leftChorus[ci];
                    this.leftChorus.push(new Voice({
                            simulationId: this.simulationId+'-Left Chorus-'+(ci-1),
                        simCoords: this.simCoords.slice(0),
                        aspectType: 'Chorus',
                        frequency: c.frequency, // this should be calculated with each thought
                        duration: c.duration,
                        intensity: c.intensity/6, // this should be a sixth of the current intensity during thought
                        position: c.position.clone(), // max position; will tend to this with M->J increase
                        forward: c.forward.clone(), // same
                        up: c.up.clone(), // same
                        mesh: c.mesh,
                        illusory: true
                    }, context, player, grand_conductor, this, this));
                }
            }

            if (system.rightChorus) {
                this.rightChorus = [];
                let c;
                for (let ci = 1; ci < 7; ++ci) {
                    c = system.rightChorus[ci];
                    this.rightChorus.push(new Voice({
                            simulationId: this.simulationId+'-Right Chorus-'+(ci-1),
                        simCoords: this.simCoords.slice(0),
                        aspectType: 'Chorus',
                        frequency: c.frequency, // this should be calculated with each thought
                        duration: c.duration,
                        intensity: c.intensity/6, // this should be a sixth of the current intensity during thought
                        position: c.position.clone(), // max position; will tend to this with M->J increase
                        forward: c.forward.clone(), // same
                        up: c.up.clone(), // same
                        mesh: c.mesh,
                        illusory: true
                    }, context, player, grand_conductor, this, this));
                }
            }

            this.fluxDepth = 0;
            this.fluxPivot = 0;
            this.setFlux = (s, e, ct = this.context.sound.currentTime)=> {
                // bring to [0,1]
                s = (s+1)/2;
                e = (e+1)/2;
                let d = (e-s)/2;// span midpoint
                let depth = Math.abs(d),
                    // flux = (d+s+1)/2;
                    flux = Math.abs((e+s)/2);

                if ((this.fluxDepth === depth) && (this.fluxPivot === flux)) return;

                this.fluxDepth = depth;
                this.fluxPivot = flux;

                if (this.oscillator) this.oscillator.setFlux(depth, flux, ct);

                // perhaps the chorus oscillator should adopt the flux and depth of the theme
                // if (this.hand) this.hand.setFlux(s,e,ct);

                // If sateliites are eventually able to fluctuate on their own.
                // if (this.satellite) this.satellite.setFlux(s,e,ct);
            }

            this.setCoverFilterRadius = (v, ct = this.context.sound.currentTime)=> {
                this.coverFilterRadius = v;
                if (this.oscillator) this.oscillator.setFilterRadius(v, ct);
            }

            this.setIF = (IF)=> {
                this.IF = IF;
                if (this.oscillator) this.oscillator.setIF(IF);
                if (this.satellite) this.satellite.setIF(IF);
                for (let k in this.rogues) this.rogues[k].setIF(IF);
            }

            this.think = ()=> {
                if (!this.hand) return;

                // NOTE: if I could get away with two hands per Conductor...

                this.orchestra.forEach((c, ci) => {
                    let div = c.head.question.diversity();
                    let candidates = c.leftChorus.concat(c.rightChorus).map((chor, chidx) => {
                        return {
                            value: chor.frequency,
                            index: chidx
                        }
                    });
                    let chosen = this.head.answer.elect({elements: candidates, target: div}); // considers other constituents when answering; speaks in tongues
                    this.responses[ci].index = chosen.index%6; // speaking in orients
                    // console.log(div,ci,chosen,c);
                });
            }

            // Who will the hand mimic? I can smell the conductor in the responses.
            this.respond = (ct = this.context.sound.currentTime)=> {
                // Delegated to the ferry as it requires knowing the mod quad values.
            }

            this.start = (ct = this.context.sound.currentTime)=> {
                this.startTime = ct;

                if (this.hand) this.hand.start(ct);
                if (this.satellite) this.satellite.start(ct);

                if (this.oscillator) this.oscillator.start(ct);

                // this.onOrchestraFrequencyChange(ct);
                this.think();
            }

            this.stop = ()=> {
                if (this.hand) this.hand.stop();
                if (this.satellite) this.satellite.stop();
                for (let k in this.rogues) this.stopRogue(k);

                if (this.oscillator) this.oscillator.stop();
            }

            this.setFrequency = (freq, ct = this.context.sound.currentTime)=> {
                // When dealing with Visual Voices, the Oculomotor's frequency is
                    // determined by the VISION_CONTEXT's getFrequency. The Voice's
                    // facing frequency will be determined by the SOUND_CONTEXT,
                    // because any auxiliary Voice of a domain is always Sound-based.
                    // Other future domain Voices will be treated similarly to Visual Voices.

                freq*=this.scale.frequency;

                // This will ensure that themes and compounds keep the original frequency.
                let osc_freq = this.oscillator ? this.oscillator.context.getFrequency(freq,this.duration/*2D Visual*/) : freq;
                // NOTE: Keep consistent in case curriculum does some black magic. I'm expecting setFrequency to be strictly deterministic and domain-consistent, but who knows?
                freq = this.domain === VOICE_DOMAIN_AUDITORY ? osc_freq : this.context.sound.getFrequency(freq);
                this.frequency = freq;

                if (this.oscillator) {
                    this.oscillator.setFrequency(osc_freq, ct);
                }

                if ((freq < 20) || (freq > 20000)) freq = SOUND_MIN_FREQUENCY;
                this.head.question.elect({elements:[freq]});

                // SOT: As it is in `getJourney`
                this.period = 1000/this.frequency;

                if (this.leftChorus) {
                    this.leftChorus.forEach((c, idx) => {
                        // SOT: As it is in `getJourney`
                        // NOTE: I expect setFrequency to be strictly deterministic and domain-consistent, but who knows?
                        c.frequency = this.frequency*Math.pow(this.chorusFundamental, -idx/6);
                    });
                }
                if (this.rightChorus) {
                    this.rightChorus.forEach((c, idx) => {
                        // SOT: As it is in `getJourney`
                        // NOTE: I expect setFrequency to be strictly deterministic and domain-consistent, but who knows?
                        c.frequency = this.frequency*Math.pow(this.chorusFundamental, idx/6);
                    });
                }

                // if (this.conductor) this.conductor.onOrchestraFrequencyChange(ct);
            }

            this.setIntensity = (intst, ct = this.context.sound.currentTime)=> {
                intst*=this.scale.intensity;
                this.intensity = intst;
                if (this.oscillator) this.oscillator.setVolume(intst, ct);

                let sixth = this.intensity/6;
                if (this.leftChorus) {
                    this.leftChorus.forEach(c => {
                        c.intensity = sixth;
                    });
                }
                if (this.rightChorus) {
                    this.rightChorus.forEach(c => {
                        c.intensity = sixth;
                    });
                }

                // if (this.conductor &&  !auxiliary) this.conductor.onOrchestraIntensityChange(ct);
            }

            this.onOrchestraFrequencyChange = (ct)=> {
                if (this.satellite) {
                    // SOT: As it is in `getJourney`
                    this.signal = this.signalIntensity = 0;
                    this.orchestra.forEach(c => {
                        if (c.orchestra) {
                            this.signal+=c.signal;
                            this.signalIntensity+=c.intensity;
                            return;
                        }
                        this.signal+=c.frequency;
                        this.signalIntensity+=c.intensity;
                    });
                    this.signal/=6;
                    this.signal+=this.frequency;
                    this.signal/=2;
                    this.signalIntensity+=this.intensity;
                    this.signalIntensity/=2;
                    // NOTE: I'm expecting setFrequency to be strictly deterministic and domain-consistent.
                    this.satellite.setFrequency(this.signal, ct);
                    this.satellite.setIntensity(this.signalIntensity, ct);
                }
            }

            this.rogues = {}

            // Rogues represent debris from the system.
            // Only systems with finite lifespans should use this.
            // MAYBE: this could go to the depths if a theme.
            // This is where theme arrows are really useful.
            this.createRogue = (ct = this.context.sound.currentTime)=> {

                let parts = [{
                    startTime: this.startTime,
                    duration: this.duration
                }];
                this.orchestra.forEach(v => {
                    parts.push({
                        startTime: v.startTime,
                        duration: v.duration
                    });
                });

                let rogue = {
                    id: this.simulationId+'-'+uuid()+'-'+ROGUE,
                    voiceId: this.simulationId,
                    speed: this.period,
                    forward: this.forward,
                    position: this.position,
                    parts: parts
                }

                let voice = new Voice({
                    simulationId: rogue.id,
                    simCoords: this.simCoords,
                    aspectType: ROGUE,
                    frequency: SOUND_MIN_FREQUENCY,
                    intensity: 0,
                    position: this.position.clone(),
                    forward: this.forward.clone(),
                    up: this.up.clone(),
                    IF: this.IF,
                    oscillator: {
                        bare: true, /// !!!
                        // coneInnerAngle: 360,
                        // coneOuterAngle: 360,
                        coneOuterGain: 0.6
                    }
                }, this.context, true, true, this, this);

                voice.start(ct); // here?

                this.rogues[rogue.id] = voice;

                return { voice: voice, rogue: rogue };
            }

            this.getRogue = (id)=> {
                return this.rogues[id];
            }

            this.stopRogue = (id)=> {
                this.rogues[id].stop();
                delete this.rogues[id];
            }

            this.reset = (ct = this.context.sound.currentTime)=> {
                if (this.oscillator) this.oscillator.alignPanner();
                if (this.satellite) this.satellite.orbitalRadius = 0;

                this.think();
                // this.onOrchestraFrequencyChange(ct);

                this.cycle++;
            }
        }

        const channels = [];
        this.channels = channels;
        let currentSolo;
        // Setup functions for manipulating channels
        const createChannel = ()=> {
            let ch = SOUND_CONTEXT.createGain();
            ch.connect(this.transferReverb);
            let channel = {
                node: ch,
                dependents: [],
                intensity: (v)=> {
                    if (v == undefined) {
                        return ch.gain.value;
                    } else {
                        ch.gain.setValueAtTime(v, SOUND_CONTEXT.currentTime);
                    }
                },
                solo_: false,
                solo: (b)=> {
                    if (b == undefined) {
                        return channel.solo_;
                    } else {
                        if (channel.solo_ === b) return;

                        if (b) {
                            if (currentSolo) {
                                currentSolo.solo_ = false;
                                currentSolo = null;
                            }

                            for (let chi = 0; chi < 6; ++chi) {
                                let ch = channels[chi];
                                if (ch === channel) continue;
                                ch.mute(true);
                            }

                            channel.mute(false);
                            currentSolo = channel;
                        } else {
                            currentSolo = null;

                            for (let chi = 0; chi < 6; ++chi) {
                                let ch = channels[chi];
                                if (ch === channel) continue;
                                ch.mute(false);
                            }
                        }

                        channel.solo_ = b;
                    }
                },
                mute_: false,
                muteValue: ch.gain.value,
                mute: (b)=> {
                    if (b == undefined) {
                        return channel.mute_
                    } else {
                        if (channel.mute_ === b) return;

                        if (b) {
                            for (let chi = 0; chi < 6; ++chi) {
                                let ch = channels[chi];
                                if (ch === channel) continue;
                                ch.mute(false);
                            }
                            channel.muteValue = channel.intensity();
                            channel.intensity(0);
                        } else {
                            channel.intensity(channel.muteValue);
                        }
                        channel.mute_ = b;
                    }
                }
            }

            return channel;
        }

        this.setChannelIntensity = async (ch,val)=> {
            if (time_ == null) return; // TODO: also return for single aspect layout
            let c = this.layout.orchestra[ch];
            c.channel.gain.setValueAtTime(val, SOUND_CONTEXT.currentTime);
            automationWorker.postMessage({
                type: JOB_TYPE_MODIFY,
                intensity: val,
                coordinates: c.simCoords
            });
        }

        // If the Player "dies" the "system dies with him."
        // Here I've captured the philosophy that the universe exists only in
            // the mind of the Player. Such statement is too vague to be useful
            // in transition. The universe which is dreamed, lives alongside the
            // universe which is supposedly real. The Player as the environment.
            // The universe stems from the Player. To be dead is to cut off all
            // mediums of sensation. The decision to live lies with the Player.
        this.volume = (vol)=> {
            if (time_ == null) return; // TODO: also return for single aspect layout

            if (vol == undefined) {
                return this.output.gain.value;
            }

            vol = Number(vol);

            this.output.gain.setValueAtTime(vol, SOUND_CONTEXT.currentTime);

            vol*=SOUND_INTENSITY_MAX;

            automationWorker.postMessage({
                type: JOB_TYPE_MODIFY,
                intensity: vol,
                coordinates: this.player.simCoords
            });

            automationWorker.postMessage({
                type: JOB_TYPE_MODIFY,
                intensity: vol,
                coordinates: this.layout.simCoords
            });
        }

        this.getVoice = (coords)=> {
            let v, orc = [this.player];
            coords.forEach(coord => {
                v = orc[coord];
                orc = v.orchestra;
            });

            return v;
        }

        this.traverseVoices = (v, omnicb, vcb, orcb, oscb, scb, hcb, rcb, pcb)=> {
            let cont;
            if (omnicb) {
                let opts = omnicb(v);
                orcb = opts.orchestraCallback;
                oscb = opts.oscillatorCallback;
                scb = opts.satelliteCallback;
                hcb = opts.handCallback;
                rcb = opts.roguesCallback;
                pcb = opts.postCallback;
                cont = !opts.stop;
            } else {
                vcb = vcb || VOID;
                cont = !vcb(v);
            }

            if (orcb && v.orchestra) {
                let cb = orcb();
                v.orchestra.forEach(c => cb(c));
            }
            if (oscb && v.oscillator) oscb(v.oscillator);
            if (scb && v.satellite) scb(v.satellite);
            if (hcb && v.hand) hcb(v.hand);
            if (rcb && v.rogues) {
                for (let k in v.rogues) rcb(voices.get(k));
            }
            if (pcb) pcb(v);
            if (cont && v.orchestra) {
                v.orchestra.forEach(c => this.traverseVoices(c, omnicb, vcb, orcb, oscb, scb, hcb, rcb));
            }
        }

        // Can only reload surface themes.
        this.updateProcess = (coords, proc)=> {
            if ( (time_ == null) || !this.journey.orchestra ) return;

            let voice = this.getVoice(coords);
            if (!voice) return;

            let p = {
                name: proc.name,
                raw: proc.raw
            }

            processWorker.postMessage({
                type: JOB_TYPE_RELOAD,
                id: voice.simulationId,
                process: p
            });
        }

        function Release(begin,dest,fb) {
            this.begin = begin;
            this.destination = dest;
            this.fb = fb || 0;
            this.amp = 0; // used for kaba filters

            // SOT
            this.lerp = (gtime)=> {

                let time;
                let px,py,pz;
                let ux,uy,uz;
                let fx,fy,fz;
                let depart_endt = this.fb/2,
                    sustain_endt = 1-this.fb/2;

                if (gtime < depart_endt) {
                    time = Math.exp(gtime)/Math.E*gtime;
                    px = (this.destination.position.x-this.begin.position.x)*depart_endt*time+this.begin.position.x;
                    py = (this.destination.position.y-this.begin.position.y)*depart_endt*time+this.begin.position.y;
                    pz = (this.destination.position.z-this.begin.position.z)*depart_endt*time+this.begin.position.z;

                    ux = (this.destination.up.x-this.begin.up.x)*depart_endt*time+this.begin.up.x;
                    uy = (this.destination.up.y-this.begin.up.y)*depart_endt*time+this.begin.up.y;
                    uz = (this.destination.up.z-this.begin.up.z)*depart_endt*time+this.begin.up.z;

                    fx = (this.destination.forward.x-this.begin.forward.x)*depart_endt*time+this.begin.forward.x;
                    fy = (this.destination.forward.y-this.begin.forward.y)*depart_endt*time+this.begin.forward.y;
                    fz = (this.destination.forward.z-this.begin.forward.z)*depart_endt*time+this.begin.forward.z;
                } else if (gtime < sustain_endt) {
                    px = (this.destination.position.x-this.begin.position.x)*sustain_endt*gtime+this.begin.position.x;
                    py = (this.destination.position.y-this.begin.position.y)*sustain_endt*gtime+this.begin.position.y;
                    pz = (this.destination.position.z-this.begin.position.z)*sustain_endt*gtime+this.begin.position.z;

                    ux = (this.destination.up.x-this.begin.up.x)*sustain_endt*gtime+this.begin.up.x;
                    uy = (this.destination.up.y-this.begin.up.y)*sustain_endt*gtime+this.begin.up.y;
                    uz = (this.destination.up.z-this.begin.up.z)*sustain_endt*gtime+this.begin.up.z;

                    fx = (this.destination.forward.x-this.begin.forward.x)*sustain_endt*gtime+this.begin.forward.x;
                    fy = (this.destination.forward.y-this.begin.forward.y)*sustain_endt*gtime+this.begin.forward.y;
                    fz = (this.destination.forward.z-this.begin.forward.z)*sustain_endt*gtime+this.begin.forward.z;
                } else { // arrival
                    time = Math.exp(gtime)/Math.E*gtime;
                    px = (this.destination.position.x-this.begin.position.x)*time+this.begin.position.x;
                    py = (this.destination.position.y-this.begin.position.y)*time+this.begin.position.y;
                    pz = (this.destination.position.z-this.begin.position.z)*time+this.begin.position.z;

                    ux = (this.destination.up.x-this.begin.up.x)*time+this.begin.up.x;
                    uy = (this.destination.up.y-this.begin.up.y)*time+this.begin.up.y;
                    uz = (this.destination.up.z-this.begin.up.z)*time+this.begin.up.z;

                    fx = (this.destination.forward.x-this.begin.forward.x)*time+this.begin.forward.x;
                    fy = (this.destination.forward.y-this.begin.forward.y)*time+this.begin.forward.y;
                    fz = (this.destination.forward.z-this.begin.forward.z)*time+this.begin.forward.z;
                }

                return {
                    position: {x:px,y:py,z:pz},
                    up: {x:ux,y:uy,z:uz},
                    forward: {x:fx,y:fy,z:fz}
                };
            }

        }

        this._alignWithVoice = (v)=> {
            this.listener.position(v.position.x,v.position.y,v.position.z);
            this.listener.forward(v.forward.x,v.forward.y,v.forward.z);
            this.listener.up(v.up.x,v.up.y,v.up.z);
        }

        // If an oscillator is outside the radius, set it's output to a value relative to
            // its deviation from the radius. Otherwise, set it's output to 1.
        function radiusAdjustment(pos, list_pos, diam) {
            let dist = pos.distanceTo(list_pos);
            if (dist > diam) {
                return Math.max(0, 1-Math.abs(diam-dist)/diam)
            }
            return 1;
        }

        async function loadCharacters(chars) {
            let char;
            for (let i = 0; i < chars.length; ++i) {
                char = chars[i];
                char.data = (await loadImage(char.url)).buffer;
            }
        }

        const stats = new Stats();
        stats.showPanel(1);
        this.stats = stats;
        const itinerary = ()=>{return this.journey.itinerary}
        let cjidx = 0;
        let now, stamp, fmstamp, dt, total = 0, fmdt, ct, duration, vstamp;
        let covered, travel;
        let cont;
        let release, currvecs;
        let list_pos, next, prev;
        let frame, chars = [], vision_canvas = document.createElement('canvas'), vision_context = vision_canvas.getContext('2d');
        vision_canvas.width = vision_canvas.height = SOT_CHARACTER_LENGTH_DEFAULT;
        vision_canvas.id = 'visionCanvas';
        document.getElementById('main-wrapper').appendChild(vision_canvas);
        const reuse_vec = new Vector();
        const pan_reuse_vec = new Vector();
        const pan_reuse_vec2 = new Vector();
        const pan_reuse_vec3 = new Vector();

        this.ferry = (ct)=> {
            if (!cont) return;

            stats.begin();

            // now = performance.now();
            now = ct;

            // TODO: set these in modify
            mf = this.voiceInfo.modQuad.mf.value;
            fm = this.voiceInfo.modQuad.fm.value;
            mb = this.voiceInfo.modQuad.mb.value;
            bm = this.voiceInfo.modQuad.bm.value;
            fb = this.voiceInfo.modQuad.fb.value;
            bf = this.voiceInfo.modQuad.bf.value;
            mj = this.voiceInfo.modQuad.mj.value;
            jm = this.voiceInfo.modQuad.jm.value;
            fj = this.voiceInfo.modQuad.fj.value;
            jf = this.voiceInfo.modQuad.jf.value;

            // TODO: move to own function for setting
                // where it will also update the total
            duration = this.player.duration;

            fm = Math.range(fm, 0, MAX_FM);

            if (this.playing) {
                fmdt = now-fmstamp; // NOTE: this will cause it to start from the previous stop if previously paused
                // fmdt/=1000;
                fmdt/=(1-fm)*duration; // NOTE: If FM === 1.0, time stops

                // Vision rate
                let VISION_RATE = 3;
                if (now - vstamp >= VISION_RATE) {
                    vstamp = now;
                    frame = true;
                }

                covered = fmdt/2;
                travel = cjidx+covered;
                time_ = travel*2*scale*duration;
            } else {
                fmstamp = now; // NOTE: this will cause it to start from the previous stop if previously paused
                vstamp = now; // NOTE: this will cause it to start from the previous stop if previously paused
            }

            dt = now-stamp;
            // dt/=1000;
            stamp = now;
            total+=dt;

            let list_pos = this.listener.position(),
                list_up = this.listener.up(),
                list_fw = this.listener.forward();

            this.traverseVoices(this.layout, (v)=> {
                // TODO: apply M->B Expectations and F->J Density to pures.
                    // Will need to scale with B->F radius on Oscillator.output.

                let tphase = total*Math.PI*2,
                    sine = Math.sin(tphase);

                // M->F Oscillation & F->M Quantization
                reuse_vec.copy(v.superposition).multiplyScalar(sine*Math.exp(mf)*mf*2);
                v.position.copy(v.initial.position).add(reuse_vec).multiplyScalar(scale);
                // if (v.leftChorus) v.leftChorus.forEach(c => c.position.copy(c.initial.position).add(reuse_vec).multiplyScalar(scale));
                // if (v.rightChorus) v.rightChorus.forEach(c => c.position.copy(c.initial.position).add(reuse_vec).multiplyScalar(scale));

                // B->F Radius
                v.scale.intensity = radiusAdjustment(v.position, list_pos, diameter);

                // set the Kaba filter Q
                v.setCoverFilterRadius(release.amp, ct);

                if ((frame === true) && (v.domain === VOICE_DOMAIN_VISUAL)) {
                    v.oscillator.applyFilter(pan_reuse_vec,pan_reuse_vec2);
                    v.drawInfo.url = v.oscillator.node.toDataURL();
                    let pan_res = v.oscillator.panner.pan(list_pos,list_up,list_fw, pan_reuse_vec,pan_reuse_vec2,pan_reuse_vec3);
                    Object.assign(v.drawInfo, pan_res);
                    chars.push(v.drawInfo);
                    v.oscillator.resetContext();
                }

                // *TODO: update frequency in compute-itinerary (also pures because models); use bmADAP

                return {
                    orchestraCallback: ()=> {
                        // J->F Virtuality
                        let dp = v.position.distanceTo(list_pos);
                        let odiv = v.virtuality.diversity();
                        // v.virtuality.elect({elements: [time_%this.total.time/this.total.time, dp/this.journey.radius, jf]});
                        v.virtuality.elect({elements: [total /* OR? (sine+1)/2 */, dp, jf]});
                        let div = v.virtuality.diversity();
                        div = div > 1 ? 1 : div;
                        let pivot = div-odiv;
                        let left = pivot-div/2,
                            right = pivot+div/2;
                        let absl = Math.abs(left);
                        if (absl > 1) {
                            right+=absl-1;
                            left = -1;
                        }
                        if (right > 1) {
                            left-=right-1;
                            right = 1;
                        }

                        return (c)=> {
                            c.setFlux(left*jf, right*jf, ct);
                        }
                    },
                    oscillatorCallback: (osc)=> {
                        osc.alignPanner();
                    },
                    satelliteCallback: (sat)=> {
                        // J->M Satellite
                        sat.oscillator.panner.coneOuterGain = jm;
                        // sat.panner.coneInnerAngle = sat.panner.coneOuterAngle = 360;
                        // sat.panner.coneInnerAngle = sat.panner.coneOuterAngle = 90+270*jm;
                        let or = sat.orbitalRadius,
                            phase = tphase*v.frequency; // WHAT????? The frequency of the satellite orbit changes with the voice. Woah this.physics
                        let x = Math.sin(phase)*or,
                            y = Math.cos(phase)*or;
                        sat.position.set(x,y,0).applyMatrix4(v.mesh.matrixWorld);
                        sat.position.add(reuse_vec).multiplyScalar(scale); // M->F Oscillation & F->M Quantization
                        sat.oscillator.alignPanner();
                        sat.orbitalRadius += fmdt*v.orbit*jm;

                        // B->F Radius
                        sat.scale.intensity = jm*radiusAdjustment(sat.position, list_pos, diameter);
                    },
                    handCallback: (hand)=> {
                        // M->J Tintinnabulation|Arpeggiation
                        // SOT: the number of cycles thorugh the 12 personas within the range
                            // of the start time to decay is equal to the period.
                            // The hand moves plays the two sides of a Voice before moving on
                            // to the next.
                        let range = v.duration,
                            interval = range/v.period,
                            pid = (ct-v.startTime)/interval, // period number
                            st = Math.floor(pid)*interval, // start time of this period
                            et = Math.floor(pid+1)*interval,
                            esr = et-st,
                            esr_interval = esr/(PERSONAS_PER_SIDE*2),
                            esr_id = (ct-st)/esr_interval,
                            idx = Math.floor(esr_id), // the child
                            idx_ = idx%PERSONAS_PER_SIDE;
                        let choridx = v.responses[idx_].index;
                        let c = v.orchestra[idx_];
                        let persona;
                        if (idx%2 === 0) {
                            // left side with idx%PERSONAS_PER_SIDE
                            persona = c.leftChorus[choridx];
                        } else {
                            // right side
                            persona = c.rightChorus[choridx];
                        }

                        hand.position.lerpVectors(c.initial.position, persona.initial.position, mj);
                        // M->F Oscillation & F->M Quantization
                        hand.position.add(reuse_vec).multiplyScalar(scale);

                        // B->F Radius
                        hand.scale.intensity = mj*radiusAdjustment(hand.position, list_pos, diameter);

                        hand.oscillator.alignPanner();

                        // The one who is chosen to advise the Voice, harmonizes with it.
                        hand.setIntensity(persona.intensity);
                        hand.setFrequency(persona.frequency);
                    },
                    roguesCallback: (r)=> {
                        // M->F Oscillation & F->M Quantization
                        if (r.needsUpdate) {
                            r.position.add(reuse_vec).multiplyScalar(scale);
                            r.oscillator.alignPanner();
                            r.needsUpdate = false;
                        }

                        // B->F Radius
                        r.scale.intensity = radiusAdjustment(r.position, list_pos, diameter);
                    },
                    postCallback: (v)=> {
                        // Update frequencies in Network
                        this.journey.rawItinerary.planned[v.simulationId].value = v.frequency;
                    },
                    stop: false
                }
            });

            this.traverseVoices(this.layout, null, (voice)=> {

                if ((ct - voice.startTime) < voice.duration) return false;

                voice.startTime = ct;

                if (voice.orchestra) {
                    voice.orchestra.forEach(v => {
                        if (v.orchestra) {
                            let rogue = v.createRogue(ct);
                            voices.set(rogue.voice.simulationId, rogue.voice);

                            rogueWorker.postMessage({
                                type: JOB_TYPE_ROGUE,
                                rogue: rogue.rogue
                            });
                            automationWorker.postMessage({
                                type: UNITY,
                                nodeId: rogue.voice.simulationId,
                                coordinates: rogue.voice.simCoords
                            });
                        }
                    });
                }

                voice.reset(ct);

                automationWorker.postMessage({
                    nodeId: voice.simulationId,
                    type: JOB_TYPE_RESET,
                    coordinates: voice.simCoords,
                    frequency: voice.frequency, // automation initial
                    intensity: voice.intensity,
                    period: voice.period,
                    systemType: voice.systemType,
                    currentTime: ct
                });

                return true;
            });

            // F->B Release
            release.fb = fb;

            if (this.playing) {
                if (frame && chars.length) {

                    // Sort and draw Visual Voices who are closer to the Player first.
                    chars.sort((a,b)=> {
                        let ad = a.distance,
                            bd = b.distance;
                        if (ad < bd) {
                            return -1;
                        } else if (bd < ad) {
                            return 1;
                        }
                        return 0;
                    });

                    let chars_ = chars.slice(0,chars.length);
                    frame = loadCharacters(chars_);

                    // DRAW CHARACTERS
                    frame.then(()=> {
                        vision_context.clearRect(0, 0, vision_canvas.width, vision_canvas.height);
                        chars_.forEach(cinfo => {
                            // vision_context.drawImage(cinfo.data, cinfo.vertical*vision_canvas.width, cinfo.horizontal*vision_canvas.height, cinfo.data.width*cinfo.scale, cinfo.data.height*cinfo.scale);
                            vision_context.drawImage(cinfo.data, cinfo.vertical*vision_canvas.width-cinfo.data.width/2, cinfo.horizontal*vision_canvas.height-cinfo.data.height/2, cinfo.data.width*cinfo.cone, cinfo.data.height*cinfo.cone);
                            // console.log(cinfo.vertical*vision_canvas.width, cinfo.horizontal*vision_canvas.height, cinfo.scale, cinfo.distance, cinfo.cone);
                            // console.log(cinfo);
                        });
                        // frame = vision_context.getImageData(0,0,vision_canvas.width,vision_canvas.height);
                    });

                    frame = null;
                    chars.length = 0;
                }

                this.dispatchEvent({ type: SIMULATION_EVENT_PROGRESS, message: {time:time_, total:Object.assign({},this.total), travel:travel} });

                // TODO: apply averaged M->F Superposition to current position.
                if (fmdt < 1) {
                    // currently transitioning

                    next = itinerary()[cjidx];
                    prev = itinerary()[cjidx-1] || itinerary()[itinerary().length-1];

                    // TODO: maybe resting position should be 100%
                        // so that the consonants are heard in full while resting
                    release.amp = prev.position.equals(next.position) ? 0 : list_pos.distanceTo(next.position)/prev.position.distanceTo(next.position);
                    let r = release.amp;
                    release.amp = Math.range(release.amp,0,1);
                    release.amp = release.amp < 0.5 ? release.amp/0.5 : (1-release.amp)/0.5;
                    // if (release.amp < 0) console.log(release.amp, r);
                    release.amp = Math.range(release.amp,0,1);

                    // move
                    currvecs = release.lerp(fmdt);
                    reuse_vec.copy(currvecs.position).multiplyScalar(scale);
                    this.listener.position(reuse_vec.x, reuse_vec.y, reuse_vec.z);
                    this.listener.up(currvecs.up.x, currvecs.up.y, currvecs.up.z);
                    this.listener.forward(currvecs.forward.x, currvecs.forward.y, currvecs.forward.z);

                } else if (fmdt < 2) {
                    // experiencing a junction
                    currvecs = itinerary()[cjidx];
                    reuse_vec.copy(currvecs.position).multiplyScalar(scale);
                    this.listener.position(reuse_vec.x, reuse_vec.y, reuse_vec.z);
                    this.listener.up(currvecs.up.x, currvecs.up.y, currvecs.up.z);
                    this.listener.forward(currvecs.forward.x, currvecs.forward.y, currvecs.forward.z);

                    // The longer the Player lingers at a system, the more the Grand Conductor will shape
                        // his world around that system.
                        // TODO: first associate a Junction with a Voice
                    // this.layout.core.frequency.update()
                } else {
                    // prepare for transition
                    fmstamp = now;

                    cjidx++;

                    if (cjidx === itinerary().length) {
                        cjidx = 0;

                        if (this.journey.processItinerary) {
                            // B->M Diversity
                            this.journey.processItinerary(bm, this.journey.rawItinerary, itinerary(), this.journey.bmADAP);
                        }

                        this.total.cycle++;
                    }

                    cjidx = cjidx%itinerary().length;
                    let s = itinerary()[cjidx],
                        e = itinerary()[cjidx+1] || itinerary()[0];

                    release.begin.position = s.position;
                    release.begin.up = s.up;
                    release.begin.forward = s.forward;

                    release.destination.position = e.position;
                    release.destination.up = e.up;
                    release.destination.forward = e.forward;

                    // console.log(cjidx,itinerary()[cjidx]);
                }
            }

            let n = this.journey.network.travel({diversity: bm, modelHigh: mb, modelLow: fj}, ct);
            // console.log(n);

            processWorker.postMessage({
                type: JOB_TYPE_LISTENER,
                listener: {
                    position: this.listener.position(),
                    forward: this.listener.forward(),
                    up: this.listener.up()
                }
            });

            stats.end();
        }
		
		function printVoice(v, n) {
            let res = {orchestra:[]}
            let curr_comp = v;
            let player = v.conductor;
            res.res_rep = res;
            res.childMark = 0;
            v.conductor = res;
            let c;

            function __domain(d) {
                switch(d) {
                    case VOICE_DOMAIN_VISUAL:
                        return 1;
                    default:
                        return 0;
                }
            }

            function __automation(a, t, acc) {
                let res = [];
                switch(t) {
                    case THEME_TYPE_SIGNAL:
                        acc.value += 1+4*2; // type, value, ratio
                        res.push( {type: 1, value: a.frequency.analogo.value, ratio: a.frequency.analogo.ratio} )
                        break;
                    case THEME_TYPE_VERSUS:
                        acc.value += (1+4*2)*3; // type, value, ratio * max # effects
                        for(let i = 0; i < 0; ++i) {
                            res.push({
                                
                            })
                        }
                        break;
                    case THEME_TYPE_CONTEXT:
                        acc.value += (1+4*2)*4; // type, value, ratio * max # effects
                        let arr = ['analogo', 'glissando', 'portamento', 'vibrato'];
                        for(let i = 0; i < arr.length; ++i) {
                            let fx = a.frequency[arr[i]];
                            if (fx) { // TODO: DELETE USELESS EFFECTS IN getJourney
                                res.push({
                                    type: i+1,
                                    value: fx.value,
                                    ratio: fx.ratio || fx.depth || fx.time || 0 // TODO: CALL ALL RATIO
                                });
                            }
                        }
                        break;
                    default:
                        break;
                }

                return res;
            }

            function __chorus(v) {
                let res = [];
                let c;
                for(let i = 0; i < 6; ++i) {
                    c = v.leftChorus[i];
                    res.push({
                        position: c.position.toArray(),
                        up: c.up.toArray(),
                        forward: c.forward.toArray()
                    });
                }
                for(let i = 0; i < 6; ++i) {
                    c = v.rightChorus[i];
                    res.push({
                        position: c.position.toArray(),
                        up: c.up.toArray(),
                        forward: c.forward.toArray()
                    });
                }

                return res;
            }

            function extract(v, acc) {
                let res = {
                    aspectType: v.orchestra ? 2 : 1,
                    domain: __domain(v.domain),
                    frequency: v.orchestra ? getRandomNumber(200, 10000) : v.frequency,
                    intensity: v.intensity,
                    drawl: v.duration,
                    bid: -1,
                    IF: v.IF,
                    position: v.orchestra ? v.asPure.position.toArray() : v.position.toArray(),
                    up: v.orchestra ? v.asPure.up.toArray() : v.up.toArray(),
                    forward: v.orchestra ? v.asPure.forward.toArray() : v.forward.toArray(),
                    superposition: v.superposition.toArray(),
                    chorusFundamental: v.chorusFundamental,
                    pid: -1,
                    automation: __automation(v.automation, v.conductor.systemType, acc),
                    chorus: __chorus(v)
                }

                if (v.orchestra) {
                    res.asConductor = {}
                    Object.assign(res.asConductor, {
                        themeType: v.systemType,
                        width: v.width,
                        depth: v.depth,
                        frequency: v.frequency,
                        quantum: v.quantum,
                        position: v.position.toArray(),
                        up: v.up.toArray(),
                        forward: v.forward.toArray(),
                        orbit: v.orbit,
                        matrix: v.mesh.matrixWorld.elements.map(e => { return e; } )
                    });
                }

                return res;
            }

            let acc = {value: 0}
            while(curr_comp && (curr_comp !== res)) {
                if (curr_comp.childMark == 6) {
                    delete curr_comp.childMark;
                    delete curr_comp.res_rep;
                    curr_comp = curr_comp.conductor;
                    continue;
                }

                if (!curr_comp.childMark) {
                    curr_comp.childMark = 0;
                    curr_comp.res_rep = {
                        orchestra:[],
                        // mo things of the voice
                    }
                    Object.assign(curr_comp.res_rep, extract(curr_comp, acc));
                    curr_comp.conductor.res_rep.orchestra[curr_comp.conductor.childMark++] = curr_comp.res_rep;
                }

                for (let ci = curr_comp.childMark; ci < 6; ++ci, curr_comp.childMark++) {
                    c = curr_comp.orchestra[ci];
                    if (c.orchestra) {
                        curr_comp = c;
                        break;
                    }
                    curr_comp.res_rep.orchestra[ci] = {}
                    Object.assign(curr_comp.res_rep.orchestra[ci], extract(c, acc));
                }
            }

            res.orchestra[0].automationSizeInBytes = acc.value;

            v.conductor = player;
            
            download(JSON.stringify(res.orchestra[0]), n, 'text/json');
            // console.log('PRINT', res.orchestra[0]);
		}

        this.play = (record)=> {
            return new Promise(async (resolve,reject)=> {
                if (!this.voice_) {
                    reject('Voice is missing.');
                    return;
                }
                if (this.playing) {
                    reject('Already playing.');
                    return;
                }
                if (this.stopping) {
                    console.warn('Tried to play while stopping. Will play after.');
                    this.playAfter = { resolve: resolve, reject: reject }
                }

                if (time_ == undefined) {
                    SOUND_CONTEXT = (opts.sound && opts.sound.context) ? opts.sound.context : new AudioContext();
                    // Why do I care about the main canvas? This should be the reuseable character canvas.
                    VISION_CONTEXT = (opts.vision && opts.vision.context) ? opts.vision.context : document.createElement('canvas').getContext('2d');
                    SOUND_CONTEXT.getFrequency = (v)=> { return this.curriculum.getFrequency(v) }
                    SOUND_CONTEXT.waveform = (v)=> { return this.curriculum.waveform(v) }
                    VISION_CONTEXT.getPosition = (v,w)=> { return this.curriculum.getPosition(v,w) }
                    VISION_CONTEXT.characterLength = (v)=> { return this.curriculum.characterLength }
                    vision_canvas.width = vision_canvas.height = VISION_CONTEXT.characterLength();
                    vision_context.clearRect(0, 0, vision_canvas.width, vision_canvas.height);

                    this.sound = SOUND_CONTEXT;
                    this.vision = VISION_CONTEXT;

                    resetListener();

                    // This is shared with the Player, if available
                    this.output = SOUND_CONTEXT.createGain();
                    // this.output.connect(SOUND_CONTEXT.destination);

                    this.reverb = new Freeverb(SOUND_CONTEXT, {
                        roomSize: 0,
                        dampening: 0,
                        wet: 1
                    });

                    this.transferReverb = new Freeverb(SOUND_CONTEXT, {
                        roomSize: TRANSFER_REVERB_ROOM_SIZE,
                        dampening: 0,
                        wet: 1
                    });

                    this.transferReverb.connect(this.reverb);
                    this.reverb.connect(SOUND_CONTEXT.destination);

                    this.output.connect(SOUND_CONTEXT.destination);

                    for (let chi = 0; chi < 7; ++chi) {
                        let channel = createChannel();
                        channel.node.connect(this.transferReverb);
                        this.channels.push(channel);
                    }

                    let mq = this.voiceInfo.modQuad || {
                        mf: {value:0},
                        fm: {value:0},
                        bj: {value:0},
                        jb: {value:0},
                        mb: {value:0},
                        bm: {value:0},
                        fb: {value:0},
                        bf: {value:0},
                        mj: {value:0},
                        jm: {value:0},
                        fj: {value:0},
                        jf: {value:0}
                    }

                    this.voiceInfo.modQuad = mq;

                    this.journeyId = guid();
                    // this.journey = await this.getJourney({ id: this.journeyId, voice: (await themes_map.get('85F67938D4A0393935A95CD51FDB7860')), type: VOICE_TYPE_THEME, modQuad: mq });
                    this.journey = await this.curriculum.getJourney({ id: this.journeyId, voice: this.voice_, type: this.type, modQuad: mq });
                    if (this.journey.id !== this.journeyId) {
                        reject('Journey is now expired.');
                        return;
                    }

                    console.log(this.journey);

                    let ct  = SOUND_CONTEXT.currentTime;

                    let vec = this.journey.itinerary[0].position;
                    this.listener.position(vec.x,vec.y,vec.z);
                    vec = this.journey.itinerary[0].forward;
                    this.listener.forward(vec.x,vec.y,vec.z);
                    vec = this.journey.itinerary[0].up;
                    this.listener.up(vec.x,vec.y,vec.z);

                    // Calculate the time it takes to travel the entire journey
                    // One second at a junction, one second to travel.
                    this.total.travel = this.journey.itinerary.length;
                    this.total.time = this.total.travel*2*(1-mq.fm.value)*this.journey.duration;
                    // this.total.time = this.total.travel*2;
                    this.total.cycle = 0;

                    if (this.journey.orchestra) {
                        let player = this.journey.player;
                        this.player = new Voice({
                            simulationId: player.itid,
                            themeType: DREAMER,
                            simCoords: player.simCoords.slice(0),
                            frequency: player.frequency,
                            intensity: player.intensity,
                            duration: player.duration,
                            orchestra: [this.journey],
                            position: this.listener.position(),
                            forward: this.listener.forward(),
                            up: this.listener.up(),
                            channel: this.output,
                            // mesh: TODO
                            period: player.period,
                            width: player.width,
                            depth: player.depth,
                            // These should be adopted along the way. Averaged in `processItinerary`
                            IF: player.IF,
                            superposition: player.superposition,
                        }, {sound: SOUND_CONTEXT, vision: VISION_CONTEXT});

                        processWorker.postMessage({
                            type: JOB_TYPE_PROCESS,
                            process: this.journey.simulation.process
                        });
                        automationWorker.postMessage({
                            type: JOB_TYPE_AUTOMATION,
                            automation: this.journey.simulation.automation,
                            currentTime: ct
                        });

                        this.layout = this.player.orchestra[0];
                        console.log(PLAYER, this.player);
                        console.log('Layout', this.layout);

                        printVoice(this.layout);

                        // Setup the reverbs
                        this.transferReverb.roomSize.setValueAtTime(TRANSFER_REVERB_ROOM_SIZE, ct);
                        // this.transferReverb.wet = TRANSFER_REVERB_WET;

                        // Also need this to set modQuad in ProcessWorker
                        Object.keys(mq).forEach(this.modify);
                    } else {
                        this.setChannelIntensity(6,this.journey.intensity);
                        let osc = new Oscillator({
                            context: SOUND_CONTEXT,
                            frequency: this.journey.frequency,
                            intensity: this.journey.intensity,
                            position: this.journey.position,
                            forward: this.journey.forward,
                            up: this.journey.up,
                            chain: [this.output]
                        });

                        // TODO: setup the Player

                        this.layout = new Voice({
                            oscillator: osc,
                            duration: this.journey.duration,
                            // fundamental: Infinity,
                        });

                        this.transferReverb.roomSize.setValueAtTime(0, ct);
                        this.transferReverb.dampening.setValueAtTime(0, ct);
                        this.transferReverb.wet = 0;

                        this.reverb.roomSize.setValueAtTime(0, ct);
                        this.reverb.wet = 0;
                    }

                    time_ = 0;
                    fmstamp = stamp = vstamp = ct;
                    cjidx = 0;
                    cont = true;
                    let s = this.journey.itinerary[0],
                        e = this.journey.itinerary[1] || s;
                    this.release = release = new Release({
                        position: s.position,
                        up: s.up,
                        forward: s.forward
                    }, {
                        position: e.position,
                        up: e.up,
                        forward: e.forward
                    }, this.voiceInfo.modQuad.fb.value);

                    if (record) {
                        this.recorder = new Recorder(this.reverb);
                        this.dispatchEvent({ type: SIMULATION_EVENT_RECORD_START });
                        this.recorder.record();
                        this.recording = true;
                    }

                    // Initialize Processes and Automations
                    processWorker.postMessage({
                        type: JOB_TYPE_LISTENER,
                        listener: {
                            initialPosition: this.listener.startPosition,
                            initialForward: this.listener.startForward,
                            initialUp: this.listener.startUp
                        }
                    });

                    this.traverseVoices(this.player, null, v => {
                        voices.set(v.simulationId, v);
                        v.start(ct);
                    });

                    let updateInterval = 0.03;
                    let blob = new Blob([
            			//the initial timeout time
            			"let timeoutTime = "+(updateInterval * 1000).toFixed(1)+";" +
            			//onmessage callback
            			"self.onmessage = function(msg){" +
            			"	timeoutTime = parseInt(msg.data);" +
            			"};" +
            			//the tick function which posts a message
            			//and schedules a new tick
            			"function tick(){" +
            			"	setTimeout(tick, timeoutTime);" +
            			"	self.postMessage('tick');" +
            			"}" +
            			//call tick initially
            			"tick();"
            		]);

            		let blobUrl = URL.createObjectURL(blob);
            		this.tickWorker = new Worker(blobUrl);
                    this.tickWorker.onmessage = (e)=> {

                        let data = e.data,
                            ct = SOUND_CONTEXT.currentTime;

                        this.ferry(ct);

                        automationWorker.postMessage({
                            type: JOB_TYPE_WORK,
                            currentTime: ct
                        });
                        rogueWorker.postMessage({
                            type: JOB_TYPE_WORK,
                            currentTime: ct
                        });
                        processWorker.postMessage({
                            type: JOB_TYPE_WORK,
                            currentTime: ct
                        });
                    }
                }

                this.playing = true;

                this.dispatchEvent({ type: SIMULATION_EVENT_PLAY });

                resolve();
            });
        }

        // Pause only freezes the Player at the current position.
        this.pause = ()=> {

            return new Promise((resolve,reject)=> {
                if (!this.playing) {
                    reject('Journey is already paused.');
                    return;
                }

                this.playing = false;
                this.dispatchEvent({ type: SIMULATION_EVENT_PAUSE });
                resolve();
            });
        }
        this.resume = ()=> {
            return this.play();
        }
        this.stop = ()=> {

            let record = null;
            if (this.recording) record = this.record();

            if (this.tickWorker) {
                this.tickWorker.terminate();
                this.tickWorker = null;
            }

            // NOTE: Consider this issue that seems to show up only when
                // perpetuation is off - Sound is asked to stop. The tickWorker
                // sends a message to other workers to do work. Their work is
                // asynchronous, so the messages to clear below get processed
                // before they finish working. Their work depends on data that
                // was cleared.
            automationWorker.postMessage({type: JOB_TYPE_CLEAR, currentTime: SOUND_CONTEXT.currentTime});
            rogueWorker.postMessage({type: JOB_TYPE_CLEAR});
            processWorker.postMessage({type: JOB_TYPE_CLEAR});

            let stopping = this.stopping;
            if (!stopping) this.stopping = true;

            return new Promise((resolve,reject)=> {
                if (stopping) {
                    reject('Already stopping.');
                    return;
                }
                if (time_ == null) {
                    reject('Sound was already stopped');
                    this.stopping = false;
                    return;
                }

                this.reverb.disconnect();
                this.transferReverb.disconnect();
                this.output.disconnect();
                this.channels.forEach(chan => chan.node.disconnect());
                this.channels.length = 0;

                this.traverseVoices(this.player, null, (v)=> {
                    v.stop();
                });
                voices.clear();
                this.layout = this.player = this.release = null;
                this.reverb = this.transferReverb = this.output = null;
                this.sound = this.vision = null;
                time_ = null;
                cjidx = 0;
                this.playing = false;
                resetListener();
                SOUND_CONTEXT = VISION_CONTEXT = null;
                visualizationHook = null;
                this.stopping = false;
                if (this.playAfter) {
                    let playAfter = this.playAfter;
                    this.play().then(()=> {
                        playAfter.resolve();
                    }).catch((err)=> {
                        playAfter.reject(err);
                    });
                    this.playAfter = null;
                }
                this.dispatchEvent({ type: SIMULATION_EVENT_STOP });
                if (record) {
                    record.then((info)=> {
                        resolve(info);
                    }).catch((err)=> {
                        console.error(err);
                        resolve();
                    });
                } else {
                    resolve();
                }
            });
        }
        this.clear = ()=> {
            return new Promise((resolve,reject)=> {
                if (time_ == null) {
                    this.voice_ = this.voiceInfo = this.journey = null;
                    this.type = '';
                    resolve();
                } else {
                    this.stop().then(()=> {
                        this.voice_ = this.voiceInfo = this.journey = null;
                        this.type = '';
                        resolve();
                    }).catch((err)=> {
                        reject(err);
                    });
                }
            });
        }
        this.persist_ = false;
        this.persist = (b)=> {
            if (b != undefined) {
                this.persist_ = b;
            } else {
                return this.persist_;
            }
        }
        this.autoplay_ = false;
        this.autoplay = (b)=> {
            if (b != undefined) {
                this.autoplay_ = b;
            } else {
                return this.autoplay_;
            }
        }
        this.recording = false;
        this.record = ()=> { // especially useful when the believer can play along with the themes
            let rec = this.recorder;
            if (this.recording) {
                this.recording = false;
                this.recorder = null;
            }
            let voiceInfo = this.voiceInfo;
            return new Promise((resolve,reject)=> {
                if (rec) {
                    let name = voiceInfo.title+'_L'+voiceInfo.lineNumber+'_T'+voiceInfo.themeNumber;
                    if (voiceInfo.voiceType === VOICE_TYPE_ASPECT) {
                        name+='_(Aspect_'+(voiceInfo.aspectIndex+1)+')';
                    } else {
                        name+='_{Theme}';
                    }

                    rec.stop();
                    rec.exportWAV((file)=> {
                        resolve({file,name});
                    });

                    this.dispatchEvent({ type: SIMULATION_EVENT_RECORD_END });

                    return;
                }

                if (time_ == null) {
                    this.play(true).then(()=> {
                        resolve();
                    }).catch((err)=> {
                        reject('Error while trying to play for recording.')
                    });
                    return;
                } else {
                    this.recorder = new Recorder(this.reverb);
                    this.dispatchEvent({ type: SIMULATION_EVENT_RECORD_START });
                    this.recorder.record();
                    this.recording = true;
                    resolve();
                }
            });
        }

        this.download = (secs)=> {
            let voiceInfo = this.voiceInfo;
            return new Promise((resolve, reject)=> {
                let srate = SOUND_CONTEXT.sampleRate;
                let ctx = new OfflineAudioContext(2, srate*secs, srate);
                let sound = new Sound({
                    context: ctx,
                    listener: {
                        forward: this.listener.startForward.clone(),
                        position: this.listener.startPosition.clone(),
                        up: this.listener.startUp.clone()
                    },
                    temple: this.curriculum
                });
                let name = voiceInfo.title+'_L'+voiceInfo.lineNumber+'_T'+voiceInfo.themeNumber;
                if (voiceInfo.voiceType === VOICE_TYPE_ASPECT) {
                    name+='_(Aspect_'+(voiceInfo.aspectIndex+1)+')';
                } else {
                    name+='_{Theme}';
                }
                sound.voice(this.voice_, this.type, this.voiceInfo);
                sound.play().then(()=> {
                    ctx.startRendering().then((buff)=> {
                        sound.stop().catch(console.error);
                        resolve({
                            buffer: buff,
                            name: name
                        });
                    }).catch((e)=> {
                        sound.stop().catch(console.error);
                        reject(e)
                    });
                }).catch(reject);
            });
        }

        // TODO: this should be set regardless
        this.visualizer = opts.vision.visualizer ? new Visualizer(opts.vision.visualizer) : null;

        function Visualizer(vz_opts) {

            let container = vz_opts.container;
            this.container = container;
            let canvas = vz_opts.canvas;
            this.canvas = canvas;
            vz_opts.fullscreen = vz_opts.fullscreen || {};
            let fcontainer = vz_opts.fullscreen;
            this.fullscreen = fcontainer;
            let renderer = new THREE.WebGLRenderer({
                canvas: this.canvas,
                alpha: false,
                stencil: false,
                depth: false,
                powerPreference: "high-performance",
                antialias: true
            });
            renderer.setPixelRatio(window.devicePixelRatio);
            // this.renderer = renderer;
            let camera = new THREE.PerspectiveCamera(75, 1, 0.1, 30000);
            camera.position.set(1000,600,1600);
            // this.camera = camera;
            let controls = new THREE.OrbitControls(camera, canvas);
            // this.controls = controls;
            let onVoiceSelect = vz_opts.onVoiceSelect;
            this.onVoiceSelect = onVoiceSelect;
            let state = null;
            this.state = ()=> { return state; }
            let otime, total; // rendering progress variables
            this.on = ()=> {
                if (state === 'on') return;
                container.classList.remove('d-none'); // must be done before display
                if (this.scene) onWindowResize();
                state = 'on';
                otime = performance.now();
                total = 0;
                if (this.scene) animate.bind(this)();
            }
            this.off = (r)=> {
                if (state === 'off') return;
                state = 'off';
                container.classList.add('d-none');
                fcontainer.classList.add('d-none');
                container.appendChild(canvas);
                if (r) this.scene = null;
                if (this.scene) onWindowResize();
            }
            let caption = container.querySelector(".cursor .caption"),
                caret = container.querySelector(".cursor .caret"),
                cursor = container.querySelector(".cursor"),
                selectedCaption = container.querySelector(".selectedCursor .caption"),
                selectedCaret = container.querySelector(".selectedCursor .caret"),
                selectedCursor = container.querySelector(".selectedCursor");
            let fullscreen = false;
            this.fullscreen = (b)=> {
                if (b == undefined) {
                    return fullscreen;
                } else if (state === 'off') {
                    return;
                } else if (b) {
                    fullscreen = true;
                    container.classList.add('d-none');
                    fcontainer.classList.remove('d-none');
                    fcontainer.appendChild(cursor);
                    fcontainer.appendChild(selectedCursor);
                    fcontainer.appendChild(canvas);
                    onWindowResize.bind(this)();
                } else {
                    fullscreen = false;
                    fcontainer.classList.add('d-none');
                    container.classList.remove('d-none');
                    container.appendChild(cursor);
                    container.appendChild(selectedCursor);
                    container.appendChild(canvas);
                    container.appendChild(canvas);
                    onWindowResize.bind(this)();
                }
                moveStats();
            }
            this.resize = onWindowResize.bind(this);

            let stats = new Stats();
            container.appendChild(stats.dom);
            let moveStats = ()=> {
                if (!stats) return;
                if (fullscreen) {
                    fcontainer.appendChild(stats.dom);
                }  else {
                    container.appendChild(stats.dom);
                }
            }

            const BLOOM_SETTINGS = {
                exposure: 1.208,
                bloomStrength: 2.563,
                bloomThreshold: 0,
                bloomRadius: 0.54
            }

            let scene, bloomPass, fxcomposer, raycaster;

            const LINE_DEFAULT_COLOR = new Three.Color(0x303030);
            const LINE_CONDUCTOR_COLOR = new Three.Color(0xf9eeb3);
            const LINE_ASPECT_COLOR = new Three.Color(0xd6b948);
            const LINE_CHORUS_COLOR = new Three.Color(0x6d5f28);
            const CONDUCTOR_OUTER_COLOR = new Three.Color(0xff0040);
            const ORIENT_OUTER_COLOR = new Three.Color(0x4000ff);
            const ASPECT_INNER_COLOR = new Three.Color(0xf9eeb3);
            const SPIRIT_PARTICLE_COLOR = new Three.Color(0xf9eeb3);
            const SPIRIT_PARTICLE_SIZE = 4;
            const BELIEVER_INNER_COLOR = new Three.Color(0xf9eeb3);
            const BELIEVER_OUTER_COLOR = new Three.Color(0xff0040);
            const MAX_NODE_SIZE = 200;
            const POINTS_PER_SELECTION_LINE = 100;
            const SELECTION_LINE_POINT_SIZE = 10;
            const LINE_DEFAULT_POINT_SIZE = 5;
            const LINE_DEFAULT_NUM_POINTS = 1200;
            const LINE_CONDUCTOR_NUM_POINTS = 1200;
            const LINE_ASPECT_NUM_POINTS = 1200;
            const LINE_CHORUS_NUM_POINTS = 600;
            const CONNECTION_RED = new Three.Color('red');
            const CONNECTION_BLUE = new Three.Color('blue');
            const HELPERS_DEFAULT = 1;
            const HELPERS_CONDUCTOR = 2;
            const HELPERS_ASPECT = 3;
            const HELPERS_LEFT_CHORUS = 4;
            const HELPERS_RIGHT_CHORUS = 5;

            const allVoices = [];
            this.allVoices = allVoices;

            let windowHalfX, windowHalfY;
            let pointer = new Three.Vector2(),
                mouse = new Three.Vector2();
            let INTERSECTED = null,
                intersected = new Three.Vector3(),
                SELECTED = null,
                selectedPure = null,
                selectedId = null;
            let sketchLines,
                updateSketchLines;
            let stars;
            let Spirit, Believer;
            let selectedConnections,
                updateConnections;

            const reuse_vec = new Three.Vector3();

            this.visualize = (journey)=> {
                this.scene = scene = new THREE.Scene();

                allVoices.length = 0;

                const helpersGeo = {
                    nodes: {
                        positions: [],
                        colors: [],
                        sizes: [],
                        alphas: []
                    }
                }
                this.helpersGeo = helpersGeo;

                let node_id = 0;
                let system_id = 0;

                let positions = [],
                    colors = [],
                    alphas = [],
                    sizes = [];

                function layoutSystem(theme) {
                    let cthemes = [];
                    let pos = theme.position;
                    positions.push(pos.x, pos.y, pos.z);
                    colors.push(CONDUCTOR_OUTER_COLOR.r, CONDUCTOR_OUTER_COLOR.g, CONDUCTOR_OUTER_COLOR.b);
                    sizes.push(MAX_NODE_SIZE*0.7);
                    alphas.push(1);
                    theme.id = node_id;
                    theme.systemId = system_id++;
                    // c.conductor = conductor;
                    // c.localId = localId || 0;
                    let orchestra = [theme];
                    ++node_id;

                    let c_, c;
                    for (let idx = 1; idx < theme.orchestra.length-1; ++idx) {
                        c_ = theme.orchestra[idx];
                        c = {
                            voiceId: c_.voiceId,
                            localId: idx,
                            conductor: theme,
                            position: c_.position,
                            up: c_.up,
                            forward: c_.forward,
                            rotation: c_.mesh.rotation
                        }
                        orchestra.push(c);

                        if (c_.orchestra) {
                            c.orchestra = c_.orchestra;
                            c.quantum = c_.quantum;
                            c.title = c_.name;
                            cthemes.push(c);
                        } else {
                            let master = c;
                            let masterId = node_id;
                            master.id = masterId;
                            master.title = c_.name+' ('+c_.frequency+' Hz)';
                            pos = master.position;
                            positions.push(pos.x, pos.y, pos.z);
                            colors.push(ASPECT_INNER_COLOR.r, ASPECT_INNER_COLOR.g, ASPECT_INNER_COLOR.b);
                            sizes.push(MAX_NODE_SIZE*0.3);
                            alphas.push(1);
                            allVoices.push(c);
                            ++node_id;

                            master.leftSystemId = system_id++;
                            master.rightSystemId = system_id++;

                            // left-hand chorus
                            let left = [master],
                                right = [master];
                            for (let choridx = 1; choridx < 8; ++choridx) {
                                let chor = c_.leftChorus[choridx];
                                c = {
                                    id: node_id,
                                    masterId: masterId,
                                    position: chor.position,
                                    up: chor.up,
                                    forward: chor.forward,
                                    rotation: chor.mesh.rotation
                                }
                                allVoices.push(c);
                                node_id++;

                                pos = chor.position;
                                positions.push(pos.x, pos.y, pos.z);
                                if (choridx < 7) {
                                    colors.push(ASPECT_INNER_COLOR.r, ASPECT_INNER_COLOR.g, ASPECT_INNER_COLOR.b);
                                    sizes.push(MAX_NODE_SIZE*0.15);
                                    alphas.push(0);
                                    c.title = c_.name+' Persona ('+chor.frequency+' Hz)';
                                } else {
                                    colors.push(ORIENT_OUTER_COLOR.r, ORIENT_OUTER_COLOR.g, ORIENT_OUTER_COLOR.b);
                                    sizes.push(MAX_NODE_SIZE*0.25);
                                    alphas.push(0);
                                    c.title = c_.name+' Choral Orient';
                                }

                                left.push(c);

                                chor = c_.rightChorus[choridx];
                                c = {
                                    id: node_id,
                                    masterId: masterId,
                                    position: chor.position,
                                    up: chor.up,
                                    forward: chor.forward,
                                    rotation: chor.mesh.rotation
                                }
                                allVoices.push(c);
                                node_id++;

                                pos = chor.position;
                                positions.push(pos.x, pos.y, pos.z);
                                if (choridx < 7) {
                                    colors.push(ASPECT_INNER_COLOR.r, ASPECT_INNER_COLOR.g, ASPECT_INNER_COLOR.b);
                                    sizes.push(MAX_NODE_SIZE*0.15);
                                    alphas.push(0);
                                    c.title = c_.name+' Persona ('+chor.frequency+' Hz)';
                                } else {
                                    colors.push(ORIENT_OUTER_COLOR.r, ORIENT_OUTER_COLOR.g, ORIENT_OUTER_COLOR.b);
                                    sizes.push(MAX_NODE_SIZE*0.25);
                                    alphas.push(0);
                                    c.title = c_.name+' Choral Orient';
                                }

                                right.push(c);
                            }

                            master.chorus = {
                                left: left,
                                right: right,
                                leftQuantum: c_.leftChorus.quantum,
                                rightQuantum: c_.rightChorus.quantum,
                            }
                        }
                    }

                    allVoices.push(theme);

                    cthemes.forEach(c => layoutSystem.bind(this)(c));

                    c_ = theme.orchestra[7];
                    pos = c_.position;
                    positions.push(pos.x, pos.y, pos.z);
                    colors.push(ORIENT_OUTER_COLOR.r, ORIENT_OUTER_COLOR.g, ORIENT_OUTER_COLOR.b);
                    sizes.push(MAX_NODE_SIZE*0.5);
                    alphas.push(1);
                    c = {
                        id: node_id,
                        title: theme.title+' Orient',
                        conductor: theme,
                        position: pos,
                        up: c_.up,
                        forward: c_.forward,
                        rotation: c_.mesh.rotation
                    }
                    allVoices.push(c);
                    orchestra.push(c);
                    ++node_id;
                }

                layoutSystem.bind(this)({
                    voiceId: journey.voiceId,
                    title: journey.name,
                    position: journey.position,
                    up: journey.up,
                    forward: journey.forward,
                    orchestra: journey.orchestra,
                    rotation: journey.mesh.rotation,
                    quantum: journey.quantum
                });

                let starsGeo = new THREE.BufferGeometry();
                positions = new Float32Array(positions);
                colors = new Float32Array(colors);
                sizes = new Float32Array(sizes);
                alphas = new Float32Array(alphas);
                starsGeo.addAttribute("position", new THREE.BufferAttribute(positions, 3));
                starsGeo.addAttribute("size", new THREE.BufferAttribute(sizes, 1));
                starsGeo.addAttribute("color", new THREE.BufferAttribute(colors, 3));
                starsGeo.addAttribute("alpha", new THREE.BufferAttribute(alphas, 1));

                let starsMat = new THREE.ShaderMaterial({
                    transparent: true,
                    depthWrite: false,
                    vertexShader: document.getElementById("starsVertexShader").textContent,
                    fragmentShader: document.getElementById("starsFragmentShader").textContent
                });

                stars = new THREE.Points(starsGeo, starsMat);

                scene.add(stars);

                // TODO: initialize buffers for satellites

                // The Judge or Spirit
                let num_genisi = 10000;
                let num_fibers = 6;
                let per_fiber = num_genisi/num_fibers;
                let mid_fiber = (num_fibers-1)/2;
                let step = 1/per_fiber;
                let scale = 30;
                let radius = 400;
                Spirit = new THREE.Group();
                let layouts = [['x','y','z'],['y','z','x'],['z','x','y']];
                let xCol = new THREE.Color('red'),
                    yCol = new THREE.Color('green'),
                    zCol = new THREE.Color('blue');
                const axisCol = (ax)=> { if (ax === 0) { return xCol } else if (ax === 1) { return yCol } else { return zCol }}
                let spiritMat = new THREE.ShaderMaterial({
                    transparent: true,
                    depthWrite: false,
                    uniforms: {
                        uNumFibers : { type: 'f', value: num_fibers },
                        uNumParticles: { type: 'f', value: num_genisi },
                        uTime: { type: 'f', value: 0 },
                        uRadius: { type: 'f', value: radius },
                        uDistort: { type: 'vec3', value: new THREE.Vector3(1,1,1) }
                    },
                    vertexShader: document.getElementById("spiritVertexShader").textContent,
                    fragmentShader: document.getElementById("spiritFragmentShader").textContent
                });

                for (let idx = 0; idx < 3; ++idx) {
                    let positions = [],
                        sizes = [],
                        colors = [],
                        alphas = [],
                        radii = [],
                        freqs = [],
                        times = [];
                    let fibers = [];
                    let axes = [];
                    let lay = layouts[idx];
                    let col = SPIRIT_PARTICLE_COLOR;
                    // let col = axisCol(idx);
                    for (let gi = 0, total = 0; gi < num_genisi; ++gi, total+=step) {
                        let fiber = -2+Math.floor(gi/per_fiber);
                        positions.push(0,0,0);
                        sizes.push(SPIRIT_PARTICLE_SIZE);
                        // colors.push(Math.random(), Math.random(), Math.random());
                        colors.push(col.r, col.g, col.b);
                        let alpha = idx === 2 ? 1 : 0;
                        alphas.push(1);
                        freqs.push(scale);
                        times.push(total);
                        fibers.push(fiber);
                        axes.push(idx);
                    }

                    let axisGeo = new THREE.BufferGeometry();
                    axisGeo.addAttribute("position", new THREE.BufferAttribute(new Float32Array(positions), 3));
                    axisGeo.addAttribute("size", new THREE.BufferAttribute(new Float32Array(sizes), 1));
                    axisGeo.addAttribute("color", new THREE.BufferAttribute(new Float32Array(colors), 3));
                    axisGeo.addAttribute("alpha", new THREE.BufferAttribute(new Float32Array(alphas), 1));
                    axisGeo.addAttribute("fiber", new THREE.BufferAttribute(new Float32Array(fibers), 1));
                    axisGeo.addAttribute("frequency", new THREE.BufferAttribute(new Float32Array(freqs), 1));
                    axisGeo.addAttribute("fiberPos", new THREE.BufferAttribute(new Float32Array(times), 1));
                    axisGeo.addAttribute("axis", new THREE.BufferAttribute(new Float32Array(axes), 1));

                    let spiritAxis = new THREE.Points(axisGeo, spiritMat);
                    Spirit.add(spiritAxis);
                }

                // The Player or Believer
                let believerGeo = new THREE.BufferGeometry();
                positions = new Float32Array( [0,0,0, 0,0,0] ),
                colors = new Float32Array( [BELIEVER_OUTER_COLOR.r,BELIEVER_OUTER_COLOR.g,BELIEVER_OUTER_COLOR.b, BELIEVER_INNER_COLOR.r,BELIEVER_INNER_COLOR.g,BELIEVER_INNER_COLOR.b] ),
                sizes = new Float32Array( [MAX_NODE_SIZE*0.7, MAX_NODE_SIZE*0.3] ),
                alphas = new Float32Array( [0.5, 0.3] );
                believerGeo.addAttribute("position", new THREE.BufferAttribute(positions, 3));
                believerGeo.addAttribute("size", new THREE.BufferAttribute(sizes, 1));
                believerGeo.addAttribute("color", new THREE.BufferAttribute(colors, 3));
                believerGeo.addAttribute("alpha", new THREE.BufferAttribute(alphas, 1));
                let believerMat = new THREE.ShaderMaterial({
                    transparent: true,
                    depthWrite: false,
                    vertexShader: document.getElementById("believerVertexShader").textContent,
                    fragmentShader: document.getElementById("believerFragmentShader").textContent
                });
                Believer = new THREE.Points(believerGeo, believerMat);
                // updateArrows(Believer, 0xf03e00, 0x303030);

                Believer.add(Spirit);

                // Believer.position.set(500,500,500);

                // scene.add(Believer);

                // Setup sketch lines
                positions = [];
                colors = [];
                sizes = [];
                alphas = [];
                [
                    [LINE_DEFAULT_NUM_POINTS, LINE_DEFAULT_COLOR, LINE_DEFAULT_POINT_SIZE],
                    [LINE_CONDUCTOR_NUM_POINTS, LINE_CONDUCTOR_COLOR, LINE_DEFAULT_POINT_SIZE],
                    [LINE_ASPECT_NUM_POINTS, LINE_ASPECT_COLOR, LINE_DEFAULT_POINT_SIZE],
                    [LINE_CHORUS_NUM_POINTS, LINE_CHORUS_COLOR, LINE_DEFAULT_POINT_SIZE],
                    [LINE_CHORUS_NUM_POINTS, LINE_CHORUS_COLOR, LINE_DEFAULT_POINT_SIZE]
                ].forEach(([num_points, col, size])=> {
                    for (let idx = 0; idx < 28*num_points; ++idx) {
                        positions.push(0,0,0);
                        alphas.push(0);
                        sizes.push(size);
                        colors.push(col.r,col.g,col.b);
                    }
                });
                positions = new Float32Array(positions);
                sizes = new Float32Array(sizes);
                colors = new Float32Array(colors);
                alphas = new Float32Array(alphas);
                let sketchLinesGeometry = new THREE.BufferGeometry();
                sketchLinesGeometry.addAttribute('position', new THREE.BufferAttribute(positions, 3) );
                sketchLinesGeometry.addAttribute('size', new THREE.BufferAttribute(sizes, 1) );
                sketchLinesGeometry.addAttribute('color', new THREE.BufferAttribute(colors, 3) );
                sketchLinesGeometry.addAttribute('alpha', new THREE.BufferAttribute(alphas, 1) );

                let material = new THREE.ShaderMaterial({
                    transparent: true,
                    depthWrite: false,
                    vertexShader: document.getElementById("connectionsVertexShader").textContent,
                    fragmentShader: document.getElementById("connectionsFragmentShader").textContent
                });

                sketchLines = new THREE.Points(sketchLinesGeometry, material);

                scene.add(sketchLines);

                updateSketchLines = (type, on, aspects, quantum)=> {

                    let position = sketchLines.geometry.attributes.position,
                        alpha = sketchLines.geometry.attributes.alpha;

                    let num_points, offset;
                    if (type === HELPERS_DEFAULT) {
                        num_points = LINE_DEFAULT_NUM_POINTS;
                        offset = 0;
                    } else if (type === HELPERS_CONDUCTOR) {
                        num_points = LINE_CONDUCTOR_NUM_POINTS;
                        offset = 28*LINE_DEFAULT_NUM_POINTS;
                    } else if (type === HELPERS_ASPECT) {
                        num_points = LINE_ASPECT_NUM_POINTS;
                        offset = 28*(LINE_CONDUCTOR_NUM_POINTS+LINE_DEFAULT_NUM_POINTS);
                    } else if (type === HELPERS_LEFT_CHORUS) {
                        num_points = LINE_CHORUS_NUM_POINTS;
                        offset = 28*(LINE_ASPECT_NUM_POINTS+LINE_CONDUCTOR_NUM_POINTS+LINE_DEFAULT_NUM_POINTS);
                    } else {
                        num_points = LINE_CHORUS_NUM_POINTS;
                        offset = 28*(LINE_CHORUS_NUM_POINTS+LINE_ASPECT_NUM_POINTS+LINE_CONDUCTOR_NUM_POINTS+LINE_DEFAULT_NUM_POINTS);
                    }

                    let step = 1/num_points,
                        total;
                    if (on) {
                        for (let idx = 0; idx < aspects.length; ++idx) {
                            let iaspect = aspects[idx];
                            for (let jdx = idx+1; jdx < aspects.length; ++jdx) {
                                let jaspect = aspects[jdx];
                                total = 0;
                                for (let pidx = 0; pidx < num_points; ++pidx,++offset) {
                                    if (on) {
                                        reuse_vec.lerpVectors(iaspect.position, jaspect.position, total);
                                        position.set(reuse_vec.toArray(), offset*3);
                                        alpha.set([1], offset);
                                    } else {
                                        position.set([0,0,0], offset*3);
                                        alpha.set([0], offset);
                                    }
                                    total+=step;
                                }
                            }
                        }
                    } else {
                        for (let idx = 0; idx < 28*num_points; ++idx, ++offset) {
                            position.set([0,0,0], offset*3);
                            alpha.set([0], offset);
                        }
                    }

                    position.needsUpdate = true;
                    alpha.needsUpdate = true;
                }

                // Setup variable connections
                let selectedConnectionsGeometry = new THREE.BufferGeometry();
                positions = [];
                colors = [];
                alphas = [];
                sizes = [];
                for (let idx = 0; idx < 5; ++idx) {
                    for (let pidx = 0; pidx < POINTS_PER_SELECTION_LINE; ++pidx) {
                        positions.push(0,0,0);
                        colors.push(0,0,0);
                        alphas.push(0);
                        sizes.push(0);
                    }
                }

                positions = new Float32Array(positions);
                sizes = new Float32Array(sizes);
                colors = new Float32Array(colors);
                alphas = new Float32Array(alphas);
                selectedConnectionsGeometry.addAttribute('position', new THREE.BufferAttribute(positions, 3));
                selectedConnectionsGeometry.addAttribute('size', new THREE.BufferAttribute(sizes, 1));
                selectedConnectionsGeometry.addAttribute('color', new THREE.BufferAttribute(colors, 3));
                selectedConnectionsGeometry.addAttribute('alpha', new THREE.BufferAttribute(alphas, 1));

                material = new THREE.ShaderMaterial({
                    transparent: true,
                    depthWrite: false,
                    vertexShader: document.getElementById("connectionsVertexShader").textContent,
                    fragmentShader: document.getElementById("connectionsFragmentShader").textContent
                });

                selectedConnections = new THREE.Points(selectedConnectionsGeometry, material);

                updateConnections = (idx, start, end, pc, col)=> {
                    let offset = idx*POINTS_PER_SELECTION_LINE;
                    let step = 1/POINTS_PER_SELECTION_LINE;
                    let position = selectedConnections.geometry.attributes.position,
                        size = selectedConnections.geometry.attributes.size,
                        color = selectedConnections.geometry.attributes.color,
                        alpha = selectedConnections.geometry.attributes.alpha;

                    if (start == undefined) {
                        for (let pidx = 0; pidx < POINTS_PER_SELECTION_LINE; ++pidx) {
                            let opidx = offset+pidx,
                                o3pidx = opidx*3;
                            alpha.array[opidx] = 0;
                            size.array[opidx] = 0;
                            color.set([0,0,0], o3pidx);
                            position.set([0,0,0], o3pidx);
                        }
                    } else {
                        let total = 0;
                        for (let pidx = 0; pidx < POINTS_PER_SELECTION_LINE; ++pidx) {
                            reuse_vec.lerpVectors(start,end,total);
                            let opidx = offset+pidx,
                                o3pidx = opidx*3;
                            if (total > pc) {
                                alpha.array[opidx] = 0;
                                size.array[opidx] = 0;
                            } else {
                                alpha.array[opidx] = 1;
                                size.array[opidx] = SELECTION_LINE_POINT_SIZE;
                            }
                            color.set([col.r,col.g,col.b], o3pidx);
                            position.set([reuse_vec.x,reuse_vec.y,reuse_vec.z], o3pidx);

                            total+=step;
                        }
                    }

                    position.needsUpdate = true;
                    size.needsUpdate = true;
                    color.needsUpdate = true;
                    alpha.needsUpdate = true;
                }

                scene.add( selectedConnections );

                // Bloom
                let renderScene = new THREE.RenderPass(scene, camera);
                bloomPass = new THREE.UnrealBloomPass( new THREE.Vector2(screen.width, screen.height), BLOOM_SETTINGS.bloomStrength, BLOOM_SETTINGS.bloomRadius, BLOOM_SETTINGS.bloomThreshold);
                bloomPass.threshold = BLOOM_SETTINGS.bloomThreshold;
                bloomPass.strength = BLOOM_SETTINGS.bloomStrength;
                bloomPass.radius = BLOOM_SETTINGS.bloomRadius;
                bloomPass.renderToScreen = true;
                fxcomposer = new THREE.EffectComposer(renderer);
                fxcomposer.setSize(canvas.width, canvas.height);
                fxcomposer.addPass(renderScene);
                fxcomposer.addPass(bloomPass);

                raycaster = new THREE.Raycaster();
                raycaster.params.Points.threshold = 25;
            }

            window.addEventListener('resize', onWindowResize.bind(this), false);
            canvas.addEventListener('mousemove', onDocumentMouseMove.bind(this), false);
            fcontainer.addEventListener('click', onDocumentMouseClick.bind(this), false);

            function onDocumentMouseMove(e) {
                let x = e.clientX,
                    y = e.clientY;
                pointer.x = x / canvas.width * 2 - 1;
                pointer.y = -(y / canvas.height) * 2 + 1;

                mouse.x = (x - windowHalfX)*10;
                mouse.y = (y - windowHalfY)*10;
			}

            function onDocumentMouseClick(e) {
                let x = e.clientX,
                    y = e.clientY;
                pointer.x = x / canvas.width * 2 - 1;
                pointer.y = -(y / canvas.height) * 2 + 1;

                mouse.x = (x - windowHalfX)*10;
                mouse.y = (y - windowHalfY)*10;

                if (state === 'on' && this.scene) {
                    handleIntersection('click');
                }
            }

            function animate() {
                if (state !== 'on' || !this.scene) return;

                requestAnimationFrame(animate.bind(this));
                controls.update();

				render.bind(this)();
				if (stats) stats.update();
            }

            function render() {

                const time = performance.now();
                let dt = time-otime;
                dt/=1000;

                bloomPass.strength = BLOOM_SETTINGS.bloomStrength - Math.random()*0.4;

                Spirit.children[0].material.uniforms.uTime.value = total*0.1;

                if (visualizationHook) visualizationHook();

                handleIntersection('mouseover');

                total+=dt;
                otime = time;

                // camera.position.x += (mouse.x - camera.position.x) * .03;
				// camera.position.y += (- mouse.y - camera.position.y) * .03;
				// camera.lookAt(scene.position);

                fxcomposer.render();
                // renderer.render(this.scene, camera);
            }

            function onWindowResize() {

                canvas.style.width = canvas.style.height = '100%';

                let width = canvas.clientWidth,
                    height = canvas.clientHeight;

                canvas.width = width;
                canvas.height = height;

				windowHalfX = width / 2;
				windowHalfY = height / 2;

				camera.aspect = width / height;
				camera.updateProjectionMatrix();

				renderer.setSize(width, height);
                if (fxcomposer) fxcomposer.setSize(width, height);
			}

            function handleIntersection(event) {

                raycaster.setFromCamera(pointer, camera);
                let intersects = raycaster.intersectObject(stars);

                if (intersects.length > 0) {
                    // if (intersects[0].distance < 10000) {
                    //
                    // }
                    let attributes = stars.geometry.attributes;
                    const idx = intersects[0].index;
                    INTERSECTED = intersected.fromBufferAttribute(attributes.position, idx);
                    let voice = allVoices[idx];
                    let localidx = voice.localId;
                    if (INTERSECTED && (!SELECTED || !(idx === selectedId)) && (voice.masterId === selectedPure || voice.masterId == undefined)) {

                        if (event === 'click') {

                            updateSketchLines(HELPERS_DEFAULT, false);

                            if (SELECTED) {
                                let pure = allVoices[selectedPure];

                                let plocalidx = pure.localId;
                                if ( (plocalidx != undefined) ) {
                                    for (let nidx = 0; nidx < 5; ++nidx) {
                                        updateConnections(nidx);
                                    }
                                }

                                if (pure.chorus) {
                                    let attrib = attributes.alpha;

                                    for (let idx = 1; idx < 8; ++idx) {
                                        let chor = pure.chorus.left[idx];
                                        attrib.setX(chor.id, 0);

                                        chor = pure.chorus.right[idx];
                                        attrib.setX(chor.id, 0);
                                    }

                                    attrib.needsUpdate = true;

                                    updateSketchLines(HELPERS_LEFT_CHORUS, false);
                                    updateSketchLines(HELPERS_RIGHT_CHORUS, false);
                                }

                                if (pure.orchestra) {
                                    updateSketchLines(HELPERS_CONDUCTOR, false);
                                }

                                if (pure.conductor) {
                                    updateSketchLines(HELPERS_ASPECT, false);
                                }
                            }
                            SELECTED = INTERSECTED.clone();
                            selectedId = idx;
                            if (voice.masterId == undefined) selectedPure = idx;
                            let pure = allVoices[selectedPure];

                            console.log(voice);

                            if (voice.orchestra) {
                                updateSketchLines(HELPERS_CONDUCTOR, true, voice.orchestra, voice.quantum);
                            }

                            if (voice.conductor) {
                                updateSketchLines(HELPERS_ASPECT, true, voice.conductor.orchestra, voice.conductor.quantum);
                            }

                            if (localidx != undefined) {
                                let nidx = 0;
                                for (let aspi = 1; aspi < 7; ++aspi) {
                                    if (aspi === localidx) continue;
                                    let pos = voice.position;
                                    let nbr = voice.conductor.orchestra[aspi].position;
                                    let red = new THREE.Color('red'), blue = new THREE.Color('blue');
                                    let col = CONNECTION_RED;
                                    if (Math.random() < 0.5) col = CONNECTION_BLUE;
                                    updateConnections(nidx, pos, nbr, Math.random(), col);
                                    nidx++;
                                }
                            }

                            if (pure.chorus) {
                                let attrib = attributes.alpha;
                                for (let idx = 1; idx < 8; ++idx) {
                                    let chor = pure.chorus.left[idx];
                                    attrib.setX(chor.id, 1);

                                    chor = pure.chorus.right[idx];
                                    attrib.setX(chor.id, 1);
                                }

                                attrib.needsUpdate = true;

                                updateSketchLines(HELPERS_LEFT_CHORUS, true, pure.chorus.left, pure.chorus.leftQuantum);
                                updateSketchLines(HELPERS_RIGHT_CHORUS, true, pure.chorus.right, pure.chorus.rightQuantum);
                            }

                            cursor.classList.add('d-none');

                            Believer.position.copy(voice.position);
                            Believer.rotation.copy(voice.rotation);
                            Believer.up.copy(voice.up);
                        } else {
                            if (!fullscreen) return;

                            let orch = voice.orchestra || (voice.conductor ? voice.conductor.orchestra : null);
                            if ((idx !== selectedId) && (idx !== selectedPure) && orch) {
                                updateSketchLines(HELPERS_DEFAULT, true, orch, voice.quantum);
                            }

                            let size = 25;
                            let screenPos = reuse_vec.copy(INTERSECTED);
                            camera.updateProjectionMatrix();
                            screenPos.project(camera);
                            const cx = Math.round(screenPos.x * windowHalfX);
                            const cy = Math.round(screenPos.y * -windowHalfY);
                            let x =  (screenPos.x+1)/2*canvas.width,
                                y = (1-screenPos.y)/2*canvas.height;
                            cursor.style.left = x+'px';
                            cursor.style.top = y+'px';
                            let csize = (size*1.2)+'px',
                                cbwidth = (size*0.17)+'px';
                            cursor.style.width = cursor.style.height = csize;
                            cursor.style.marginLeft = cursor.style.marginTop = csize/2;
                            caret.style.borderWidth = cbwidth;
                            caption.innerHTML = voice.title;
                            // TweenMax.to(cursor, 0.1, {
                            //     x: cx,
                            //     y: cy
                            // });
                            // INTERSECTED = null;
                            cursor.classList.remove('d-none');
                        }
                    }
                } else if (event === 'mouseover') {
                    cursor.classList.add('d-none');
                    updateSketchLines(HELPERS_DEFAULT, false);
                }

                if (SELECTED && fullscreen) {
                    let attributes = stars.geometry.attributes;
                    let voice = allVoices[selectedId];

                    let size = 25;
                    selectedCursor.classList.remove('d-none');
                    let screenPos = reuse_vec.copy(SELECTED);
                    camera.updateProjectionMatrix();
                    screenPos.project(camera);
                    const cx = Math.round(screenPos.x * windowHalfX);
                    const cy = Math.round(screenPos.y * -windowHalfY);
                    let x =  (screenPos.x+1)/2*canvas.width,
                        y = (1-screenPos.y)/2*canvas.height;
                    selectedCursor.style.left = x+'px';
                    selectedCursor.style.top = y+'px';
                    let csize = (size*1.25)+'px',
                        cbwidth = (size*0.17)+'px';
                    selectedCursor.style.width = selectedCursor.style.height = csize;
                    selectedCursor.style.marginLeft = selectedCursor.style.marginTop = csize/2;
                    selectedCaret.style.borderWidth = cbwidth;
                    selectedCaption.innerHTML = voice.title;
                    // TweenMax.to(selectedCursor, 0.1, {
                    //     x: cx,
                    //     y: cy
                    // });
                } else {
                    selectedCursor.classList.add('d-none');
                }
            }
        }

        Object.assign(this, EventDispatcher.prototype);
    }

    const hexCubeSynth = (freq,numSides,component,dl)=> {
        freq = freq || 440;
        numSides = numSides || 6;

        const signsForQuad = (vals,q) => {
            let x = vals[0],
                y = vals[1];
            switch(q%4) {
                case 0:
                    return [x,y];
                case 1:
                    return [-x,y];
                case 2:
                    return [-x,-y];
                case 3:
                    return [x,-y];
            }
        }

        let threeTwo = (baselen,outer)=> {
            // 3D
            let hypo3 = Math.sqrt( Math.pow(baselen,2) + Math.pow(outer,2) );
            let O = Math.asin( outer/hypo3 ); // angle opposite outer

            // 2D
            let thirdinnerang = 180 - (90+O);
            //thirdouterang = 180 - (O+60); // aka little angle
            let base = outer*Math.sin(60*Math.PI/180);
            let height = base/Math.tan(thirdinnerang);

            return [base,height];
        }
        this.threeTwo = threeTwo;

        let audioCtx = new (window.AudioContext || window.webkitAudioContext)();
        let numSecs = 3,
            perSide = audioCtx.sampleRate/(numSides*freq);
        // Create an empty three-second stereo buffer at the sample rate of the AudioContext
        let myArrayBuffer = audioCtx.createBuffer(2, audioCtx.sampleRate * numSecs, audioCtx.sampleRate);
        let pcms = [];
        for (let channel = 0; channel < myArrayBuffer.numberOfChannels; channel++) {
            // This gives us the actual array that contains the data
            let nowBuffering = myArrayBuffer.getChannelData(channel);
            let loc = 0, bufloc = 0, pbufloc = 0, baselen = 1, coords = null, side = 0, samp = 0, end = 0;
            for (let seci = 0; seci < numSecs; ++seci) {
                for (let cyci = 0; cyci < freq; ++cyci) {
                    bufloc = seci*audioCtx.sampleRate+(audioCtx.sampleRate/freq)*cyci;
                    end = seci*audioCtx.sampleRate+(audioCtx.sampleRate/freq)*(cyci+1);
                    bufloc = Math.floor(bufloc);

                    for (pbufloc = 0; bufloc < end; ++bufloc, ++pbufloc) {
                        side = Math.floor(pbufloc/(perSide/numSecs));
                        baselen = side % 2 === 0 ? 1 : Math.sqrt(2);

                        loc = pbufloc/(audioCtx.sampleRate/freq);

                        coords = signsForQuad(threeTwo(baselen,1-loc),side);

                        if (component === 'y') {
                            nowBuffering[bufloc] = coords[1];
                        } else {
                            nowBuffering[bufloc] = coords[0];
                        }
                    }
                }
            }

            pcms.push(myArrayBuffer.getChannelData(channel));
        }

        if (dl) {
            let worker = new Worker(packager.WAVE_WORKER_PATH);
            worker.onmessage = (e) => {
                let file = new Blob([e.data.buffer], {type:"audio/wav"});
                download(file,'hex_cube_syth.wav','audio/wav');
            }
            worker.postMessage({
                pcmArrays: pcms,
                config: {
                    sampleRate: audioCtx.sampleRate
                }
            });
            return;
        }

        // Get an AudioBufferSourceNode.
        // This is the AudioNode to use when we want to play an AudioBuffer
        let source = audioCtx.createBufferSource();

        // set the buffer in the AudioBufferSourceNode
        source.buffer = myArrayBuffer;

        // connect the AudioBufferSourceNode to the
        // destination so we can hear the sound
        source.connect(audioCtx.destination);

        // start the source playing
        // source.start();

        return source;
    }
    this.hexCubeSynth = hexCubeSynth;

    this.M = M;
    this.Memnat = M;
    this.Aspect = Aspect;
    this.Theme = Theme;
    this.Shell = Shell;
    this.Entity = Entity;
    this.Process = Process;
    this.Line = Line;
    this.Simulation = Simulation;

    let dom_panel_player = null,
        dom_controls_panel = null,
        dom_aspect_controls = null,
        dom_theme_controls = null,
        dom_shell_controls = null,
        dom_sot_controls = null,
        dom_player_controls = null,
        dom_master_controls = null,
        dom_entity_controls = null,
        dom_line_controls = null,
        dom_scene_controls = null,
        dom_script_controls = null,
        dom_sound_player = null,
        updateAspectControls = null;

    const createAspects = (aspns)=> {
        let res = [];
        for (let i = 0; i < aspns.length; i++) {
            let n_ = aspns[i],
                fi = n_.lastIndexOf('~'),
                n = n_.substring(0,fi) || n_,
                freqs_ = fi > -1 ? n_.slice(fi+1).split('|') : null,
                freqs = null;
            if (freqs_) {
                freqs = [];
                for (let i = 0; i < freqs_.length; i++) {
                    let f = Number.parseFloat(freqs_[i]);
                    if (!f) {
                        n = n_;
                        freqs = null;
                        break;
                    }
                    freqs.push(f);
                }
            }
            let masp = new M.Aspect({
                    id: guid(),
                    name: n,
                    abbr: n.substring(0,2),
                    type: M.ASPECT_TYPE_PURE,
                    frequencies: freqs,
                    onHighlight: updateAspectControls
                }),
                asp = new Aspect(masp);
            if (this.initialized) {
                aspects_map.set(masp.id,asp);

                CUD.create.aspects.set(masp.id,asp);
            }

            res.push(asp);
        }
        return res;
    }

    const createShells = (shs) => {
        let res = []
        for (let i = 0; i < shs.length; ++i) {
            let [shell_name, aspIds] = shs[i];
            let shell = new Shell(guid(),shell_name,aspIds);

            if (this.initialized) {
                shells_map.set(shell.id,shell);
                CUD.create.shells.set(shell.id,shell);
            }
            res.push(shell);
        }

        return res;
    }

    const createEntities = (ents) => {
        let res = []
        for (let i = 0; i < ents.length; ++i) {
            let n = ents[i];
            let ent = new Entity(guid(),n);

            if (this.initialized) {
                entities_map.set(ent.id,ent);

                CUD.create.entities.set(ent.id,ent);
            }
            res.push(ent);
        }

        return res;
    }

    const createProcesses = (procns) => {
        let res = []
        for (let i = 0; i < procns.length; ++i) {
            let name_ = procns[i],
                pi = name_.lastIndexOf('`');

            if (pi < 0) {
                console.warn('Skipping creation of process, '+name_+' with missing path.');
                continue;
            }

            let name = name_.substring(0,pi),
                path = name_.substring(pi+1),
                proc = new Process(guid(),name,path);


            if (this.initialized) {
                processes_map.set(proc.id,proc);

                CUD.create.processes.set(proc.id,proc);
            }
            res.push(proc);
        }

        return res;
    }

    const createSOTs = (sotns) => {
        let res = []
        for (let i = 0; i < sotns.length; ++i) {
            let name_ = sotns[i],
                pi = name_.lastIndexOf('`');

            if (pi < 0) {
                console.warn('Skipping creation of SOT, '+name_+' with missing path.');
                continue;
            }

            let name = name_.substring(0,pi),
                path = name_.substring(pi+1),
                sot = new SchoolOfThought(guid(),name,path);


            if (this.initialized) {
                CUD.create.schools.set(sot.id,sot);
            }
            res.push(sot);
        }

        return res;
    }

    this.createAspects = createAspects;
    this.createShells = createShells;
    this.createEntities = createEntities;
    this.createProcesses = createProcesses;
    this.createSOTs = createSOTs;

    this.initialized = false;

    this.judal = async ()=> {

        dom_panel_player = document.getElementById('panelPlayer');
        dom_controls_panel = document.getElementById('controls-panel');
        dom_aspect_controls = {
            elem: document.getElementById('aspect-controls'),
            name: document.getElementById('aspect-controls').querySelector('.aspect-name'),
            abbr: document.getElementById('aspect-controls').querySelector('.aspect-abbr'),
            id: document.getElementById('aspect-controls').querySelector('.controls-id'),
            frequency: document.getElementById('aspect-controls').querySelector('.aspect-frequency'),
            play: document.getElementById('aspect-controls').querySelector('.aspect-being a.play'),
            cover: document.getElementById('aspect-controls').querySelector('.aspect-covering'),
            neighbor_1: {
                elem: document.getElementById('neighbor-1'),
                input: document.getElementById('neighbor-1-value'),
                abbr: document.getElementById('neighbor-1-abbr'),
                prepend: document.getElementById('neighbor-1').querySelector('.input-group-prepend'),
                arrowMenu: document.getElementById('neighbor-1').querySelector('.arrow-menu'),
                arrowUse: document.querySelector('#neighbor-1-dropdown .arrow-icon use')
            },
            neighbor_2: {
                elem: document.getElementById('neighbor-2'),
                input: document.getElementById('neighbor-2-value'),
                abbr: document.getElementById('neighbor-2-abbr'),
                arrowMenu: document.getElementById('neighbor-2').querySelector('.arrow-menu'),
                arrowUse: document.querySelector('#neighbor-2-dropdown .arrow-icon use')
            },
            neighbor_3: {
                elem: document.getElementById('neighbor-3'),
                input: document.getElementById('neighbor-3-value'),
                abbr: document.getElementById('neighbor-3-abbr'),
                arrowMenu: document.getElementById('neighbor-3').querySelector('.arrow-menu'),
                arrowUse: document.querySelector('#neighbor-3-dropdown .arrow-icon use')
            },
            neighbor_4: {
                elem: document.getElementById('neighbor-4'),
                input: document.getElementById('neighbor-4-value'),
                abbr: document.getElementById('neighbor-4-abbr'),
                arrowMenu: document.getElementById('neighbor-4').querySelector('.arrow-menu'),
                arrowUse: document.querySelector('#neighbor-4-dropdown .arrow-icon use')
            },
            neighbor_5: {
                elem: document.getElementById('neighbor-5'),
                input: document.getElementById('neighbor-5-value'),
                abbr: document.getElementById('neighbor-5-abbr'),
                arrowMenu: document.getElementById('neighbor-5').querySelector('.arrow-menu'),
                arrowUse: document.querySelector('#neighbor-5-dropdown .arrow-icon use')
            }
        }
        dom_theme_controls = {
            elem: document.getElementById('theme-controls'),
            id: document.getElementById('theme-controls').querySelector('.controls-id'),
            dimensions: {
                elem: document.getElementById('theme-controls').querySelector('.dimensions'),
                depth: document.getElementById('theme-controls').querySelector('.dimensions .depth'),
                width: document.getElementById('theme-controls').querySelector('.dimensions .width')
            },
            name: document.getElementById('theme-controls').querySelector('.theme-name'),
            positioning: document.getElementById('theme-controls').querySelector('.theme-positioning'),
            position: document.getElementById('theme-controls').querySelector('input.theme-position'),
            quantization: document.getElementById('theme-controls').querySelector('.theme-quantization'),
            quantum: document.getElementById('theme-controls').querySelector('input.theme-quantum'),
            squatting: document.getElementById('theme-controls').querySelector('.theme-squatting'),
            squat: document.getElementById('theme-controls').querySelector('input.theme-squat'),
            subControls: document.getElementById('theme-controls').querySelector('.sub-controls'),
            soundControls: {
                duration: document.getElementById('theme-controls').querySelector('.theme-duration'),
                process: {
                    elem: document.getElementById('process-controls').querySelector('select.theme-tuning')
                },
                play: document.getElementById('theme-controls').querySelector('.theme.play-mute-solo-reload .play'),
                reload: document.getElementById('theme-controls').querySelector('.theme.play-mute-solo-reload .reload'),
            },
            visualization: {
                visualizer: document.getElementById('theme-controls').querySelector('.theme-visualization .visualizer'),
                visualize: document.getElementById('theme-controls').querySelector('.theme-visualization .visualize'),
                fullscreen: document.getElementById('theme-controls').querySelector('.theme-visualization .fullscreen'),
                canvas: document.getElementById('theme-controls').querySelector('.theme-visualization .visualizer canvas')
            }
        }
        dom_shell_controls = {
            elem: document.getElementById('shell-controls'),
            id: document.getElementById('shell-controls').querySelector('.controls-id'),
            name: document.getElementById('shell-controls').querySelector('.shell-name'),
            aspect1: {
                name: document.querySelector('#shellAspect1 input.aspect-name'),
                abbr: document.querySelector('#shellAspect1 input.aspect-abbr')
            },
            aspect2: {
                name: document.querySelector('#shellAspect2 input.aspect-name'),
                abbr: document.querySelector('#shellAspect2 input.aspect-abbr')
            },
            aspect3: {
                name: document.querySelector('#shellAspect3 input.aspect-name'),
                abbr: document.querySelector('#shellAspect3 input.aspect-abbr')
            },
            aspect4: {
                name: document.querySelector('#shellAspect4 input.aspect-name'),
                abbr: document.querySelector('#shellAspect4 input.aspect-abbr')
            },
            aspect5: {
                name: document.querySelector('#shellAspect5 input.aspect-name'),
                abbr: document.querySelector('#shellAspect5 input.aspect-abbr')
            },
            aspect6: {
                name: document.querySelector('#shellAspect6 input.aspect-name'),
                abbr: document.querySelector('#shellAspect6 input.aspect-abbr')
            },
        }
        dom_player_controls = {
            elem: document.getElementById('player-controls'),
            frequency: document.getElementById('player-controls').querySelector('.player-being input.player-frequency'),
            intensity: document.getElementById('player-controls').querySelector('.player-presence input.player-intensity'),
            intensityRange: document.getElementById('player-controls').querySelector('.player-presence input.intensity-range'),
            duration: document.getElementById('player-controls').querySelector('.player-progression input.player-duration'),
            process: {
                elem: document.getElementById('player-controls').querySelector('.player-process'),
                select: document.getElementById('player-controls').querySelector('.player-process select.player-master'),
                id: document.getElementById('player-controls').querySelector('.player-process .controls-id'),
            },
            sotControls: {
                elem: document.getElementById('sot-controls'),
                id: document.getElementById('sot-controls').querySelector('.controls-id'),
                select: document.getElementById('sot-controls').querySelector('select.sot'),
            }
        }
        dom_sot_controls = dom_player_controls.sotControls;
        dom_master_controls = dom_player_controls.process;
        dom_entity_controls = {
            elem: document.getElementById('entity-controls'),
            id: document.getElementById('entity-controls').querySelector('.controls-id'),
            select: document.getElementById('entity-controls').querySelector('select.line-entity')
        }
        dom_line_controls = {
            elem: document.getElementById('line-controls'),
            name: document.getElementById('line-controls').querySelector('.line-name'),
            position: document.getElementById('line-controls').querySelector('.line-position')
        }
        dom_scene_controls = {
            elem: document.getElementById('scene-controls'),
            id: document.getElementById('scene-controls').querySelector('.controls-id'),
            name: document.getElementById('scene-controls').querySelector('.scene-name')
        }
        dom_script_controls = {
            elem: document.getElementById('script-controls'),
            id: document.getElementById('script-controls').querySelector('.controls-id'),
            name: document.getElementById('script-controls').querySelector('.script-name'),
            processRootPath: document.getElementById('script-controls').querySelector('.process-root-path'),
            meta: {
                numBaseAspects: document.querySelector('#script-controls .meta .num-base-aspects'),
                numThemes: document.querySelector('#script-controls .meta .num-themes'),
                numShells: document.querySelector('#script-controls .meta .num-shells'),
                numEntities: document.querySelector('#script-controls .meta .num-entities'),
                numProcesses: document.querySelector('#script-controls .meta .num-processes'),
                numLines: document.querySelector('#script-controls .meta .num-lines'),
                numScenes: document.querySelector('#script-controls .meta .num-scenes'),
                dateCreated: document.querySelector('#script-controls .meta .date-created'),
                lastSaveTime: document.querySelector('#script-controls .meta .last-save-time')
            }
        }
        dom_sound_player = {
            elem: document.getElementById('soundPlayer'),
            intensityRange: document.getElementById('soundPlayer').querySelector('.output-intensity-range'),
            titling: document.getElementById('soundPlayer').querySelector('.titling'),
            title: document.getElementById('soundPlayer').querySelector('.title'),
            lineNumber: document.getElementById('soundPlayer').querySelector('.line-number'),
            themeNumber: document.getElementById('soundPlayer').querySelector('.theme-number'),
            voiceType: document.getElementById('soundPlayer').querySelector('.voice-type'),
            play: document.getElementById('soundPlayer').querySelector('.play'),
            visualize: document.getElementById('soundPlayer').querySelector('.visualize'),
            stop: document.getElementById('soundPlayer').querySelector('.stop'),
            timing: {
                elapsed: document.getElementById('soundPlayer').querySelector('.timing .elapsed'),
                total: document.getElementById('soundPlayer').querySelector('.timing .total')
            },
            travel: {
                elapsed: document.getElementById('soundPlayer').querySelector('.travel .elapsed'),
                total: document.getElementById('soundPlayer').querySelector('.travel .total')
            },
            cycle: document.getElementById('soundPlayer').querySelector('.cycle'),
            persist: document.getElementById('soundPlayer').querySelector('.persist'),
            autoplay: document.getElementById('soundPlayer').querySelector('.autoplay'),
            download: document.getElementById('soundPlayer').querySelector('.download'),
            record: document.getElementById('soundPlayer').querySelector('.record')
        }

        // setup sound controls for aspects
        for (let i = 0; i < n_ss.length; ++i) {
            let n = n_ss[i];
            dom_theme_controls.soundControls[n] = {
                name: document.getElementById('theme-controls').querySelector('.'+n+'.channel-controls .aspect-being .aspect-name'),
                frequency: document.getElementById('theme-controls').querySelector('.'+n+'.channel-controls .aspect-being input.aspect-frequency'),
                intensity: document.getElementById('theme-controls').querySelector('.'+n+'.channel-controls .aspect-presence input.aspect-intensity'),
                intensityRange: document.getElementById('theme-controls').querySelector('.'+n+'.channel-controls .aspect-presence .intensity-range'),
                duration: document.getElementById('theme-controls').querySelector('.'+n+'.channel-controls .aspect-progression input.aspect-duration'),
                process: {
                    elem: document.getElementById('theme-controls').querySelector('.'+n+'.channel-controls .aspect-process select.aspect-tuning')
                },
                // play: document.getElementById('theme-controls').querySelector('.'+n+'.channel-controls .play-mute-solo-reload .play'),
                mute: document.getElementById('theme-controls').querySelector('.'+n+'.channel-controls .play-mute-solo-reload .mute'),
                solo: document.getElementById('theme-controls').querySelector('.'+n+'.channel-controls .play-mute-solo-reload .solo'),
                reload: document.getElementById('theme-controls').querySelector('.'+n+'.channel-controls .play-mute-solo-reload .reload')
            }
        }

        const dom_overlay = {
            elem: document.getElementById('overlay'),
            close: document.querySelector('#overlay .close.btn'),
            create: {
                aspects: {
                    elem: document.querySelector('#overlay .create-aspects'),
                    textarea: document.querySelector('#overlay .create-aspects textarea'),
                    submit: document.querySelector('#overlay .create-aspects .submit'),
                    cancel: document.querySelector('#overlay .create-aspects .cancel')
                },
                shell: {
                    elem: document.querySelector('#overlay .create-shell'),
                    naming: document.querySelector('#overlay .create-shell .naming input'),
                    display: document.querySelector('#overlay .create-shell .display'),
                    displayType: document.querySelector('#overlay .create-shell .display-wrapper .type'),
                    submit: document.querySelector('#overlay .create-shell .submit'),
                    cancel: document.querySelector('#overlay .create-shell .cancel')
                },
                theme: {
                    elem: document.querySelector('#overlay .create-theme'),
                    naming: document.querySelector('#overlay .create-theme .naming input'),
                    display: document.querySelector('#overlay .create-theme .display'),
                    submit: document.querySelector('#overlay .create-theme .submit'),
                    cancel: document.querySelector('#overlay .create-theme .cancel')
                },
                entities: {
                    elem: document.querySelector('#overlay .create-entities'),
                    textarea: document.querySelector('#overlay .create-entities textarea'),
                    submit: document.querySelector('#overlay .create-entities .submit'),
                    cancel: document.querySelector('#overlay .create-entities .cancel')
                },
                processes: {
                    elem: document.querySelector('#overlay .create-processes'),
                    textarea: document.querySelector('#overlay .create-processes textarea'),
                    submit: document.querySelector('#overlay .create-processes .submit'),
                    cancel: document.querySelector('#overlay .create-processes .cancel')
                },
                sots: {
                    elem: document.querySelector('#overlay .create-sots'),
                    textarea: document.querySelector('#overlay .create-sots textarea'),
                    submit: document.querySelector('#overlay .create-sots .submit'),
                    cancel: document.querySelector('#overlay .create-sots .cancel')
                }
            },
            themesMenu: {
                elem: document.querySelector('#overlay .themes-menu'),
                themes: document.querySelector('#overlay .themes-menu .themes'),
                meta: document.querySelector('#overlay .themes-menu .meta'),
                display: document.querySelector('#overlay .themes-menu .display'),
                number: document.querySelector('#overlay .themes-menu .display-wrapper .info .number'),
                name: document.querySelector('#overlay .themes-menu .display-wrapper .info .name'),
                searching: {
                    form: document.querySelector('#overlay .themes-menu #themesMenuForm'),
                    input: document.querySelector('#overlay .themes-menu .searching input'),
                },
                cancel: document.querySelector('#overlay .themes-menu .cancel')
            },
            scenesMenu: {
                elem: document.querySelector('#overlay .scenes-menu'),
                scriptName: document.querySelector('#overlay .scenes-menu .script-name span'),
                scenes: document.querySelector('#overlay .scenes-menu .scenes'),
                cancel: document.querySelector('#overlay .scenes-menu .cancel')
            },
            scriptsMenu: {
                elem: document.querySelector('#overlay .scripts-menu'),
                scripts: document.querySelector('#overlay .scripts-menu .scripts'),
                cancel: document.querySelector('#overlay .scripts-menu .cancel')
            },
            keyboardShortcuts: {
                elem: document.querySelector('#overlay .keyboard-shortcuts'),
                listings: document.querySelector('#overlay .keyboard-shortcuts .listings'),
                cancel: document.querySelector('#overlay .keyboard-shortcuts .cancel')
            },
            import: {
                elem: document.querySelector('#overlay .import-thing'),
                info: document.querySelector('#overlay .import-thing .info'),
                file: document.querySelector('#overlay .import-thing input[type="file"]'),
                submit: document.querySelector('#overlay .import-thing a.import'),
                cancel: document.querySelector('#overlay .import-thing a.cancel'),
                display: document.querySelector('#overlay .import-thing .display'),
                doIt: async (str,thing)=> {
                    let obj = null;
                    try {
                        obj = JSON.parse(str);
                    } catch (e) {
                        packager.dialog.alert('An error occured while trying to import your content.');
                        console.error(e)
                    }

                    switch (thing) {
                        case 'aspects':
                            if (obj.aspects) obj = obj.aspects;
                            for (let id in obj) {
                                let asp_ = obj[id];
                                if (asp_.type === M.ASPECT_TYPE_COMPOUND) {
                                    console.warn('Ignoring compound aspect. Compound aspects are themes. Aspects are made from themes automatically, so import themes if you want to use them as aspects.');
                                    continue;
                                }
                                if ((await aspects_map.get(id)).isAspect) continue;
                                let masp = new M.Aspect({
                                    id: id,
                                    name: asp_.name,
                                    abbr: asp_.abbr,
                                    type: asp_.type,
                                    frequencies: asp_.frequencies,
                                    onHighlight: updateAspectControls
                                });
                                let asp = new Aspect(masp);
                                CUD.create.aspects.set(id,asp);
                                aspects_map.set(id,asp);
                            }
                            break;
                        case 'shells':
                            if (obj.shells) obj = obj.shells;
                            for (let id in obj) {
                                let shell_ = obj[id];
                                if ((await shells_map.get(id)).isShell) continue;
                                let ignore = false, strange = [];
                                for (let i = 0; i < shell_.aspectIds; ++i) {
                                    let aid = shell_.aspectIds[i];
                                    if (!(await aspects_map.get(aid)).isAspect) {
                                        strange.push(aid);
                                        ignore = true;
                                    }
                                }
                                if (ignore) {
                                    console.warn('Ignoring shell, '+shell_.name+', with ID, '+shell_.id+', because it contains aspect(s): '+strange+'. Import aspects before importing shells.');
                                }
                                let shell = new Shell({
                                    id: id,
                                    name: shell_.name,
                                    aspectIds: shell_.aspectIds
                                });
                                CUD.create.shells.set(id,shell)
                                shells_map.set(id,shell);
                            }
                            break;
                        case 'entities':
                            if (obj.entities) obj = obj.entities;
                            for (let id in obj) {
                                let ent_ = obj[id];
                                if ((await entities_map.get(id)).isEntity) continue;
                                let ent = new Entity({
                                    id: id,
                                    name: ent_.name,
                                });
                                CUD.create.entities.set(id,ent)
                                entities_map.set(id,ent);
                            }
                            break;
                        case 'processes':
                            if (obj.processes) obj = obj.processes;
                            for (let id in obj) {
                                let proc_ = obj[id];
                                if ((await processes_map.get(id)).isProcess) continue;
                                let proc = new Process({
                                    id: id,
                                    name: proc_.name,
                                    path: proc_.path
                                });
                                CUD.create.processes.set(id,proc)
                                processes_map.set(id,proc);
                            }
                            break;
                        default:

                    }
                }
            }
        }

        // finish setting up Create.Shell
        for (let i = 0; i < n_ss.length; i++) {
            let tag = n_ss[i];
            dom_overlay.create.shell[tag] = {};
            dom_overlay.create.shell[tag].abbr = dom_overlay.create.shell.elem.querySelector('.aspect.'+tag+' input.abbr');
        }

        const dom_visualizer = {
            elem: document.querySelector('#visualizer'),
            // canvas: document.querySelector('#visualizer canvas'),
        }

        const dom_scripting_container = document.getElementById('scripting-container');
        const dom_workspace_title = {
            setTextContent: (str)=> {
                let elem;
                if (str != undefined) {
                    elem = dom_scripting_container.querySelector('.workspace-title');
                    document.title = str+' | Memnat Scripting';
                    elem.textContent = str;
                }
                elem = dom_scripting_container.querySelector('.school');
                elem.textContent = this.script.player.sotId ? ' @ '+this.script.school.name : '';
            }
        }
        const dom_script = document.getElementById('script');
        const dom_toolbar = {
            elem: document.getElementById('scripting-toolbar'),
            create: {
                aspects: document.querySelector('#scripting-toolbar .create a.create-aspects'),
                shell: document.querySelector('#scripting-toolbar .create a.create-shell'),
                entities: document.querySelector('#scripting-toolbar .create a.create-entities'),
                processes: document.querySelector('#scripting-toolbar .create a.create-processes'),
                sots: document.querySelector('#scripting-toolbar .create a.create-sots'),
                line: document.querySelector('#scripting-toolbar .create a.create-line'),
                linePrepend: document.querySelector('#scripting-toolbar .create a.create-line-prepend'),
                scene: document.querySelector('#scripting-toolbar .create a.create-scene'),
                script: document.querySelector('#scripting-toolbar .create a.create-script')
            },
            goto: {
                submit: document.querySelector('#scripting-toolbar .goto-line .goto-line-button'),
                form: document.querySelector('#scripting-toolbar .goto-line form'),
                currentLine: document.querySelector('#scripting-toolbar .goto-line form input'),
                numLines: document.querySelector('#scripting-toolbar .goto-line .goto-num-lines')
            },
            browse: {
                aspects: document.querySelector('#scripting-toolbar .browse a.browse-aspects'),
                shells: document.querySelector('#scripting-toolbar .browse a.browse-shells'),
                themes: document.querySelector('#scripting-toolbar .browse a.browse-themes'),
                entities: document.querySelector('#scripting-toolbar .browse a.browse-entities'),
                processes: document.querySelector('#scripting-toolbar .browse a.browse-processes'),
                sots: document.querySelector('#scripting-toolbar .browse a.browse-sots'),
            },
            import: {
                aspects: document.querySelector('#scripting-toolbar .import a.import-aspects'),
                shells: document.querySelector('#scripting-toolbar .import a.import-shells'),
                entities: document.querySelector('#scripting-toolbar .import a.import-entities'),
                processes: document.querySelector('#scripting-toolbar .import a.import-processes')
            },
            load: document.querySelector('#scripting-toolbar li.load a'),
            save: document.querySelector('#scripting-toolbar li.save a'),
            export: document.querySelector('#scripting-toolbar li.export a'),
            scenesMenu: document.querySelector('#scripting-toolbar li.scenes-menu a'),
            exit: document.querySelector('#scripting-toolbar li.exit a')
        }
        const dom_toggle_controls = document.getElementById('toggle-controls');
        // TODO: let width serve as progress indicator
        const dom_quiet_alert = document.getElementById('quietAlert');

        this.visualizeTheme__ = (data)=> {
            let width = 640,
                height = 480;
            var scene = new THREE.Scene();
            var camera = new THREE.PerspectiveCamera( 75, width/height, 0.1, 1000 );
            var controls = new THREE.OrbitControls( camera );

            var sphere = new THREE.SphereBufferGeometry( 0.25, 16, 8 );
            //lights
            let light1 = new THREE.PointLight( 0xff0040, 2, 70, 2 );
            light1.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: 0xff0040 } ) ) );
            scene.add( light1 );
            let light2 = new THREE.PointLight( 0x0040ff, 2, 70, 2 );
            light2.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: 0x0040ff } ) ) );
            scene.add( light2 );
            let light3 = new THREE.PointLight( 0x80ff80, 2, 70, 2 );
            light3.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: 0x80ff80 } ) ) );
            scene.add( light3 );
            let light4 = new THREE.PointLight( 0xffaa00, 2, 70, 2 );
            light4.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: 0xffaa00 } ) ) );
            scene.add( light4 );

            var renderer = new THREE.WebGLRenderer({ antialias: true });
            renderer.setPixelRatio( window.devicePixelRatio );
            renderer.setSize( width, height );
            dom_overlay.visualizer.elem.appendChild( renderer.domElement );
            dom_overlay.elem.classList.remove('d-none');

            function generateTexture() {
                var canvas = document.createElement( 'canvas' );
                canvas.width = 256;
                canvas.height = 256;
                var context = canvas.getContext( '2d' );
                var image = context.getImageData( 0, 0, 256, 256 );
                var x = 0, y = 0;
                for ( var i = 0, j = 0, l = image.data.length; i < l; i += 4, j ++ ) {
                    x = j % 256;
                    y = x == 0 ? y + 1 : y;
                    image.data[ i ] = 255;
                    image.data[ i + 1 ] = 255;
                    image.data[ i + 2 ] = 255;
                    image.data[ i + 3 ] = Math.floor( x ^ y );
                }
                context.putImageData( image, 0, 0 );
                return canvas;
            }

            function drawOrchestra(orchestra,color) {
                if (!orchestra) return;
                var geometry = new THREE.Geometry();
                var texture = new THREE.Texture( generateTexture() );
                texture.needsUpdate = true;
                var material =new THREE.MeshPhongMaterial( { color: color || 0xdddddd, specular: 0x009900, shininess: 30, map: texture } );
                material.side = Three.DoubleSide;
                // material.wireframe = true;
                orchestra.map(c => {
                    geometry.vertices.push(c.position || c);
                    drawOrchestra(c.orchestra,c.color);
                });
                for (let i = 1; i <= 6; ++i) {
                    let p = i%6+1;
                    geometry.faces.push(new Three.Face3(0,i,p));
                    geometry.faces.push(new Three.Face3(7,i,p));
                }
                geometry.verticesNeedUpdate = true;
                geometry.computeBoundingSphere();
                geometry.computeFaceNormals();
                geometry.computeVertexNormals();
                var cube = new THREE.Mesh(geometry, material);
                scene.add(cube);
            }

            drawOrchestra(data.orchestra,data.color);

            camera.position.z = 5;
            controls.update();

            function animate() {
                var time = Date.now() * 0.0005;
                requestAnimationFrame( animate );
                controls.update();
                light1.position.x = Math.sin( time * 0.7 ) * 30;
                light1.position.y = Math.cos( time * 0.5 ) * 40;
                light1.position.z = Math.cos( time * 0.3 ) * 30;
                light2.position.x = Math.cos( time * 0.3 ) * 30;
                light2.position.y = Math.sin( time * 0.5 ) * 40;
                light2.position.z = Math.sin( time * 0.7 ) * 30;
                light3.position.x = Math.sin( time * 0.7 ) * 30;
                light3.position.y = Math.cos( time * 0.3 ) * 40;
                light3.position.z = Math.sin( time * 0.5 ) * 30;
                light4.position.x = Math.sin( time * 0.3 ) * 30;
                light4.position.y = Math.cos( time * 0.7 ) * 40;
                light4.position.z = Math.sin( time * 0.5 ) * 30;
                renderer.render(scene, camera);
            }
            animate();
        }

        this.visualizeTheme = (scene, container, canvas, camera, opts)=> {

            container.classList.remove('d-none'); // must be done before display

            // let canvas = dom_visualizer.canvas;

            let width = canvas.clientWidth,
                height = canvas.clientHeight;

            canvas.width = width;
            canvas.height = height;

            camera = camera || new THREE.PerspectiveCamera(40, width/height, 0.1, 15000);
            camera.position.z = 1600;
            let controls = new THREE.MapControls(camera, canvas);

            scene.background = new THREE.Color( 0xffffff );

            opts = opts || { antialias:true };
            let ropts = { canvas:canvas }
            Object.assign(ropts, opts);

            var renderer = new THREE.WebGLRenderer(ropts);
            renderer.setPixelRatio( window.devicePixelRatio );
            renderer.setSize( width, height );
            // dom_visualizer.elem.appendChild( renderer.domElement );

            let stats = new Stats();
            container.appendChild( stats.dom );

            // updateControlsPanelState(CONTROLS_PANEL_SCENE_STATE);

            function onWindowResize() {

                canvas.style.width = canvas.style.height = '100%';

                let width = canvas.clientWidth,
                    height = canvas.clientHeight;

                canvas.width = width;
                canvas.height = height;

				// windowHalfX = window.innerWidth / 2;
				// windowHalfY = window.innerHeight / 2;

				camera.aspect = width / height;
				camera.updateProjectionMatrix();

				renderer.setSize( width, height );

			}

            window.addEventListener( 'resize', onWindowResize, false );
            dom_toggle_controls.addEventListener('click', onWindowResize);

            function animate() {

				requestAnimationFrame( animate );
                controls.update();

				render();
				stats.update();

			}

            function render() {
                renderer.render( scene, camera );
            }

            animate();

            return { camera:camera, onResize:onWindowResize };
        }

        aspects_map.set = (id,asp)=> {
            let asp_ = aspects_map_[id];
            if (!asp_) {
                aspects_map.size++;
                if (asp._aspect.type === M.ASPECT_TYPE_PURE) {
                    let meta = dom_script_controls.meta.numBaseAspects;
                    let num = Number.parseInt(meta.textContent);
                    meta.textContent = num+1;
                }
            }
            aspects_map_[id] = asp;
        }
        aspects_map.get_ = (id)=> { return aspects_map_[id]; }
        aspects_map.get = (id,formod)=> {
            return new Promise((resolve,reject)=> {
                if ((id == undefined) || (id === '')) {
                    resolve();
                    return;
                }

                let asp = aspects_map_[id];

                if (!formod && asp) {
                    resolve(asp);
                } else {
                    let myresolve = (asp)=> {
                        if (asp._aspect.type === M.ASPECT_TYPE_COMPOUND) {
                            themes_map.get(id,formod).then((theme)=> {
                                asp._aspect.name = theme._theme.name();
                                if (!formod) aspects_map.set(id, asp);
                                resolve(asp);
                            }).catch((err)=> {
                                console.error(err);
                                reject(err);
                            });
                        } else {
                            if (!formod) aspects_map.set(id, asp);
                            resolve(asp);
                        }
                    }

                    packager.aspectsLoader.work({
                        type: ITEM_TYPE_ASPECT,
                        id: id,
                        packager: litePackager,
                        resolve: myresolve,
                        reject: reject
                    });
                }
            });
        }
        aspects_map.clear = ()=> {
            dom_script_controls.meta.numBaseAspects.textContent = 0;
            aspects_map.size = 0;
            aspects_map_ = {};
        }
        aspects_map[Symbol.iterator] = function*() { for (const k of Object.keys(aspects_map_)) yield [k,aspects_map_[k]]; }
        aspects_map.delete = (id)=> {
            let asp = aspects_map_[id];
            if (asp && asp._aspect.type === M.ASPECT_TYPE_PURE) {
                let meta = dom_script_controls.meta.numBaseAspects;
                let num = Number.parseInt(meta.textContent);
                meta.textContent = num-1;
                aspects_map.size--;
            }
            let d = aspects_map_[id];
            delete aspects_map_[id];
            return d == undefined;
        }

        themes_map.set = async (id,theme)=> {
            let theme_ = themes_map_[id];
            if (!theme_) {
                let meta = dom_script_controls.meta.numThemes;
                let num = Number.parseInt(meta.textContent);
                meta.textContent = num+1;
                themes_map.size++;
            }
            themes_map_[id] = theme;
        }
        themes_map.get_ = (id)=> { return themes_map_[id]; }
        themes_map.get = (id,formod)=> {
            return new Promise((resolve,reject)=> {
                if ((id == undefined) || (id === '')) {
                    resolve();
                    return;
                }

                let theme = themes_map_[id];

                if (!formod && theme) {
                    resolve(theme);
                } else {
                    let myresolve = (theme)=> {
                        if (!formod) themes_map.set(id,theme);
                        resolve(theme);
                    }

                    packager.themesLoader.work({
                        type: ITEM_TYPE_THEME,
                        id: id,
                        packager: litePackager,
                        resolve: myresolve,
                        reject: reject
                    });
                }
            });
        }
        themes_map.clear = ()=> {
            dom_script_controls.meta.numThemes.textContent = 0;
            themes_map.size = 0;
            themes_map_ = {};
        }
        themes_map[Symbol.iterator] = function*() { for (const k of Object.keys(themes_map_)) yield [k,themes_map_[k]]; }
        themes_map.delete = (id)=> {
            let theme = themes_map_[id];
            if (theme) {
                let meta = dom_script_controls.meta.numThemes;
                let num = Number.parseInt(meta.textContent);
                meta.textContent = num-1;
                themes_map.size--;
            }
            let d = themes_map_[id];
            delete themes_map_[id];
            return d == undefined;
        }

        shells_map.set = (id,shell)=> {
            let shell_ = shells_map_[id];
            if (!shell_) {
                let meta = dom_script_controls.meta.numShells;
                let num = Number.parseInt(meta.textContent);
                meta.textContent = num+1;
                shells_map.size++;
                // TODO: add to `altered` map
            }
            shells_map_[id] = shell;
        }
        shells_map.get_ = (id)=> { return shells_map_[id]; }
        shells_map.get = (id,formod)=> {
            return new Promise((resolve,reject)=> {
                if ((id == undefined) || (id === '')) {
                    resolve();
                    return;
                }

                let shell = shells_map_[id];

                if (!formod && shell) {
                    resolve(shell);
                } else {
                    let myresolve = (shell)=> {
                        if (!formod) shells_map.set(id,shell);
                        resolve(shell);
                    }

                    packager.shellsLoader.work({
                        type: ITEM_TYPE_SHELL,
                        id: id,
                        packager: litePackager,
                        resolve: myresolve,
                        reject: reject
                    });
                }
            });
        }
        shells_map.clear = ()=> {
            dom_script_controls.meta.numShells.textContent = 0;
            shells_map.size = 0;
            shells_map_ = {};
        }
        shells_map[Symbol.iterator] = function*() { for (const k of Object.keys(shells_map_)) yield [k,shells_map_[k]]; }
        shells_map.delete = (id)=> {
            let shell = shells_map_[id];
            if (shell) {
                let meta = dom_script_controls.meta.numShells;
                let num = Number.parseInt(meta.textContent);
                meta.textContent = num-1;
                shells_map.size--;
            }
            let d = shells_map_[id];
            delete shells_map_[id];
            return d == undefined;
        }

        entities_map.set = (id,ent)=> {
            let ent_ = entities_map_[id];
            if (!ent_) {
                let meta = dom_script_controls.meta.numEntities;
                let num = Number.parseInt(meta.textContent);
                meta.textContent = num+1;
                entities_map.size++;
                // TODO: add to `altered` map
            }
            entities_map_[id] = ent;
        }
        entities_map.get_ = (id)=> { return entities_map_[id] }
        entities_map.get = (id,formod)=> {
            return new Promise((resolve,reject)=> {
                if ((id == undefined) || (id === '')) {
                    resolve();
                    return;
                }

                let ent = entities_map_[id];

                if (!formod && ent) {
                    resolve(ent);
                } else {
                    let myresolve = (ent)=> {
                        if (!formod) entities_map.set(id,ent);
                        resolve(ent);
                    }

                    packager.entitiesLoader.work({
                        type: ITEM_TYPE_ENTITY,
                        id: id,
                        packager: litePackager,
                        resolve: myresolve,
                        reject: reject
                    });
                }
            });
        }
        entities_map.clear = ()=> {
            dom_script_controls.meta.numEntities.textContent = 0;
            entities_map.size = 0;
            entities_map_ = {};
        }
        entities_map[Symbol.iterator] = function*() { for (const k of Object.keys(entities_map_)) yield [k,entities_map_[k]]; }
        entities_map.delete = (id)=> {
            let ent = entities_map_[id];
            if (ent) {
                let meta = dom_script_controls.meta.numEntities;
                let num = Number.parseInt(meta.textContent);
                meta.textContent = num-1;
                entities_map.size--;
            }
            let d = entities_map_[id];
            delete entities_map_[id];
            return d == undefined;
        }

        processes_map.set = (id,proc)=> {
            let proc_ = processes_map_[id];
            if (!proc_) {
                let meta = dom_script_controls.meta.numProcesses;
                let num = Number.parseInt(meta.textContent);
                meta.textContent = num+1;
                processes_map.size++;
                // TODO: add to `altered` map
            }
            processes_map_[id] = proc;
        }
        processes_map.get_ = (id)=> { return processes_map_[id]; }
        processes_map.get = (id,formod)=> {
            return new Promise((resolve,reject)=> {
                if ((id == undefined) || (id === '')) {
                    resolve();
                    return;
                }

                let proc = processes_map_[id];

                if (!formod && proc) {
                    loadProcess(proc).then(()=> {
                        resolve(proc);
                    }).catch((err)=> {
                        console.error(err);
                        resolve(proc);
                    });
                } else {
                    let myresolve = proc => {
                        if (!formod) processes_map.set(id,proc);
                        loadProcess(proc).then(()=> {
                            resolve(proc);
                        }).catch((err)=> {
                            console.error(err);
                            resolve(proc);
                        });
                    }

                    packager.processesLoader.work({
                        id: id,
                        packager: litePackager,
                        resolve: myresolve,
                        reject: reject
                    });
                }
            });
        }
        processes_map.clear = ()=> {
            dom_script_controls.meta.numProcesses.textContent = 0;
            processes_map.size = 0;
            processes_map_ = {};
        }
        processes_map[Symbol.iterator] = function*() { for (const k of Object.keys(processes_map_)) yield [k,processes_map_[k]]; }
        processes_map.delete = (id)=> {
            let proc = processes_map_[id];
            if (proc) {
                let meta = dom_script_controls.meta.numProcesses;
                let num = Number.parseInt(meta.textContent);
                meta.textContent = num-1;
                processes_map.size--;
            }
            let d = processes_map_[id];
            delete processes_map_[id];
            return d == undefined;
        }

        const SOT_SCRIPT_INFO = {
            ASPECT_TYPE_PURE: M.ASPECT_TYPE_PURE,
            ASPECT_TYPE_COMPOUND: M.ASPECT_TYPE_COMPOUND,

            THEME_TYPE_CONTEXT: M.THEME_TYPE_CONTEXT,
            THEME_TYPE_VERSUS: M.THEME_TYPE_VERSUS,
            THEME_TYPE_SIGNAL: M.THEME_TYPE_SIGNAL,

            ARROW_TYPE_ACKNOWLEDGEMENT: M.ARROW_TYPE_ACKNOWLEDGEMENT,
            ARROW_TYPE_SIMILARITY: M.ARROW_TYPE_SIMILARITY,
            ARROW_TYPE_CAUSATION: M.ARROW_TYPE_CAUSATION,
            ARROW_TYPE_DEPENDENCE: M.ARROW_TYPE_DEPENDENCE,
            ARROW_TYPE_FOUNDATION: M.ARROW_TYPE_FOUNDATION,
            ARROW_TYPE_SPECTRUM: M.ARROW_TYPE_SPECTRUM,
            ARROW_TYPE_SCALE: M.ARROW_TYPE_SCALE,

            VOICE_TYPE_ASPECT: VOICE_TYPE_ASPECT,
            VOICE_TYPE_THEME: VOICE_TYPE_THEME,

            THREE: Three,
            Three: Three,

            PUBLIC_SCHOOL: ()=> {
                return {
                    getJourney: PUBLIC_SCHOOL.getJourney,
                    getFrequency: PUBLIC_SCHOOL.getFrequency,
                    getPosition: PUBLIC_SCHOOL.getPosition,
                    waveform: PUBLIC_SCHOOL.waveform,
                    characterLength: PUBLIC_SCHOOL.characterLength,
                }
            },

            packager: {
                name: packager.name
            },

            aspects: {
                get: aspects_map.get
            },
            themes: {
                get: themes_map.get
            },
            shells: {
                get: shells_map.get
            },
            entities: {
                get: entities_map.get
            },
            processes: {
                get: processes_map.get
            }
        }

        schools_map.get = (id)=> {
            return new Promise((resolve,reject)=> {
                if ((id == undefined) || (id === '')) {
                    resolve();
                    return;
                }

                packager.generalLoader.work({
                    type: ITEMS_TYPE_SCHOOLS,
                    query: {
                        limit: 1,
                        comparators: [
                            {
                                key: 'id',
                                predicate: 'equals',
                                value: id
                            }
                        ]
                    },
                    packager: litePackager,
                    resolve: (data)=> {
                        let sot_ = data.items[0],
                            sot = new SchoolOfThought(sot_.id, sot_.name, sot_.path);
                        sot.creationTime = sot_.creationTime;
                        loadProcess(sot).then(()=> {
                            let obj = {};
                            new Function('data', sot.raw).bind(obj)(SOT_SCRIPT_INFO);
                            if (obj.getJourney) sot.getJourney = obj.getJourney;
                            if (obj.getFrequency) sot.getFrequency = obj.getFrequency;
                            if (obj.getPosition) sot.getPosition = obj.getPosition;
                            if (obj.waveform) sot.waveform = obj.waveform;
                            if (obj.characterLength != undefined) sot.characterLength = obj.characterLength;
                            resolve(sot);
                        }).catch((err)=> {
                            console.error(err);
                            reject(sot);
                        });
                    },
                    reject: reject
                });
            });
        }

        lines_map.set = (id,line)=> {
            let line_ = lines_map_[id];
            if (!line_) {
                let meta = dom_script_controls.meta.numLines;
                let num = Number.parseInt(meta.textContent);
                meta.textContent = num+1;
                lines_map.size++;
            }
            lines_map_[id] = line;
        };
        lines_map.get = (id)=> { return lines_map_[id]; }
        lines_map.clear = ()=> {
            dom_script_controls.meta.numLines.textContent = 0;
            lines_map.size = 0;
            lines_map_ = {};
        }
        lines_map[Symbol.iterator] = function*() { for (const k of Object.keys(lines_map_)) yield [k,lines_map_[k]]; }
        lines_map.delete = (id)=> {
            let line = lines_map_[id];
            if (line) {
                let meta = dom_script_controls.meta.numLines;
                let num = Number.parseInt(meta.textContent);
                meta.textContent = num-1;
                lines_map.size--;
            }
            let d = lines_map_[id];
            delete lines_map_[id];
            return d == undefined;
        }

        scenes_map.set = (id,scene)=> {
            let scene_ = scenes_map_[id];
            if (!scene_) {
                let meta = dom_script_controls.meta.numScenes;
                let num = Number.parseInt(meta.textContent);
                meta.textContent = num+1;
                scenes_map.size++;
            }
            scenes_map_[id] = scene;
        };
        scenes_map.get = (id)=> { return scenes_map_[id]; }
        scenes_map.clear = ()=> {
            dom_script_controls.meta.numScenes.textContent = 0;
            scenes_map.size = 0;
            scenes_map_ = {};
        }
        scenes_map[Symbol.iterator] = function*() { for (const k of Object.keys(scenes_map_)) yield [k,scenes_map_[k]]; }
        scenes_map.delete = (id)=> {
            let scene = scenes_map_[id];
            if (scene) {
                let meta = dom_script_controls.meta.numScenes;
                let num = Number.parseInt(meta.textContent);
                meta.textContent = num-1;
                scenes_map.size--;
            }
            let d = scenes_map_[id];
            delete scenes_map_[id];
            return d == undefined;
        }

        let sceneId = guid();
        this.scene =  new Scene(sceneId,sceneId,1);
        this.scene.creationTime = Date.now();
        scenes_map.set(1,this.scene);

        this.script = new Script(guid());
        this.script.creationTime = Date.now();

        let aspectNeighbors = [dom_aspect_controls.neighbor_1,dom_aspect_controls.neighbor_2,dom_aspect_controls.neighbor_3,dom_aspect_controls.neighbor_4,dom_aspect_controls.neighbor_5];

        const updateControlsPanelState = async (st, opts)=> {
            opts = opts || {};

            let line = opts.line,
                theme = opts.theme,
                shell = opts.shell,
                aspectIndex = opts.aspectIndex,
                keepScroll = opts.keepScroll;

            if (!keepScroll) dom_controls_panel.scrollTo(0,0);

            let ol = lines_map.get(Number.parseInt(dom_line_controls.elem.dataset.lineNumber));
            if (ol) ol.elem.classList.remove('line-active');

            if (line) {
                dom_line_controls.elem.dataset.lineNumber = line.number;
                dom_line_controls.position.value = line.number;
                line.elem.classList.add('line-active');
                if (line.entity) {
                    if (dom_entity_controls.selectize[0].selectize.options[line.entity.id]) {
                        dom_entity_controls.selectize[0].selectize.updateOption(line.entity.id, {id: line.entity.id, name: line.entity.name});
                    } else {
                        dom_entity_controls.selectize[0].selectize.addOption({id: line.entity.id, name: line.entity.name});
                    }
                    dom_entity_controls.selectize[0].selectize.setValue(line.entity.id, true);
                    dom_entity_controls.selectize[0].selectize.refreshOptions(false);

                    dom_entity_controls.id.classList.remove('d-none');
                    dom_entity_controls.id.textContent = 'ID: '+line.entity.id;
                } else {
                    dom_entity_controls.selectize[0].selectize.clear(true);
                    dom_entity_controls.id.classList.add('d-none');
                    dom_entity_controls.id.textContent = '';
                }
            } else {
                delete dom_line_controls.elem.dataset.lineNumber;
            }

            let ot = themes_map.get_(dom_theme_controls.elem.dataset.themeId);

            if (theme) {

                if (ot && ot._theme.id !== theme._theme.id) {
                    document.getElementById(ot.memnat.id).classList.remove('theme-active');
                    if (ot.memnat.clearSelection) ot.memnat.clearSelection();
                }

                document.getElementById(theme.memnat.id).classList.add('theme-active');

                dom_theme_controls.elem.dataset.themeId = theme._theme.id;
                dom_theme_controls.id.textContent = 'ID: '+theme._theme.id;
                dom_theme_controls.dimensions.depth.textContent = theme._theme.depth;
                dom_theme_controls.dimensions.width.textContent = theme._theme.width;
                dom_theme_controls.name.value = theme._theme.name();

                if ([M.THEME_TYPE_SIGNAL, M.THEME_TYPE_VERSUS, M.THEME_TYPE_CONTEXT].includes(theme._theme.type)) {
                    dom_theme_controls.position.value = theme.position;
                    dom_theme_controls.quantum.value = theme._theme.quantum;
                    dom_theme_controls.squat.value = theme._theme.squat;
                    dom_theme_controls.positioning.classList.remove('d-none');
                    dom_theme_controls.quantization.classList.remove('d-none');
                    dom_theme_controls.squatting.classList.remove('d-none');
                }

                switch (theme._theme.type) {
                    case M.THEME_TYPE_CONTEXT:
                        aspectNeighbors[0].prepend.classList.remove('d-none');
                        for (let i = 0; i < aspectNeighbors.length; i++) {
                            let n = aspectNeighbors[i];
                            n.elem.classList.remove('d-none');
                        }
                        break;
                    case M.THEME_TYPE_SIGNAL:
                        let n = aspectNeighbors[0]
                        n.abbr.textContent = 'Value';
                        n.elem.classList.remove('d-none');
                        n.prepend.classList.add('d-none');
                        for (let i = 1; i < aspectNeighbors.length; ++i) {
                            let m = aspectNeighbors[i];
                            m.elem.classList.add('d-none');
                        }
                        break;
                    case M.THEME_TYPE_VERSUS:
                        aspectNeighbors[0].prepend.classList.remove('d-none');
                        for (let i = 0; i < 3; ++i) {
                            let n = aspectNeighbors[i];
                            n.elem.classList.remove('d-none');
                        }
                        for (let i = 3; i < aspectNeighbors.length; ++i) {
                            let n = aspectNeighbors[i];
                            n.elem.classList.add('d-none');
                        }
                        break;
                    case M.THEME_TYPE_TEXT:
                        dom_theme_controls.positioning.classList.add('d-none');
                        dom_theme_controls.quantization.classList.add('d-none');
                        dom_theme_controls.squatting.classList.add('d-none');
                        for (let i = 0; i < aspectNeighbors.length; i++) {
                            let n = aspectNeighbors[i];
                            n.elem.classList.add('d-none');
                        }
                        break;
                    default:
                }

                if (theme._theme.type !== M.THEME_TYPE_TEXT) {
                    shell = await shells_map.get(theme._theme.shellId);

                    // Update sound controls
                    for (let i = 0; i < shell.aspectIds.length; ++i) {
                        let asp = (await aspects_map.get(shell.aspectIds[i]))._aspect,
                            n = n_ss[i],
                            asp_sc = dom_theme_controls.soundControls[n];
                        asp_sc.name.textContent = asp.abbr();
                        // TODO: separate each frequency with a delimiter to support multidimensional beings (coefficients)
                        asp_sc.frequency.value = asp.type === M.ASPECT_TYPE_PURE ? asp.frequencies(0) : '';
                        asp_sc.intensity.value = theme._theme.intensities(i)/SOUND_INTENSITY_MAX;
                        asp_sc.intensityRange.value = asp_sc.intensity.value;
                        asp_sc.duration.value = theme._theme.durations(i);

                        let sel = apsui_asp_procs[i],
                            procid = theme._theme.processIds(i);
                        if (procid) {
                            asp_sc.process.elem.dataset.processId = procid;
                            let proc = await processes_map.get(procid);
                            if (sel.selectize.options[procid]) {
                                sel.selectize.updateOption(procid, {id: procid, name: proc.name, path: proc.path});
                            } else {
                                sel.selectize.addOption({id: procid, name: proc.name, path: proc.path});
                            }
                            sel.selectize.setValue(procid, true);
                            sel.selectize.refreshOptions(false);
                        } else {
                            sel.selectize.clear(true);
                        }

                        if (asp.type === M.ASPECT_TYPE_PURE) {
                            asp_sc.frequency.setAttribute('placeholder', 'Set aspect '+n_ss[i].toUpperCase()+' frequency');
                            asp_sc.frequency.removeAttribute('readonly');
                            asp_sc.frequency.removeAttribute('title');
                        } else {
                            asp_sc.frequency.setAttribute('placeholder','--');
                            asp_sc.frequency.setAttribute('readonly','');
                            asp_sc.frequency.setAttribute('title', 'Applies to only pure aspects');
                        }
                    }

                    dom_theme_controls.soundControls.duration.value = theme._theme.duration();
                    let procid = theme._theme.processId(),
                        sel = dom_theme_controls.soundControls.process.selectize[0];
                    if (procid) {
                        dom_theme_controls.soundControls.process.elem.dataset.processId = procid;
                        let proc = await processes_map.get(procid);
                        if (sel.selectize.options[procid]) {
                            sel.selectize.updateOption(procid, {id: procid, name: proc.name, path: proc.path});
                        } else {
                            sel.selectize.addOption({id: procid, name: proc.name, path: proc.path});
                        }
                        sel.selectize.setValue(procid, true);
                        sel.selectize.refreshOptions(false);
                    } else {
                        sel.selectize.clear(true);
                    }

                    if (this.simulation.voice() === theme && this.simulation.time() != null) {
                        for (let i = 0; i < n_ss.length; ++i) {
                            let n = n_ss[i],
                                asp_sc = dom_theme_controls.soundControls[n],
                                chan = this.simulation.channels[i],
                                mutef = chan.mute() ? 'add' : 'remove',
                                solof = chan.solo() ? 'add' : 'remove';
                            asp_sc.mute.classList[mutef]('on');
                            asp_sc.solo.classList[solof]('on');
                        }

                        this.simulation.visualizer.on(); // should show only if sound was initally played with visuals
                    }
                }

            } else {
                if (ot) {
                    document.getElementById(ot.memnat.id).classList.remove('theme-active');
                    if (ot.memnat.clearSelection) ot.memnat.clearSelection();
                }
                delete dom_theme_controls.elem.dataset.themeId;

                for (let i = 0; i < n_ss.length; ++i) delete dom_theme_controls.soundControls[n_ss[i]].process.elem.dataset.processId;
                dom_theme_controls.soundControls.process.elem.dataset.processId;

                if (!this.simulation.persist()) toggleSoundPlayer(false);
            }

            if (shell) {
                dom_shell_controls.name.value = shell.name();
                dom_shell_controls.id.textContent = 'ID: '+shell.id;
                dom_shell_controls.elem.dataset.shellId = shell.id;

                for (let i = 0; i < shell.aspectIds.length; ++i) {
                    let dom_asp = dom_shell_controls['aspect'+(i+1)],
                        asp = await aspects_map.get(shell.aspectIds[i]);

                    dom_asp.name.value = asp._aspect.type === M.ASPECT_TYPE_PURE ? asp._aspect.name : (await themes_map.get(asp._aspect.id))._theme.name();
                    dom_asp.abbr.value = asp._aspect.abbr();

                    if (asp._aspect.type === M.ASPECT_TYPE_PURE) {
                        dom_asp.name.removeAttribute('readonly');
                        dom_asp.name.removeAttribute('title');
                    } else {
                        dom_asp.name.setAttribute('readonly','');
                        dom_asp.name.setAttribute('title','To edit a compound aspect, edit the theme it is made from');
                    }
                }

                if (aspectIndex != undefined) {
                    let asp = await aspects_map.get(shell.aspectIds[aspectIndex]);
                    dom_aspect_controls.elem.dataset.aspectId = asp._aspect.id;
                    dom_aspect_controls.elem.dataset.aspectIndex = aspectIndex;
                } else {
                    delete dom_aspect_controls.elem.dataset.aspectId;
                    delete dom_aspect_controls.elem.dataset.aspectIndex;
                }
            } else {
                delete dom_shell_controls.elem.dataset.shellId;
            }

            // reset theme sound controls
            for (let i = 0; i < n_ss.length; ++i) {
                let n = n_ss[i],
                    asp_sc = dom_theme_controls.soundControls[n];
                asp_sc.mute.classList.remove('on');
                asp_sc.solo.classList.remove('on');
                // if (!this.simulation.persist() || !this.simulation.voice()) {
                //     let chan = this.simulation.channels[i];
                //     chan.mute(false);
                //     chan.solo(false);
                // }
            }

            this.simulation.visualizer.off(true);
            dom_theme_controls.visualization.visualize.classList.remove('on');
            dom_theme_controls.visualization.fullscreen.classList.remove('on');

            if (!this.simulation.persist() || !this.simulation.voice() || (this.simulation.time() == null)) {
                //  change/set the audio source

                let voice = null,
                    voicetype = '';

                if (aspectIndex != undefined) {
                    voice = await aspects_map.get(shell.aspectIds[aspectIndex]);
                    voicetype = VOICE_TYPE_ASPECT;
                } else if (theme && theme._theme.type !== M.THEME_TYPE_TEXT) {
                    voice = theme;
                    voicetype = VOICE_TYPE_THEME;
                }

                if (voice && (voice !== this.simulation.voice())) {
                    let voiceInfo = {
                        title: voice._aspect ? (voice._aspect.name || themes_map.get_(voice._aspect.id)._theme.name()) : voice._theme.name(),
                        lineNumber: line.number, // TODO: update player if line number changes or anything about the line changes
                        themeNumber: theme.position, // TODO: update player if theme position changes or anything about the theme changes,
                        voiceType: voicetype,
                        aspectIndex: aspectIndex,
                        modQuad: (voice._theme && voice.line.modQuad) ? voice.line.modQuad.theme._theme.meaning : null
                    }
                    let cb = ()=> {
                        this.simulation.voice(voice,voicetype,voiceInfo);

                        toggleSoundPlayer(true, voiceInfo);

                        if (this.simulation.autoplay()) {
                            this.simulation.play();
                        }
                    }
                    if (this.simulation.time() != null) {
                        this.simulation.addEventListener('stop', cb, true);
                        this.simulation.stop();
                    } else {
                        cb();
                    }
                } else if (!voice) {
                    toggleSoundPlayer(false)
                    this.simulation.clear();
                    if (this.simulation.time() != null) this.simulation.stop();
                }
            }

            // change panel displays
            switch (st) {
                case CONTROLS_PANEL_ASPECT_STATE:
                    display(dom_aspect_controls.elem, true);
                    display(dom_theme_controls.elem, true);
                    display(dom_shell_controls.elem, true);
                    display(dom_entity_controls.elem, true);
                    display(dom_player_controls.elem, true);
                    display(dom_line_controls.elem, true);
                    display(dom_scene_controls.elem, true);
                    break;
                case CONTROLS_PANEL_THEME_STATE:
                    display(dom_theme_controls.elem, true);
                    let supps = [M.THEME_TYPE_SIGNAL, M.THEME_TYPE_VERSUS, M.THEME_TYPE_CONTEXT],
                        show_subs = theme ? supps.includes(theme._theme.type) : false;
                    display(dom_theme_controls.subControls, show_subs);
                    display(dom_aspect_controls.elem, false);
                    display(dom_shell_controls.elem, show_subs);
                    display(dom_entity_controls.elem, true);
                    display(dom_player_controls.elem, true);
                    display(dom_line_controls.elem, true);
                    display(dom_scene_controls.elem, true);
                    break;
                case CONTROLS_PANEL_SHELL_STATE:
                    display(dom_shell_controls.elem, true);
                    display(dom_aspect_controls.elem, false);
                    display(dom_theme_controls.elem, false);
                    display(dom_entity_controls.elem, true);
                    display(dom_player_controls.elem, true);
                    display(dom_line_controls.elem, true);
                    display(dom_scene_controls.elem, true);
                    break;
                case CONTROLS_PANEL_ENTITY_STATE:
                    display(dom_entity_controls.elem, true);
                    display(dom_aspect_controls.elem, false);
                    display(dom_theme_controls.elem, false);
                    display(dom_shell_controls.elem, false);
                    display(dom_player_controls.elem, true);
                    display(dom_line_controls.elem, true);
                    display(dom_scene_controls.elem, true);
                    break;
                case CONTROLS_PANEL_LINE_STATE:
                    display(dom_line_controls.elem, true);
                    display(dom_aspect_controls.elem, false);
                    display(dom_theme_controls.elem, false);
                    display(dom_shell_controls.elem, false);
                    display(dom_player_controls.elem, true);
                    display(dom_entity_controls.elem, true);
                    display(dom_scene_controls.elem, true);
                    break;
                case CONTROLS_PANEL_SCENE_STATE:
                    display(dom_scene_controls.elem, true);
                    display(dom_aspect_controls.elem, false);
                    display(dom_theme_controls.elem, false);
                    display(dom_shell_controls.elem, false);
                    display(dom_player_controls.elem, true);
                    display(dom_entity_controls.elem, false);
                    display(dom_line_controls.elem, false);
                    break;
                default:
                    display(dom_aspect_controls.elem, false);
                    display(dom_theme_controls.elem, false);
                    display(dom_shell_controls.elem, false);
                    display(dom_entity_controls.elem, false);
                    display(dom_player_controls.elem, false);
                    display(dom_line_controls.elem, false);
                    display(dom_scene_controls.elem, false);
            }
        }

        // This fixes Bootstrap buttons staying in focus after click
        const fixButtonResponse = b => b.addEventListener('click', e => {
            $(e.target).blur();
            e.preventDefault();
        });
        document.querySelectorAll('[role="button"]').forEach(b => fixButtonResponse(b));

        // Enable all tooltips
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        // Set intensity max of intensity ranges and number inputs
        document.querySelectorAll('.intensity-range, .aspect-intensity').forEach(e => {
            e.setAttribute('max', 1);
        });

        const toggleSoundPlayer = (on, info)=> {
            if (on === true) {
                dom_sound_player.elem.classList.remove('d-none');
                dom_panel_player.classList.add('player-active');
            } else if (on === false) {
                dom_sound_player.elem.classList.add('d-none');
                dom_panel_player.classList.remove('player-active');
            } else if(dom_panel_player.classList.contains('player-active')) {
                toggleSoundPlayer(true, info);
            } else {
                toggleSoundPlayer(false);
            }

            if (info) {
                dom_sound_player.title.textContent = shorten(info.title, 42);
                dom_sound_player.lineNumber.textContent = info.lineNumber;
                dom_sound_player.themeNumber.textContent = info.themeNumber;
                if (info.voiceType === VOICE_TYPE_THEME) {
                    dom_sound_player.voiceType.textContent = "{Theme}"
                } else {
                    dom_sound_player.voiceType.textContent = "(Aspect-"+(info.aspectIndex+1)+")";
                }
            }
        }

        // NOTE: perform validation before updating Memnat objects
        // arg 'data' is either all the outgoing edges of a single
        // aspect in order or a single node value
        updateAspectControls = async (themeid, spos, data)=> {
            let theme = await themes_map.get(themeid),
                shell = await shells_map.get(theme._theme.shellId),
                asp = await aspects_map.get(shell.aspectIds[spos]),

                asp_c = dom_aspect_controls;

            onThemeSelect(theme, M.CLICKED_ON_CANVAS,true);

            updateControlsPanelState(CONTROLS_PANEL_ASPECT_STATE, {line: theme.line, theme: theme, aspectIndex:spos});

            asp_c.name.value = asp._aspect.type === M.ASPECT_TYPE_PURE ? asp._aspect.name : (await themes_map.get(asp._aspect.id))._theme.name();
            asp_c.abbr.value = asp._aspect.abbr();
            dom_theme_controls.name.value = theme._theme.name();
            dom_shell_controls.name.value = shell.name();

            asp_c.id.textContent = 'ID: '+asp._aspect.id;
            dom_theme_controls.id.textContent = 'ID: '+theme._theme.id;
            dom_shell_controls.id.textContent = 'ID: '+shell.id;

            if (asp._aspect.type === M.ASPECT_TYPE_PURE) {
                // TODO: show pure branding
                asp_c.name.removeAttribute('readonly');
                asp_c.name.removeAttribute('title');
                asp_c.frequency.value = asp._aspect.frequencies(0);
                asp_c.frequency.removeAttribute('readonly');
                asp_c.frequency.removeAttribute('title');
                asp_c.frequency.setAttribute('placeholder','0.0');
                asp_c.cover.value = asp._aspect.cover();
                asp_c.cover.removeAttribute('readonly');
            } else {
                // TODO: remove pure branding
                asp_c.name.setAttribute('readonly','');
                asp_c.name.setAttribute('title','To edit a compound aspect, edit the theme it is made from.');
                [asp_c.frequency, asp_c.cover].forEach(e => {
                    e.value = '';
                    e.setAttribute('readonly','');
                    e.setAttribute('title','Applies to only pure aspects');
                    e.setAttribute('placeholder','*Applies to only pure aspects*');
                });
            }

            switch (theme._theme.type) {
                case M.THEME_TYPE_CONTEXT:
                    for (let i = 0; i < aspectNeighbors.length; i++) {
                        let n = aspectNeighbors[i],
                            e = data[i],
                            asp = await aspects_map.get(shell.aspectIds[e.epos]);
                        n.input.dataset.neighborPosition = e.epos;
                        n.input.value = e.value;
                        n.abbr.textContent = asp._aspect.abbr();
                        switch (e.arrowType) {
                            case M.ARROW_TYPE_SIMILARITY:
                                n.arrowUse.setAttribute('xlink:href','#similarity-arrow');
                                break;
                            case M.ARROW_TYPE_CAUSATION:
                                n.arrowUse.setAttribute('xlink:href','#causation-arrow');
                                break;
                            case M.ARROW_TYPE_DEPENDENCE:
                                n.arrowUse.setAttribute('xlink:href','#dependence-arrow');
                                break;
                            case M.ARROW_TYPE_SPECTRUM:
                                n.arrowUse.setAttribute('xlink:href','#spectrum-arrow');
                                break;
                        }
                    }
                    break;
                case M.THEME_TYPE_SIGNAL:
                    let n = aspectNeighbors[0],
                        asp = await aspects_map.get(shell.aspectIds[spos]);
                    n.input.dataset.neighborPosition = spos;
                    n.input.value = data.value;
                    break;
                case M.THEME_TYPE_VERSUS:
                    for (let i = 0; i < 3; ++i) {
                        let n = aspectNeighbors[i],
                            e = data[i],
                            asp = await aspects_map.get(shell.aspectIds[e.epos]);
                        n.input.dataset.neighborPosition = e.epos;
                        n.input.value = e.value;
                        n.abbr.textContent = asp._aspect.abbr();
                        // TODO: use e.arrowType
                    }
                    break;
                default:
            }
        }

        // TODO: make compatible with all themes
        aspectNeighbors.map((n) => {
            $(n.input).on('change', async (e)=> {
                let epos = Number.parseInt(n.input.dataset.neighborPosition),
                    spos = Number.parseInt(dom_aspect_controls.elem.dataset.aspectIndex),
                    tid = dom_theme_controls.elem.dataset.themeId,
                    theme = await themes_map.get(tid);
                if (e.type == 'blur') {
                    n.input.value = theme.memnat.valueForMeaning(spos,epos);
                    return;
                }
                let v = Math.range(Number.parseFloat(n.input.value), 0, 1) || 0;
                if (theme._theme.type === M.THEME_TYPE_SIGNAL) {
                    theme.memnat.setMeaningValue(spos,v);
                } else {
                    theme.memnat.setMeaningValue(spos,epos,v);
                }

                CUD.update.themes.set(tid, 'meaning', theme._theme.meaning);

                n.input.blur();
            });

            n.arrowMenu.querySelectorAll('.arrow-item').forEach(item => {
                item.addEventListener('click', async ()=> {
                    let arrow_type = Number.parseInt(item.dataset.arrowType),
                        epos = Number.parseInt(n.input.dataset.neighborPosition),
                        spos = Number.parseInt(dom_aspect_controls.elem.dataset.aspectIndex),
                        tid = dom_theme_controls.elem.dataset.themeId,
                        theme = await themes_map.get(tid);

                    switch (arrow_type) {
                        case M.ARROW_TYPE_SIMILARITY:
                            n.arrowUse.setAttribute('xlink:href','#similarity-arrow');
                            break;
                        case M.ARROW_TYPE_CAUSATION:
                            n.arrowUse.setAttribute('xlink:href','#causation-arrow');
                            break;
                        case M.ARROW_TYPE_DEPENDENCE:
                            n.arrowUse.setAttribute('xlink:href','#dependence-arrow');
                            break;
                        case M.ARROW_TYPE_SPECTRUM:
                            n.arrowUse.setAttribute('xlink:href','#spectrum-arrow');
                            break;
                    }

                    theme.memnat.setArrow(spos,epos,arrow_type);

                    CUD.update.themes.set(tid, 'meaning', theme._theme.meaning);
                });
            });
        });

        const sotSelect_UI = ()=> {
            dom_sot_controls.selectize = $(dom_sot_controls.select).selectize({
                // selectOnTab: true,
                searchField: ['name', 'path'],
                labelField: 'name',
                valueField: 'id',
                sortField: {
    				field: 'name',
    				direction: 'asc'
    			},
                // persist: false,
                create: false,
                onChange: async (v)=> {

                    this.script.player.sotId = v;

                    if (!v) {
                        this.simulation.curriculum = PUBLIC_SCHOOL;
                        this.script.school = PUBLIC_SCHOOL;
                        delete dom_sot_controls.elem.dataset.sotId;
                        dom_sot_controls.id.classList.add('d-none');

                        dom_workspace_title.setTextContent();

                        return;
                    }

                    let finishEnrolling = (school)=> {
                        dom_sot_controls.elem.dataset.sotId = school.id;
                        dom_sot_controls.id.textContent = 'ID: '+school.id;
                        dom_sot_controls.id.classList.remove('d-none');
                        dom_workspace_title.setTextContent();
                    }

                    schools_map.get(v).then((school)=> {
                        this.simulation.curriculum = school;
                        this.script.school = school;
                        finishEnrolling();
                    }).catch((school)=> {
                        packager.dialog.alert('An error occured while enrolling in SOT, '+school.name+':'+school.id+'. The Public School will be used. Reload to try to reenter the school.');
                        this.simulation.curriculum = PUBLIC_SCHOOL;
                        this.script.school = PUBLIC_SCHOOL;
                        finishEnrolling();
                    });
                },
                render: {
                    option: (item,escape)=> {
                        return  '<div class="select-dropdown-option">'+
                                    '<ul>'+
                                        '<li class="name">'+item.name+'</li>'+
                                        '<li class="id">'+item.id+'</li>'+
                                        '<li>'+shorten(item.path,42)+'</li>'+
                                    '</ul>'+
                                '</div>';
                    }
                },
                load: (query,callback)=> {
                    if (!query.length) return callback();

                    packager.generalLoader.work({
                        type: ITEMS_TYPE_SCHOOLS,
                        packager: litePackager,
                        query: {
                            limit: 30,
                            comparators: [
                                {
                                    key: 'name',
                                    predicate: 'contains',
                                    ignoreCase: true,
                                    value: query
                                },
                                {
                                    key: 'path',
                                    predicate: 'contains',
                                    ignoreCase: true,
                                    value: query
                                }
                            ]
                        },
                        persist: CUD.partialDrain(null,[ITEMS_TYPE_SCHOOLS]),
                        resolve: (data)=> {
                            // TODO: put back items on persist failure if data.putBack
                            callback(data.items);
                        },
                        reject: (data)=> {
                            console.error('An error occured while loading schoolS.');
                            console.error(data.error);
                            callback();
                        }
                    });
                }
            });
        }

        sotSelect_UI();

        const masterSelect_UI = ()=> {
            dom_master_controls.selectize = $(dom_master_controls.select).selectize({
                // selectOnTab: true,
                searchField: ['name', 'path'],
                labelField: 'name',
                valueField: 'id',
                sortField: {
    				field: 'name',
    				direction: 'asc'
    			},
                // persist: false,
                create: false,
                onChange: async (v)=> {

                    this.script.player.masterId = v;

                    if (!v) {
                        // check if sound is currently playing
                            // remove player process of Sound
                        delete dom_master_controls.elem.dataset.masterId;
                        dom_master_controls.id.classList.add('d-none');
                        return;
                    }

                    // if sound is playing, update Sound player process
                    let proc = await processes_map.get(v);

                    dom_master_controls.elem.dataset.masterId = proc.id;
                    dom_master_controls.id.textContent = 'ID: '+proc.id;
                    dom_master_controls.id.classList.remove('d-none');
                },
                render: {
                    option: (item,escape)=> {
                        // Use most current item
                        let it_m = processes_map.get_(item.id);
                        if (it_m) {
                            item.name = it_m.name;
                            item.path = it_m.path;
                        }
                        return  '<div class="select-dropdown-option">'+
                                    '<ul>'+
                                        '<li class="name">'+item.name+'</li>'+
                                        '<li class="id">'+item.id+'</li>'+
                                        '<li>'+shorten(item.path,42)+'</li>'+
                                    '</ul>'+
                                '</div>';
                    }
                },
                load: (query,callback)=> {
                    if (!query.length) return callback();

                    packager.generalLoader.work({
                        type: ITEMS_TYPE_PROCESSES,
                        packager: litePackager,
                        query: {
                            limit: 30,
                            comparators: [
                                {
                                    key: 'name',
                                    predicate: 'contains',
                                    ignoreCase: true,
                                    value: query
                                },
                                {
                                    key: 'path',
                                    predicate: 'contains',
                                    ignoreCase: true,
                                    value: query
                                }
                            ]
                        },
                        persist: CUD.partialDrain(null,[ITEMS_TYPE_PROCESSES]),
                        resolve: (data)=> {
                            // TODO: put back items on persist failure if data.putBack
                            callback(data.items);
                        },
                        reject: (data)=> {
                            console.error('An error occured while loading processes.');
                            console.error(data.error);
                            callback();
                        }
                    });
                }
            });
        }

        masterSelect_UI();

        // Entity select field
        const entitySelect_UI = ()=> {
            dom_entity_controls.selectize = $(dom_entity_controls.select).selectize({
                // selectOnTab: true,
                searchField: ['name'],
                labelField: 'name',
                valueField: 'id',
                sortField: {
    				field: 'name',
    				direction: 'asc'
    			},
                // persist: false,
                create: false,
                onChange: async (v)=> {
                    let line = lines_map.get(Number.parseInt(dom_line_controls.elem.dataset.lineNumber));
                    if (!line) return;
                    if (!v) {
                        if (line.entity) {
                            line.entity.coords.delete(line.number);
                            line.entity = null;
                            delete dom_entity_controls.elem.dataset.entityId;
                        }
                        if (line.modQuad && line.modQuad.elem) {
                            line.modQuad.elem.remove();
                        }
                        line.modQuad = null;
                        line.domEntity().textContent = '';
                        line.domEntity().classList.add('d-none');
                        dom_entity_controls.id.classList.add('d-none');
                        return;
                    }
                    let ent = await entities_map.get(v),
                        mq = line.modQuad,
                        oent_id = dom_entity_controls.elem.dataset.entityId,
                        oent = entities_map.get_(oent_id);
                    if (oent) oent.coords.delete(line.number);

                    // Writers must rely solely on text to describe the entity if
                    // they are using a textual theme for the line.
                    if (line.themes.length === 0 || line.themes[0]._theme.type !== M.THEME_TYPE_TEXT) {
                        if (!mq || !mq.theme.memnat) {
                            mq = mq || {};
                            if (!mq.theme) {
                                let _theme = new M.Theme({
                                    id: guid(),
                                    name: 'Modifying Quadrilateral for line '+line.id,
                                    type: M.THEME_TYPE_MOD_QUAD
                                });
                                mq.theme = new Theme(_theme);
                                // TODO: onSelect, onMeaningChange
                                _theme.onMeaningChange = (d)=> onMeaningChange(mq.theme,d);
                                _theme.onMeaningCommit = (d)=> onMeaningCommit(mq.theme,d);
                                _theme.onSelect = (clk)=> onThemeSelect(mq.theme,clk);
                                mq.theme.line = line;
                                mq.theme.position = 0;
                            }
                        }
                        let mem = mq.elem;
                        if (!mem) {
                            mem = document.createElement('div');
                            mem.id = 'modquad-'+mq.theme._theme.id;
                            mem.classList.add('col-3','memnat', 'modquad');
                            line.elem.querySelector('.line-memnat .memnat-row').prepend(mem);
                            mq.elem = mem;
                        }
                        if (!mq.theme.memnat) { // when will this ever be the case so far
                            mq.theme.memnat = new M.ModQuad({
                                theme: mq.theme._theme,
                                container: mem.id
                            });
                        }

                        line.modQuad = mq;

                        mem.scrollIntoView({block:'center',behavior:SCROLL_BEHAVIOR});
                    }

                    dom_entity_controls.elem.dataset.entityId = ent.id;
                    line.entity = ent;
                    line.entity.coords.add(line.number);

                    dom_entity_controls.id.textContent = 'ID: '+ent.id;
                    dom_entity_controls.id.classList.remove('d-none');
                    line.domEntity().textContent = ent.name;
                    line.domEntity().classList.remove('d-none');
                },
                render: {
                    option: (item,escape)=> {
                        // Use most current item
                        let it_m = entities_map.get_(item.id);
                        if (it_m) item.name = it_m.name;
                        return  '<div class="select-dropdown-option">'+
                                    '<ul>'+
                                        '<li class="name">'+item.name+'</li>'+
                                        '<li class="id">'+item.id+'</li>'+
                                    '</ul>'+
                                '</div>';
                    }
                },
                load: (query,callback)=> {
                    if (!query.length) return callback();

                    packager.generalLoader.work({
                        type: ITEMS_TYPE_ENTITIES,
                        packager: litePackager,
                        query: {
                            limit: 30,
                            comparators: [
                                {
                                    key: 'name',
                                    predicate: 'contains',
                                    ignoreCase: true,
                                    value: query
                                }
                            ]
                        },
                        persist: CUD.partialDrain(null,[ITEMS_TYPE_ENTITIES]),
                        resolve: (data)=> {
                            // TODO: put back items on persist failure if data.putBack
                            callback(data.items);
                        },
                        reject: (data)=> {
                            console.error('An error occured while loading entities.');
                            console.error(data.error);
                            callback();
                        }
                    });
                }
            });
        }

        entitySelect_UI();

        const processSelect_UI = ()=> {
            dom_theme_controls.soundControls.process.selectize = $(dom_theme_controls.soundControls.process.elem).selectize({
                // selectOnTab: true,
                searchField: ['name','path'],
                labelField: 'name',
                valueField: 'id',
                sortField: {
    				field: 'name',
    				direction: 'asc'
    			},
                // persist: false,
                create: false,
                onChange: async (v)=> {
                    let theme = await themes_map.get(dom_theme_controls.elem.dataset.themeId);
                    if (!theme) return;
                    if (!v) {
                        // TODO: check if sound of this theme is playing to update process in Sound
                        theme._theme.processId('');
                        return;
                    }
                    let proc = await processes_map.get(v),
                        oproc_id = dom_theme_controls.soundControls.process.elem.dataset.processId;
                    if (oproc_id) (await processes_map.get(oproc_id)).coords.delete(theme._theme.id);

                    theme._theme.processId(proc.id);
                    dom_theme_controls.soundControls.process.elem.dataset.processId = proc.id;
                    proc.coords.add(theme._theme.id);

                    // TODO: check if theme is playing
                        // if so, load process and update process in Sound
                },
                render: {
                    option: (item,escape)=> {
                        // Use most current item
                        let it_m = processes_map.get_(item.id);
                        if (it_m) {
                            item.name = it_m.name;
                            item.path = it_m.path;
                        }
                        return  '<div class="select-dropdown-option">'+
                                    '<ul>'+
                                        '<li class="name">'+item.name+'</li>'+
                                        '<li class="id">'+item.id+'</li>'+
                                        '<li>'+shorten(item.path,42)+'</li>'+
                                    '</ul>'+
                                '</div>';
                    }
                },
                load: (query,callback)=> {
                    if (!query.length) return callback();

                    packager.generalLoader.work({
                        type: ITEMS_TYPE_PROCESSES,
                        packager: litePackager,
                        query: {
                            limit: 30,
                            comparators: [
                                {
                                    key: 'name',
                                    predicate: 'contains',
                                    ignoreCase: true,
                                    value: query
                                },
                                {
                                    key: 'path',
                                    predicate: 'contains',
                                    ignoreCase: true,
                                    value: query
                                }
                            ]
                        },
                        persist: CUD.partialDrain(null,[ITEMS_TYPE_PROCESSES]),
                        resolve: (data)=> {
                            // TODO: put back items on persist failure if data.putBack
                            callback(data.items);
                        },
                        reject: (data)=> {
                            console.error('An error occured while loading processes.');
                            console.error(data.error);
                            callback();
                        }
                    });
                }
            });
        }

        processSelect_UI();

        const apsui_asp_procs = [];
        const aspectProcessSelect_UI = ()=> {
            for (let i = 0; i < n_ss.length; ++i) {
                let n = n_ss[i],
                    asp_sc = dom_theme_controls.soundControls[n];
                asp_sc.process.selectize = $(asp_sc.process.elem).selectize({
                    // selectOnTab: true,
                    searchField: ['name','path','id'],
                    labelField: 'name',
                    valueField: 'id',
                    sortField: {
        				field: 'name',
        				direction: 'asc'
        			},
                    // persist: false,
                    create: false,
                    onChange: async (v)=> {
                        let theme = await themes_map.get(dom_theme_controls.elem.dataset.themeId);
                        if (!theme) return;
                        if (!v) {
                            // TODO: check if sound of this theme is playing to update Tone.Transport
                            theme._theme.processIds(i,'');
                            return;
                        }
                        let proc = await processes_map.get(v),
                            oproc_id = asp_sc.process.elem.dataset.processId,
                            oproc = processes_map.get_(oproc_id);
                        if (oproc) oproc.coords.delete(theme._theme.id);

                        theme._theme.processIds(i,proc.id);
                        asp_sc.process.elem.dataset.processId = proc.id;
                        proc.coords.add(theme._theme.id);

                        // TODO: update Tone.Transport if aspect, theme, or another theme using this aspect is playing
                    },
                    render: {
                        option: (item,escape)=> {
                            // Use most current item
                            let it_m = processes_map.get_(item.id);
                            if (it_m) {
                                item.name = it_m.name;
                                item.path = it_m.path;
                            }
                            return  '<div class="select-dropdown-option">'+
                                        '<ul>'+
                                            '<li class="name">'+item.name+'</li>'+
                                            '<li class="id">'+item.id+'</li>'+
                                            '<li>'+shorten(item.path,42)+'</li>'+
                                        '</ul>'+
                                    '</div>';
                        }
                    },
                    load: (query,callback)=> {
                        if (!query.length) return callback();

                        packager.generalLoader.work({
                            type: ITEMS_TYPE_PROCESSES,
                            packager: litePackager,
                            query: {
                                limit: 30,
                                comparators: [
                                    {
                                        key: 'name',
                                        predicate: 'contains',
                                        ignoreCase: true,
                                        value: query
                                    },
                                    {
                                        key: 'path',
                                        predicate: 'contains',
                                        ignoreCase: true,
                                        value: query
                                    }
                                ]
                            },
                            persist: CUD.partialDrain(null,[ITEMS_TYPE_PROCESSES]),
                            resolve: (data)=> {
                                // TODO: put back items on persist failure if data.putBack
                                callback(data.items);
                            },
                            reject: (data)=> {
                                console.error('An error occured while loading processes.');
                                console.error(data.error);
                                callback();
                            }
                        });
                    }
                });
                apsui_asp_procs.push(asp_sc.process.selectize[0]);
            }
        }

        aspectProcessSelect_UI();

        const hookLineButtons = (line)=> {
            let lb = line.domNumber(),
                pb = line.domPrepend(),
                ab = line.domAppend(),
                atb = line.domAddTheme(),
                db = line.domRemove(),
                eb = line.domEntity();

            eb.addEventListener('click', ()=> updateControlsPanelState(CONTROLS_PANEL_ENTITY_STATE, {line: line}));
            eb.addEventListener('dblclick', ()=> {
                packager.dialog.prompt({
                    message: 'Rename the entity, '+line.entity.name,
                    callback: (name)=> {
                        if (!name) return;
                        eb.textContent = name;
                        line.entity.name = name;
                        if (dom_entity_controls.selectize[0].selectize.options[line.entity.id]) {
                            dom_entity_controls.selectize[0].selectize.updateOption(line.entity.id, {id: line.entity.id, name: line.entity.name});
                        } else {
                            dom_entity_controls.selectize[0].selectize.addOption({id: line.entity.id, name: line.entity.name});
                        }
                        dom_entity_controls.selectize[0].selectize.setValue(line.entity.id, true);
                        dom_entity_controls.selectize[0].selectize.refreshOptions(false);
                        for (let coord of line.entity.coords) {
                            let l = lines_map.get(coord);
                            eb.textContent = name;
                        }
                        CUD.update.entities.set(line.entity.id, 'name', name);
                    }
                });
            });

            lb.addEventListener('click', () => updateControlsPanelState(CONTROLS_PANEL_LINE_STATE, {line:line}));
            pb.addEventListener('click', e => addLine(true,e.target));
            ab.addEventListener('click', e => addLine(false,e.target));
            atb.addEventListener('click', () => addTheme_UI(line));
            db.addEventListener('click', () => repositionLine(line,lines_map.size,true));

            [lb,pb,ab,atb,db,eb].map(b => {
                fixButtonResponse(b);
                if (packager.name !== 'web') {
                    b.setAttribute('tabindex','-1');
                }
            });

        }

        const repositionTheme = (theme,pos,del)=> {
            if (pos < 1) {
                repositionTheme(theme,1,del);
                return;
            }
            if (pos > theme.line.themes.length) {
                repositionTheme(theme,theme.line.themes.length,del);
                return;
            }

            if (pos === theme.position && !del) return;

            if (pos < theme.position) {
                for (let i = pos-1; i < theme.position-1; ++i) {
                    let t = theme.line.themes[i];
                    t.position++;
                    t.memnat.setPosition(''+t.position);
                    CUD.update.themes.set(t._theme.id, 'position', t.position);
                }
            } else {
                for (let i = theme.position; i < pos; ++i) {
                    let t = theme.line.themes[i];
                    t.position--;
                    t.memnat.setPosition(''+t.position);
                    CUD.update.themes.set(t._theme.id, 'position', t.position);
                }
            }
            let opos = theme.position;
            theme.line.themes.splice(theme.position-1,1);
            theme.line.themes.splice(pos-1,0,theme);

            let mem = document.getElementById(theme.memnat.id);

            if (del) {
                theme.line.themes.pop();
                themes_map.delete(theme._theme.id);
                mem.remove();

                delete dom_theme_controls.elem.dataset.themeId;
                delete dom_shell_controls.elem.dataset.shellId;

                let shell = shells_map.get_(theme._theme.shellId);
                if (shell) shell.coords.delete(theme._theme.id);

                CUD.update.themes.set(theme._theme.id, 'position', null);
                // CUD.delete.themes.add(theme._theme.id); // themes can linger outside lines

                return;
            }

            theme.position = pos;
            CUD.update.themes.set(theme._theme.id, 'position', pos);

            let mr = theme.line.elem.querySelector('.line-memnat .memnat-row'),
                ctx = theme.memnat;

            if (dom_theme_controls.elem.dataset.themeId === theme._theme.id) dom_theme_controls.position.value = theme.position;

            pos = theme.line.modQuad ? pos+1 : pos; // To ensure mod quad stays in first position
            opos = theme.line.modQuad ? opos+1 : opos;

            if (pos < opos) {
                mr.insertBefore(mem, mr.children[pos-1])
            } else if ((pos > opos) && (pos >= mr.children.length)) {
                mr.appendChild(mem);
            } else {
                mr.insertBefore(mem, mr.children[pos]);
            }
            ctx.setPosition(''+theme.position);
            mem.querySelectorAll('.direction-label').forEach(dl => dl.classList.remove('active'));

            onThemeSelect(theme,M.CLICKED_ON_THEME_TITLE);
        }

        let sizeTheOverlays = ()=> {
            $("#overlay").resize().each(()=> {
                let h = $(window).height();
                let w = $(window).width();
                $("#overlay").css("height", h);
                $("#overlay").css("width", w);
            });
        };

        sizeTheOverlays();

        const restScene = async ()=> {

            // This will automatically save the current scene
            // changes on scene change.
            // if (this.script.data) {
            //     await saveScript();
            //     this.scene.lineIds = this.script.data.scenes[this.scene.id].lineIds;
            // }

            [aspects_map, shells_map, themes_map, entities_map, processes_map].map(m => m.clear());

            for (const [num,l] of lines_map) {
                l.elem.remove();
                if (l.modQuad && l.modQuad.elem) l.modQuad.elem.remove();
            }

            lines_map.clear();
        }

        const loadScene = async (pos, after)=> {

            clearControls({script: true});

            this.scene = scenes_map.get(pos);

            dom_scene_controls.id.textContent = 'ID: '+this.scene.id;
            dom_scene_controls.name.value = this.scene.name;

            let lines = new Array(this.scene.lineIds.length);

            dom_scene_controls.name.value = this.scene.name;

            dom_workspace_title.setTextContent(this.scene.name + ' (Scene '+this.scene.position+')'+' - '+this.script.name); // TODO: +authors

            updateControlsPanelState(CONTROLS_PANEL_SCENE_STATE);

            return new Promise((resolve,reject)=> {
                if (lines.length === 0) {
                    resolve();
                    return;
                }

                let num_themes = 0;

                let myresolve = async (data) => {
                    let items = data.items;
                    let line = null;

                    for (let i = 0; i < items.length; ++i) {
                        let line_ = items[i];
                        line = new Line(line_.id,line_.number);
                        line.creationTime = line_.creationTime;
                        line.saveTime = line_.saveTime;
                        if (line_.entityId) {
                            entities_map.get(line_.entityId).then((entity)=> {
                                line.entity = entity;
                                line.entity.coords.add(line.number);
                            });
                        }
                        if (line_.modQuad) {
                            let mqt = line_.modQuad.theme;
                            let theme = new Theme(new M.Theme({
                                id: mqt.id,
                                name: mqt.name,
                                meaning: mqt.meaning,
                                type: mqt.type,
                                onMeaningChange: (d)=> onMeaningChange(theme, d),
                                onMeaningCommit: (d)=> onMeaningCommit(theme, d)
                            }));
                            theme.line = line;
                            line.modQuad = { theme: theme }
                        }

                        let line_themes = [];

                        for (let j = 0; j < line_.themeIds.length; ++j) {
                            let tid = line_.themeIds[j];
                            let theme = await themes_map.get(tid);
                            theme.line = line;
                            if (theme._theme.type !== M.THEME_TYPE_TEXT) {
                                shells_map.get(theme._theme.shellId).then((shell)=> {
                                    shell.coords.add(theme._theme.id);
                                });
                            }
                            line_themes.push(theme);
                            num_themes++;
                        }

                        line.themes = line_themes;

                        lines[line.number-1] = line;
                    }

                    drawLines();
                }

                packager.generalLoader.work({
                    type: ITEMS_TYPE_LINES,
                    packager: litePackager,
                    ids: this.scene.lineIds,
                    resolve: myresolve,
                    reject: (data)=> { console.error(data.error) }
                });

                async function drawLines() {
                    let lc = 0, tc = 0,
                        ql = packager.dialog.quietLoading;
                    ql('0%');

                    let missing = [];

                    function checkStats(rm) {
                        rm ? t+=rm : tc++;
                        ql(Math.ceil(tc/num_themes*100 || 99)+'%');

                        if (tc >= num_themes) { // greater than for 0 themes case
                            ql('100%').close();
                            if (missing.length > 0) {
                                packager.dialog.alert('These lines are missing: '+missing);
                            }

                            resolve();
                        }
                    }

                    for (let i = 0; i < lines.length; ++i) {
                        let line = lines[i];

                        if (!line) {
                            line = await addLine(null,null,true);
                            line.number = i+1;
                            missing.push(line.number);
                            console.log('here');
                        }

                        lines_map.set(line.number,line);

                        await new Promise((lresolve,lreject)=> {
                            loadTemplate('templates/scene-line.html', async (lt) => {
                                // TODO: consider appending all lines at once
                                    // after all themes have been drawn
                                dom_script.appendChild(lt);
                                line.elem = lt;
                                lt.dataset.lineNumber = line.number;
                                line.domNumber().textContent = line.number;
                                hookLineButtons(line);

                                if (line.entity) {
                                    // dom_entity_controls.id.classList.remove('d-none');
                                    // dom_entity_controls.id.textContent = 'ID: '+line.entity.id;
                                    // dom_entity_controls.selectize[0].selectize.setTextboxValue(line.entity.name);
                                    // dom_entity_controls.selectize[0].selectize.setValue(line.entity.id, true);
                                    line.domEntity().textContent = line.entity.name;
                                    dom_entity_controls.elem.dataset.entityId = line.entity.id;
                                    line.domEntity().classList.remove('d-none');

                                    if (line.modQuad) {
                                        if ((line.themes.length > 0) && (line.themes[0]._theme.type === M.THEME_TYPE_TEXT)) {
                                            console.error('Ignoring mod quad for line, '+line.id+', because that line has a textual theme.')
                                        } else {
                                            let mq = line.modQuad,
                                                mem = document.createElement('div');
                                            mem.id = 'modquad-'+mq.theme._theme.id;
                                            mem.classList.add('col-3','memnat', 'modquad');
                                            line.elem.querySelector('.line-memnat .memnat-row').prepend(mem);
                                            mq.elem = mem;
                                            mq.theme.memnat = new M.ModQuad({
                                                theme: mq.theme._theme,
                                                container: mem.id
                                            });
                                        }
                                    }
                                }


                                for (let j = 0; j < line.themes.length; ++j) {

                                    let theme = line.themes[j],
                                        _theme = theme._theme;
                                    if ((_theme.type === M.THEME_TYPE_TEXT) && (j > 0 || (line.modQuad && line.modQuad.elem))) {
                                        console.error('Tried to add textual theme beside other themes. Textual themes require a line all to themselves.');

                                        checkStats();

                                        continue;
                                    }
                                    let mem = document.createElement('div');
                                    _theme.type === M.THEME_TYPE_TEXT ? mem.classList.add('col-12','memnat') : mem.classList.add('col-3','memnat');
                                    line.elem.querySelector('.line-memnat .memnat-row').appendChild(mem);

                                    mem.addEventListener('mouseover', ()=> {
                                        mem.querySelectorAll('.direction-label').forEach(dl => dl.classList.add('active'));
                                    });

                                    mem.addEventListener('mouseout', ()=> {
                                        mem.querySelectorAll('.direction-label').forEach(dl => dl.classList.remove('active'));
                                    });

                                    mem.id = 'memnat-'+_theme.id;
                                    mem.dataset.themeId = _theme.id;

                                    if (_theme.shellId) {
                                        shells_map.get(_theme.shellId).then((shell)=> {
                                            shell.coords.add(_theme.id);
                                        });
                                    }

                                    // theme.memnat = await drawMemnat(_theme.id,mem.id);
                                    // checkStats();
                                    drawMemnat(_theme.id, mem.id, false).then((memnat)=> {
                                        theme.memnat = memnat;
                                        checkStats();
                                    });

                                    if (line.themes[j+1] && _theme.type === M.THEME_TYPE_TEXT) {
                                        console.error('Tried to add theme beside textual theme. Textual themes require a line all to themselves.');
                                        checkStats(line.themes.length-j);
                                        break;
                                    }
                                }

                                lc++;

                                lresolve();
                            });
                        });

                        if (lc === lines.length) {
                            dom_toolbar.goto.numLines.textContent = '/'+lines.length;
                            if (tc === num_themes) checkStats();
                        }
                    }
                }
            });
        }

        // let width = $(window).width();
        // $(window).resize(()=> {
        //     if($("#overlay").width() != width){
        //         width = $("#overlay").width();
        //         sizeTheOverlays();
        //     }
        // });

        // Store list of scripts in separate JSON file with id and name only.
            // Use it to populate the scripts_UI
        let scriptsMenu_init = false;
        const scriptsMenu_UI = (work)=> {

            dom_overlay.elem.classList.remove('d-none');
            dom_overlay.scriptsMenu.elem.classList.remove('d-none');

            let loading = packager.dialog.loading('Gathering scripts...');

            let scripts = null;

            let myresolve = (data)=> {
                scripts = data.items;
                if (scripts.length === 0) {
                    loading.close();
                } else {
                    displayScripts();
                }
            }

            let myreject = (data)=> {
                loading.close();
                dom_overlay.scriptsMenu.cancel.dispatchEvent(new Event('click'));
                packager.dialog.alert('An error occured while gathering scripts.');
                console.error(data.error);
            }

            packager.generalLoader.work({
                type: ITEMS_TYPE_SCRIPTS,
                packager: litePackager,
                resolve: myresolve,
                reject: myreject
            });

            const displayScripts = async ()=> {

                let lc = 0;
                for (let i = 0; i < scripts.length; ++i) {
                    let script = scripts[i];
                    await new Promise((resolve,reject)=> {
                        loadTemplate('templates/script-item.html', t => {
                            dom_overlay.scriptsMenu.scripts.appendChild(t);
                            t.dataset.scriptId = script.id; // why?

                            let sl = t.querySelector('.script-name'),
                                ss = t.querySelector('.time-meta'),
                                sd = t.querySelector('.script-delete');

                            fixButtonResponse(sl);

                            sl.innerHTML = script.id === this.script.id ? '<span class="current-script">Current script: ' : '<span>Script: ';
                            sl.innerHTML+='</span>'+script.name;
                            sl.innerHTML+='<br><span>Date created: '+new Date(script.creationTime).toLocaleString()+'</span>';

                            ss.textContent = new Date(script.saveTime).toLocaleString();

                            sl.addEventListener('click', ()=> {

                                dom_overlay.scriptsMenu.cancel.dispatchEvent(new Event('click'));

                                if (script.id !== this.script.id) {
                                    setTimeout(()=> work(script.id), 500);
                                }
                            });
                            sd.addEventListener('click', ()=> {
                                if (this.script.id === script.id) {
                                    packager.dialog.alert('Tried to delete the current script. Choose or create a different script first.');
                                    return;
                                }

                                packager.dialog.confirm('Confirm deletion of script, '+script.name, (cf)=> {
                                    if (!cf) return;

                                    let dels = CUD.new();
                                    dels.delete.scripts.add(script.id);
                                    dels = dels.drain();

                                    packager.generalPersister.work({
                                        type: ITEM_TYPE_CUD,
                                        packager: litePackager,
                                        cud: dels,
                                        reject: (data)=> {
                                            packager.dialog.alert('An error occured while deleting script, '+script.name);
                                            console.error(data.error);
                                        }
                                    });

                                    t.remove();
                                });
                            });

                            lc++;

                            if (lc === scripts.length) loading.close();

                            resolve();
                        });
                    });
                }
            }

            let close = ()=> {
                let scripts_dom = dom_overlay.scriptsMenu.scripts;
                while (scripts_dom.firstChild) scripts_dom.removeChild(scripts_dom.firstChild);
                dom_overlay.scriptsMenu.elem.classList.add('d-none');
            }

            dom_overlay.close.addEventListener('click', close, {once:true});

            if (scriptsMenu_init) return;

            scriptsMenu_init = true;

            dom_overlay.scriptsMenu.cancel.addEventListener('click', ()=> dom_overlay.close.dispatchEvent(new Event('click')));
        }

        let scenesMenu_init = false;
        const scenesMenu_UI = ()=> {
            dom_overlay.elem.classList.remove('d-none');
            dom_overlay.scenesMenu.elem.classList.remove('d-none');

            dom_overlay.scenesMenu.scriptName.textContent = this.script.name;

            for (let i = 1; i <= scenes_map.size; ++i) {
                let scene = scenes_map.get(i);

                loadTemplate('templates/scene-item.html', t => {

                    dom_overlay.scenesMenu.scenes.appendChild(t);
                    t.dataset.sceneId = scene.id;

                    let sl = t.querySelector('.scene-name'),
                        sp_form = t.querySelector('form'),
                        sp = t.querySelector('.scene-position'),
                        sd = t.querySelector('.scene-delete');

                    fixButtonResponse(sl);

                    sl.innerHTML = scene.id === this.scene.id ? '<span class="current-scene">Current Scene: ' : '<span>Scene: ';
                    sl.innerHTML+='</span>'+scene.name;
                    sp.value = scene.position;

                    let repositionScene = (pos, del)=> {

                        scenes_map.delete(scene.position);

                        if (pos < scene.position) {
                            for (let j = scene.position-1; j >= pos; --j) {
                                let s = scenes_map.get(j);
                                scenes_map.delete(j);
                                s.position = j+1;
                                scenes_map.set(s.position,s);
                                let sc = dom_overlay.scenesMenu.scenes.children[j-1];
                                sc.querySelector('.scene-position').value = s.position;
                            }
                        } else {
                            for (let j = scene.position+1; j <= pos; ++j) {
                                let s = scenes_map.get(j);
                                scenes_map.delete(j);
                                s.position = j-1;
                                scenes_map.set(s.position,s);
                                let sc = dom_overlay.scenesMenu.scenes.children[j-1];
                                sc.querySelector('.scene-position').value = s.position;
                            }
                        }

                        if (del) {
                            t.remove();
                            return;
                        }

                        sp.value = pos;
                        scene.position = pos;

                        scenes_map.set(pos,scene);

                        if (pos === scenes_map.size) {
                            dom_overlay.scenesMenu.scenes.appendChild(t);
                        } else {
                            dom_overlay.scenesMenu.scenes.insertBefore(t, dom_overlay.scenesMenu.scenes.children[pos-1]);
                        }
                    }

                    sp_form.addEventListener('submit', (e)=> {
                        e.preventDefault();

                        let pos = Math.max(1, Math.min(Number.parseInt(sp.value), scenes_map.size));
                        if (!pos || pos === scene.position) {
                            sp.value = scene.position;
                            return;
                        }

                        repositionScene(pos);
                    });

                    sl.addEventListener('click', async ()=> {
                        if (scene.id !== this.scene.id) {
                            await restScene();
                            loadScene(scene.position);
                        }

                        dom_overlay.scenesMenu.cancel.dispatchEvent(new Event('click'));
                    });
                    sd.addEventListener('click', ()=> {
                        if (this.scene.id === scene.id) {
                            packager.dialog.alert('Tried to delete the current scene. Choose or create a different scene first.');
                            return;
                        }
                        packager.dialog.confirm('Confirm deletion of scene '+scene.position+', '+scene.name, (cf)=> {
                            if (cf) {
                                repositionScene(scenes_map.size,true);
                            }
                        });
                    });
                });
            }

            if (scenesMenu_init) return;

            scenesMenu_init = true;

            dom_overlay.scenesMenu.cancel.addEventListener('click', ()=> dom_overlay.close.dispatchEvent(new Event('click')));

            dom_overlay.close.addEventListener('click', ()=> {
                let scenes = dom_overlay.scenesMenu.scenes;
                while (scenes.firstChild) scenes.removeChild(scenes.firstChild);
                dom_overlay.scenesMenu.elem.classList.add('d-none');
            });
        }

        const player_UI = ()=> {

            function secondsToHms(d) {
                d = Number(d);
                let h = Math.floor(d / 3600),
                    m = Math.floor(d % 3600 / 60),
                    s = Math.floor(d % 3600 % 60);

                return {
                    hms: [h,m,s],
                    hms_text: [h < 10 ? '0'+h : ''+h, m < 10 ? '0'+m : ''+m, s < 10 ? '0'+s : ''+s]
                }
            }

            this.simulation = new Simulation({
                temple: PUBLIC_SCHOOL,
                sound: {
                    listener: {
                        position: new Three.Vector3(0,0,0),
                        forward: new Three.Vector3(1,1,1),
                        up: new Three.Vector3(0,1,0)
                    },
                },
                vision: {
                    visualizer: {
                        container: dom_theme_controls.visualization.visualizer,
                        canvas: dom_theme_controls.visualization.canvas,
                        fullscreen: dom_visualizer.elem
                    },
                    // context: dom_theme_controls.visualization.visualizer
                }
            });

            dom_sound_player.timing.total.textContent = '--';
            dom_sound_player.travel.total.textContent = '--';

            this.simulation.addEventListener(SIMULATION_EVENT_PROGRESS, (e)=> {
                let time = e.message.time,
                    travel = e.message.travel,
                    total = e.message.total,
                    frame = e.message.frame;
                let hms = secondsToHms(time).hms_text;
                dom_sound_player.timing.elapsed.textContent = hms[0]+':'+hms[1]+':'+hms[2];
                dom_sound_player.travel.elapsed.textContent = travel.toFixedNumber(3);
                hms = secondsToHms(total.time).hms_text;
                dom_sound_player.timing.total.textContent = hms[0]+':'+hms[1]+':'+hms[2];
                dom_sound_player.travel.total.textContent = total.travel;
                dom_sound_player.cycle.textContent = total.cycle;

                if (frame) {

                }
            });

            this.simulation.addEventListener(SIMULATION_EVENT_PLAY, (e)=> {
                if (!this.simulation.playing) return;
                dom_sound_player.play.textContent = 'Pause';
            });

            this.simulation.addEventListener(SIMULATION_EVENT_PAUSE, (e)=> {
                dom_sound_player.play.textContent = 'Play';
            });

            this.simulation.addEventListener(SIMULATION_EVENT_STOP, (e)=> {
                dom_sound_player.play.textContent = 'Play';
                // let hms = secondsToHms(this.simulation.total.time).hms_text;
                // dom_sound_player.timing.total.textContent = hms[0]+':'+hms[1]+':'+hms[2];
                dom_sound_player.timing.elapsed.textContent = '00:00:00';
                dom_sound_player.travel.elapsed.textContent = '0';
                dom_sound_player.timing.total.textContent = '--';
                dom_sound_player.travel.total.textContent = '--';
                dom_sound_player.cycle.textContent = '0';

                // TODO: reset mixer UI (mute, solo)
            });

            this.simulation.addEventListener(SIMULATION_EVENT_RECORD_START, (e)=> {
                dom_sound_player.record.classList.add('alt-on');
                dom_sound_player.record.textContent = 'Recording';
            });

            this.simulation.addEventListener(SIMULATION_EVENT_RECORD_END, (e)=> {
                dom_sound_player.record.classList.remove('alt-on');
                dom_sound_player.record.textContent = 'Record';
            });

            $(dom_sound_player.intensityRange).on('input change', ()=> {
                let val = Number(dom_sound_player.intensityRange.value);
                this.simulation.volume(val);
                dom_player_controls.intensityRange.value = val;
                dom_player_controls.intensity.value = val;
            });

            dom_sound_player.play.addEventListener('click', ()=> {
                if (this.simulation.playing) {
                    this.simulation.pause().catch((err)=> {
                        console.error(err);
                    });
                } else {
                    if (this.simulation.time() == null) {
                        dom_player_controls.frequency.dispatchEvent(new Event('change'));
                        dom_player_controls.intensity.dispatchEvent(new Event('change'));
                        dom_player_controls.duration.dispatchEvent(new Event('change'));
                    }
                    let finishPlay = (ql)=> {
						packager.generalPersister.work({
							type: ITEM_TYPE_CUD,
							packager: litePackager,
							cud: CUD.partialDrain(null,['shells','themes','aspects']), // saving just shells would be enough
							reject: (data)=> {
								console.error(e.data.error);
							},
							resolve: ()=> {
								this.simulation.resume().then(()=> {
									ql('').close();
								}).catch((err)=> {
									console.error(err);
								});
							}
						});
                    }
                    let msg;
                    let ql = packager.dialog.quietLoading;
                    if (this.simulation.time() == null) {
                        msg = 'Preparing journey...';
                        ql(msg);
                        let sot = this.script.school;
                        if (sot && (sot !== PUBLIC_SCHOOL)) {
                            loadProcess(sot).then(()=> {
                                let obj = {};
                                new Function('data', sot.raw).bind(obj)(SOT_SCRIPT_INFO);
                                if (obj.getJourney) sot.getJourney = obj.getJourney;
                                if (obj.getFrequency) sot.getFrequency = obj.getFrequency;
                                if (obj.getPosition) sot.getPosition = obj.getPosition;
                                if (obj.waveform) sot.waveform = obj.waveform;
                                if (obj.characterLength != undefined) sot.characterLength = obj.characterLength;
                                this.simulation.curriculum = sot;
                                finishPlay(ql);
                            }).catch((err)=> {
                                this.simulation.curriculum = PUBLIC_SCHOOL;
                                console.error(err);
                                finishPlay(ql);
                            });
                        } else {
                            finishPlay(ql);
                        }
                    } else {
                        msg = 'Resuming journey...';
                        finishPlay(ql);
                    }

                }
            });

            dom_sound_player.visualize.addEventListener('click', ()=> {
                this.simulation.visualizer.on();
                // if (!this.simulation.playing) this.simulation.resume();
                let theme = themes_map.get_(dom_theme_controls.elem.dataset.themeId);
                if (theme === this.simulation.voice()) {
                    dom_theme_controls.visualization.visualize.classList.add('on');
                    dom_theme_controls.visualization.visualizer.scrollIntoView({block:'center',behavior:SCROLL_BEHAVIOR});
                }
            });

            dom_sound_player.stop.addEventListener('click', (e)=> {
                this.simulation.stop().then((info)=> {
                    if (!info) return;
                    let {file,name} = info;
                    download(file, name+'.wav', 'audio/wav');
                }).catch((err)=> {
                    console.error(err)
                });
            });
            dom_sound_player.persist.addEventListener('click', ()=> {
                if(this.simulation.persist()) {
                    dom_sound_player.persist.classList.add('off');
                } else {
                    dom_sound_player.persist.classList.remove('off');
                }
                this.simulation.persist(!this.simulation.persist());
            });
            dom_sound_player.autoplay.addEventListener('click', ()=> {
                if(this.simulation.autoplay()) {
                    dom_sound_player.autoplay.classList.add('off');
                } else {
                    dom_sound_player.autoplay.classList.remove('off');
                }
                this.simulation.autoplay(!this.simulation.autoplay());
            });
            dom_sound_player.record.addEventListener('click', ()=> {
                this.simulation.record().then((info)=> {
                    if (!info) return;
                    let {file,name} = info;
                    download(file, name+'.wav', 'audio/wav');
                }).catch((err)=> {
                    console.error(err)
                });
            });
            dom_sound_player.download.addEventListener('click', ()=> {
                packager.dialog.prompt({
                    message: 'Download: How many seconds? (default = 9)',
                    callback: (secs)=> {
                        secs = Number.parseFloat(secs);
                        if (!secs) secs = 9;
                        let loading = packager.dialog.loading('Rendering...');
                        this.simulation.download(secs).then(({buffer, name})=> {
                            let worker = new Worker(packager.WAVE_WORKER_PATH);
                            worker.postMessage({
                                pcmArrays: [ buffer.getChannelData(0), buffer.getChannelData(1) ],
                                config: {
                                    sampleRate: buffer.sampleRate
                                }
                            });
                            worker.onmessage = (e)=> {
                                let file = new Blob([e.data.buffer], {type:"audio/wav"});
                                download(file,name+'.wav','audio/wav');
                                loading.close();
                            }
                        }).catch((err)=> {
                            loading.close();
                            packager.dialog.alert('An error occured while downloading.');
                            console.error(err)
                        });
                    }
                });
            });

            // In controls panel
            $(dom_player_controls.frequency).on('change', (e)=> {
                let val = Math.abs( Number.parseFloat(dom_player_controls.frequency.value.trim()) ) || SOUND_MIN_FREQUENCY;
                dom_player_controls.frequency.value = val;
                this.script.player.frequency = val;
                dom_player_controls.frequency.blur();
            });
            $(dom_player_controls.intensity).on('change', (e)=> { // NOTE: Player intensity is always in [0,1];
                let val = Math.abs( Number.parseFloat(dom_player_controls.intensity.value.trim()) );
                if (isNaN(val)) val = 1;
                dom_player_controls.intensity.value = val;
                dom_player_controls.intensityRange.value = val;
                dom_sound_player.intensityRange.value = val;
                this.script.player.intensity = val;
                dom_player_controls.intensity.blur();
            });
            $(dom_player_controls.intensityRange).on('input change', (e)=> {
                let val = Number.parseFloat(dom_player_controls.intensityRange.value);
                dom_player_controls.intensity.value = val;
                dom_sound_player.intensityRange.value = val;
                this.simulation.volume(val);
                if (e.type === 'change') {
                    this.script.player.intensity = val;
                }
            });
            $(dom_player_controls.duration).on('change', (e)=> {
                let val = Number.parseFloat(dom_player_controls.duration.value.trim()) || SOUND_DURATION_DEFAULT;
                dom_player_controls.duration.value = val;
                this.script.player.duration = val;
                dom_player_controls.duration.blur();
            });
        }
        player_UI();

        let keyboardShortcuts_init = false;
        const keyboardShortcuts_UI = ()=> {
            dom_overlay.elem.classList.remove('d-none');
            dom_overlay.keyboardShortcuts.elem.classList.remove('d-none');

            if (keyboardShortcuts_init) return;
            keyboardShortcuts_init = true;

            let kbss = {
                create: {
                    heading: 'Create',
                    shortcuts: [
                        {function:'Create aspects',mac:'cmd+shift+a',windows:'ctrl+shift+a'},
                        {function:'Create shell',mac:'cmd+shift+s',windows:'ctrl+shift+s'},
                        {function:'Create entities',mac:'cmd+shift+e',windows:'ctrl+shift+e'},
                        {function:'Create line (append to current line)',mac:'cmd+l',windows:'ctrl+l'},
                        {function:'Create line (prepend to current line)',mac:'cmd+shift+l',windows:'ctrl+shift+l'},
                        {function:'New scene',mac:'cmd+n',windows:'ctrl+n'},
                        {function:'New script',mac:'cmd+shift+n',windows:'ctrl+shift+n'}
                    ]
                },
                browse: {
                    heading: 'Browse',
                    shortcuts: [
                        {function:'Browse themes in a line',mac:'cmd+left/right OR tab/shift+tab',windows:'ctrl+left/right OR tab/shift+tab'},
                        {function:'Browse lines',mac:'cmd+up/down',windows:'ctrl+up/down'},
                        {function:'Position theme left/right',mac:'cmd+shift+left/right',windows:'ctrl+shift+left/right'},
                        {function:'Position line up/down', mac:'cmd+shift+up/down',windows:'ctrl+shift+up/down'},
                        {function:'Highlight aspect in theme',mac:'1-6',windows:'1-6'},
                        {function:'Select source aspect',mac:'cmd+(1-6)',windows:'ctrl+(1-6)'},
                        {function:'Select destination aspect',mac:'cmd+shift+(1-6)',windows:'ctrl+shift+(1-6)'},
                        {function:'Select shell of a theme',mac:'s',windows:'s'},
                        {function:'Select mod-quad of a line',mac:'m',windows:'m'},
                        {function:'Select entity of a line',mac:'e',windows:'e'}
                    ]
                },
                manipulation: {
                    heading: 'Copy/Paste/Set',
                    shortcuts: [
                        {function:'Increase aspect/node value',mac:'cmd+=',windows:'ctrl+='},
                        {function:'Reduce aspect/edge value',mac:'cmd+-',windows:'ctrl+-'},
                        {function:'Set step value for aspect/edge change',mac:'shift+s',windows:'shift+s'},
                        {function:'Set arrow to other aspect (start aspect must be highlighted)',mac:'opt+(1-6) (<a href="#arrows" class="tosoto-standard-link2">arrow</a>)',windows:'alt+(1-6) (<a href="#arrows" class="tosoto-standard-link2">arrow</a>)'},
                        {function:'Edit entity\'s name (of current line)',mac:'shift+e',windows:'shift+e'},
                        {function:'Copy theme (theme or aspect must be active)',mac:'cmd+c t',windows:'ctrl+c t'},
                        {function:'Copy entity (line or entity must be acttive)',mac:'cmd+c e',windows:'ctrl+c e'},
                        {function:'Copy shell (shell, aspect, or theme must be active)',mac:'cmd+c s',windows:'ctrl+c s'},
                        {function:'Create theme from copied theme',mac:'cmd+v t',windows:'ctrl+v t'},
                        {function:'Create Context theme from copied shell',mac:'cmd+opt+c',windows:'ctrl+alt+c'},
                        {function:'Create Signal theme from copied shell',mac:'cmd+opt+s',windows:'ctrl+alt+s'},
                        {function:'Create Versus theme from copied shell',mac:'cmd+opt+v',windows:'ctrl+alt+v'},
                        {function:'Create Text theme',mac:'cmd+opt+t',windows:'ctrl+alt+t'},
                        {function:'Set copied entity for line',mac:'cmd+v e',windows:'ctrl+v e'},
                    ]
                },
                arrows: {
                    heading: 'Arrows',
                    id: 'arrows',
                    shortcuts: [
                        {function:'Context arrow similarity',mac:'-',windows:'-'},
                        {function:'Context arrow causation',mac:'shift+.',windows:'shift+.'},
                        {function:'Context arrow dependence/foundation',mac:'shift+\\',windows:'shift+\\'},
                        {function:'Context arrow spectrum/scale',mac:']',windows:']'}
                    ]
                },
                misc: {
                    heading: 'Miscellaneous',
                    shortcuts: [
                        {function:'Go to line',mac:'cmd+g',windows:'ctrl+g'},
                        {function:'Toggle control panel',mac:'cmd+\\',windows:'ctrl+\\'},
                        {function:'Save',mac:'cmd+s',windows:'ctrl+s'},
                        {function:'Load',mac:'cmd+o',windows:'ctrl+o'},
                        {function:'Show keyboard shortcuts',mac:'?',windows:'?'},
                        {function:'Close window',mac:'cmd+w',windows:'ctrl+w'}
                    ]
                }
            }

            let dl = dom_overlay.keyboardShortcuts.listings;
            for (let k in kbss) {
                let l = kbss[k],
                    h = document.createElement('h3'),
                    funcs = document.createElement('h5'),
                    mac = document.createElement('h5'),
                    windows = document.createElement('h5');
                h.classList.add('col-12');
                h.innerHTML = l.heading;
                if (l.id) h.id = l.id;
                dl.appendChild(h);
                funcs.innerHTML = 'Function';
                mac.innerHTML = 'Mac';
                windows.innerHTML = 'Windows';
                [funcs,mac,windows].map (t => { t.classList.add('col-4'); dl.appendChild(t); });
                for (let i = 0; i < l.shortcuts.length; ++i) {
                    let sct = l.shortcuts[i],
                        f = document.createElement('p'),
                        m = document.createElement('code'),
                        w = document.createElement('code');
                    f.innerHTML = sct.function;
                    m.innerHTML = sct.mac;
                    w.innerHTML = sct.windows;
                    [f,m,w].map(t => { t.classList.add('col-4'); dl.appendChild(t); });
                    dl.appendChild(document.createElement('hr'));
                }
            }

            dom_overlay.keyboardShortcuts.cancel.addEventListener('click', ()=> dom_overlay.close.dispatchEvent(new Event('click')));
            dom_overlay.close.addEventListener('click', ()=> {
                dom_overlay.keyboardShortcuts.elem.classList.add('d-none');
            });
        }

        let caui_asps_sel = null;
        const createAspects_UI = ()=> {

            dom_overlay.elem.classList.remove('d-none');
            dom_overlay.create.aspects.elem.classList.remove('d-none');

            if (caui_asps_sel) {
                caui_asps_sel[0].selectize.focus();
                return;
            }

            caui_asps_sel = $(dom_overlay.create.aspects.elem.querySelector('textarea')).selectize({
                // selectOnTab: true,
                delimiter: ',',
                plugins: ['remove_button','restore_on_backspace'],
                persist: false,
                create: true,
                createFilter: (input)=> {
                    if(input.length > 0 && input.length <= CHAR_MAX_LENGTH) {
                        return true;
                    }
                    packager.dialog.alert('An aspect\'s name must be between 1 and 255 characters.');
                    return false;
                },
                render: {
                    item: (data, escape)=> {
                        return '<div class="create-aspect-item">' + escape(data.text) + '</div>';
                    }
                }
            });

            caui_asps_sel[0].selectize.focus();

            $(document).on('click', '.create-aspect-item', (e)=> {
                // TODO
                // console.log('TODO: play frequency of ', e.target.dataset.value);
            });

            dom_overlay.create.aspects.cancel.addEventListener('click', ()=> dom_overlay.close.dispatchEvent(new Event('click')));

            dom_overlay.create.aspects.submit.addEventListener('click', ()=> {
                let loading = packager.dialog.loading('Creating aspects...');
                let aspns = caui_asps_sel[0].selectize.items;
                createAspects(aspns);
                loading.close();
                dom_overlay.close.dispatchEvent(new Event('click'));
            });

            dom_overlay.close.addEventListener('click', ()=> {
                for (let i = 0; i < caui_asps_sel.length; ++i) {
                    caui_asps_sel[i].selectize.clear();
                    caui_asps_sel[i].selectize.clearOptions();
                    caui_asps_sel[i].selectize.clearCache();
                }

                dom_overlay.create.aspects.elem.classList.add('d-none');
            });
        }

        let csui_aspects = null,
            csui_shell_display = null,
            csui_theme_display = null;
        // Allow new aspects to be made here as well.
        const createShell_UI = ()=> {

            dom_overlay.elem.classList.remove('d-none');
            dom_overlay.create.shell.elem.classList.remove('d-none');

            dom_overlay.create.shell.naming.focus();

            packager.generalPersister.work({
                type: ITEM_TYPE_CUD,
                packager: litePackager,
                cud: CUD.partialDrain(null,['themes','aspects']),
                reject: (data)=> {
                    console.error(e.data.error);
                }
            });

            if (csui_aspects) return;

            csui_aspects = [];
            let numAspects = 0;
            let new_asps = new Set();
            dom_overlay.create.shell.elem.querySelectorAll('.aspect select').forEach(select => {
                let selectId = numAspects++,
                    aspect = $(select).selectize({
                        // selectOnTab: true,
                        searchField: ['name'],
                        labelField: 'name',
                        valueField: 'id',
                        sortField: {
            				field: 'name',
            				direction: 'asc'
            			},
                        // persist: false,
                        create: (name) => {
                            let masp = new M.Aspect({
                                    id: guid(),
                                    name: name,
                                    abbr: name.substring(0,2),
                                    type: M.ASPECT_TYPE_PURE,
                                    frequencies: [0,0,0]
                                });
                            aspects_map.set(masp.id,new Aspect(masp));
                            new_asps.add(masp.id);
                            return masp;
                        },
                        createFilter: (input)=> {
                            if(input.length > 0 && input.length <= CHAR_MAX_LENGTH) {
                                return true;
                            }
                            packager.dialog.alert('An aspect\'s name must be between 1 and 255 characters.');
                            return false;
                        },
                        render: {
                            option: (asp,escape)=> {
                                let it_m = null;
                                if (asp.type === M.ASPECT_TYPE_COMPOUND) {
                                    asp.name = asp.theme.name;
                                    it_m = themes_map.get_(asp.id);
                                    if (it_m) asp.name = it_m._theme.name(); // use most current name
                                } else {
                                    it_m = aspects_map.get_(asp.id);
                                    if (it_m) asp.name = it_m._aspect.name || asp.name;
                                }
                                return  '<div class="select-dropdown-option">'+
                                            '<ul>'+
                                                '<li class="name">'+(new_asps.has(asp.id) ? 'NEW! ' : '')+asp.name+'</li>'+
                                                '<li class="id">'+asp.id+'</li>'+
                                                '<li class="meta">'+(asp.type === M.ASPECT_TYPE_PURE ? '*Pure* ~'+asp.frequencies[0]+'~' : '{Theme - '+asp.depth+' x '+asp.width+'}')+'</li>'+
                                            '</ul>'+
                                        '</div>';
                            }
                        },
                        load: (query,callback)=> {
                            // Use opt groups to separate aspects and themes
                            if (!query.length) return callback();

                            packager.generalLoader.work({
                                type: ITEMS_TYPE_ASPECTS,
                                packager: litePackager,
                                query: {
                                    limit: 30,
                                    comparators: [
                                        {
                                            key: 'name',
                                            predicate: 'contains',
                                            ignoreCase: true,
                                            value: query
                                        }
                                    ],
                                    // A resolution replaces result from store with and object contaning
                                    // values of the given `donationKeys` from a donor of the `donorType`
                                    // found using an optional `donorKey` or the value of the instigator's
                                    // `key` (an array may be resolved into an array of donations). This is
                                    // the case if `subjectKey` is null and an array is stored at `key`. If
                                    // the `subjectKey` is null, then the `key` value will be used to find
                                    // the donor with the same value at the same `key`; in this case, `key`
                                    // must point to a string value (for matching). In the case that more
                                    // than one donor is found, the first is chosen.
                                    // If an array is found at `key`, then the value of the donor at `donorKey`
                                    // must be compared to the corresponding value in the array. Otherwise, a
                                    // `subjectKey` must be provided. If `donorKey` is null, then the `subjectKey`
                                    // is used in its place, so one of them must be an actual value;
                                    resolutions: [
                                        {
                                            subjectKey: 'id', // must be null or a key to string
                                            key: 'name', // must be a key to string or array value
                                            newKey: 'theme',
                                            donorKey: 'id', // must be null or a key to string
                                            donorType: 'themes',
                                            donationKeys: ['name', 'width', 'depth'],
                                        }
                                    ]
                                },
                                // persist: CUD.partialDrain(null,['aspects']), // done when shell creation is intitiated
                                resolve: (data)=> {
                                    callback(data.items);
                                },
                                reject: (err)=> {
                                    console.error('An error occured while loading entities.');
                                    console.error(err);
                                    callback();
                                }
                            });
                        },
                        onChange: async (v)=> {
                            if (!v) return;
                            let aspect = await aspects_map.get(v);
                            let abbr = aspect._aspect.abbr();
                            for (let i = 0; i < csui_aspects.length; ++i) {
                                if (i === selectId) continue;
                                if (csui_aspects[i].selectize.items[0] === csui_aspects[selectId].selectize.items[0]) {
                                    abbr = dom_overlay.create.shell[n_ss[i]].abbr.value;
                                    break;
                                }
                            }
                            dom_overlay.create.shell[n_ss[selectId]].abbr.value = abbr;
                        }
                    });
                csui_aspects.push(aspect[0]);

                $(dom_overlay.create.shell[n_ss[selectId]].abbr).on('change', async (e)=> {
                    let csui_asp = csui_aspects[selectId].selectize;
                    if (!csui_asp.items[0]) return;
                    let this_abbr = dom_overlay.create.shell[n_ss[selectId]].abbr;
                    let val = this_abbr.value.trim();
                    if (!val) {
                        val = (await aspects_map.get(csui_asp.items[0]))._aspect.abbr();
                        this_abbr.value = val;
                    }
                    for (let i = 0; i < csui_aspects.length; ++i) {
                        if (i === selectId) continue;
                        if (csui_aspects[i].selectize.items[0] === csui_aspects[selectId].selectize.items[0]) {
                            dom_overlay.create.shell[n_ss[i]].abbr.value = val;
                        }
                    }
                    this_abbr.blur();
                });
            });

            csui_shell_display = $(dom_overlay.create.shell.displayType).selectize({
                // selectOnTab: true,
                // persist: false,
                create: false,
                onChange: (v)=> {
                    // TODO:  send to Memnat for redrawing,
                        // possibly changing the display type
                    let dt = Number.parseInt(v);
                    if (!dt) dt = null;
                    if (csui_theme_display && dt === csui_theme_display.type) return;
                    switch (dt) {
                        case M.THEME_TYPE_CONTEXT:
                            // draw Context
                            break;
                        case M.THEME_TYPE_SIGNAL:
                            // draw Signal
                            break;
                        case M.THEME_TYPE_VERSUS:
                            // draw Versus
                            break;
                        default:
                            // TODO: remove theme drawing
                            csui_theme_display = null;
                    }
                    if (csui_theme_display) csui_theme_display.type = dt;
                }
            });

            dom_overlay.create.shell.cancel.addEventListener('click', ()=> dom_overlay.close.dispatchEvent(new Event('click')));

            dom_overlay.create.shell.submit.addEventListener('click', async ()=> {
                let loading = packager.dialog.loading('Creating shell...');
                let shell_name = dom_overlay.create.shell.naming.value.trim(),
                    asp_vs = [],
                    asp_abbrs = [],
                    valid = true;

                if (!shell_name) valid = false;

                for (let i = 0; i < csui_aspects.length; ++i) {
                    let tag = n_ss[i];
                    let asp_v = csui_aspects[i].selectize.items[0],
                        asp_abbr = dom_overlay.create.shell[tag].abbr.value.trim();
                    if (!asp_v || !asp_abbr) valid = false;
                    asp_vs.push(asp_v);
                    asp_abbrs.push(asp_abbr);
                }
                if (!valid) {
                    loading.close();
                    packager.dialog.alert('All fields require valid input.');
                    return;
                }

                // Only pure M.Aspects may have their names changed.

                // If an M.Aspect has the ASPECT_TYPE_COMPOUND type value,
                // it's a Theme. Check if it exists in aspects_map under the
                // Theme id and create it as an Aspect if necessary.

                let aspIds = [],
                    upd_abbrs = {};
                for (let i = 0; i < asp_vs.length; ++i) {
                    let id = asp_vs[i],
                        abbr = asp_abbrs[i],
                        asp = await aspects_map.get(id);
                    upd_abbrs[id] = abbr;
                    aspIds.push(id);
                }

                let abbrs_ks = Object.keys(upd_abbrs);
                if (abbrs_ks.length > 0) {
                    loading.message('Updating aspect abbreviations...');
                    for (let id in upd_abbrs) {
                        let abbr = upd_abbrs[id];
                        (await aspects_map.get(id))._aspect.abbr(abbr);

                        // Update the aspect in shell controls if present
                        let shell = await shells_map.get(dom_shell_controls.elem.dataset.shellId);
                        if (shell) {
                            let aid = shell.aspectIds[dom_aspect_controls.elem.dataset.aspectIndex];
                            if (aid === id) dom_aspect_controls.abbr.value = abbr;
                        }
                    }
                    loading.close();
                }

                createShells([[shell_name, aspIds]]);

                loading.close();
                dom_overlay.close.dispatchEvent(new Event('click'));
            });

            dom_overlay.close.addEventListener('click', ()=> {

                dom_overlay.create.shell.naming.value = '';

                for (let i = 0; i < csui_aspects.length; ++i) {
                    csui_aspects[i].selectize.clear();
                    csui_aspects[i].selectize.clearOptions();
                    csui_aspects[i].selectize.clearCache();

                    dom_overlay.create.shell[n_ss[i]].abbr.value = '';
                }
                for (let i = 0; i < csui_shell_display.length; ++i) {
                    csui_shell_display[i].selectize.clear();
                    csui_shell_display[i].selectize.clearCache();
                }

                dom_overlay.create.shell.elem.classList.add('d-none');

                new_asps.clear();
            });
        }

        let ceui_ents_sel = null;
        const createEntities_UI = ()=> {

            dom_overlay.elem.classList.remove('d-none');
            dom_overlay.create.entities.elem.classList.remove('d-none');

            if (ceui_ents_sel) {
                ceui_ents_sel[0].selectize.focus();
                return;
            }

            ceui_ents_sel = $(dom_overlay.create.entities.textarea).selectize({
                // selectOnTab: true,
                delimiter: ',',
                plugins: ['remove_button','restore_on_backspace'],
                persist: false,
                create: true,
                createFilter: (input)=> {
                    if(input.length > 0 && input.length <= CHAR_MAX_LENGTH) {
                        return true;
                    }
                    packager.dialog.alert('An entity\'s name must be between 1 and 255 characters.');
                    return false;
                },
                render: {
                    item: (data, escape)=> {
                        return '<div>' + escape(data.text) + '</div>';
                    }
                }
            });

            ceui_ents_sel[0].selectize.focus();

            dom_overlay.create.entities.cancel.addEventListener('click', ()=> dom_overlay.close.dispatchEvent(new Event('click')));

            dom_overlay.create.entities.submit.addEventListener('click', ()=> {
                let loading = packager.dialog.loading('Creating entities...');
                let entns = ceui_ents_sel[0].selectize.items;

                createEntities(entns);

                loading.close();
                dom_overlay.close.dispatchEvent(new Event('click'));
            });

            dom_overlay.close.addEventListener('click', ()=> {
                for (let i = 0; i < ceui_ents_sel.length; ++i) {
                    ceui_ents_sel[i].selectize.clear();
                    ceui_ents_sel[i].selectize.clearOptions();
                    ceui_ents_sel[i].selectize.clearCache();
                }

                dom_overlay.create.entities.elem.classList.add('d-none');
            });
        }

        let cpui_procs_sel = null;
        const createProcesses_UI = ()=> {

            dom_overlay.elem.classList.remove('d-none');
            dom_overlay.create.processes.elem.classList.remove('d-none');

            if (cpui_procs_sel) {
                cpui_procs_sel[0].selectize.focus();
                return;
            }

            cpui_procs_sel = $(dom_overlay.create.processes.textarea).selectize({
                // selectOnTab: true,
                delimiter: ',',
                plugins: ['remove_button','restore_on_backspace'],
                persist: false,
                create: true,
                createFilter: (input)=> {
                    if(input.length > 0 && input.length <= CHAR_MAX_LENGTH) {
                        return true;
                    }
                    packager.dialog.alert('A process\' name must be between 1 and 255 characters.');
                    return false;
                },
                render: {
                    item: (data, escape)=> {
                        return '<div>' + escape(data.text) + '</div>';
                    }
                }
            });

            cpui_procs_sel[0].selectize.focus();

            dom_overlay.create.processes.cancel.addEventListener('click', ()=> dom_overlay.close.dispatchEvent(new Event('click')));

            dom_overlay.create.processes.submit.addEventListener('click', ()=> {
                let loading = packager.dialog.loading('Creating processes...');
                let procns = cpui_procs_sel[0].selectize.items;

                createProcesses(procns);

                loading.close();
                dom_overlay.close.dispatchEvent(new Event('click'));
            });

            dom_overlay.close.addEventListener('click', ()=> {
                for (let i = 0; i < cpui_procs_sel.length; ++i) {
                    cpui_procs_sel[i].selectize.clear();
                    cpui_procs_sel[i].selectize.clearOptions();
                    cpui_procs_sel[i].selectize.clearCache();
                }

                dom_overlay.create.processes.elem.classList.add('d-none');
            });
        }

        let csui_sots_sel = null;
        const createSOTs_UI = ()=> {

            dom_overlay.elem.classList.remove('d-none');
            dom_overlay.create.sots.elem.classList.remove('d-none');

            if (csui_sots_sel) {
                csui_sots_sel[0].selectize.focus();
                return;
            }

            csui_sots_sel = $(dom_overlay.create.sots.textarea).selectize({
                // selectOnTab: true,
                delimiter: ',',
                plugins: ['remove_button','restore_on_backspace'],
                persist: false,
                create: true,
                createFilter: (input)=> {
                    if(input.length > 0 && input.length <= CHAR_MAX_LENGTH) {
                        return true;
                    }
                    packager.dialog.alert('An SOT\'s name must be between 1 and 255 characters.');
                    return false;
                },
                render: {
                    item: (data, escape)=> {
                        return '<div>' + escape(data.text) + '</div>';
                    }
                }
            });

            csui_sots_sel[0].selectize.focus();

            dom_overlay.create.sots.cancel.addEventListener('click', ()=> dom_overlay.close.dispatchEvent(new Event('click')));

            dom_overlay.create.sots.submit.addEventListener('click', ()=> {
                let loading = packager.dialog.loading('Creating SOTs...');
                let sotns = csui_sots_sel[0].selectize.items;

                createSOTs(sotns);

                loading.close();
                dom_overlay.close.dispatchEvent(new Event('click'));
            });

            dom_overlay.close.addEventListener('click', ()=> {
                for (let i = 0; i < csui_sots_sel.length; ++i) {
                    csui_sots_sel[i].selectize.clear();
                    csui_sots_sel[i].selectize.clearOptions();
                    csui_sots_sel[i].selectize.clearCache();
                }

                dom_overlay.create.sots.elem.classList.add('d-none');
            });
        }

        let atui_theme_type = null,
            atui_shell = null,
            atui_theme_display = null,
            atui_line = null;
        const addTheme_UI = (line)=> {
            // The UI/UX flow for adding a theme. From here,
            // addTheme will be called.

            packager.generalPersister.work({
                type: ITEM_TYPE_CUD,
                packager: litePackager,
                cud: CUD.partialDrain(null,['shells','themes','aspects']), // saving just shells would be enough
                reject: (data)=> {
                    console.error(e.data.error);
                }
            });

            if (line.themes.length > 0 && line.themes[0]._theme.type === M.THEME_TYPE_TEXT) {
                packager.dialog.alert('You tried to add a theme on the same line as a textual theme. A textual theme requires a line all to itself. Create a new line and/or delete this line.');
                return;
            }

            dom_overlay.elem.classList.remove('d-none');
            dom_overlay.create.theme.elem.classList.remove('d-none');

            atui_line = line;

            dom_overlay.create.theme.naming.focus();

            if (atui_theme_type) return;

            let dt = null;

            atui_theme_type = $(dom_overlay.create.theme.elem.querySelector('select.theme-type')).selectize({
                // selectOnTab: true,
                // persist: false,
                create: false,
                options: [
                    {name:'Signal', value:M.THEME_TYPE_SIGNAL},
                    {name:'Versus', value:M.THEME_TYPE_VERSUS},
                    {name:'Context', value:M.THEME_TYPE_CONTEXT},
                    {name:'Text', value:M.THEME_TYPE_TEXT}
                ],
                valueField: 'value',
                labelField: 'name',
                searchField: 'name',
                sortField: {
    				field: 'value',
    				direction: 'asc'
    			},
                onChange: (v)=> {
                    // TODO:  send to Memnat for redrawing,
                        // possibly changing the display type
                    dt = Number.parseInt(v);
                    if (!dt) dt = null;
                    if (atui_theme_display && dt === atui_theme_display.type) return;
                    if (dt === M.THEME_TYPE_TEXT) {
                        atui_shell[0].selectize.clear();
                        atui_shell[0].selectize.disable();
                    } else {
                        atui_shell[0].selectize.enable();
                    }
                    switch (dt) {
                        case M.THEME_TYPE_CONTEXT:
                            // draw Context
                            break;
                        case M.THEME_TYPE_SIGNAL:
                            // draw Signal
                            break;
                        case M.THEME_TYPE_VERSUS:
                            // draw Versus
                            break;
                        default:
                            // TODO: remove theme drawing
                            atui_theme_display = null;
                    }
                    if (atui_theme_display) atui_theme_display.type = dt;
                }
            });

            atui_shell = $(dom_overlay.create.theme.elem.querySelectorAll('select.shell')).selectize({
                // selectOnTab: true,
                searchField: ['name'],
                labelField: 'name',
                valueField: 'id',
                sortField: {
    				field: 'name',
    				direction: 'asc'
    			},
                // persist: false,
                create: false,
                onChange: (v)=> {
                    if (!v || !atui_theme_display) return;
                    // let aspects = shells_map.get(v).aspectIds.map(id => aspects_map.get(id));
                    // TODO: redraw theme display with new aspects
                },
                render: {
                    option: (shell,escape)=> {
                        let it_m = shells_map.get_(shell.id);
                        if (it_m) shell.name = it_m.name();
                        let abbrs = shell.aspects.map(asp => {
                            it_m = aspects_map.get_(asp.id);
                            if (it_m) asp.abbr = it_m._aspect.abbr();
                            return asp.abbr;
                        });
                        return  '<div class="select-dropdown-option">'+
                                    '<ul>'+
                                        '<li class="name">'+shell.name+'</li>'+
                                        '<li class="id">'+shell.id+'</li>'+
                                        '<li class="meta">'+abbrs+'</li>'+
                                    '</ul>'+
                                '</div>';
                    }
                },
                load: (query,callback)=> {
                    if (!query.length) return callback();

                    packager.generalLoader.work({
                        type: ITEMS_TYPE_SHELLS,
                        packager: litePackager,
                        query: {
                            limit: 30,
                            comparators: [
                                {
                                    key: 'name',
                                    predicate: 'contains',
                                    ignoreCase: true,
                                    value: query
                                }
                            ],
                            resolutions: [
                                {
                                    key: 'aspectIds',
                                    newKey: 'aspects',
                                    donorKey: 'id',
                                    donorType: 'aspects',
                                    donationKeys: ['id','abbr'],
                                    enforce: true
                                }
                            ]
                        },
                        // persist: CUD.partialDrain(null,[ITEMS_TYPE_SHELLS]), // done when theme creation is initiated
                        resolve: (data)=> {
                            // TODO: put back items on persist failure if data.putBack
                            callback(data.items);
                        },
                        reject: (err)=> {
                            console.error('An error occured while loading shells.');
                            console.error(err);
                            callback();
                        }
                    });
                }
            });

            dom_overlay.create.theme.cancel.addEventListener('click', ()=> dom_overlay.close.dispatchEvent(new Event('click')));

            dom_overlay.create.theme.submit.addEventListener('click', ()=> {

                let loading = packager.dialog.loading('Creating theme...');

                let name = dom_overlay.create.theme.naming.value.trim(),
                    shell_id = atui_shell[0].selectize.items[0],
                    valid = true;

                if (!name || !dt || (!shell_id && dt !== M.THEME_TYPE_TEXT)) valid = false;
                if (!valid) {
                    loading.close();
                    packager.dialog.alert('All fields require valid input.');
                    return;
                }

                let props = {
                    name: name,
                    line: atui_line,
                    shellId: shell_id,
                    type: dt
                }

                if (dt) {
                    addTheme(props);
                } else {
                    packager.dialog.alert('Error: Strange theme type selected.');
                }

                loading.close();
                dom_overlay.close.dispatchEvent(new Event('click'));
            });

            dom_overlay.close.addEventListener('click', ()=> {

                dom_overlay.create.theme.naming.value = '';

                for (let i = 0; i < atui_theme_type.length; ++i) {
                    atui_theme_type[i].selectize.clear();
                    atui_theme_type[i].selectize.clearCache();
                }

                for (let i = 0; i < atui_shell.length; ++i) {
                    atui_shell[i].selectize.clear();
                    atui_shell[i].selectize.clearCache();
                }

                dom_overlay.create.theme.elem.classList.add('d-none');
            });
        }

        let browseThemes_init = false;
        const browseThemes_UI = ()=> {

            dom_overlay.elem.classList.remove('d-none');
            dom_overlay.themesMenu.elem.classList.remove('d-none');

            let loading = packager.dialog.loading('Gathering themes...');

            let themes = null,
                active = null;

            function resetInputs(t) {
                dom_overlay.themesMenu.number.textContent = '';
                dom_overlay.themesMenu.name.textContent = '';
                if(!t) dom_overlay.themesMenu.searching.input.value = '';
            }

            function defaultLoad() {

                let limit = 30;

                let myresolve = (data)=> {
                    themes = data.items;
                    displayThemes();
                    dom_overlay.themesMenu.meta.textContent = 'Viewing '+themes.length+' most-recently created';
                    resetInputs();
                    // TODO: remove memnat from display;
                }

                let myreject = (data)=> {
                    loading.close();
                    dom_overlay.themesMenu.cancel.dispatchEvent(new Event('click'));
                    packager.dialog.alert('An error occured while gathering themes.');
                    console.error(data.error);
                }

                packager.generalLoader.work({
                    type: ITEMS_TYPE_THEMES,
                    query: {
                        limit: limit,
                        sort: {
                            key: 'creationTime',
                            order: 'desc'
                        }
                    },
                    packager: litePackager,
                    resolve: myresolve,
                    reject: myreject
                });
            }

            defaultLoad();

            const displayThemes = async ()=> {

                active = null;

                let lc = 0;
                if (lc === themes.length) loading.close();

                for (let i = 0; i < themes.length; ++i) {
                    let theme = themes[i];
                    await new Promise((resolve,reject)=> {
                        loadTemplate('templates/theme-item.html', t => {
                            dom_overlay.themesMenu.themes.appendChild(t);
                            t.dataset.scriptId = script.id;

                            let typeinfo = M.themeTypeName(theme.type);

                            let tnum = t.querySelector('.number-meta'),
                                tct = t.querySelector('.time-meta'),
                                tb = t.querySelector('.theme-button'),
                                tn = t.querySelector('.theme-name'),
                                tt = t.querySelector('.theme-type'),
                                ts = t.querySelector('.theme-shell');

                            theme.dom = {
                                number: tnum,
                                creationTime: tct,
                                button: tb,
                                name: tn,
                                type: tt,
                                shell: ts
                            }

                            fixButtonResponse(tb);

                            let num = i+1;
                            tnum.textContent = '('+num+')';
                            tct.textContent = new Date(theme.creationTime).toLocaleString();
                            tn.textContent = theme.name;
                            tt.textContent = 'Type: '+typeinfo.name;

                            packager.generalLoader.work({
                                type: ITEMS_TYPE_SHELLS,
                                ids: [theme.shellId],
                                query: {
                                    resolutions: [
                                        {
                                            key: 'aspectIds',
                                            newKey: 'aspects',
                                            donorKey: 'id',
                                            donorType: 'aspects',
                                            donationKeys: ['id','abbr'],
                                            enforce: true
                                        }
                                    ]
                                },
                                packager: litePackager,
                                resolve: (data)=> {
                                    let shell = data.items[0];
                                    theme.shell = shell;
                                    ts.textContent = 'Shell: '+shell.name+' ('+shell.aspects.map(asp => asp.abbr)+')';
                                },
                                reject: (data)=> {
                                    console.error(data.error);
                                }
                            });

                            tb.addEventListener('click', ()=> {
                                console.log('update display for', theme);
                                if (active) active.dom.button.classList.remove('active');
                                active = theme;
                                tb.classList.add('active');
                                dom_overlay.themesMenu.number.textContent = '('+num+')';
                                dom_overlay.themesMenu.name.textContent = theme.name;
                                // TODO: set memnat in display
                            });

                            lc++;

                            if (lc === themes.length) loading.close();

                            resolve();
                        });
                    });
                }
            }


            let close = ()=> {
                let themes_dom = dom_overlay.themesMenu.themes;
                while (themes_dom.firstChild) themes_dom.removeChild(themes_dom.firstChild);
                resetInputs();
                // TODO: remove memnat from display;
                dom_overlay.themesMenu.elem.classList.add('d-none');
            }

            dom_overlay.close.addEventListener('click', close, {once:true});

            if (browseThemes_init) return;

            browseThemes_init = true;

            dom_overlay.themesMenu.searching.input.addEventListener('change', ()=> {
                let val = dom_overlay.themesMenu.searching.input.value;

                loading = packager.dialog.loading('Gathering themes...');

                resetInputs(true);

                // TODO: remove memnat from display;
                let themes_dom = dom_overlay.themesMenu.themes;
                while (themes_dom.firstChild) themes_dom.removeChild(themes_dom.firstChild);

                if (!val) {
                    defaultLoad();
                    return;
                }

                packager.generalLoader.work({
                    type: ITEMS_TYPE_THEMES,
                    packager: litePackager,
                    query: {
                        limit: Infinity,
                        comparators: [
                            {
                                key: 'name',
                                predicate: 'contains',
                                ignoreCase: true,
                                value: val
                            }
                        ],
                        sort: {
                            order: 'asc',
                            key: 'name',
                            ignoreCase: true
                        }
                    },
                    resolve: (data)=> {
                        themes = data.items;
                        displayThemes();
                        dom_overlay.themesMenu.meta.textContent = 'Found '+data.count;
                    },
                    reject: (data)=> {
                        packager.dialog.alert('An error occured while gathering themes.');
                        console.error(data.error);
                    }
                });
            });

            dom_overlay.themesMenu.cancel.addEventListener('click', ()=> dom_overlay.close.dispatchEvent(new Event('click')));

        }

        let iaui = false,
            iaui_content = null;
        const import_UI = (p,l,info,work)=> {
            dom_overlay.elem.classList.remove('d-none');
            dom_overlay.import.elem.classList.remove('d-none');

            dom_overlay.import.info.textContent = info ? info : '';
            dom_overlay.import.submit.textContent = l ? l : 'Import '+p;

            dom_overlay.import.file.focus();

            if (iaui) return;

            iaui = true;

            dom_overlay.import.file.addEventListener('change', (e)=> {
                let file = e.target.files[0],
                    reader = new FileReader();
                reader.onload = (e_)=> {
                    iaui_content = e_.target.result;
                    dom_overlay.import.display.textContent = e_.target.result;
                }
                reader.readAsText(file);
            });

            dom_overlay.import.cancel.addEventListener('click', ()=> dom_overlay.close.dispatchEvent(new Event('click')));

            dom_overlay.import.submit.addEventListener('click', ()=> {
                let loading = packager.dialog.loading('Importing '+p+'...');

                if (!iaui_content) {
                    loading.close();
                    packager.dialog.alert('Choose a file to upload.');
                    return;
                }

                work(iaui_content);

                loading.close();
                dom_overlay.close.dispatchEvent(new Event('click'));
            });

            dom_overlay.close.addEventListener('click', ()=> {

                // clear file input
                iaui_content = null;
                dom_overlay.import.elem.querySelector('form').reset();
                dom_overlay.import.display.textContent = '';

                dom_overlay.import.elem.classList.add('d-none');
            });
        }

        const drawMemnat = async (themeId,c_,focus)=> {
            let ctx = null,
                theme = await themes_map.get(themeId),
                type = theme._theme.type,
                shell = theme._theme.shellId ? await shells_map.get(theme._theme.shellId) : null,
                asps = null;

            if (shell) {
                asps = [];
                for (let i = 0; i < shell.aspectIds.length; ++i) {
                    let aid = shell.aspectIds[i],
                        asp = await aspects_map.get(aid);
                    asps.push(asp._aspect);
                }
            }

            let onArrowSet = null;
            if (type === M.THEME_TYPE_CONTEXT) {
                onArrowSet = (spos,epos,arr_t)=> {
                    let n = aspectNeighbors[epos];
                    switch (arr_t) {
                        case M.ARROW_TYPE_SIMILARITY:
                            n.arrowUse.setAttribute('xlink:href','#similarity-arrow');
                            break;
                        case M.ARROW_TYPE_CAUSATION:
                            n.arrowUse.setAttribute('xlink:href','#causation-arrow');
                            break;
                        case M.ARROW_TYPE_DEPENDENCE:
                            n.arrowUse.setAttribute('xlink:href','#dependence-arrow');
                            break;
                        case M.ARROW_TYPE_SPECTRUM:
                            n.arrowUse.setAttribute('xlink:href','#spectrum-arrow');
                            break;
                    }
                }
            } else if(type === M.THEME_TYPE_VERSUS) {
                // TODO
            }

            switch (theme._theme.type) {
                case M.THEME_TYPE_CONTEXT:
                    ctx = new M.Context({
                        theme: theme._theme,
                        aspects: asps,
                        container: c_,
                        shellName: shell.name(),
                        themePosition: theme.position,
                        onArrowSet: onArrowSet
                    });
                    break;
                case M.THEME_TYPE_SIGNAL:
                    ctx = new M.Signal({
                        theme: theme._theme,
                        aspects: asps,
                        container: c_,
                        shellName: shell.name(),
                        themePosition: theme.position
                    });
                    break;
                case M.THEME_TYPE_VERSUS:
                    ctx = new M.Versus({
                        theme: theme._theme,
                        aspects: asps,
                        container: c_,
                        shellName: shell.name(),
                        themePosition: theme.position,
                        onArrowSet: onArrowSet
                    });
                    break;
                case M.THEME_TYPE_TEXT:
                    ctx = new M.Text({
                        theme: theme._theme,
                        container: c_,
                        focus: focus
                    });
                    break;
                default:
            }

            if (theme._theme.type !== M.THEME_TYPE_TEXT && theme._theme.type !== M.THEME_TYPE_MOD_QUAD) {
                ctx.setFarRightCallback((tid)=> {
                    repositionTheme(theme,theme.line.themes.length);
                });

                ctx.setFarLeftCallback((tid)=> {
                    repositionTheme(theme,1);
                });

                ctx.setLeftCallback((tid)=> {
                    repositionTheme(theme,theme.position-1);
                });

                ctx.setRightCallback((tid)=> {
                    repositionTheme(theme,theme.position+1);
                });

                ctx.setDeleteCallback((tid,c)=> {
                    repositionTheme(theme,theme.line.themes.length,true);
                    theme.line.domNumber().dispatchEvent(new Event('click'));
                });
            }

            return ctx;
        }

        const onThemeSelect = async (theme,clk,forAspect)=> {
            let _theme = theme._theme;

            let blockScroll = 'center';

            if (!forAspect && theme.memnat.clearSelection) theme.memnat.clearSelection();

            if (_theme.type === M.THEME_TYPE_TEXT) {
                theme.memnat.activate();
                if (!forAspect) updateControlsPanelState(CONTROLS_PANEL_THEME_STATE, {line: theme.line, theme: theme});
                blockScroll = 'end';
            } else if (_theme.type === M.THEME_TYPE_MOD_QUAD) {
                // if (!forAspect) updateControlsPanelState(CONTROLS_PANEL_LINE_STATE, {line: theme.line });
            } else {
                switch (clk) {
                    case M.CLICKED_ON_SHELL_TITLE:
                        let shell = await shells_map.get(_theme.shellId);
                        if (!forAspect) updateControlsPanelState(CONTROLS_PANEL_SHELL_STATE, {line: theme.line, shell: shell});
                        break;
                    case M.CLICKED_ON_CANVAS:
                    case M.CLICKED_ON_THEME_TITLE:
                        if (!forAspect) updateControlsPanelState(CONTROLS_PANEL_THEME_STATE, {line: theme.line, theme: theme});
                        break;
                }
            }


            // document.getElementById(theme.memnat.id).scrollIntoView({block:blockScroll,behavior:SCROLL_BEHAVIOR});
        }

        const onMeaningChange = async (theme,e)=> {
            let _theme = theme._theme;

            switch (_theme.type) {
                case M.THEME_TYPE_TEXT:
                    break;
                case M.THEME_TYPE_SIGNAL:
                    dom_aspect_controls['neighbor_1'].input.value = e.value;
                    break;
                case M.THEME_TYPE_MOD_QUAD:
                    if (!this.simulation.voiceInfo || !theme.line || (theme.line.modQuad.theme._theme.meaning !== this.simulation.voiceInfo.modQuad)) return;
                    this.simulation.modify(e.key, false);
                    break;
                case M.THEME_TYPE_CONTEXT:
                case M.THEME_TYPE_VERSUS:
                    let shell = await shells_map.get(_theme.shellId),
                        spos = e.spos,
                        epos = _theme.type === M.THEME_TYPE_VERSUS ? e.epos_ : e.epos,
                        k = 'neighbor_'+(_theme.type === M.THEME_TYPE_VERSUS ? epos+1 : (epos < spos ? epos+1 : epos)),
                        n = dom_aspect_controls[k];
                    n.input.value = e.value;
                    break;
                default:

            }
        }

        const onMeaningCommit = async (theme,e)=> {
            let _theme = theme._theme;

            switch (_theme.type) {
                case M.THEME_TYPE_TEXT:
                    document.getElementById(theme.memnat.id).classList.remove('theme-active');
                    CUD.update.themes.set(_theme.id, 'meaning', _theme.meaning);
                    break;
                case M.THEME_TYPE_CONTEXT:
                case M.THEME_TYPE_VERSUS:
                case M.THEME_TYPE_SIGNAL:
                    CUD.update.themes.set(_theme.id, 'meaning', _theme.meaning);
                    break;
                case M.THEME_TYPE_MOD_QUAD:
                    if (!this.simulation.voiceInfo || !theme.line || (theme.line.modQuad.theme._theme.meaning !== this.simulation.voiceInfo.modQuad)) return;
                    this.simulation.modify(e.key, true);
                    break;
                default:

            }
        }

        async function measureSystem(theme, temp=true) {
            let res = {width:1, depth:1}
            let shell = await shells_map.get(theme._theme.shellId, temp);
            let cdepth = 0;
            for (let ai = 0; ai < 6; ++ai) {
                let aid = shell.aspectIds[ai];
                let asp = await aspects_map.get(aid, temp);
                if (asp._aspect.type === M.ASPECT_TYPE_COMPOUND) {
                    asp = await themes_map.get(aid, temp);
                    let cres = await measureSystem(asp, temp);
                    res.width+=cres.width;
                    cdepth = Math.max(cdepth, cres.depth);
                }
            }
            res.depth+=cdepth;

            return res;
        }
        this.measureSystem = measureSystem;

        const addTheme = async (props)=> {
            let line = props.line,
                type = props.type,
                shellId = props.shellId;

            if (line) {
                if (type === M.THEME_TYPE_TEXT && (line.themes.length > 0 || (line.modQuad && line.modQuad.elem))) {
                    packager.dialog.alert('A textual theme requires a line all to itself. Delete the other themes or create a new line.');
                    return;
                }

                if (line.themes.length > 0 && line.themes[0].type === M.THEME_TYPE_TEXT) {
                    packager.dialog.alert('You tried to add a theme on the same line as a textual theme. A textual theme requires a line all to itself. Create a new line and/or delete this line.');
                    return;
                }
            }

            let shell = shellId ? await shells_map.get(shellId) : null;

            let id = props.id || guid();
            props.id = id;
            // props.onSelect = (clk)=> onThemeSelect(id,clk);
            // props.onMeaningChange = (d)=> onMeaningChange(id, d);

            let _theme = new M.Theme(props),
                theme = new Theme(_theme,line);

            _theme.onSelect = (clk)=> onThemeSelect(theme,clk);
            _theme.onMeaningChange = (d)=> onMeaningChange(theme, d);
            _theme.onMeaningCommit = (d)=> onMeaningCommit(theme, d);

            // add the theme as an aspect
            if (_theme.type !== M.THEME_TYPE_TEXT) {
                let asp = new Aspect(new M.Aspect({
                    id: _theme.id,
                    name: _theme.name(),
                    abbr: _theme.name().substring(0,2),
                    type: M.ASPECT_TYPE_COMPOUND,
                    onHighlight: updateAspectControls
                }));
                aspects_map.set(_theme.id, asp);
                CUD.create.aspects.set(_theme.id,asp);

                // Calcuate depth and width of theme.
                    // The hexi network is infinitely flat, hence
                        // the width being the number of themes.
                    // The depth is the number of systems visited
                        // before reaching the theme with only pure
                        // aspects.
                let temp_load = false; // ?
                let {width,depth} = await measureSystem(theme, temp_load);
                _theme.width = width;
                _theme.depth = depth;
            }

            themes_map.set(_theme.id,theme);
            CUD.create.themes.set(_theme.id,theme);

            if (!line) return theme;

            line.themes.push(theme);

            let mem = document.createElement('div');
            type === M.THEME_TYPE_TEXT ? mem.classList.add('col-12','memnat') : mem.classList.add('col-3','memnat');
            line.elem.querySelector('.line-memnat .memnat-row').appendChild(mem);

            mem.addEventListener('mouseover', ()=> {
                mem.querySelectorAll('.direction-label').forEach(dl => dl.classList.add('active'));
            });

            mem.addEventListener('mouseout', ()=> {
                mem.querySelectorAll('.direction-label').forEach(dl => dl.classList.remove('active'));
            });

            let mn = line.themes.length;
            mem.id = 'memnat-'+theme._theme.id;
            mem.dataset.themeId = theme._theme.id;

            theme.position = mn;

            if (shell) shell.coords.add(theme._theme.id);

            theme.memnat = await drawMemnat(theme._theme.id,mem.id, true);

            onThemeSelect(theme,M.CLICKED_ON_THEME_TITLE);

            return theme;
        }
        this.addTheme = addTheme;

        const addLine = (pre,target,blank)=> {
            return new Promise((resolve,reject)=> {
                loadTemplate('templates/scene-line.html', t => {
                    let id = null,
                        ln = null,
                        line = null;
                    if (target) {
                        let sl = target.parentNode.parentNode,
                            sln = Number.parseInt(sl.dataset.lineNumber);
                        ln = pre ? sln : sln+1;

                        if (!blank) {
                            pre ? dom_script.insertBefore(t,sl) : (sl.nextSibling && sl.nextSibling.classList.contains('scene-line') ? dom_script.insertBefore(t,sl.nextSibling) : dom_script.appendChild(t));
                        }
                    } else {
                        ln = pre ? 1 : lines_map.size+1;
                        if (!blank) {
                            pre ? dom_script.prepend(t) : dom_script.appendChild(t);
                        }
                    }

                    if (!blank) {
                        let cln = Number.parseInt(dom_line_controls.elem.dataset.lineNumber);

                        for (let i = lines_map.size; i >= ln; --i) {
                            line = lines_map.get(i);
                            if (cln === line.number) dom_line_controls.elem.dataset.lineNumber = cln+1;
                            lines_map.delete(i);
                            line.number = i+1;
                            line.elem.dataset.lineNumber = i+1;
                            line.domNumber().textContent = i+1;
                            lines_map.set(i+1,line);
                        }

                        t.dataset.lineNumber = ln;
                        t.querySelector('.line-number').textContent = ln;

                        id = guid();
                        line = new Line(id,ln,t);
                        line.creationTime = Date.now();
                        hookLineButtons(line);
                        lines_map.set(ln,line);

                        dom_toolbar.goto.numLines.textContent = '/'+lines_map.size;

                        line.elem.scrollIntoView({block:'center',behavior:SCROLL_BEHAVIOR});
                    } else {
                        line = new Line(guid(),0,t);
                        line.creationTime = Date.now();
                    }

                    resolve(line);
                });
            });
        }
        this.addLine = addLine;

        const repositionLine = (line,pos,del)=> {
            if (pos < 1) {
                repositionLine(line,1,del);
                return;
            }
            if (pos > lines_map.size) {
                repositionLine(line,lines_map.size,del);
                return;
            }

            if (pos === line.number && !del) return;

            lines_map.delete(line.number);

            if (del) {
                line.elem.remove();
                dom_toolbar.goto.numLines.textContent = '/'+lines_map.size;
                if (line.entity) line.entity.coords.delete(line.number);

                for (let i = 0; i < line.themes.length; i++) {
                    let theme = line.themes[i];
                    themes_map.delete(theme._theme.id);
                    if (theme._theme.type !== M.THEME_TYPE_TEXT) {
                        let shell = shells_map.get_(theme._theme.shellId);
                        if (shell) shell.coords.delete(theme._theme.id);
                    }
                }
            }

            let l = null;

            if (pos < line.number) {
                for (let i = line.number-1; i >= pos; --i) {
                    l = lines_map.get(i);
                    lines_map.delete(i);
                    if (dom_line_controls.elem.dataset.lineNumber === (''+l.number)) dom_line_controls.elem.dataset.lineNumber = i+1;
                    l.number = i+1;
                    l.elem.dataset.lineNumber = i+1;
                    l.elem.querySelector('.line-number').textContent = i+1;
                    if (l.entity) {
                        l.entity.coords.delete(i);
                        l.entity.coords.add(l.number);
                    }
                    lines_map.set(l.number,l);
                }
            } else {
                for (let i = line.number+1; i <= pos; ++i) {
                    l = lines_map.get(i);
                    lines_map.delete(i);
                    if (dom_line_controls.elem.dataset.lineNumber === (''+l.number)) dom_line_controls.elem.dataset.lineNumber = i-1;
                    l.number = i-1;
                    l.elem.dataset.lineNumber = i-1;
                    l.elem.querySelector('.line-number').textContent = i-1;
                    if (l.entity) {
                        l.entity.coords.delete(i);
                        l.entity.coords.add(l.number);
                    }
                    lines_map.set(l.number,l);
                }
            }

            if (del) {
                if (dom_line_controls.elem.dataset.lineNumber === (''+line.number)) updateControlsPanelState(CONTROLS_PANEL_SCENE_STATE);
                return;
            }

            if (dom_line_controls.elem.dataset.lineNumber === (''+line.number)) dom_line_controls.elem.dataset.lineNumber = pos;

            lines_map.set(pos,line);
            line.number = pos;
            if (line.entity) line.entity.coords.add(line.number);
            line.elem.dataset.lineNumber = pos;
            line.domNumber().textContent = pos;

            if (pos === lines_map.size) {
                dom_script.appendChild(line.elem);
            } else {
                dom_script.insertBefore(line.elem, lines_map.get(pos+1).elem);
            }

            line.elem.scrollIntoView({block:'center',behavior:SCROLL_BEHAVIOR});
        }

        // Controls setup
        $(dom_aspect_controls.name).on('change', async (e)=> {
            let shell = await shells_map.get(dom_shell_controls.elem.dataset.shellId),
                aidx = Number.parseInt(dom_aspect_controls.elem.dataset.aspectIndex),
                asp = await aspects_map.get(shell.aspectIds[aidx]),
                dom_asp = dom_shell_controls['aspect'+(aidx+1)],
                val = dom_aspect_controls.name.value.trim();
            if (val) {
                asp._aspect.name = val;
                dom_asp.name.value = val;
                CUD.update.aspects.set(asp._aspect.id, 'name', val);
            } else {
                dom_asp.name.value = asp._aspect.name;
            }
            dom_aspect_controls.name.blur();
        });
        $(dom_aspect_controls.cover).on('change', async (e)=> {
            let shell = await shells_map.get(dom_shell_controls.elem.dataset.shellId),
                aidx = Number.parseInt(dom_aspect_controls.elem.dataset.aspectIndex),
                asp = await aspects_map.get(shell.aspectIds[aidx]),
                val = dom_aspect_controls.cover.value.trim();
            asp._aspect.cover(val);
            CUD.update.aspects.set(asp._aspect.id, 'cover', asp._aspect.cover());
            dom_aspect_controls.cover.blur();
        });
        $(dom_aspect_controls.abbr).on('change', async (e)=> {
            let shell = await shells_map.get(dom_shell_controls.elem.dataset.shellId),
                aidx = Number.parseInt(dom_aspect_controls.elem.dataset.aspectIndex),
                asp = await aspects_map.get(shell.aspectIds[aidx]),
                dom_asp = dom_shell_controls['aspect'+(aidx+1)],
                val = dom_aspect_controls.abbr.value.trim();
            if (val) {
                asp._aspect.abbr(val);
                dom_asp.abbr.value = val;
                CUD.update.aspects.set(asp._aspect.id, 'abbr', val);
            } else {
                dom_asp.abbr.value = asp._aspect.abbr();
            }
            dom_aspect_controls.abbr.blur();
        });
        $(dom_aspect_controls.frequency).on('change', async (e)=> {
            // TODO: read frequencies separated by delimiter to support multidimensional beings
            let shell = await shells_map.get(dom_shell_controls.elem.dataset.shellId),
                aidx = Number.parseInt(dom_aspect_controls.elem.dataset.aspectIndex),
                asp = await aspects_map.get(shell.aspectIds[aidx]),
                val = Math.max(SOUND_MIN_FREQUENCY, Number.parseFloat(dom_aspect_controls.frequency.value.trim())) || SOUND_MIN_FREQUENCY;
            dom_aspect_controls.frequency.value = val;
            dom_theme_controls.soundControls[n_ss[aidx]].frequency.value = val;
            asp._aspect.frequencies(0,val);
            CUD.update.aspects.set(asp._aspect.id, 'frequencies', asp._aspect.frequencies());
            // TODO: update Tone.Transport if aspect, theme, or another theme using this aspect is playing
            dom_aspect_controls.frequency.blur();
        });

        $(dom_theme_controls.name).on('change', async (e)=> {
            let theme = await themes_map.get(dom_theme_controls.elem.dataset.themeId),
                val = dom_theme_controls.name.value.trim();
            if (val) {
                theme._theme.name(val);
                //(await aspects_map.get(theme._theme.id))._aspect.name = val;
                CUD.update.themes.set(theme._theme.id, 'name', val);
            } else {
                dom_theme_controls.name.value = theme._theme.name();
            }
            dom_theme_controls.name.blur();
        });
        $(dom_theme_controls.position).on('change', (e)=> {
            let theme = themes_map.get_(dom_theme_controls.elem.dataset.themeId),
                pos = Number.parseInt(dom_theme_controls.position.value.trim());
            if (!isNaN(pos)) repositionTheme(theme,pos);
            dom_theme_controls.position.value = theme.position;
            dom_theme_controls.position.blur();
        });
        $(dom_theme_controls.quantum).on('change', (e)=> {
            let theme = themes_map.get_(dom_theme_controls.elem.dataset.themeId),
                val = Number.parseInt(dom_theme_controls.quantum.value.trim());
            if (!isNaN(val) && val) {
                theme.quantum = val;
                CUD.update.themes.set(theme._theme.id, 'quantum', val);
            }
            dom_theme_controls.quantum.value = theme.quantum;
            dom_theme_controls.quantum.blur();
        });
        $(dom_theme_controls.squat).on('change', (e)=> {
            let theme = themes_map.get_(dom_theme_controls.elem.dataset.themeId),
                val = Number.parseInt(dom_theme_controls.squat.value.trim());
            if (!isNaN(val) && val) {
                theme.squat = val;
                CUD.update.themes.set(theme._theme.id, 'squat', val);
            }
            dom_theme_controls.squat.value = theme.squat;
            dom_theme_controls.squat.blur();
        });
        for (let i = 0; i < n_ss.length; ++i) {
            let n = n_ss[i],
                asp_sc = dom_theme_controls.soundControls[n];
            $(asp_sc.frequency).on('change', (e)=> {
                // TODO: read frequencies separated by delimiter to support multidimensional beings
                let theme = themes_map.get_(dom_theme_controls.elem.dataset.themeId),
                    shell = shells_map.get_(theme._theme.shellId),
                    asp = aspects_map.get_(shell.aspectIds[i]),
                    val = Math.max(0, Number.parseFloat(asp_sc.frequency.value.trim())) || 0;
                asp_sc.frequency.value = val;
                if (dom_aspect_controls.elem.dataset.aspectIndex == i) dom_aspect_controls.frequency.value = val;
                asp._aspect.frequencies(0,val);
                CUD.update.aspects.set(asp._aspect.id, 'frequencies', asp._aspect.frequencies());
                // TODO: update Tone.Transport if aspect, theme, or another theme using this aspect is playing
                asp_sc.frequency.blur();
            });
            // Intensity is in [0,1] for UI, [0,SOUND_INTENSITY_MAX] behind the scenes.
            $(asp_sc.intensity).on('change', (e)=> {
                let theme = themes_map.get_(dom_theme_controls.elem.dataset.themeId),
                    val = Math.range(Number.parseFloat(asp_sc.intensity.value.trim()), 0, 1);
                if (isNaN(val)) val = 1;
                let val_ = val*SOUND_INTENSITY_MAX;
                asp_sc.intensity.value = val;
                asp_sc.intensityRange.value = val;
                theme._theme.intensities(i,val_);
                let info = this.simulation.voiceInfo,
                    ln = Number.parseInt(dom_line_controls.elem.dataset.lineNumber);
                if (info.lineNumber === ln && info.themeNumber === theme.position) this.simulation.setChannelIntensity(i,val_);
                CUD.update.themes.set(theme._theme.id, 'intensities', theme._theme.intensities());
                // TODO: update Tone.Transport if aspect, theme, or another theme using this aspect is playing
                asp_sc.intensity.blur();
            });
            $(asp_sc.intensityRange).on('input change', (e)=> {
                let theme = themes_map.get_(dom_theme_controls.elem.dataset.themeId),
                    val = Number.parseFloat(asp_sc.intensityRange.value),
                    val_ = val*SOUND_INTENSITY_MAX;
                asp_sc.intensity.value = val;
                let info = this.simulation.voiceInfo,
                    ln = Number.parseInt(dom_line_controls.elem.dataset.lineNumber);
                if (info.lineNumber === ln && info.themeNumber === theme.position) this.simulation.setChannelIntensity(i,val_);
                if (e.type === 'change') {
                    theme._theme.intensities(i,val_);
                    CUD.update.themes.set(theme._theme.id, 'intensities', theme._theme.intensities());
                }
            });
            $(asp_sc.duration).on('change', (e)=> {
                let theme = themes_map.get_(dom_theme_controls.elem.dataset.themeId),
                    val = Number.parseFloat(asp_sc.duration.value.trim());
                if (isNaN(val)) val = SOUND_DURATION_DEFAULT;
                asp_sc.duration.value = val;
                theme._theme.durations(i,val);
                CUD.update.themes.set(theme._theme.id, 'durations', theme._theme.durations());
                // TODO: restart Tone.Transport if aspect, theme, or another theme using this aspect is playing
                asp_sc.duration.blur();
            });
            $(asp_sc.mute).on('click', (e)=> {
                let theme = themes_map.get_(dom_theme_controls.elem.dataset.themeId);
                // TODO: theme must be playing to work;
                let voice = this.simulation.voice();
                if (!voice || !voice._theme || (voice._theme.id !== theme._theme.id)) return;
                asp_sc.mute.classList.add('on');
                asp_sc.solo.classList.remove('on');
                // Set this channel's solo property to false
            });
            $(asp_sc.solo).on('click', (e)=> {
                let theme = themes_map.get_(dom_theme_controls.elem.dataset.themeId);
                // TODO: theme must be playing to work;
                let voice = this.simulation.voice();
                if (!voice || !voice._theme || (voice._theme.id !== theme._theme.id)) return;
                asp_sc.solo.classList.add('on');
                asp_sc.mute.classList.remove('on');
                // Set this channel's mute property to false
                // Play theme
            });
        }
        $(dom_theme_controls.soundControls.duration).on('change', (e)=> {
            let theme = themes_map.get_(dom_theme_controls.elem.dataset.themeId),
                val = Number.parseFloat(dom_theme_controls.soundControls.duration.value.trim());
            if (isNaN(val)) val = SOUND_DURATION_DEFAULT;
            dom_theme_controls.soundControls.duration.value = val;
            theme._theme.duration(val);
            CUD.update.themes.set(theme._theme.id, 'duration', theme._theme.duration());
            // TODO: update Tone.Transport if aspect, theme, or another theme using this aspect is playing
            dom_theme_controls.soundControls.duration.blur();
        });
        dom_theme_controls.visualization.visualize.addEventListener('click', ()=> {
            // TODO: check if the current voice is the same as the currenly selected theme.
                // Try to select theme before doing anything.
            // TODO: ignore if the current voice is an aspect
                // OR render a scene with a single star
            let vzr = this.simulation.visualizer;
            let fb = dom_theme_controls.visualization.visualize,
                cls = fb.classList;
            if  (vzr.state() === 'on') {
                vzr.off();
                cls.remove('on');
                dom_theme_controls.visualization.fullscreen.classList.remove('on');
            } else {
                let theme = themes_map.get_(dom_theme_controls.elem.dataset.themeId),
                    voicetype = VOICE_TYPE_THEME,
                    cb = ()=> {
                        let voiceInfo = {
                            title: theme._theme.name(),
                            lineNumber: theme.line.number, // TODO: update player if line number changes or anything about the line changes
                            themeNumber: theme.position, // TODO: update player if theme position changes or anything about the theme changes,
                            voiceType: voicetype,
                            modQuad: theme.line.modQuad ? theme.line.modQuad.theme._theme.meaning : null
                        }

                        toggleSoundPlayer(true, voiceInfo);

                        this.simulation.voice(theme,voicetype,voiceInfo);

                        dom_theme_controls.visualization.visualizer.scrollIntoView({block:'center',behavior:SCROLL_BEHAVIOR});

                        dom_sound_player.visualize.dispatchEvent(new Event('click'));

                        cls.add('on');
                    }
                if (this.simulation.time() == null) {
                    cb();
                } else {
                    dom_sound_player.stop.dispatchEvent(new CustomEvent('click', {
                        detail: {
                            callback: cb
                        }
                    }));
                }
            }
        });
        dom_theme_controls.visualization.fullscreen.addEventListener('click', ()=> {
            if (this.simulation.visualizer.state() === 'off') return;
            let fb = dom_theme_controls.visualization.fullscreen,
                cls = fb.classList;
            if (cls.contains('on')) {
                this.simulation.visualizer.fullscreen(false);
                cls.remove('on');
            } else {
                this.simulation.visualizer.fullscreen(true);
                cls.add('on');
            }
        });


        $(dom_shell_controls.name).on('change', (e) => {
            let shell = shells_map.get_(dom_shell_controls.elem.dataset.shellId),
                val = dom_shell_controls.name.value.trim();
            if (val) {
                shell.name(val);
                CUD.update.shells.set(shell.id, 'name', shell.name());
            } else {
                dom_shell_controls.name.value = shell.name();
            }
            dom_shell_controls.name.blur();
        });
        for (let i = 0; i < 6; ++i) {
            let dom_asp = dom_shell_controls['aspect'+(i+1)];
            $(dom_asp.name).on('change', (e) => {
                let shell = shells_map.get_(dom_shell_controls.elem.dataset.shellId),
                    aspect = aspects_map.get_(shell.aspectIds[i]),
                    val = dom_asp.name.value.trim();
                if (val) {
                    aspect._aspect.name = val;
                    if (dom_aspect_controls.elem.dataset.aspectId === aspect._aspect.id) dom_aspect_controls.name.value = val;
                    CUD.update.aspects.set(aspect._aspect.id, 'name', val);
                } else {
                    dom_asp.name.value = aspect._aspect.name;
                }
                dom_asp.name.blur();
            });
            $(dom_asp.abbr).on('change', (e) => {
                let shell = shells_map.get_(dom_shell_controls.elem.dataset.shellId),
                    aspect = aspects_map.get_(shell.aspectIds[i]),
                    val = dom_asp.abbr.value.trim();
                if (val) {
                    aspect._aspect.abbr(val);
                    if (dom_aspect_controls.elem.dataset.aspectId === aspect._aspect.id) dom_aspect_controls.abbr.value = val;
                    CUD.update.aspects.set(aspect._aspect.id, 'abbr', val);
                } else {
                    dom_asp.abbr.value = aspect._aspect.abbr();
                }
                dom_asp.abbr.blur();
            });
        }

        $(dom_line_controls.position).on('change', (e) => {
            let ln = Number.parseInt(dom_line_controls.elem.dataset.lineNumber),
                line = lines_map.get(ln),
                pos = Number.parseInt(dom_line_controls.position.value);
            if (isNaN(pos)) {
                dom_line_controls.position.value = line.number;
            } else {
                repositionLine(line,pos);
                dom_line_controls.position.value = line.number;
            }
            dom_line_controls.position.blur();
        });

        dom_scene_controls.id.textContent = 'ID: '+this.scene.id;
        $(dom_scene_controls.name).on('change', (e)=> {
            let val = dom_scene_controls.name.value.trim();
            let info = val + ' (Scene '+this.scene.position+')'+' - '+this.script.name; // TODO: +authors
            this.scene.name = val ? val : this.scene.id;
            dom_workspace_title.setTextContent(info);
            dom_scene_controls.name.blur();
        });

        dom_script_controls.id.textContent = 'ID: '+this.script.id;
        $(dom_script_controls.name).on('change', (e)=> {
            let val = dom_script_controls.name.value.trim();
            this.script.name = val ? val : this.script.id;
            let info = this.scene.name + ' (Scene '+this.scene.position+')'+' - '+this.script.name; // TODO: +authors
            dom_workspace_title.setTextContent(info);
            dom_script_controls.name.blur();
        });
        $(dom_script_controls.processRootPath).on('change', (e)=> {
            let val = dom_script_controls.processRootPath.value.trim();
            if (isValidURL(val)) {
                this.script.processRootPath = val;
                dom_script_controls.processRootPath.value = val;
            } else {
                if (val) console.warn('Ignoring strange URL, '+val+', as the process root path')
                this.script.processRootPath = '';
                dom_script_controls.processRootPath.value = '';
            }
            dom_script_controls.processRootPath.blur();
        });
        dom_script_controls.meta.dateCreated.textContent = new Date(this.script.creationTime).toLocaleString();

        // Overlay setup
        dom_overlay.close.addEventListener('click', ()=> {
            dom_overlay.elem.classList.add('d-none');
        });

        let clearControls = (exc)=> {
            exc = exc || {};

            [   dom_entity_controls.selectize,
                // dom_sot_controls.selectize,
                // dom_master_controls.selectize,
                caui_asps_sel,
                ceui_ents_sel,
                cpui_procs_sel,
                csui_sots_sel,
                atui_shell,
            ].map(s => {
                    if (s) {
                        s[0].selectize.clear();
                        s[0].selectize.clearOptions();
                        s[0].selectize.clearCache();
                    }
                });

            [
                csui_shell_display,
                csui_theme_display,
                atui_theme_type,
                atui_theme_display
            ].map(s => {
                if (s) {
                    s[0].selectize.clear();
                    s[0].selectize.clearCache();
                }
            });

            if (csui_aspects) {
                for (let i = 0; i < csui_aspects.length; ++i) {
                    csui_aspects[i].selectize.clear();
                    csui_aspects[i].selectize.clearOptions();
                    csui_aspects[i].selectize.clearCache();
                }
            }

            for (let i = 0; i < apsui_asp_procs.length; ++i) {
                apsui_asp_procs[i].selectize.clear();
                apsui_asp_procs[i].selectize.clearOptions();
                apsui_asp_procs[i].selectize.clearCache();
            }

            dom_scene_controls.name.value = '';
            dom_scene_controls.id.textContent = '';

            if (!exc.script) {
                dom_script_controls.name.value = '';
                dom_script_controls.id.textContent = '';
                dom_player_controls.frequency.value = SOUND_MIN_FREQUENCY;
                dom_player_controls.intensity.value = dom_player_controls.intensityRange.value = dom_sound_player.intensityRange.value = 0.3;
                dom_player_controls.duration.value = SOUND_DURATION_DEFAULT;

                [
                    dom_sot_controls.selectize,
                    dom_master_controls.selectize,
                ].map(s => {
                        if (s) {
                            s[0].selectize.clear();
                            s[0].selectize.clearOptions();
                            s[0].selectize.clearCache();
                        }
                    });
            }

            dom_workspace_title.setTextContent('');

            // Toolbar
            dom_toolbar.goto.currentLine.value = '1';
            dom_toolbar.goto.numLines.textContent = '/0'

            delete dom_line_controls.elem.dataset.lineNumber;
            delete dom_theme_controls.elem.dataset.themeId;
            delete dom_aspect_controls.elem.dataset.aspectIndex;
            delete dom_shell_controls.elem.dataset.shellId;
            delete dom_sot_controls.elem.dataset.sotId;
            delete dom_master_controls.elem.dataset.masterId;

            CUD.reset();
        }

        // Set up toolbar items functionalities
        dom_toolbar.create.aspects.addEventListener('click', ()=> createAspects_UI());
        dom_toolbar.create.shell.addEventListener('click', ()=> createShell_UI());
        dom_toolbar.create.entities.addEventListener('click', ()=> createEntities_UI());
        dom_toolbar.create.processes.addEventListener('click', ()=> createProcesses_UI());
        dom_toolbar.create.sots.addEventListener('click', ()=> createSOTs_UI());
        dom_toolbar.create.line.addEventListener('click', ()=> addLine());
        dom_toolbar.create.linePrepend.addEventListener('click', ()=> addLine(true));
        dom_toolbar.create.scene.addEventListener('click', async ()=> {

            const doNewScene = async (name)=> {
                if (!name) return;

                let loading = packager.dialog.loading('Creating scene...');

                clearControls({script:true});

                await restScene();

                let sceneId = guid();
                name = name.trim() || sceneId;
                this.scene = new Scene(sceneId, name, scenes_map.size+1);
                scenes_map.set(this.scene.position, this.scene);

                updateControlsPanelState(CONTROLS_PANEL_SCENE_STATE);

                dom_scene_controls.name.value = name;
                dom_scene_controls.id.textContent = 'ID: '+this.scene.id;

                dom_workspace_title.setTextContent(this.scene.name + ' (Scene '+this.scene.position+')'+' - '+this.script.name); // TODO: +authors

                addLine();

                loading.close();
            }
            let msg = 'Name the new scene:';
            switch (packager.name) {
                case 'electron':
                    packager.dialog.prompt({
                        message: msg,
                        callback: doNewScene
                    });
                    break;
                default:
                    packager.dialog.prompt({
                        message: msg,
                        callback: doNewScene
                    });
            }
        });
        dom_toolbar.create.script.addEventListener('click', ()=> {

            const doNewScript = (name)=> {

                if (!name) return;

                let loading = packager.dialog.loading('Creating script...');

                clearControls();

                for (const [pos,l] of lines_map) {
                    l.elem.remove();
                    if (l.modQuad && l.modQuad.elem) l.modQuad.elem.remove();
                }

                [aspects_map,themes_map,shells_map,entities_map,lines_map,scenes_map].map(m => m.clear());

                dom_toolbar.goto.currentLine.value = '1';
                dom_toolbar.goto.numLines.textContent = '/0';

                let scriptId = guid();
                name = name.trim() || scriptId;
                this.script = new Script(scriptId, name);

                let sceneId = guid();
                this.scene = new Scene(sceneId, sceneId, 1);
                scenes_map.set(1, this.scene);

                updateControlsPanelState(CONTROLS_PANEL_SCENE_STATE);

                dom_scene_controls.id.textContent = 'ID: '+this.scene.id;
                dom_script_controls.name.value = name;
                dom_script_controls.id.textContent = 'ID: '+this.script.id;

                dom_workspace_title.setTextContent(this.scene.name + ' (Scene '+this.scene.position+')'+' - '+this.script.name); // TODO: +authors

                addLine();

                loading.close();
            }
            let msg = 'Name the new script:';
            switch (packager.name) {
                case 'electron':
                    packager.dialog.prompt({
                        message: msg,
                        callback: doNewScript
                    });
                    break;
                default:
                    packager.dialog.prompt({
                        message: msg,
                        callback: doNewScript
                    });
            }
        });

        // dom_toolbar.browse.aspects.addEventListener('click', ()=> browseAspects_UI());
        // dom_toolbar.browse.shells.addEventListener('click', ()=> browseShells_UI());
        dom_toolbar.browse.themes.addEventListener('click', ()=> browseThemes_UI());
        // dom_toolbar.browse.entities.addEventListener('click', ()=> browseEntities_UI());
        // dom_toolbar.browse.processes.addEventListener('click', ()=> browseProcesses_UI());
        // dom_toolbar.browse.sots.addEventListener('click', ()=> browseSOTs_UI());

        let gotoLine = (e)=> {
            e.preventDefault();
            if (lines_map.size === 0) {
                dom_toolbar.goto.currentLine.value = 0;
                return;
            }
            let ln = Math.range(Number.parseInt(dom_toolbar.goto.currentLine.value.trim()),1,lines_map.size),
                line = lines_map.get(ln);
            dom_toolbar.goto.currentLine.value = ln;
            line.domNumber().dispatchEvent(new Event('click'));
            // TODO: use custom scroll function that accounts for fixed toolbar height
            line.elem.scrollIntoView({block:'center',behavior:SCROLL_BEHAVIOR});
        }
        dom_toolbar.goto.submit.addEventListener('click', gotoLine);
        dom_toolbar.goto.form.addEventListener('submit', gotoLine);
        ['aspects','shells','entities','processes'].map(p => {
            dom_toolbar.import[p].addEventListener('click', ()=> import_UI(p,null,null, (str)=> dom_overlay.import.doIt(str,p)));
        });
        dom_toolbar.load.addEventListener('click', (e)=> {

            let doLoad = async(_script_, after)=> {

                if (!_script_) return;

                try {
                    await restScene();

                    scenes_map.clear();

                    let script_ = typeof _script_ === 'string' ? JSON.parse(_script_) : _script_;
                    this.script = new Script(script_.id, script_.name, script_);
                    this.script.processRootPath = script_.processRootPath || '';

                    for (let id in script_.scenes) {
                        let scene_ = script_.scenes[id],
                            scene = new Scene(id,scene_.name,scene_.position,scene_.lineIds);
                        scene.creationTime = scene_.creationTime;
                        scene.saveTime = scene_.saveTime;
                        scenes_map.set(scene.position,scene);
                    }

                    let scene_1 = scenes_map.get(1);
                    // load only lines of first scene (position 1)
                    // update themes' line properties
                    // add line, reposition it
                    // draw themes in line
                    if (!scene_1) {
                        let msg = 'Error: Expected Scene 1 while loading from content. Reload this app and try again.';
                        packager.dialog.alert(msg);
                        console.error(msg);
                        return;
                    }

                    loadScene(1).then(after);

                    dom_script_controls.name.value = this.script.name;
                    dom_script_controls.id.textContent = 'ID: '+this.script.id;

                    dom_script_controls.processRootPath.value = this.script.processRootPath;

                    [dom_sot_controls.selectize, dom_master_controls.selectize].map(s => {
                        s[0].selectize.clear();
                        s[0].selectize.clearOptions();
                        s[0].selectize.clearCache();
                    });

                    delete dom_sot_controls.elem.dataset.sotId;
                    dom_sot_controls.id.classList.add('d-none');

                    delete dom_master_controls.elem.dataset.masterId;
                    dom_master_controls.id.classList.add('d-none');

                    let finishEnrolling = (school)=> {
                        if (dom_sot_controls.selectize[0].selectize.options[school.id]) {
                            dom_sot_controls.selectize[0].selectize.updateOption(school.id, {id: school.id, name: school.name, path: school.path});
                        } else {
                            dom_sot_controls.selectize[0].selectize.addOption({id: school.id, name: school.name, path: school.path});
                        }
                        dom_sot_controls.selectize[0].selectize.setValue(school.id, true);
                        dom_sot_controls.selectize[0].selectize.refreshOptions(false);

                        dom_sot_controls.elem.dataset.sotId = school.id;
                        dom_sot_controls.id.textContent = 'ID: '+school.id;
                        dom_sot_controls.id.classList.remove('d-none');

                        dom_workspace_title.setTextContent();
                    }

                    Object.assign(this.script.player, script_.player);

                    dom_player_controls.frequency.value = this.script.player.frequency = this.script.player.frequency || SOUND_MIN_FREQUENCY;
                    dom_player_controls.intensity.value = this.script.player.intensity = this.script.player.intensity || 0.3;
                    dom_player_controls.intensityRange.value = this.script.player.intensity;
                    dom_sound_player.intensityRange.value = this.script.player.intensity;
                    dom_player_controls.duration.value = this.script.player.duration = this.script.player.duration || SOUND_DURATION_DEFAULT;

                    if (this.script.player.sotId) {
                        schools_map.get(this.script.player.sotId).then((school)=> {
                            this.simulation.curriculum = school;
                            this.script.school = school;

                            finishEnrolling(school);
                        }).catch((school)=> {
                            packager.dialog.alert('An error occured while enrolling in SOT, '+school.name+':'+school.id+'. The Public School will be used. Reload to try to reenter the school.');
                            this.simulation.curriculum = PUBLIC_SCHOOL;
                            this.script.school = PUBLIC_SCHOOL;

                            finishEnrolling(school);
                        });
                    }

                    if (this.script.player.masterId) {
                        let proc = await processes_map.get(this.script.player.masterId);
                        if (dom_master_controls.selectize[0].selectize.options[proc.id]) {
                            dom_master_controls.selectize[0].selectize.updateOption(proc.id, {id: proc.id, name: proc.name, path: proc.path});
                        } else {
                            dom_master_controls.selectize[0].selectize.addOption({id: proc.id, name: proc.name, path: proc.path});
                        }
                        dom_master_controls.selectize[0].selectize.setValue(proc.id, true);
                        dom_master_controls.selectize[0].selectize.refreshOptions(false);

                        dom_master_controls.elem.dataset.masterId = proc.id;
                        dom_master_controls.id.classList.remove('d-none');
                        dom_master_controls.id.textContent = 'ID: '+proc.id;

                        // dom_workspace_title.setTextContent();
                    }

                    dom_script_controls.meta.dateCreated.textContent = new Date(script_.creationTime).toLocaleString();
                    this.script.creationTime = script_.creationTime;
                    this.script.saveTime = script_.saveTime;

                    dom_script_controls.meta.lastSaveTime.textContent = new Date(script_.saveTime).toLocaleString();

                } catch (e) {
                    packager.dialog.alert('An error occured while reading your content. Reload/restart the window to try again. More info is in the console log.');
                    console.error(e);
                }
            }

            // TODO: Commented out for testing
            // dom_toolbar.save.dispatchEvent(new Event('click'));

            scriptsMenu_UI((script_id)=> {
                let loading = packager.dialog.loading('Loading script, '+script_id+'...');

                packager.generalLoader.work({
                    type: ITEM_TYPE_SCRIPT,
                    scriptId: script_id,
                    packager: litePackager,
                    resolve: (data)=> {
                        doLoad(data.item, ()=> { loading.closeAll() });
                    },
                    reject: (data)=> {
                        loading.close();
                        dom_overlay.scriptsMenu.cancel.dispatchEvent(new Event('click'));
                        packager.dialog.alert('An error occured while loading script with ID, '+script_id);
                        console.error(data.error);
                    }
                });
            });
        });

        let SAVING = false;
        const saveScript = async ()=> {
            let loading = packager.dialog.quietLoading('Saving...');

            SAVING = true;

            // This is redundant given that data is liekly saved
            // when the Memnat loses focus. This could be useful
            // when supporting key shortcuts.
            let t = await  themes_map.get(dom_theme_controls.elem.dataset.themeId),
                ctx = t ? t.memnat : null;
            if (ctx && ctx.saveData) ctx.saveData();

            // TODO: save script authors

            // trigger validation and definition of Player settings
            dom_player_controls.frequency.dispatchEvent(new Event('change'));
            dom_player_controls.intensity.dispatchEvent(new Event('change'));
            dom_player_controls.duration.dispatchEvent(new Event('change'));

            let script = {
                creationTime: this.script.data ? this.script.data.creationTime : this.script.creationTime,
                // saveTime: Date.now(),
                id: this.script.id,
                name: this.script.name,
                processRootPath: this.script.processRootPath,
                player: this.script.player
            }

            packager.generalPersister.work({
                type: ITEM_TYPE_SCRIPT,
                packager: litePackager,
                cud: CUD.drain(),
                script: script,
                lines: JSON.stringify(lines_map_, getCircularReplacer()),
                scenes: JSON.stringify(scenes_map_, getCircularReplacer()),
                currentScene: this.scene.id,
                resolve: (data)=> {
                    console.log(data);
                    let res = data.script;
                    dom_script_controls.meta.lastSaveTime.textContent = new Date(res.saveTime).toLocaleString();
                    this.script.data = res;
                    console.log(res);
                    SAVING = false;
                    loading.close();
                },
                reject: (data)=> {
                    let errmsg = 'An error occured while saving this script. Export a copy of it to be safe.';
                    console.error(data.error);
                    packager.dialog.alert(errmsg);
                    SAVING = false;
                    loading.close();
                }
            });

            CUD.reset();
        }

        dom_toolbar.save.addEventListener('click', () => saveScript());

        // Autosave
        // let autosave_intervalID = setInterval(()=> {
        //     if (SAVING || this.script.data) return;
        //     dom_toolbar.save.dispatchEvent(new Event('click'));
        // }, 120000 /* 2 mins */);

        dom_toolbar.export.addEventListener('click', ()=> {

            // dom_toolbar.save.dispatchEvent(new Event('click'));

            let loading = packager.dialog.quietLoading('Exporting script...');

            switch (packager.name) {
                case 'electron':
                    const { dialog } = packager.module.remote;
                    dialog.showSaveDialog({
                        title: 'Exports a copy of this script',
                        defaultPath: (this.script.name || this.script.id)+'.json'
                    }, (filename)=> {
                        if (!filename) {
                            loading.close();
                            return;
                        }
                        let thisData = JSON.stringify(this.script.data, null, 4);
                        packager.fs.writeFile(filename,thisData,(err,data) => {
                            loading.close();
                            if (err) console.error(err);
                        });
                    });
                    break;
                default:
                    download(JSON.stringify(this.script.data, null, 4),escape((this.script.name || this.script.id).replace(/ /g,"_")+'.json'),'text/json');
                    loading.close();
            }
        });

        dom_toolbar.scenesMenu.addEventListener('click', ()=> scenesMenu_UI());

        dom_toggle_controls.addEventListener('click', (e)=> {
            let didShow = toggleDisplay(dom_panel_player);
            if (didShow) {
                dom_toggle_controls.querySelector('[role="button"]').innerHTML = '&raquo;';
            } else {
                dom_toggle_controls.querySelector('[role="button"]').innerHTML = '&laquo;';
            }

            if (this.simulation.visualizer.state() === 'on') {
                this.simulation.visualizer.resize();
            }
        });

        // Exit
        switch (packager.name) {
            case 'electron':
                dom_toolbar.exit.addEventListener('click', ()=> {
                    packager.dialog.confirm('Confirm exit', (cf)=> {
                        if (cf) packager.module.remote.getCurrentWindow().close();
                    });
                });
                document.querySelectorAll('#main-wrapper button, #main-wrapper [role="button"], #main-wrapper input, #main-wrapper textarea').forEach(item => {
                    item.setAttribute('tabindex','-1');
                });
                break;
            default:
                dom_toolbar.exit.addEventListener('click', ()=> dom_toolbar.load.dispatchEvent(new Event('click')));
        }

        let loading_ = null;

        packager.dialog = {
            alert: (msg)=> {
                let opts = {
                    message: msg,
                    buttons: [vex.dialog.buttons.YES]
                };
                vex.dialog.alert(opts);
            },
            prompt: vex.dialog.prompt,
            confirm: (msg,cb)=> {
                vex.dialog.confirm({
                    message: msg,
                    callback: cb
                });
            },
            loading: (msg)=> {
                if (loading_) {
                    let dom_msg = loading_.modal.form.querySelector('.vex-dialog-message');
                    dom_msg.innerHTML+='<br>'+msg;
                    loading_.count++;
                } else {
                    loading_ = {
                        modal: vex.dialog.alert({
                            message: msg,
                            buttons: []
                        }),
                        count: 1
                    }
                }
                return {
                    message: packager.dialog.loading,
                    close: ()=> {
                        if (loading_) {
                            loading_.count--;
                            if (loading_.count < 1) {
                                loading_.modal.close();
                                loading_ = null;
                            }
                        }
                    },
                    closeAll: ()=> {
                        loading_.modal.close();
                        loading_ = null;
                    }
                }
            },
            quietLoading: (msg)=> {
                dom_quiet_alert.textContent = msg;
                dom_quiet_alert.classList.remove('d-none');

                return {
                    close: ()=> {
                        dom_quiet_alert.classList.add('d-none');
                    }
                }
            }
        }

        // FIX: selectize.js bootstrap compatibility
        const selectizeBSFix = ()=> {
            let cs = ['row','col','form-control'];
            for (let i = 1; i < 13; ++i) cs.push('col-'+i);
            document.querySelectorAll('.selectize-dropdown.form-control').forEach(e => {
                cs.map(c => e.classList.remove(c));
            });
            document.querySelectorAll('.selectize-input input').forEach(e => {
                let style = e.parentNode.parentNode.style.border;
                e.addEventListener('focus', ()=> e.parentNode.parentNode.style.border = '1px solid rgb(40,40,40)');
                e.addEventListener('blur', ()=> e.parentNode.parentNode.style.border = style);
            });

        }
        selectizeBSFix();

        let testBegin = ()=> {

            // New script setup
            addLine(null,null).then(line => line.domNumber().dispatchEvent(new Event('click')));

            /// TESTING (QUICKSTART)
            let tentan = new Entity('tentan-14563246325363','Tentan Mise'),
                gobacho = new Entity('gobacho-3184362528325', 'Gobacho');
            let proc_laulo = new Process('002D01B26DA3DE8EA9BACECB3B7B779D','Laulo Emesanry','gobacho/laulo_emesanry.js'),
                proc_cannan = new Process('ED0F88978FC5282C5763C9792BB533EA','Canaan','http://localhost:40/processes/love_phi/philos_adelphos/canaan.js'),
                proc_jogesh = new Process('VGRJ437N356D08F5HI843NVPS84SR3N3','Jogesh Pombeal','/outernationalist/jogesh_pombeal.js');
            entities_map.set(tentan.id, tentan);
            entities_map.set(gobacho.id, gobacho);
            processes_map.set(proc_laulo.id,proc_laulo);
            processes_map.set(proc_cannan.id,proc_cannan);
            processes_map.set(proc_jogesh.id,proc_jogesh);

            let asps = [
                new M.Aspect({
                    id: '3C38F6D5C5E401A231B9F19F354606DC',
                    name: "Albenetho",
                    abbr: "Ab",
                    type: M.ASPECT_TYPE_PURE,
                    frequencies: [0,0,0],
                    onHighlight: updateAspectControls
                }),
                new M.Aspect({
                    id: 'A1FA4A050FC8AF97F8C436FF8B70EC94',
                    name: "Boudwarpol",
                    abbr: "Bd",
                    type: M.ASPECT_TYPE_PURE,
                    frequencies: [0,0,0],
                    onHighlight: updateAspectControls
                }),
                new M.Aspect({
                    id: 'F37E02501082D7C6029BC89DAE33797B',
                    name: "Csacerex",
                    abbr: "Cx",
                    type: M.ASPECT_TYPE_PURE,
                    frequencies: [0,0,0],
                    onHighlight: updateAspectControls
                }),
                new M.Aspect({
                    id: 'DE6B53D546AAF83041CA02B79898DEA7',
                    name: "Dipghatm",
                    abbr: "Dg",
                    type: M.ASPECT_TYPE_PURE,
                    frequencies: [0,0,0],
                    onHighlight: updateAspectControls
                }),
                new M.Aspect({
                    id: 'AC2A1CB7CD54BAB9EAE8F49897873078',
                    name: "Efenemgri",
                    abbr: "Ef",
                    type: M.ASPECT_TYPE_PURE,
                    frequencies: [0,0,0],
                    onHighlight: updateAspectControls
                }),
                new M.Aspect({
                    id: '89F0CEB04980B7D5B51C5B98C982A100',
                    name: "Freggomola",
                    abbr: "Fg",
                    type: M.ASPECT_TYPE_PURE,
                    frequencies: [0,0,0],
                    onHighlight: updateAspectControls
                })
            ];

            let vaechorum = new Shell("Vaechorum", "Vaechorum", asps.map(a => a.id));
            shells_map.set(vaechorum.id, vaechorum);

            for (let i = 0; i < asps.length; i++) {
                aspects_map.set(asps[i].id, new Aspect(asps[i]));
            }

            addLine(null,null).then(async (line) => {
                await addTheme({
                    line: line,
                    id: '43D92510ACC2AC0DD7474B70408C61A9',
                    name: 'Nenthi',
                    type: M.THEME_TYPE_CONTEXT,
                    shellId: vaechorum.id
                });
                await addTheme({
                    line: line,
                    id: '61F8C999DB75AB5FF951F6AD15FBBB9D',
                    name: 'Drosht',
                    type: M.THEME_TYPE_SIGNAL,
                    shellId: vaechorum.id
                });
                await addTheme({
                    line: line,
                    id: 'CA576B2C69BCEE60B308466C7FB210EB',
                    name: 'Hiben',
                    type: M.THEME_TYPE_VERSUS,
                    shellId: vaechorum.id
                });
                await addTheme({
                    line: line,
                    id: '2DAD2F805F1994E6421A4F1F48B94D4A',
                    name: 'Turuimagji',
                    type: M.THEME_TYPE_CONTEXT,
                    shellId: vaechorum.id
                });
            });
        }

        let COPIED_SHELL_ID = null,
            COPIED_THEME_ID = null,
            COPIED_ENTITY_ID = null;
        let THEME_STEP_VALUE = 0.05;

        let loading = packager.dialog.loading('Preparing...');

        let res = null;

        // Main preparations
        switch (packager.name) {
            case 'electron':
                packager.module = require('electron');
                const { app } = packager.module.remote;
                const { webFrame } = packager.module;
                const homeDir = require('os').homedir();
                const fs = require('fs');
                const request = require('request');
                const path = require('path');
                packager.fs = fs;
                packager.request = request;
                packager.path = path;
                packager.app = app;
                webFrame.setVisualZoomLevelLimits(1,1);
                webFrame.setLayoutZoomLevelLimits(0,0);
                let ms_dir = path.join(homeDir, 'Documents','/Memnat Scripting'),
                    asps_dir = path.join(ms_dir, '/aspects'),
                    shells_dir = path.join(ms_dir, '/shells'),
                    themes_dir = path.join(ms_dir, '/themes'),
                    ents_dir = path.join(ms_dir, '/entities'),
                    procs_dir = path.join(ms_dir, '/processes'),
                    lines_dir = path.join(ms_dir, '/lines'),
                    scenes_dir = path.join(ms_dir, '/scenes'),
                    scripts_dir = path.join(ms_dir, '/scripts'),
                    scripts_file = path.join(ms_dir, 'scripts.json');
                packager.memnatDir = ms_dir;
                packager.aspectsDir = asps_dir;
                packager.shellsDir = shells_dir;
                packager.themesDir = themes_dir;
                packager.entitiesDir = ents_dir;
                packager.processesDir = procs_dir;
                packager.linesDir = lines_dir;
                packager.scenesDir = scenes_dir;
                packager.scriptsDir = scripts_dir;
                packager.scriptsFile = scripts_file;
                [ ms_dir
                , asps_dir
                , shells_dir
                , themes_dir
                , ents_dir
                , procs_dir
                , lines_dir
                , scenes_dir
                , scripts_dir].map(p => { if (!fs.existsSync(p)) fs.mkdirSync(p) });
                if (!fs.existsSync(scripts_file)) fs.writeFileSync(scripts_file,JSON.stringify({},null,4),{encoding:'utf-8'});
                packager.PERSIST_WORKER_PATH = litePackager.PERSIST_WORKER_PATH = '../scripting.html/js/PersistWorker.js';
                packager.LOAD_WORKER_PATH = litePackager.LOAD_WORKER_PATH = '../scripting.html/js/LoadWorker.js';
                packager.WAVE_WORKER_PATH = litePackager.WAVE_WORKER_PATH = '../scripting.html/js/WaveWorker.js';

                litePackager.memnatDir = packager.memnatDir;
                litePackager.aspectsDir = packager.aspectsDir;
                litePackager.shellsDir = packager.shellsDir;
                litePackager.themesDir = packager.themesDir;
                litePackager.entitiesDir = packager.entitiesDir;
                litePackager.processesDir = packager.processesDir;
                litePackager.linesDir = packager.linesDir;
                litePackager.scenesDir = packager.scenesDir;
                litePackager.scriptsDir = packager.scriptsDir;
                litePackager.scriptsFile = packager.scriptsFile;

                //testBegin();
                break;
            default:

                let dbexists = await Dexie.exists('memnat'),
                    memnat = new Dexie('memnat');

                // if (!dbexists) {
                    // memnat.close();
                    // memnat.version(1).stores({
                    //     aspects: 'id,saveTime,creationTime,abbr,name,type,*freqs',
                    //     themes: 'id,saveTime,creationTime,name,position,shellId,diversity,quantum,squat,duration,processId,*intensities,*durations,*processIds,type,depth,width',
                    //     shells: 'id,saveTime,creationTime,name,*aspectIds',
                    //     entities: 'id,saveTime,creationTime,name',
                    //     processes: 'id,saveTime,creationTime,name,path',
                    //     lines: 'id,saveTime,creationTime,number,*themeIds,entityId',
                    //     scenes: 'id,saveTime,creationTime,name,position,*lineIds',
                    //     scripts_lite: 'id,saveTime,creationTime,name',
                    //     scripts: 'id,saveTime,creationTime,name,processRootPath,*sceneIds'
                    // });
                    memnat.version(2).stores({
                        aspects: 'id,saveTime,creationTime,abbr,name,type,*freqs,cover',
                        themes: 'id,saveTime,creationTime,name,position,shellId,diversity,quantum,squat,duration,processId,*intensities,*durations,*processIds,type,depth,width',
                        shells: 'id,saveTime,creationTime,name,*aspectIds',
                        entities: 'id,saveTime,creationTime,name',
                        processes: 'id,saveTime,creationTime,name,path',
                        schools: 'id,saveTime,creationTime,name,path',
                        lines: 'id,saveTime,creationTime,number,*themeIds,entityId',
                        scenes: 'id,saveTime,creationTime,name,position,*lineIds',
                        scripts_lite: 'id,saveTime,creationTime,name',
                        scripts: 'id,saveTime,creationTime,name,player,processRootPath,*sceneIds'
                    });
                // }

                await memnat.open();
				
				// memnat.transaction('rw', memnat.table('aspects'), async ()=> {
					// let asps = memnat.table('aspects');
					// await asps.each(async (asp, c)=> {
						// if (asp.frequencies == null) {
							// await asps.update(asp.id, {frequencies: [getRandomNumber(200, 3000)]});
						// }
					// });
				// }).then(()=> {
					// console.log("done");
				// }).catch((err)=> {
					// console.error(err);
				// });

                packager.module = memnat;
                packager.module.name = 'memnat';
                packager.PERSIST_WORKER_PATH = litePackager.PERSIST_WORKER_PATH = '../js/PersistWorker.js';
                packager.LOAD_WORKER_PATH = litePackager.LOAD_WORKER_PATH = '../js/LoadWorker.js';
                packager.WAVE_WORKER_PATH = litePackager.WAVE_WORKER_PATH = '../js/WaveWorker.js';

                litePackager.module = { name: packager.module.name }

                // testBegin();
        }

        // Setup loader and persist workers

        packager.aspectsLoader = { workBag: {} };
        packager.themesLoader = { workBag: {} };
        packager.shellsLoader = { workBag: {} };
        packager.entitiesLoader = { workBag: {} };
        packager.processesLoader = { workBag: {} };
        packager.generalLoader = { workBag: {} };

        packager.generalPersister = { workBag: {} };

        let aspects_loader = new Worker(packager.LOAD_WORKER_PATH);
        aspects_loader.onmessage = (e)=> {
            let data = e.data,
                type = data.type;

            let [resolve,reject] = packager.aspectsLoader.workBag[data.workId];
            delete packager.aspectsLoader.workBag[data.workId];

            switch (type) {
                case 'done':
                    let res = data.item,
                        masp = new M.Aspect({
                            id: res.id,
                            name: res.name,
                            abbr: res.abbr,
                            type: res.type,
                            frequencies: res.frequencies,
                            cover: res.cover,
                            onHighlight: updateAspectControls
                        });
                    let asp = new Aspect(masp);
                    asp.creationTime = res.creationTime;
                    asp.saveTime = res.saveTime;
                    if (resolve) resolve(asp);
                    break;
                case 'error':
                    console.error(data.error);
                    if (reject) reject(data.error);
                    break;
                default:

            }
        }

        let themes_loader = new Worker(packager.LOAD_WORKER_PATH);
        themes_loader.onmessage = (e)=> {
            let data = e.data,
                type = data.type;

            let [resolve,reject] = packager.themesLoader.workBag[data.workId];
            delete packager.themesLoader.workBag[data.workId];

            switch (type) {
                case 'done':
                    let res = data.item,
                        mtheme = new M.Theme({
                                id: res.id,
                                name: res.name,
                                shellId: res.shellId,
                                type: res.type,
                                diversity: res.diversity,
                                meaning: res.meaning,
                                duration: res.duration,
                                quantum: res.quantum,
                                squat: res.squat,
                                processId: res.processId,
                                intensities: res.intensities,
                                durations: res.durations,
                                processIds: res.processIds,
                                width: res.width,
                                depth: res.depth
                            }),
                        theme = new Theme(mtheme);
                    mtheme.onSelect = (clk)=> onThemeSelect(theme, clk);
                    mtheme.onMeaningChange = (d)=> onMeaningChange(theme, d);
                    mtheme.onMeaningCommit = (d)=> onMeaningCommit(theme, d);

                    theme.creationTime = res.creationTime
                    theme.saveTime = res.saveTime;
                    theme.position = res.position;

                    if(resolve) resolve(theme);

                    break;
                case 'error':
                    console.error(data.error);
                    if(reject) reject(data.error);
                    break;
                default:

            }

        }

        let shells_loader = new Worker(packager.LOAD_WORKER_PATH);
        shells_loader.onmessage = async (e)=> {
            let data = e.data,
                type = data.type;

            let [resolve,reject] = packager.shellsLoader.workBag[data.workId];
            delete packager.shellsLoader.workBag[data.workId];

            switch (type) {
                case 'done':
                    let res = data.item;
                    let shell = new Shell(res.id,res.name,res.aspectIds);
                    shell.creationTime = res.creationTime;
                    shell.saveTime = res.saveTime;
                    if(resolve) resolve(shell);
                    break;
                case 'error':
                    console.error(data.error);
                    if(reject) reject(data.error);
                    break;
                default:

            }
        }

        let entities_loader = new Worker(packager.LOAD_WORKER_PATH);
        entities_loader.onmessage = async (e)=> {
            let data = e.data,
                type = data.type;

            let [resolve,reject] = packager.entitiesLoader.workBag[data.workId];
            delete packager.entitiesLoader.workBag[data.workId];

            switch (type) {
                case 'done':
                    let res = data.item;
                    let ent = new Entity(res.id,res.name);
                    ent.creationTime = res.creationTime;
                    ent.saveTime = res.saveTime;
                    if (resolve) resolve(ent);
                    break;
                case 'error':
                    console.error(data.error);
                    if(reject) reject(data.error);
                    break;
                default:

            }

        }

        let processes_loader = new Worker(packager.LOAD_WORKER_PATH);
        processes_loader.onmessage = async (e)=> {
            let data = e.data,
                type = data.type;

            let [resolve,reject] = packager.processesLoader.workBag[data.workId];
            delete packager.processesLoader.workBag[data.workId];

            switch (type) {
                case 'done':
                    let res = data.item;
                    let proc = new Process(res.id,res.name,res.path);
                    proc.creationTime = res.creationTime;
                    proc.saveTime = res.saveTime;
                    if (resolve) resolve(proc);
                    break;
                case 'error':
                    console.error(data.error);
                    if (reject) reject(data.error);
                    break;
                default:

            }

        }

        let general_loader = new Worker(packager.LOAD_WORKER_PATH);
        general_loader.onmessage = async (e)=> {
            let data = e.data,
                type = data.type;

            let [resolve,reject] = packager.generalLoader.workBag[data.workId];
            delete packager.generalLoader.workBag[data.workId];

            switch (type) {
                case 'done':
                    delete data.workId;

                    if (resolve) resolve(data);
                    break;
                case 'error':
                    if (reject) reject(data);
                    break;
                default:

            }

        }

        let general_persister = new Worker(packager.PERSIST_WORKER_PATH);
        general_persister.onmessage = async (e)=> {
            let data = e.data,
                type = data.type;

            let [resolve,reject] = packager.generalPersister.workBag[data.workId];
            delete packager.generalPersister.workBag[data.workId];

            switch (type) {
                case 'done':
                    delete data.workId;

                    if (resolve) resolve(data);
                    break;
                case 'error':
                    if (reject) reject(data);
                    break;
                default:

            }

        }

        [
            [packager.aspectsLoader, ITEM_TYPE_ASPECT, aspects_loader],
            [packager.themesLoader, ITEM_TYPE_THEME, themes_loader],
            [packager.shellsLoader, ITEM_TYPE_SHELL, shells_loader],
            [packager.entitiesLoader, ITEM_TYPE_ENTITY, entities_loader],
            [packager.processesLoader, ITEM_TYPE_PROCESS, processes_loader],
            [packager.generalLoader, null, general_loader],
            [packager.generalPersister, null, general_persister]

        ].map(([pworker,type,worker])=> {
            pworker.work = (data)=> {
                let entry = [data.resolve, data.reject];
                delete data.resolve;
                delete data.reject;

                let workId = guid();

                pworker.workBag[workId] = entry;

                data.workId = workId;
                data.type = data.type || type;

                worker.postMessage(data);
            }
        });

        (function setupKeyBindings() {
            // const Mousetrap = require('mousetrap');

            // From Mousetrap source:
            Mousetrap.prototype.stopCallback = function(e, element) {
                var self = this;

                function _belongsTo(element, ancestor) {
                    if (element === null || element === document) {
                        return false;
                    }

                    if (element === ancestor) {
                        return true;
                    }

                    return _belongsTo(element.parentNode, ancestor);
                }

                // if the element has the class "mousetrap" then no need to stop
                if ((' ' + element.className + ' ').indexOf(' mousetrap ') > -1) {
                    return false;
                }

                if (_belongsTo(element, self.target)) {
                    return false;
                }

                let esc = e.key === 'Escape';
                if (esc && (element.tagName == 'INPUT' || element.tagName == 'SELECT' || element.tagName == 'TEXTAREA' || element.isContentEditable)) element.blur();

                // stop for input, select, and textarea and if overlay or dialog is active (except for ESC key)
                return element.tagName == 'INPUT' || element.tagName == 'SELECT' || element.tagName == 'TEXTAREA' || element.isContentEditable || (!esc && (!document.getElementById('overlay').classList.contains('d-none') || document.body.classList.contains('vex-open')));
            }

            let hotkeys = [
                'command+r',
                'ctrl+r',
                'command+a',
                'ctrl+a',
                'command+shift+=',
                'ctrl+shift+=',
                'command+-',
                'ctrl+-',
                'command+w',
                'ctrl+w'
            ];
            Mousetrap.bind(hotkeys, (e)=> {
                return false;
            });
            // Refresh (dev)
            Mousetrap.bind(['command r e s t a r t','ctrl r e s t a r t'], ()=> {
                window.location.reload();
                return false;
            });
            // Create
            Mousetrap.bind(['command+shift+a','ctrl+shift+a'], ()=> {
                dom_overlay.close.dispatchEvent(new Event('click'));
                dom_toolbar.create.aspects.dispatchEvent(new Event('click'));
                return false;
            });
            Mousetrap.bind(['command+shift+s','ctrl+shift+s'], ()=> {
                dom_overlay.close.dispatchEvent(new Event('click'));
                dom_toolbar.create.shell.dispatchEvent(new Event('click'));
                return false;
            });
            Mousetrap.bind(['command+shift+t','ctrl+shift+t'], ()=> {
                dom_overlay.close.dispatchEvent(new Event('click'));
                let line = lines_map.get(Number.parseInt(dom_line_controls.elem.dataset.lineNumber));
                if (!line) return false;
                line.domAddTheme().dispatchEvent(new Event('click'));
                return false;
            });
            Mousetrap.bind(['command+shift+e','ctrl+shift+e'], ()=> {
                dom_overlay.close.dispatchEvent(new Event('click'));
                dom_toolbar.create.entities.dispatchEvent(new Event('click'));
                return false;
            });
            Mousetrap.bind(['command+l','ctrl+l'], ()=> {
                let ln = Number.parseInt(dom_line_controls.elem.dataset.lineNumber),
                    oline = ln ? lines_map.get(ln) : null,
                    target = oline ? oline.domAppend() : null;
                addLine(false,target).then((line)=> {
                    line.domNumber().dispatchEvent(new Event('click'));
                });
                return false;
            });
            Mousetrap.bind(['command+shift+l','ctrl+shift+l'], ()=> {
                let ln = Number.parseInt(dom_line_controls.elem.dataset.lineNumber),
                    oline = ln ? lines_map.get(ln) : null,
                    target = oline ? oline.domPrepend() : null;
                addLine(true,target).then((line)=> {
                    line.domNumber().dispatchEvent(new Event('click'));
                });
                return false;
            });
            Mousetrap.bind(['ctrl+n', 'command+n'], ()=> {
                dom_toolbar.create.scene.dispatchEvent(new Event('click'));
                return false;
            });
            Mousetrap.bind(['ctrl+shift+n', 'command+shift+n'], ()=> {
                switch (packager.name) {
                    case 'electron':
                        packager.module.ipcRenderer.send('new-window');
                        break;
                    default:
                        dom_toolbar.create.script.dispatchEvent(new Event('click'));
                }
                return false;
            });
            // Copy item
            // Mousetrap.bind(['command+c','ctrl+c'], ()=> {
            //     let themeId = dom_theme_controls.elem.dataset.themeId,
            //         shellId = dom_shell_controls.elem.dataset.shellId,
            //         entityId = dom_entity_controls.elem.dataset.entityId;
            //
            //     if (themeId) {
            //         let theme = themes_map.get_(themeId);
            //         if (theme._theme.type === M.THEME_TYPE_MOD_QUAD) return;
            //         COPIED_THEME_ID = themeId;
            //     } else if (shellId) {
            //         COPIED_SHELL_ID =  shellId
            //     } else if (entityId) {
            //         COPIED_ENTITY_ID = entityId;
            //     }
            //     return false;
            // });
            // Copy theme
            Mousetrap.bind(['command+c t','ctrl+c t'], ()=> {
                let themeId = dom_theme_controls.elem.dataset.themeId;

                if (themeId) {
                    let theme = themes_map.get_(themeId);
                    if (theme._theme.type === M.THEME_TYPE_MOD_QUAD) return;
                    COPIED_THEME_ID = themeId;
                }
                return false;
            });
            // Copy shell
            Mousetrap.bind(['command+c s','ctrl+c s'], ()=> {
                let shellId = dom_shell_controls.elem.dataset.shellId;

                if (shellId) COPIED_SHELL_ID = shellId;
                return false;
            });
            // Copy entity
            Mousetrap.bind(['command+c e','ctrl+c e'], ()=> {
                let entityId = dom_shell_controls.elem.dataset.entityId;

                if (entityId) COPIED_ENTITY_ID = entityId;
                return false;
            });
            // Create theme from copied theme
            Mousetrap.bind(['command+v t','ctrl+v t'], ()=> {
                let theme = themes_map.get_(COPIED_THEME_ID),
                    line = lines_map.get(Number.parseInt(dom_line_controls.elem.dataset.lineNumber));
                if (!line || !theme)  return false;
                let title = 'Context';
                switch (theme._theme.type) {
                    case M.THEME_TYPE_SIGNAL:
                        title = 'Signal';
                        break;
                    case M.THEME_TYPE_VERSUS:
                        title = 'Versus';
                        break;
                    case M.THEME_TYPE_TEXT:
                        title = 'Text';
                        break;
                    default:
                }
                packager.dialog.prompt({
                    message: '[Paste theme] Name the new '+title+' theme:',
                    callback: (name)=> {
                        if (!name) return;
                        // NOTE: set theme
                        addTheme({
                            line: line,

                            id: guid(),
                            name: name,
                            shellId: theme._theme.shellId,
                            diversity: theme._theme.diversity,
                            meaning: theme._theme.meaning,
                            duration: theme._theme.duration,
                            processId: theme._theme.processId,
                            intensities: theme._theme.intensities(),
                            durations: theme._theme.durations(),
                            processIds: theme._theme.processIds(),
                            type: theme._theme.type,
                            onSelect: theme._theme.onSelect,
                            onMeaningChange: theme._theme.onMeaningChange
                        });
                    }
                });
                return false;
            });
            // Create theme from copied shell
            [   ['c','Context',M.THEME_TYPE_CONTEXT],
                ['s','Signal',M.THEME_TYPE_SIGNAL],
                ['v','Versus',M.THEME_TYPE_VERSUS]
            ].map(info => {
                let key = info[0],
                    title = info[1],
                    type = info[2];
                Mousetrap.bind(['command+option+'+key,'ctrl+alt+'+key], ()=> {
                    let shell = shells_map.get_(COPIED_SHELL_ID);
                    let line = lines_map.get(Number.parseInt(dom_line_controls.elem.dataset.lineNumber));
                    if (!shell || !line) return false;
                    packager.dialog.prompt({
                        message: 'Name the new '+title+ ' theme:',
                        callback: (name)=> {
                            if (!name) return;
                            addTheme({
                                line: line,
                                name: name,
                                type: type,
                                shellId: COPIED_SHELL_ID
                            });
                        }
                    });
                    return false;
                });
            });
            // Create text theme
            Mousetrap.bind(['command+option+t','ctrl+alt+t'], ()=> {
                let ln = Number.parseInt(dom_line_controls.elem.dataset.lineNumber);
                if (!ln) return false;
                let line = lines_map.get(ln);
                packager.dialog.prompt({
                    message: 'Name the Text theme',
                    callback: (name)=> {
                        if (!name) return;
                        addTheme({
                            line: line,
                            name: name,
                            type: M.THEME_TYPE_TEXT
                        });
                    }
                });
                return false;
            });
            // Set copied entity for line
            Mousetrap.bind(['command+v e','ctrl+v e'], ()=> {
                let line = lines_map.get(Number.parseInt(dom_line_controls.elem.dataset.lineNumber)),
                    entity = entities_map.get_(COPIED_ENTITY_ID);
                if (!line || !entity) return false;
                line.domNumber().dispatchEvent(new Event('click'));
                if (dom_entity_controls.selectize[0].selectize.options[entity.id]) {
                    dom_entity_controls.selectize[0].selectize.updateOption(entity.id, {id: entity.id, name: entity.name});
                } else {
                    dom_entity_controls.selectize[0].selectize.addOption({id: entity.id, name: entity.name});
                }
                dom_entity_controls.selectize[0].selectize.setValue(entity.id, true);
                dom_entity_controls.selectize[0].selectize.refreshOptions(false);
                return false;
            });
            // Change entity name
            Mousetrap.bind('shift+e', ()=> {
                let line = lines_map.get(Number.parseInt(dom_line_controls.elem.dataset.lineNumber));
                if (!line || !line.entity) return false;
                line.domEntity().dispatchEvent(new Event('dblclick'));
                return false;
            });
            // Goto line
            Mousetrap.bind(['ctrl+g', 'command+g'], ()=> {
                packager.dialog.prompt({
                    message: 'Enter the line number',
                    callback: (ln_)=> {
                        let ln = Number.parseInt(ln_);
                        if (!ln) return;
                        dom_toolbar.goto.currentLine.value = ln;
                        dom_toolbar.goto.submit.dispatchEvent(new Event('click'));
                    }
                })
                return false;
            });
            // Save
            Mousetrap.bind(['command+s','ctrl+s'], ()=> {
                dom_toolbar.save.dispatchEvent(new Event('click'));
                return false;
            });
            // Load
            Mousetrap.bind(['command+o','ctrl+o'], ()=> {
                dom_toolbar.load.dispatchEvent(new Event('click'));
                return false;
            });
            // position theme
            Mousetrap.bind(['ctrl+shift+left', 'command+shift+left'], ()=> {
                let tid = dom_theme_controls.elem.dataset.themeId;
                if (!tid) return false;
                let theme = themes_map.get_(tid);
                repositionTheme(theme, theme.position-1);
                return false;
            });
            Mousetrap.bind(['ctrl+shift+right', 'command+shift+right'], ()=> {
                let tid = dom_theme_controls.elem.dataset.themeId;
                if (!tid) return false;
                let theme = themes_map.get_(tid);
                repositionTheme(theme, theme.position+1);
                return false;
            });
            // position line
            Mousetrap.bind(['ctrl+shift+up', 'command+shift+up'], ()=> {
                let ln = Number.parseInt(dom_line_controls.elem.dataset.lineNumber);
                if (!ln) return false;
                let line = lines_map.get(ln);
                repositionLine(line, line.number-1);
                return false;
            });
            Mousetrap.bind(['ctrl+shift+down', 'command+shift+down'], ()=> {
                let ln = Number.parseInt(dom_line_controls.elem.dataset.lineNumber);
                if (!ln) return false;
                let line = lines_map.get(ln);
                repositionLine(line, line.number+1);
                return false;
            });
            // Select shell in a line
            Mousetrap.bind('s', ()=> {
                let tid = dom_theme_controls.elem.dataset.themeId;
                if (!tid) return false;
                let theme = themes_map.get_(tid),
                    type = theme._theme.type;
                if (type === M.THEME_TYPE_CONTEXT || type === M.THEME_TYPE_SIGNAL || type === M.THEME_TYPE_VERSUS) {
                    onThemeSelect(theme,M.CLICKED_ON_SHELL_TITLE);
                }
                return false;
            });
            // Select mod-quad in a line
            Mousetrap.bind('m', ()=> {
                let ln = Number.parseInt(dom_line_controls.elem.dataset.lineNumber);
                if (!ln) return false;
                let line = lines_map.get(ln);
                if (line.modQuad) onThemeSelect(line.modQuad.theme,M.CLICKED_ON_CANVAS);
                return false;
            });
            // Select entity in a line/ Activate Entity Controls
            Mousetrap.bind('e', ()=> {
                let line = lines_map.get(Number.parseInt(dom_line_controls.elem.dataset.lineNumber));
                if (!line || !line.entity) return false;
                line.domEntity().dispatchEvent(new Event('click'));
                return false;
            });
            // Move through themes in a line
            Mousetrap.bind(['tab','ctrl+right','command+right'], ()=> {
                let ln = Number.parseInt(dom_line_controls.elem.dataset.lineNumber);
                if (!ln) return false;
                let line = lines_map.get(ln),
                    tid = dom_theme_controls.elem.dataset.themeId;
                let theme = themes_map.get_(tid) || line.themes[0];
                if (!theme) return false;
                let idx = tid ? theme.position : 0,
                    nt = line.themes[idx%line.themes.length];
                onThemeSelect(nt,M.CLICKED_ON_CANVAS);
                return false;
            });
            Mousetrap.bind(['shift+tab','ctrl+left','command+left'], ()=> {
                let ln = Number.parseInt(dom_line_controls.elem.dataset.lineNumber);
                if (!ln) return false;
                let line = lines_map.get(ln),
                    tid = dom_theme_controls.elem.dataset.themeId;
                let theme = themes_map.get_(tid) || line.themes[0];
                if (!theme) return false;
                let idx = tid ? theme.position-2 : 0;
                if (idx < 1) idx = line.themes.length+idx;
                let nt = line.themes[idx%line.themes.length];
                onThemeSelect(nt,M.CLICKED_ON_CANVAS);
                return false;
            });
            // Move through lines
            Mousetrap.bind(['ctrl+up', 'command+up'], ()=> {
                let line = lines_map.get(Number.parseInt(dom_line_controls.elem.dataset.lineNumber)-1) || lines_map.get(1);
                if (!line) return false;
                line.domNumber().dispatchEvent(new Event('click'));
                return false;
            });
            Mousetrap.bind(['ctrl+down', 'command+down'], ()=> {
                let line = lines_map.get(Number.parseInt(dom_line_controls.elem.dataset.lineNumber)+1) || lines_map.get(1);
                if (!line) return false;
                line.domNumber().dispatchEvent(new Event('click'));
                return false;
            });
            // Select node
            [1,2,3,4,5,6].map(num => {

                // Highlight through aspects
                Mousetrap.bind([''+num], ()=> {
                    let tid = dom_theme_controls.elem.dataset.themeId;
                    if (!tid) return false;
                    let theme = themes_map.get_(tid);
                    if (theme.memnat.highlightAspect) theme.memnat.highlightAspect(num-1);
                    return false;
                });
                // Primary select through aspects
                Mousetrap.bind(['command+'+num,'ctrl+'+num], ()=> {
                    let tid = dom_theme_controls.elem.dataset.themeId;
                    if (!tid) return false;
                    let theme = themes_map.get_(tid);
                    if (theme.memnat.selectAspect) theme.memnat.selectAspect(num-1,true);
                    return false;
                });
                // Secondary select through aspects
                Mousetrap.bind(['command+shift+'+num,'ctrl+shift+'+num], ()=> {
                    let tid = dom_theme_controls.elem.dataset.themeId;
                    if (!tid) return false;
                    let theme = themes_map.get_(tid);
                    if (theme.memnat.selectAspect) theme.memnat.selectAspect(num-1);
                    return false;
                });

                // Set arrow from node to other node
                [
                    ['-',M.ARROW_TYPE_SIMILARITY],
                    ['shift+.',M.ARROW_TYPE_CAUSATION],
                    ['shift+\\',M.ARROW_TYPE_DEPENDENCE],
                    [']',M.ARROW_TYPE_SPECTRUM]
                ].map(arr => {
                    Mousetrap.bind(['option+'+num+' '+arr[0],'alt+'+num+' '+arr[0]], (e)=> {
                        let theme = themes_map.get_(dom_theme_controls.elem.dataset.themeId),
                            aidx = Number.parseInt(dom_aspect_controls.elem.dataset.aspectIndex),
                            oaidx = num-1;
                        if (!theme || !aidx || aidx === oaidx) return false;
                        switch (theme._theme.type) {
                            case M.THEME_TYPE_CONTEXT:
                                theme.memnat.setArrow(aidx,oaidx,arr[1],true);
                                break;
                            case M.THEME_TYPE_VERSUS:
                                break;
                            default:
                        }
                        return false;
                    });
                })
            });
            // Change node or edge value
            Mousetrap.bind(['cmd+=','ctrl+='], ()=> {
                let tid = dom_theme_controls.elem.dataset.themeId;
                if (!tid) return false;
                let theme = themes_map.get_(tid);
                switch (theme._theme.type) {
                    case M.THEME_TYPE_CONTEXT:
                    case M.THEME_TYPE_VERSUS:
                    case M.THEME_TYPE_MOD_QUAD:
                        theme._theme.setMeaningValue(null,null,THEME_STEP_VALUE);
                        break;
                    case M.THEME_TYPE_SIGNAL:
                        theme._theme.setMeaningValue(null,THEME_STEP_VALUE);
                        break;
                    default:
                }
                return false;
            });
            Mousetrap.bind(['cmd+-','ctrl+-'], ()=> {
                let tid = dom_theme_controls.elem.dataset.themeId;
                if (!tid) return false;
                let theme = themes_map.get_(tid);
                switch (theme._theme.type) {
                    case M.THEME_TYPE_CONTEXT:
                    case M.THEME_TYPE_VERSUS:
                    case M.THEME_TYPE_MOD_QUAD:
                        theme._theme.setMeaningValue(null,null,-THEME_STEP_VALUE);
                        break;
                    case M.THEME_TYPE_SIGNAL:
                        theme._theme.setMeaningValue(null,-THEME_STEP_VALUE);
                        break;
                    default:
                }
                return false;
            });
            // Set THEME_STEP_VALUE value for node/edge value change
            Mousetrap.bind(['shift+z'], ()=> {
                packager.dialog.prompt({
                    message: 'Enter a step value:',
                    callback: (v_)=> {
                        let v = Number.parseFloat(v_);
                        if (!v) return;
                        THEME_STEP_VALUE = Math.min(1, Math.max(0, Math.abs(v)));
                    }
                })
                return false;
            });
            // Toggle controls
            Mousetrap.bind(['command+\\','ctrl+\\'], ()=> {
                dom_toggle_controls.dispatchEvent(new Event('click'));
                return false;
            });
            // ESC
            Mousetrap.bind('esc', ()=> {
                if (!dom_overlay.elem.classList.contains('d-none')){
                    dom_overlay.close.dispatchEvent(new Event('click'));
                    return false;
                }
                let tid = dom_theme_controls.elem.dataset.themeId;
                if (tid) {
                    let theme = themes_map.get_(tid);
                    if (theme._theme.type === M.THEME_TYPE_TEXT) {
                        theme.memnat.saveData(true);
                        return false;
                    }
                }
            });
            // Keyboard shortcuts
            let kbs_UI
            Mousetrap.bind('?', (e)=> {
                keyboardShortcuts_UI();
                return false;
            });
            // Close window
            Mousetrap.bind(['ctrl+w', 'command+w'], ()=> {
                dom_toolbar.exit.dispatchEvent(new Event('click'));
                return false;
            });
        })();

        addLine();

        let journey;
        themes_map.get('4BADA6000B75C0DD7A4EF57C31B996AE').then(async (t) => {
            journey = await getJourney({id:'ajourney', voice: t, type:VOICE_TYPE_THEME });
            console.log(journey);
            window.journey = journey;
        });

        loading.closeAll();

        this.initialized = true;
    } // EOF judal()
}
