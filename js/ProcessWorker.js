importScripts('constants.js', 'utils.js');

// Handles running aspect and theme Processes

const levels = new Map();
const callbacks = new Map();
const reload = new Map();
const modQuad = { mf:0, fm:0, bj:0, jb:0, mb:0, bm:0, fb:0, bf:0, mj:0, jm:0, fj:0, jf:0 }
const resetMQ = ()=>{ for(let k in modQuad) modQuad[k]=0; }
const getMod = (m)=>{ return modQuad[m] }
const listener = {
    position: {x:0,y:0,z:0},
    forward: {x:0,y:0,z:0},
    up: {x:0,y:0,z:0},
    initialPosition: {x:0,y:0,z:0},
    initialForward: {x:0,y:0,z:0},
    initialUp: {x:0,y:0,z:0}
}
const resetListener = ()=> {
    Object.assign(listener.position, listener.initialPosition);
    Object.assign(listener.forward, listener.initialForward);
    Object.assign(listener.up, listener.initialUp);
}
const getListener = (v)=> { return Object.assign({}, listener[v]) }
let mainProcess;

// Processes such as the J->B Process may want to assign values based
    // on the current value. They can use `getVoiceData` to retrieve
    // current information about the voice which should share an ID
    // with the Process.
// A Process may reach into sub processes at any level. This is the
    // only way outer Processes may impact the identity of a voice.
    // This ensures that only one Process is matched to and disturbing
    // a voice directly. That Process is an Angel receiving suggestions
    // from God (the Outermost). Process extent can be enforced by using
    // one function from the outer Process to alter sub Processes' values.
    // This will require sub trees to be propagated to the ends for each
    // outer Process, for each level from the bottom.
    // Right now, its free game for all outer Processes.
// A Process value must be in the form { frequency, intensity }
function doProcess(e) {
    let data = e.data || e,
        ct = data.currentTime;
let num = Math.random();
    switch (data.type) {
        case JOB_TYPE_WORK:

            async function work() {
                let size = levels.size;
                for (let lid = size-1; lid >= 0; --lid) {
                    let processes = levels.get(lid);
                    for (let pid = 0; pid < processes.length; ++pid) {
                        let proc = processes[pid],
                            rproc = reload.get(proc.id);
                        if (rproc) proc.reload(rproc);
                        try {
                            proc.value = await proc.work({
                                id: proc.id,
                                currentTime: ct,
                                level: proc.level,
                                coordinates: proc.coordinates.slice(0),
                                value: proc.value == undefined ? null : Object.assign({}, proc.value),
                                subProcesses: proc.subProcesses ? proc.subProcesses.map(p => { return {level: p.level, coordinates: p.coordinates.slice(0)} }) : null,
                                interference: (coords)=> { return getInterference(coords, lid) },
                                deepInterference: (cb)=> { return deepInterference(proc, lid, cb) },
                                listener: getListener,
                                modQuad: getMod,
                                getVoice: getVoiceData,
                            });
                            if (isNaN(proc.value.frequency) || isNaN(proc.value.intensity)) throw 'Process value must be in the form, {frequency, intensity}';
                        } catch (e) {
                            proc.work = MINIMUM_CHANGE;
                            proc.value = await proc.work();
                            console.trace('Error occured while running Process, '+proc.name+', with voice ID, '+proc.id+'. Will skip it from now on: '+e);
                        }
                    }
                }
            }

            work().then(()=> { postProcess(mainProcess, num); });
            break;
        case JOB_TYPE_PROCESS:
            mainProcess = new Process(data.process);
            console.log(mainProcess);
            break;
        case JOB_TYPE_VOICE:
            callbacks.get(data.jobId)(data.voice);
            callbacks.delete(data.jobId);
            break;
        case JOB_TYPE_MOD_QUAD:
            Object.assign(modQuad, data.modQuad);
            break;
        case JOB_TYPE_LISTENER:
            Object.assign(listener, data.listener);
            break;
        case JOB_TYPE_RELOAD:
            reload.set(data.id, data.process);
            break;
        case JOB_TYPE_CLEAR:
            levels.clear();
            callbacks.clear();
            reload.clear();
            resetMQ();
            resetListener();
            mainProcess = null;
    }
}

onmessage = doProcess;

function postProcess(proc, num) {
    if (!proc) return; // because work() may run after being asked to clear.
    postMessage({
        type: JOB_TYPE_PROCESS,
        id: proc.id,
        value: proc.value
    });

    if (proc.subProcesses) proc.subProcesses.forEach(p => postProcess(p));
}

function getVoiceData(id) {
    return new Promise((resolve, reject)=> {
        let fid = uuid();
        postMessage({
            type: JOB_TYPE_VOICE,
            id: id,
            jobId: fid
        });

        callbacks.set(fid, (voice)=> {
            resolve(voice);
        });
    });
}

function deepInterference(p, lid, cb) {
    let sps = p.subProcesses;
    if (sps) {
        for (let spi = 0; spi < sps.length; ++spi) {
            let sp = sps[spi],
                cont = cb(getInterference(null,lid,sp));
            if (cont) deepInterference(sp, lid, cb);
        }
    }
}

function getInterference(coords, thisLevel, item) {
    if (!item) {
        let sps = [mainProcess];
        for (let ci = 0; ci < coords.length; ++ci)  {
            item = sps[coords[ci]];
            if (!item) return;
            sps = item.subProcesses;
            if(!sps && (ci+1 < coords.length)) return;
        }
        if (thisLevel >= item.level) return;
    }

    return {
        id: item.id,
        level: item.level,
        coordinates: item.coordinates.slice(0),
        value: item.value ? Object.assign({}, item.value) : null,
        interfere: item.interfere(thisLevel),
        subProcesses: item.subProcesses ? item.subProcesses.map(p => { return {level: p.level, coordinates: p.coordinates.slice(0)} }) : null
    }
}

function Process(process, level=0, coords=[0]) {
    this.id = process.id;
    // this.name = process.name; // for error tracking
    // this.raw = process.raw;
    this.level = level;
    let level_ = levels.get(level);
    if (!level_) {
        level_ = [];
        levels.set(level, level_);
    }
    level_.push(this);

    this.coordinates = coords;

    if (process.subProcesses) {
        this.subProcesses = process.subProcesses.map((p, idx) => {
            let p_coords = coords.slice(0);
            p_coords.push(idx);
            return new Process(p, level+1, p_coords);
        });
    }

    this.interfere = (lev) => {
        return (freq, intst) => {
            if (this.value == undefined) return;

            let sz = levels.size-lev,
                power = (this.level+1)/sz*modQuad.jb;
            let val, range;
            if (freq != undefined) {
                val = this.value.frequency;
                range = freq-val;
                this.value.frequency = val+range*power;
            }

            if (intst != undefined) {
                val = this.value.intensity;
                range = intst-val;
                this.value.intensity = val+range*power;
            }
        }
    }
    this.reload = (rproc)=> {
        this.name = rproc.name || this.name;
        if (rproc.raw) {
            this.raw = rproc.raw;
            try {
                let obj = {}
                new Function(this.raw).bind(obj)();
                this.work = obj.work;
            } catch (e) {
                this.work = MINIMUM_CHANGE;
                console.warn('Error occured while building process with name, '+this.name+': '+e);
            }
        }
    }
    this.work = MINIMUM_CHANGE;
    this.reload(process);
}
