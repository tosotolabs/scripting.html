'use strict'

const ThemeMaker = function(scripting) {
    const S = scripting || new Scripting(),
        M = S.M;
    const n_ss = ['a','b','c','d','e','f'],
        vn_ss = ['la','lb','lc','ra','rb','rc'],
        mfbj_ss = ['m','f','b','j'];

    // [ "scripts_lite", "processes", "shells", "entities", "aspects", "scripts", "themes" ].map(e=>Lockr.rm(e))

    const adjectives = ["adamant", "adroit", "amatory", "animistic", "antic", "arcadian", "baleful", "bellicose", "bilious", "boorish", "calamitous", "caustic", "cerulean", "comely", "concomitant", "contumacious", "corpulent", "crapulous", "defamatory", "didactic", "dilatory", "dowdy", "efficacious", "effulgent", "egregious", "endemic", "equanimous", "execrable", "fastidious", "feckless", "fecund", "friable", "fulsome", "garrulous", "guileless", "gustatory", "heuristic", "histrionic", "hubristic", "incendiary", "insidious", "insolent", "intransigent", "inveterate", "invidious", "irksome", "jejune", "jocular", "judicious", "lachrymose", "limpid", "loquacious", "luminous", "mannered", "mendacious", "meretricious", "minatory", "mordant", "munificent", "nefarious", "noxious", "obtuse", "parsimonious", "pendulous", "pernicious", "pervasive", "petulant", "platitudinous", "precipitate", "propitious", "puckish", "querulous", "quiescent", "rebarbative", "recalcitant", "redolent", "rhadamanthine", "risible", "ruminative", "sagacious", "salubrious", "sartorial", "sclerotic", "serpentine", "spasmodic", "strident", "taciturn", "tenacious", "tremulous", "trenchant", "turbulent", "turgid", "ubiquitous", "uxorious", "verdant", "voluble", "voracious", "wheedling", "withering", "zealous"];

    const nouns = ["ninja", "chair", "pancake", "statue", "unicorn", "rainbows", "laser", "senor", "bunny", "captain", "nibblets", "cupcake", "carrot", "gnomes", "glitter", "potato", "salad", "toejam", "curtains", "beets", "toilet", "exorcism", "stick figures", "mermaid eggs", "sea barnacles", "dragons", "jellybeans", "snakes", "dolls", "bushes", "cookies", "apples", "ice cream", "ukulele", "kazoo", "banjo", "opera singer", "circus", "trampoline", "carousel", "carnival", "locomotive", "hot air balloon", "praying mantis", "animator", "artisan", "artist", "colorist", "inker", "coppersmith", "director", "designer", "flatter", "stylist", "leadman", "limner", "make-up artist", "model", "musician", "penciller", "producer", "scenographer", "set decorator", "silversmith", "teacher", "auto mechanic", "beader", "bobbin boy", "clerk of the chapel", "filling station attendant", "foreman", "maintenance engineering", "mechanic", "miller", "moldmaker", "panel beater", "patternmaker", "plant operator", "plumber", "sawfiler", "shop foreman", "soaper", "stationary engineer", "wheelwright", "woodworkers"];

    const getRandomName = ()=> { return adjectives[getRandomInt(0,adjectives.length)]+' '+nouns[getRandomInt(0,nouns.length)] }

    this.aspects = {}
    this.themes = {}
    this.shells = {}
    this.branes = [];

    this.cleanup = ()=> {
        if(S.initialized) {
            for (let k in this.aspects) {
                S.aspects.delete(k);
            }
            for (let k in this.themes) {
                S.themes.delete(k);
                S.aspects.delete(k);
            }
            for (let k in this.shells) {
                S.shells.delete(k);
            }
        }

        this.aspects = {}
        this.themes = {}
        this.shells = {}
    }

    const genMeaning = (type)=> {
        let meaning = {};
        switch (type) {
            case M.THEME_TYPE_SIGNAL:
                for (let i = 0; i < n_ss.length; ++i) meaning[n_ss[i]] = { value: Math.random(), arrowType:M.ARROW_TYPE_ACKNOWLEDGEMENT }
                break;
            case M.THEME_TYPE_VERSUS:
                for (let i = 0; i < 3; ++i) {
                    let a_s = vn_ss[i];
                    for (let j = 3; j < 6; ++j) {
                        let b_s = vn_ss[j],
                            pos1 = getRandomInt(0,2),
                            pos2 = pos1 === 0 ? 1 : 0;
                        meaning[a_s+b_s] = { value: Math.random(), position: pos1, arrowType:M.ARROW_TYPE_SIMILARITY }
                        meaning[b_s+a_s] = { value: Math.random(), position: pos2, arrowType:M.ARROW_TYPE_SIMILARITY }
                    }
                }
                break;
            case M.THEME_TYPE_MOD_QUAD:
                for (let i = mfbj_ss.length-1; i > 0; --i) {
                    let b_s = mfbj_ss[i];
                    for (let j = 0; j < i; ++j) {
                        let a_s = mfbj_ss[j],
                        pos1 = getRandomInt(0,2),
                        pos2 = pos1 === 0 ? 1 : 0;
                        meaning[a_s+b_s] = { value: Math.random(), position: pos1, arrowType:M.ARROW_TYPE_SIMILARITY }
                        meaning[b_s+a_s] = { value: Math.random(), position: pos2, arrowType:M.ARROW_TYPE_SIMILARITY }
                    }
                }
                break;
            default: // Context
                for (let i = n_ss.length-1; i > 0; --i) {
                    let b_s = n_ss[i];
                    for (let j = 0; j < i; ++j) {
                        let a_s = n_ss[j],
                        pos1 = getRandomInt(0,2),
                        pos2 = pos1 === 0 ? 1 : 0;
                        meaning[a_s+b_s] = { value: Math.random(), position: pos1, arrowType:getRandomInt(1,5) }
                        meaning[b_s+a_s] = { value: Math.random(), position: pos2, arrowType:getRandomInt(1,5) }
                    }
                }
        }

        return meaning;
    }

    const genAspects = (props)=> {
        props = props || {};
        let num = props.count,
            minf = props.minFreq == undefined ? 200 : props.minFreq,
            maxf = props.maxFreq == undefined ? 15000 : props.maxFreq,
            aspns = [];
        for (let i = 0; i < num; ++i) aspns.push(getRandomName()+'~'+getRandomNumber(minf,maxf));
        let asps = S.createAspects(aspns),
            asp, cover;
        for (let i = 0; i < num; ++i) {
            asp = asps[i];
            cover = props.cover == undefined ? (Math.random() < 0.5 ? this.branes.random() : '') : (Math.random() < props.cover ? this.branes.random() : '');
            asp._aspect.cover(cover || '');
            this.aspects[asp._aspect.id] = asp;
        }
        return asps;
    }

    // NOTE: May create more themes than asked because shell generation may create themes
    // to fill depth
    const genThemes = async (props)=> {
        props = props || {};
        let num = props.count,
            mindur = props.minDuration || 2,
            maxdur = props.maxDuration || 6,
            minvol = (props.minIntensity == undefined || (props.minIntensity < 0.75)) ? 0.75 : props.minIntensity,
            maxvol = (props.maxIntensity == undefined || (props.maxIntensity < 0.75)) ? 4 : props.maxIntensity,
            aminvol = props.aspMintensity || 0.2,
            amaxvol = props.aspMaxtensity || 2,
            taa = props.themeAsAspect,
            maxdepth = props.maxDepth || 0;

        let shells = await genShells(props),
            themes = [];

        for (let i = 0; i < num; ++i) {
            let shell = shells[i],
                theme = await genTheme({
                    duration: getRandomNumber(mindur,maxdur),
                    minDuration: mindur,
                    maxDuration: maxdur,
                    minIntensity: aminvol,
                    maxIntensity: aminvol,
                    minFreq: props.minFreq,
                    maxFreq: props.maxFreq,
                    shellId: shell.id,
                    line: props.line,
                    cover: props.cover
                });
                themes.push(theme);
        }

        return themes;
    }

    const genShells = async (props)=> {
        props = props || {};
        let num = props.count,
            minf = props.minFreq,
            maxf = props.maxFreq,
            taa = props.themeAsAspect,
            maxd = props.maxDepth || 0,
            mindur = props.minDuration || 2,
            maxdur = props.maxDuration || 6,
            minvol = (props.minIntensity == undefined || (props.minIntensity < 0.75)) ? 0.75 : props.minIntensity,
            maxvol = (props.maxIntensity == undefined || (props.maxIntensity < 0.75)) ? 4 : props.maxIntensity,
            aminvol = props.aspMintensity || 0.2,
            amaxvol = props.aspMaxtensity || 2,
            cover = props.cover,
            keys = Object.keys(props),
            tprops = {};

        for (let i = 0; i < keys.length; ++i) tprops[keys[i]] = props[keys[i]];
        tprops.line = null;

        let names_aids = [];
        for (let n = 0; n < num; ++n) {
            let asps = null,
                aids = [];
            if (taa && maxd) {
                // let nts = getRandomInt(1,7); // basic randomness
                let nts = 0;
                for (let i = 0; i < 6; ++i) {
                    if (Math.random() < 0.2) nts++; // adjustable randomness
                }

                tprops.count = nts;
                tprops.maxDepth = getRandomInt(1,maxd)-1;
                let themes = await genThemes(tprops);

                asps = genAspects({
                    count: 6-nts,
                    minFreq: minf,
                    maxFreq: maxf,
                    cover: cover
                });

                for (let i = 0; i < themes.length; ++i) aids.push(themes[i]._theme.id);
                for (let i = 0; i < asps.length; ++i) aids.push(asps[i]._aspect.id);


                shuffle(aids);
            } else {
                asps = genAspects({
                    count: 6,
                    minFreq: minf,
                    maxFreq: maxf,
                    cover: cover
                });
                for (let i = 0; i < 6; ++i) aids.push(asps[i]._aspect.id);
            }

            names_aids.push([getRandomName(),aids]);
        }
        let shells = S.createShells(names_aids);
        shells.forEach(shell => { this.shells[shell.id] = shell });

        return shells;
    }

    const genTheme = async (props)=> {
        props = props || {};
        let type = props.type == undefined ? getRandomInt(1,4) : props.type,
            duration = props.duration == undefined ? 3 : props.duration,
            mindur = props.minDuration || 2,
            maxdur = props.maxDuration || 6,
            minvol = (props.minIntensity == undefined || (props.minIntensity < 0.75)) ? 0.75 : props.minIntensity,
            maxvol = (props.maxIntensity == undefined || (props.maxIntensity < 0.75)) ? 4 : props.maxIntensity,
            shellId = props.shellId || (await genShells({count:1, minFreq:props.minFreq, maxFreq:props.maxFreq, maxDepth:props.maxDepth, cover:props.cover, themeAsAspect: props.themeAsAspect}))[0].id,
            shell = this.shells[shellId] || await S.shells.get(shellId),
            intensities = [],
            durations = [],
            meaning = genMeaning(type),
            theme = null;

        shell.aspectIds.forEach((aid)=> {
            let asp = this.aspects[aid];
            if (asp._aspect.cover()) {
                durations.push(0); // 0 == align with buffer duration
                intensities.push(getRandomNumber(5,SOUND_INTENSITY_MAX));
            } else {
                durations.push(getRandomNumber(mindur,maxdur));
                intensities.push(getRandomNumber(minvol,maxvol));
            }
        });

        if (S.initialized) {
            theme = await S.addTheme({
                name: getRandomName(),
                shellId: shellId,
                type: type,
                meaning: meaning,
                duration: duration,
                intensities: intensities,
                durations: durations,
                line: props.line
            });
            this.themes[theme._theme.id] = theme;
            this.aspects[theme._theme.id] = await S.aspects.get(theme._theme.id);
        } else {
            let _theme = new M.Theme({
                id: guid(),
                name: getRandomName(),
                shellId: shellId,
                type: type,
                meaning: meaning,
                duration: duration,
                intensities: intensities,
                durations: durations
            });
            theme = new S.Theme(_theme);
            this.themes[_theme.id] = theme;
            this.aspects[_theme.id] = new S.Aspect(new M.Aspect({
                id: _theme.id,
                name: _theme.name(),
                abbr: _theme.name().substring(0,2),
                type: M.ASPECT_TYPE_COMPOUND
            }));
        }

        return theme;
    }

    this.genMeaning = genMeaning;
    this.genAspects = genAspects;
    this.genThemes = genThemes;
    this.genShells = genShells;
    this.genTheme = genTheme;
}
